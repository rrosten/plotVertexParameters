//
//  MSVxHistGroup.cpp
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include "PlottingPackage/MSVxHistGroup.h"
#include <stdio.h>
#include <iostream>

void MSVxHistGroup::initialize(bool isSignal){

    std::cout << "initializing vertex hist group...  " << std::endl;

    //all vertices
    hMSVxBar = new VertexHistograms;
    hMSVxEC = new VertexHistograms;
    hMSVxFull = new VertexHistograms;

    hMSVxBar->initializeHistograms("MSVertex","bar");
    hMSVxEC->initializeHistograms("MSVertex","ec");
    hMSVxFull->initializeHistograms("MSVertex","full");

    //good vertices
    hMSVxBarGood = new VertexHistograms;
    hMSVxECGood = new VertexHistograms;
    hMSVxFullGood = new VertexHistograms;

    hMSVxBarGood->initializeHistograms("MSVertex","barGood");
    hMSVxECGood->initializeHistograms("MSVertex","ecGood");
    hMSVxFullGood->initializeHistograms("MSVertex","fullGood");

    if(isSignal){
        //matched good vertices (valid for llps only):
        hMSVxBarGoodMatched = new VertexHistograms;
        hMSVxECGoodMatched = new VertexHistograms;
        hMSVxFullGoodMatched = new VertexHistograms;
        hMSVxBarGoodMatched->initializeHistograms("MSVertex","barGoodMatched");
        hMSVxECGoodMatched->initializeHistograms("MSVertex","ecGoodMatched");
        hMSVxFullGoodMatched->initializeHistograms("MSVertex","fullGoodMatched");

        //matched bad vertices (valid for llps only):
        hMSVxBarBadMatched = new VertexHistograms;
        hMSVxECBadMatched = new VertexHistograms;
        hMSVxFullBadMatched = new VertexHistograms;

        hMSVxBarBadMatched->initializeHistograms("MSVertex","barBadMatched");
        hMSVxECBadMatched->initializeHistograms("MSVertex","ecBadMatched");
        hMSVxFullBadMatched->initializeHistograms("MSVertex","fullBadMatched");
    }
    //GVCHistograms *hGVCMSVxBar = new VertexHistograms;
    // GVCHistograms *hGVCMSVxEC = new VertexHistograms;

    //hGVCMSVxBar->initializeHistograms("MSVertex","bar");
    //hGVCMSVxEC->initializeHistograms("MSVertex","ec");

    return;

}

/*void MSVxHistGroup::fillVariables(VertexVariables* vxVar){
    std::cout << "need to write this function still" << std::endl;
    return;


}*/
void MSVxHistGroup::fillFromTree(TChain *tree, int isJZMC){

    //all vertices
    std::string noiso("event_passMuvtx_noiso");
    std::string iso("event_passMuvtx");

    std::string bar("TMath::Abs(MSVertex_eta) < 0.8 && MSVertex_indexTrigRoI==0 && MSVertex_passesHitThresholds");
    std::string ec("TMath::Abs(MSVertex_eta) > 1.3 && TMath::Abs(MSVertex_eta) < 2.5  && MSVertex_passesHitThresholds");
    bar = bar + " && " + noiso;
    ec = ec + " && " + noiso;
    std::string full = "( ("+bar+") || ("+ec+") )";


    std::cout << "filling msvx barrel" << std::endl;
    hMSVxBar->fillFromTree(isJZMC, tree, bar);
    hMSVxEC->fillFromTree(isJZMC, tree,ec);
    hMSVxFull->fillFromTree(isJZMC, tree, full);

    //good vertices
    hMSVxBarGood->fillFromTree(isJZMC, tree, bar+" && MSVertex_isGood");
    hMSVxECGood->fillFromTree(isJZMC, tree, ec +" && MSVertex_isGood");
    hMSVxFullGood->fillFromTree(isJZMC, tree, full+" && MSVertex_isGood");

    std::string match = "MSVertex_indexLLP > -1"; //unmatched is -99

    //matched good vertices (valid for llps only):

    hMSVxBarGoodMatched->fillFromTree(isJZMC, tree,   bar+" && "+match+" && MSVertex_isGood");
    hMSVxECGoodMatched->fillFromTree(isJZMC, tree,     ec+" && "+match+" && MSVertex_isGood");
    hMSVxFullGoodMatched->fillFromTree(isJZMC, tree,full+" && " +match+" && MSVertex_isGood");


    //matched bad vertices (valid for llps only):
    hMSVxBarBadMatched->fillFromTree(isJZMC, tree,   bar+" && "+match+" && !MSVertex_isGood" );
    hMSVxECBadMatched->fillFromTree(isJZMC, tree,     ec+" && "+match+" && !MSVertex_isGood");
    hMSVxFullBadMatched->fillFromTree(isJZMC, tree,full + " && " +match+" && !MSVertex_isGood");

    return;

}
