//
//  VertexHistograms.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#include "PlottingPackage/VertexHistograms.h"
#include <iostream>
#include "TCanvas.h"
#include "PlottingPackage/InitializationUtils.h"

void VertexHistograms::initializeHistograms(TString obj, TString str){
    
    std::cout << "initializing vertex histograms...  " << std::endl;
    
    object = obj;
    locn = str;
    
    int nBins=50;
        h_eta            = new TH1D(obj+"_eta_"+str  ,obj+"_eta_"+str   ,nBins,-3.0,3.0);
        h_eta_ABS        = new TH1D(obj+"_etaABS_"+str  ,obj+"_etaABS_"+str   ,60,0,3.0);
        h_eta_2MATCH     = new TH1D(obj+"_eta_2MATCH_"+str  ,obj+"_eta_2MATCH_"+str   ,60,0,3.0);
        h_phi            = new TH1D(obj+"_phi_"+str  ,obj+"_phi_"+str   ,nBins,-3.5,3.5);
        h_R              = new TH1D(obj+"_R_"+str    ,obj+"_R_"+str     ,nBins,0,10);
        h_z              = new TH1D(obj+"_z_"+str    ,obj+"_z_"+str     ,nBins,-15,15);
        h_nTrks          = new TH1D(obj+"_nTrks_"+str,obj+"_nTrks_"+str ,15,0,15);
        h_nMDT           = new TH1D(obj+"_nMDT_"+str ,obj+"_nMDT_"+str  ,100,0,5000);
        h_nRPC           = new TH1D(obj+"_nRPC_"+str ,obj+"_nRPC_"+str  ,100,0,5000);
        h_nTGC           = new TH1D(obj+"_nTGC_"+str ,obj+"_nTGC_"+str  ,100,0,5000);
        h_eta->Sumw2();h_phi->Sumw2();h_R->Sumw2();h_z->Sumw2();h_nTrks->Sumw2();h_nMDT->Sumw2();
        h_nTGC->Sumw2();h_nRPC->Sumw2();
    //
    
    //
    h_phieta = new TH2D(obj+"_phieta"+str,obj+"_phieta"+str,25,-2.5,2.5,33,-3.15,3.15);
    
    h_MDTvsTrig = new TH2D(obj+"_MDTvsTrig"+str,obj+"_MDTvsTrig"+str,nBins,0,5000,nBins,0,5000);
    h_zR    = new TH2D(obj+"_zR"+str,obj+"_zR"+str,40,-15,15,20,0,10);
    h_JetdR_vs_Et = new TH2D(obj+"_JetdR_vs_Et"+str,obj+"_JetdR_vs_Et"+str,100,0,5,100,0,200);
    h_TrackdR_vs_Pt = new TH2D(obj+"_TrackdR_vs_Pt"+str,obj+"_TrackdR_vs_Pt"+str,100,0,5,100,0,20);
 
    return;
    
}
void VertexHistograms::fillFromTree(int isJZMC, TChain* &tree, const std::string &cuts){
    
    std::cout << "drawing histograms!" << std::endl;
    double jetSliceWeights[13];
    plotVertexParameters::setJetSliceWeights(jetSliceWeights);
	
	TString globalWeight;
	if(isJZMC){ globalWeight = TString("eventWeight*")+std::to_string(jetSliceWeights[isJZMC-1])+"*("+cuts+")";}
	else{ globalWeight = TString("eventWeight*(")+cuts+")";}

    TString doubleVxMatch = TString("eventWeight*(")+cuts+")*(MSVertex_eta@.size() == 2 && MSVertex_indexLLP[0] == MSVertex_indexLLP[1] && MSVertex_indexLLP[0] != -99)";
    
	std::cout << "global weight :" << globalWeight << std::endl;

    tree->Draw("TMath::Abs(MSVertex_eta) >>"+TString(h_eta_ABS->GetName()),globalWeight+"*(MSVertex_indexLLP != -99)","");
    tree->Draw("TMath::Abs(MSVertex_eta) >>"+TString(h_eta_2MATCH->GetName()),doubleVxMatch,"");
    tree->Draw("MSVertex_eta >>"+TString(h_eta->GetName()),globalWeight,"");
    tree->Draw("MSVertex_phi >>"+TString(h_phi->GetName()),globalWeight,"");
    tree->Draw("MSVertex_R*0.001 >>"+TString(h_R->GetName()),globalWeight,"");
    tree->Draw("MSVertex_z*0.001 >>"+TString(h_z->GetName()),globalWeight,"");
    tree->Draw("MSVertex_nMDT >>"+TString(h_nMDT->GetName()),globalWeight,"");
    tree->Draw("MSVertex_nRPC >>"+TString(h_nRPC->GetName()),globalWeight,"");
    tree->Draw("MSVertex_nTGC >>"+TString(h_nTGC->GetName()),globalWeight,"");
    tree->Draw("MSVertex_nTrks >>"+TString(h_nTrks->GetName()),globalWeight,"");
    tree->Draw("MSVertex_phi:MSVertex_eta >>"+TString(h_phieta->GetName()),globalWeight,"COLZ");
    tree->Draw("MSVertex_R*0.001:MSVertex_z*0.001 >>"+TString(h_zR->GetName()),globalWeight,"COLZ");

    if(cuts.find("0.8") != std::string::npos){ //barrel
        tree->Draw("MSVertex_nRPC:MSVertex_nMDT >>"+TString(h_MDTvsTrig->GetName()),globalWeight,"COLZ");

    } else if(cuts.find("2.5") != std::string::npos){ //endcaps
        tree->Draw("MSVertex_nTGC:MSVertex_nMDT >>"+TString(h_MDTvsTrig->GetName()),globalWeight,"COLZ");

    }
    
    return;
    
}
