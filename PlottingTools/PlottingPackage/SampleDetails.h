//
//  SampleDetails.h
//  plotVertexParameters
//
//  Created by Heather Russell on 10/10/16.
//
//

#ifndef plotVertexParameters_SampleDetails_h
#define plotVertexParameters_SampleDetails_h
#include "TString.h"

namespace SampleDetails{
    
    enum SampleID {mg250, mg500, mg800, mg1200, mg1500, mg2000, unknown};
    int mediatorMass;
    double MediatorXS = 1.0; //in pb
    double mediatorXS = 1.0; //in pb
    int llpMass;
    double sim_ctau; //in mm
    int pdgid;
    double BVxSysErr_up;
    double EVxSysErr_up;
    double BTrigSysErr_up;
    double ETrigSysErr_up;
    double BVxSysErr_down;
    double EVxSysErr_down;
    double BTrigSysErr_down;
    double ETrigSysErr_down;
    double BJetBSysErr_up = 0;
    double BJetBSysErr_down = 0;
    double EJetBSysErr_up = 0;
    double EJetBSysErr_down = 0;
    double BJetESysErr_up = 0;
    double BJetESysErr_down = 0;
    double EJetESysErr_up = 0;
    double EJetESysErr_down = 0;
    double BJetSysErr_up = 0;
    double BJetSysErr_down = 0;
    double EJetSysErr_up = 0;
    double EJetSysErr_down = 0;

    double global2VxError_up = 0;
    double global2VxError_down = 0;
    double globalABCDError_up = 0;
    double globalABCDError_down = 0;
    double globalCombError_up = 0;
    double globalCombError_down = 0;
    
    double bkg2VxError_up = 1.38; 
    double bkg2VxError_down = 0.62;
    double bkgABCDError_up = 1.38; 
    double bkgABCDError_down = 0.62;
    double bkgCombError_up = 1.38; 
    double bkgCombError_down = 0.62;
    
    SampleID s_id = SampleID::unknown;
    
    void setGlobalVariables(TString sampleID){
        llpMass = 100; pdgid = 3000001;
        if(sampleID == "mg250"){
            s_id = SampleID::mg250;
            mediatorMass = 250; mediatorXS = 1190.35;
            sim_ctau = 0.96;
            BVxSysErr_up = 0.009; EVxSysErr_up = 0.006;
            BTrigSysErr_up = 0.012; ETrigSysErr_up = 0.021;
            
            BVxSysErr_down = 0.005; EVxSysErr_down = 0.008;
            BTrigSysErr_down = 0.012; ETrigSysErr_down = 0.020;
            
            BJetBSysErr_up =     0.036;   EJetBSysErr_up =     0.053;
            BJetBSysErr_down  = 0.027;   EJetBSysErr_down  = 0.044;
            
            BJetESysErr_up =     0.072;   EJetESysErr_up =     0.065;
            BJetESysErr_down  = 0.084;   EJetESysErr_down  = 0.107;
            
            global2VxError_up = 1.06304; global2VxError_down = 0.939059;
            globalABCDError_up = 1.13445; globalABCDError_down = 0.889553;
            globalCombError_up = 1.08871; globalCombError_down = 0.926203;
            
        } else if(sampleID == "mg500"){
            s_id = SampleID::mg500;
            mediatorMass = 500; mediatorXS = 27.4171;
            sim_ctau = 0.76;
            BVxSysErr_up = 0.004; EVxSysErr_up = 0.006;
            BTrigSysErr_up = 0.012; ETrigSysErr_up = 0.017;
            
            BVxSysErr_down = 0.007; EVxSysErr_down = 0.006;
            BTrigSysErr_down = 0.011; ETrigSysErr_down = 0.01;
            
            BJetBSysErr_up =     0.017;   EJetBSysErr_up =     0.025;
            BJetBSysErr_down  = 0.015;   EJetBSysErr_down  = 0.021;
            
            BJetESysErr_up =     0.034;   EJetESysErr_up =     0.054;
            BJetESysErr_down  = 0.031;   EJetESysErr_down  = 0.032;
            
            global2VxError_up = 1.06025; global2VxError_down = 0.942783;
            globalABCDError_up = 1.06179; globalABCDError_down = 0.962025;
            globalCombError_up = 1.05544; globalCombError_down = 0.965597;

            
        } else if(sampleID == "mg800"){
            s_id    = SampleID::mg800;
            mediatorMass = 800; mediatorXS = 1.4891;
            sim_ctau = 0.62;
            BVxSysErr_up = 0.01; EVxSysErr_up = 0.006;
            BTrigSysErr_up = 0.012; ETrigSysErr_up = 0.014;
            
            BVxSysErr_down = 0.005; EVxSysErr_down = 0.006;
            BTrigSysErr_down = 0.011; ETrigSysErr_down = 0.013;
            
            BJetBSysErr_up =     0.007;   EJetBSysErr_up =     0.013;
            BJetBSysErr_down  = 0.005;   EJetBSysErr_down  = 0.009;
            
            BJetESysErr_up =     0.014;   EJetESysErr_up =     0.043;
            BJetESysErr_down  = 0.016;   EJetESysErr_down  = 0.022;
            
            global2VxError_up = 1.05741; global2VxError_down = 0.945445;
            globalABCDError_up = 1.04773; globalABCDError_down = 0.979163;
            globalCombError_up = 1.04499; globalCombError_down = 0.980145;
            
        } else if(sampleID == "mg1200"){
            s_id = SampleID::mg1200;
            mediatorMass = 1200; mediatorXS = 0.0856418;
            sim_ctau = 0.5;
            BVxSysErr_up = 0.009; EVxSysErr_up = 0.006;
            BTrigSysErr_up = 0.011; ETrigSysErr_up = 0.014;
            
            BVxSysErr_down = 0.005; EVxSysErr_down = 0.010;
            BTrigSysErr_down = 0.010; ETrigSysErr_down = 0.013;
            
            BJetBSysErr_up =     0.002;   EJetBSysErr_up =     0.003;
            BJetBSysErr_down  = 0.002;   EJetBSysErr_down  = 0.002;
            
            BJetESysErr_up =     0.006;   EJetESysErr_up =     0.011;
            BJetESysErr_down  = 0.005;   EJetESysErr_down  = 0.008;
            
            global2VxError_up = 1.05904; global2VxError_down = 0.94446;
            globalABCDError_up = 1.04677; globalABCDError_down = 0.983091;
            globalCombError_up = 1.04478; globalCombError_down = 0.983652;
            
        } else if(sampleID == "mg1500"){
            s_id = SampleID::mg1500;
            mediatorMass = 1500; mediatorXS = 0.0141903;
            sim_ctau = 0.45;
            BVxSysErr_up = 0.008; EVxSysErr_up = 0.004;
            BTrigSysErr_up = 0.010; ETrigSysErr_up = 0.015;
            
            BVxSysErr_down = 0.005; EVxSysErr_down = 0.011;
            BTrigSysErr_down = 0.010; ETrigSysErr_down = 0.013;
            
            BJetBSysErr_up =     0.002;   EJetBSysErr_up =     0.005;
            BJetBSysErr_down  = 0.001;   EJetBSysErr_down  = 0.002;
            
            BJetESysErr_up =     0.007;   EJetESysErr_up =     0.011;
            BJetESysErr_down  = 0.004;   EJetESysErr_down  = 0.005;
            
            global2VxError_up = 1.05765; global2VxError_down = 0.945206;
            globalABCDError_up = 1.04828; globalABCDError_down = 0.983289;
            globalCombError_up = 1.04641; globalCombError_down = 0.983808;
            
        } else if(sampleID == "mg2000"){
            s_id = SampleID::mg2000;
            mediatorMass = 2000; mediatorXS = 0.000981077;
            sim_ctau = 0.37;
            BVxSysErr_up = 0.005; EVxSysErr_up = 0.005;
            BTrigSysErr_up = 0.011; ETrigSysErr_up = 0.011;
        
            BVxSysErr_down = 0.004; EVxSysErr_down = 0.007;
            BTrigSysErr_down = 0.008; ETrigSysErr_down = 0.011;
            
            BJetBSysErr_up =     0.001;   EJetBSysErr_up =     0.001;
            BJetBSysErr_down  = 0.001;   EJetBSysErr_down  = 0.001;
            
            BJetESysErr_up =     0.002;   EJetESysErr_up =     0.007;
            BJetESysErr_down  = 0.003;   EJetESysErr_down  = 0.004;
            
            global2VxError_up = 1.06073; global2VxError_down = 0.942992;
            globalABCDError_up = 1.04851; globalABCDError_down = 0.984171;
            globalCombError_up = 1.04689; globalCombError_down = 0.984591;

        } else if (sampleID == "mH125mS5lt5"){
            //s_id = SampleID::mH125mS5lt5;
            mediatorMass = 125; mediatorXS = 48.58;//N3LO value. NNLO+NNLL = 44.14.
            sim_ctau = 0.127;
            BVxSysErr_up = 0.009; EVxSysErr_up= 0.004; BTrigSysErr_up = 0.025; ETrigSysErr_up = 0.053;
            BVxSysErr_down= 0.001; EVxSysErr_down = 0.001; BTrigSysErr_down= 0.032; ETrigSysErr_down= 0.047;
        } else if (sampleID == "mH125mS5lt9"){
            //s_id = SampleID::mH125mS5lt9;
            mediatorMass = 125; mediatorXS = 48.58;//N3LO value. NNLO+NNLL = 44.14.
            sim_ctau = 0.228;
            BVxSysErr_up = 0.0; EVxSysErr_up = 0.0;
            BTrigSysErr_up = 0.0; ETrigSysErr_up = 0.0;
            
            BVxSysErr_down = 0.0; EVxSysErr_down = 0.0;
            BTrigSysErr_down = 0.0; ETrigSysErr_down = 0.0;
        } else if (sampleID == "mH125mS8lt5"){
            //s_id = SampleID::mH125mS8lt5;
            mediatorMass = 125; mediatorXS = 48.58;//N3LO value. NNLO+NNLL = 44.14.
            sim_ctau = 0.200;
            BVxSysErr_up = 0.031; EVxSysErr_up= 0.002; BTrigSysErr_up = 0.025; ETrigSysErr_up = 0.048;
            BVxSysErr_down= 0.008; EVxSysErr_down = 0.005; BTrigSysErr_down= 0.025; ETrigSysErr_down= 0.049;
        } else if (sampleID == "mH125mS8lt9"){
            //s_id = SampleID::mH125mS8lt9;
            mediatorMass = 125; mediatorXS = 48.58;//N3LO value. NNLO+NNLL = 44.14.
            sim_ctau = 0.375;
            BVxSysErr_up = 0.025; EVxSysErr_up = 0.015;
            BTrigSysErr_up = 0.021; ETrigSysErr_up = 0.044;
            
            BVxSysErr_down = 0.015; EVxSysErr_down = 0.011;
            BTrigSysErr_down = 0.024; ETrigSysErr_down = 0.048;
        } else if (sampleID == "mH125mS15lt5"){
            //s_id = SampleID::mH125mS15lt5;
            mediatorMass = 125; mediatorXS = 48.58;//N3LO value. NNLO+NNLL = 44.14.
            sim_ctau = 0.580;
            BVxSysErr_up = 0.007; EVxSysErr_up= 0.001; BTrigSysErr_up = 0.033; ETrigSysErr_up = 0.048;
            BVxSysErr_down= 0.004; EVxSysErr_down = 0.005; BTrigSysErr_down= 0.026; ETrigSysErr_down= 0.045;
        } else if (sampleID == "mH125mS15lt9"){
            //s_id = SampleID::mH125mS15lt9;
            mediatorMass = 125; mediatorXS = 48.58;//N3LO value. NNLO+NNLL = 44.14.
            sim_ctau = 0.710;
            BVxSysErr_up = 0.0; EVxSysErr_up = 0.0;
            BTrigSysErr_up = 0.0; ETrigSysErr_up = 0.0;
            
            BVxSysErr_down = 0.0; EVxSysErr_down = 0.0;
            BTrigSysErr_down = 0.0; ETrigSysErr_down = 0.0;
        } else if (sampleID == "mH125mS25lt5"){
            //s_id = SampleID::mH125mS25lt5;
            mediatorMass = 125; mediatorXS = 48.58;//N3LO value. NNLO+NNLL = 44.14.
            sim_ctau = 0.76;
            BVxSysErr_up = 0.020; EVxSysErr_up = 0.001; BTrigSysErr_up = 0.022; ETrigSysErr_up = 0.042;
            BVxSysErr_down= 0.005; EVxSysErr_down = 0.001; BTrigSysErr_down = 0.023; ETrigSysErr_down= 0.044;
        } else if (sampleID == "mH125mS25lt9"){
            //s_id = SampleID::mH125mS25lt9;
            mediatorMass = 125; mediatorXS = 48.58;//N3LO value. NNLO+NNLL = 44.14.
            sim_ctau = 1.21;
            BVxSysErr_up = 0.043; EVxSysErr_up = 0.01;
            BTrigSysErr_up = 0.023; ETrigSysErr_up = 0.046;
            
            BVxSysErr_down = 0.031; EVxSysErr_down = 0.009;
            BTrigSysErr_down = 0.023; ETrigSysErr_down = 0.044;
        } else if (sampleID == "mH125mS40lt5"){
            //s_id = SampleID::mH125mS40lt5;
            mediatorMass = 125; mediatorXS = 48.58;//N3LO value. NNLO+NNLL = 44.14.
            sim_ctau = 1.18;
            BVxSysErr_up = 0.008; EVxSysErr_up= 0.002; BTrigSysErr_up = 0.031; ETrigSysErr_up = 0.046;
            BVxSysErr_down= 0.003; EVxSysErr_down = 0.006; BTrigSysErr_down= 0.023; ETrigSysErr_down= 0.042;
        } else if (sampleID == "mH125mS40lt9"){
            //s_id = SampleID::mH125mS40lt9;
            mediatorMass = 125; mediatorXS = 48.58;//N3LO value. NNLO+NNLL = 44.14.
            sim_ctau = 1.90;
            BVxSysErr_up = 0.024; EVxSysErr_up = 0.008;
            BTrigSysErr_up = 0.033; ETrigSysErr_up = 0.046;
            
            BVxSysErr_down = 0.021; EVxSysErr_down = 0.011;
            BTrigSysErr_down = 0.022; ETrigSysErr_down = 0.042;
        } else if (sampleID == "mH125mS55lt5"){
            //s_id = SampleID::mH125mS55lt5;
            mediatorMass = 125; mediatorXS = 48.58;//N3LO value. NNLO+NNLL = 44.14.
            sim_ctau = 1.54;
            BVxSysErr_up = 0.009; EVxSysErr_up= 0.013; BTrigSysErr_up = 0.017; ETrigSysErr_up = 0.056;
            BVxSysErr_down= 0.003; EVxSysErr_down = 0.006; BTrigSysErr_down= 0.018; ETrigSysErr_down= 0.045;
        } else if (sampleID == "mH125mS55lt9"){
            //s_id = SampleID::mH125mS55lt9;
            mediatorMass = 125; mediatorXS = 48.58;//N3LO value. NNLO+NNLL = 44.14.
            sim_ctau = 2.73;
            BVxSysErr_up = 0.022; EVxSysErr_up = 0.017;
            BTrigSysErr_up = 0.020; ETrigSysErr_up = 0.054;
            
            BVxSysErr_down = 0.022; EVxSysErr_down = 0.013;
            BTrigSysErr_down = 0.021; ETrigSysErr_down = 0.050;
        }  else if (sampleID == "mH100mS8lt5"){
            //s_id = SampleID::mH125mS55lt5;
            mediatorMass = 100; mediatorXS = 1.;
            sim_ctau = 0.24;
            BVxSysErr_up=    0.019; EVxSysErr_up= 0.004; BTrigSysErr_up=   0.028; ETrigSysErr_up=   0.053;
            BVxSysErr_down= 0.007; EVxSysErr_down=   0.013; BTrigSysErr_down= 0.025; ETrigSysErr_down= 0.046;
        }  else if (sampleID == "mH100mS25lt5"){
            //s_id = SampleID::mH125mS55lt5;
            mediatorMass = 100; mediatorXS = 1.;
            sim_ctau = 0.74;
            BVxSysErr_up=   0.001; EVxSysErr_up= 0.001; BTrigSysErr_up=   0.027; ETrigSysErr_up=   0.059;
            BVxSysErr_down= 0.007; EVxSysErr_down=   0.001; BTrigSysErr_down= 0.021; ETrigSysErr_down= 0.048;
        }  else if (sampleID == "mH200mS8lt5"){
            //s_id = SampleID::mH125mS55lt5;
            mediatorMass = 200; mediatorXS =  1.;
            sim_ctau = 0.17;
            BVxSysErr_up=   0.036; EVxSysErr_up= 0.002; BTrigSysErr_up=   0.024; ETrigSysErr_up=   0.047;
            BVxSysErr_down= 0.003; EVxSysErr_down=   0.005; BTrigSysErr_down= 0.028; ETrigSysErr_down= 0.044;
        }  else if (sampleID == "mH200mS25lt5"){
            //s_id = SampleID::mH125mS55lt5;
            mediatorMass = 200; mediatorXS =  1.;
            sim_ctau = 0.54;
            BVxSysErr_up=   0.001; EVxSysErr_up= 0.001; BTrigSysErr_up=   0.019; ETrigSysErr_up=   0.036;
            BVxSysErr_down= 0.000; EVxSysErr_down=   0.001; BTrigSysErr_down= 0.019; ETrigSysErr_down= 0.033;
        }  else if (sampleID == "mH200mS50lt5"){
            //s_id = SampleID::mH125mS55lt5;
            mediatorMass = 200; mediatorXS =  1.;
            sim_ctau = 1.70;
            BVxSysErr_up=   0.003; EVxSysErr_up= 0.001; BTrigSysErr_up=   0.017; ETrigSysErr_up=   0.033;
            BVxSysErr_down= 0.005; EVxSysErr_down=   0.001; BTrigSysErr_down= 0.020; ETrigSysErr_down= 0.037;
        }  else if (sampleID == "mH400mS50lt5"){
            //s_id = SampleID::mH125mS55lt5;
            mediatorMass = 400; mediatorXS =  1.;
            sim_ctau = 0.7;
            BVxSysErr_up=   0.004; EVxSysErr_up= 0.002; BTrigSysErr_up=   0.015; ETrigSysErr_up=   0.026;
            BVxSysErr_down= 0.001; EVxSysErr_down=   0.002; BTrigSysErr_down= 0.017; ETrigSysErr_down= 0.027;
        }  else if (sampleID == "mH400mS100lt5"){
            //s_id = SampleID::mH125mS55lt5;
            mediatorMass = 400; mediatorXS =  1.;
            sim_ctau = 1.46;
            BVxSysErr_up=   0.001; EVxSysErr_up= 0.001; BTrigSysErr_up=   0.015; ETrigSysErr_up=   0.028;
            BVxSysErr_down= 0.006; EVxSysErr_down=   0.001; BTrigSysErr_down= 0.016; ETrigSysErr_down= 0.026;
        }  else if (sampleID == "mH600mS50lt5"){
            //s_id = SampleID::mH125mS55lt5;
            mediatorMass = 600; mediatorXS =  1.;
            sim_ctau = 0.520;
            BVxSysErr_up=   0.007; EVxSysErr_up= 0.003; BTrigSysErr_up=   0.015; ETrigSysErr_up=   0.024;
            BVxSysErr_down= 0.001; EVxSysErr_down=   0.003; BTrigSysErr_down= 0.016; ETrigSysErr_down= 0.023;
        }  else if (sampleID == "mH600mS150lt5"){
            //s_id = SampleID::mH125mS55lt5;
            mediatorMass = 600; mediatorXS =  1.;
            sim_ctau = 1.72;
            BVxSysErr_up=   0.007; EVxSysErr_up= 0.002; BTrigSysErr_up=   0.014; ETrigSysErr_up=   0.022;
            BVxSysErr_down= 0.002; EVxSysErr_down=   0.003; BTrigSysErr_down= 0.013; ETrigSysErr_down= 0.023;
        }  else if (sampleID == "mH1000mS50lt5"){
            //s_id = SampleID::mH125mS55lt5;
            mediatorMass = 1000; mediatorXS =  1.;
            sim_ctau = 0.38;
            BVxSysErr_up=   0.001; EVxSysErr_up= 0.006; BTrigSysErr_up=   0.014; ETrigSysErr_up=   0.018;
            BVxSysErr_down= 0.008; EVxSysErr_down=   0.008; BTrigSysErr_down= 0.012; ETrigSysErr_down= 0.018;
        }  else if (sampleID == "mH1000mS150lt5"){
            //s_id = SampleID::mH125mS55lt5;
            mediatorMass = 1000; mediatorXS =  1.;
            sim_ctau = 1.17;
            BVxSysErr_up=   0.001; EVxSysErr_up= 0.003; BTrigSysErr_up=   0.011; ETrigSysErr_up=   0.016;
            BVxSysErr_down= 0.004; EVxSysErr_down=   0.006; BTrigSysErr_down= 0.011; ETrigSysErr_down= 0.018;
        }  else if (sampleID == "mH1000m400lt5"){
            //s_id = SampleID::mH125mS55lt5;
            mediatorMass = 1000; mediatorXS =  1.;
            sim_ctau = 3.96;
            BVxSysErr_up=   0.005; EVxSysErr_up= 0.012; BTrigSysErr_up=   0.013; ETrigSysErr_up=   0.023;
            BVxSysErr_down= 0.028; EVxSysErr_down=   0.012; BTrigSysErr_down= 0.014; ETrigSysErr_down= 0.024;
        }else {
            s_id = SampleID::unknown;
            mediatorMass = 0; mediatorXS = 1.;
            sim_ctau = 0.0;
            BVxSysErr_up = 0.0; EVxSysErr_up = 0.0;
            BTrigSysErr_up = 0.0; ETrigSysErr_up = 0.0;
            
            BVxSysErr_down = 0.0; EVxSysErr_down = 0.0;
            BTrigSysErr_down = 0.0; ETrigSysErr_down = 0.0;
            
            BJetSysErr_up =     0.0;   EJetSysErr_up =     0.0;
            BJetSysErr_down  = 0.0;   EJetSysErr_down  = 0.0;
        }
    }
}

#endif
