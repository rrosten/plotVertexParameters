//
//  RPCScaling.h
//  plotVertexParameters
//
//  Created by Heather Russell on 06/10/16.
//
//

#ifndef __plotVertexParameters__RPCScaling__
#define __plotVertexParameters__RPCScaling__

#include <stdio.h>

class RPCScaling
{
public:
     double deltaMC = 13.262;
     double sigmaMC = 2.084;
     double deltaD = 12.171;
     double sigmaD = 2.9548;
    
     double params[4] = {deltaD,sigmaD,deltaMC,sigmaMC};
     double paramsMC[2] = {deltaMC, sigmaMC};
     double paramsData[2] = {deltaD, sigmaD};
    
    double effFunc(double *x,double *par);
    double ratioFunc(double *x,double *par);
     double getDeltaT(double Lxyz, double beta);

     double scaleMCtoData(double deltaT);
     double unscaleMC(double deltaT);
     double scaletoData(double deltaT);
     double scaletoMC(double deltaT);
    
     double scaleMCtoData(double Lxyz, double beta){ return scaleMCtoData(getDeltaT(Lxyz,beta));}
     double unscaleMC(double Lxyz, double beta){ return unscaleMC(getDeltaT(Lxyz,beta));}
     double scaletoData(double Lxyz, double beta){ return scaletoData(getDeltaT(Lxyz,beta));}
     double scaletoMC(double Lxyz, double beta){ return scaletoMC(getDeltaT(Lxyz,beta));}
    
    RPCScaling(){};
    
};

#endif /* defined(__plotVertexParameters__RPCScaling__) */
