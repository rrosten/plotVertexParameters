#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/SampleDetails.h"
#include "TStopwatch.h"
#include "TROOT.h"
#include <TString.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

using boost::property_tree::ptree;
using boost::property_tree::write_xml;
using boost::property_tree::xml_writer_settings;

using namespace std;

TStopwatch timer;
void makeXML(double sigErrUp, double sigErrDown, double bkgErrUp, double bkgErrDown, std::string sample_name, std::string outputDirectory);
void makeXML(double sigErrUp, double sigErrDown, double bkgErrUp, double bkgErrDown, std::string sample_name, std::string outputDirectory){

    //    boost::property_tree::xml_writer_settings<char> settings('\t', 1, "utf-8\"?>\n<!DOCTYPE Channel SYSTEM \'HistFactorySchema.dtd\'>\n<?\"");
    ptree tree;
    tree.add("Channel.<xmlattr>.Name", "channel1");
    tree.add("Channel.<xmlattr>.InputFile", "./data/counting_exp_data.root");
    tree.add("Channel.<xmlattr>.HistoName", "");

    ptree& data_book = tree.add("Channel.Data", "");
    data_book.add("<xmlattr>.HistoName","data");
    data_book.add("<xmlattr>.HistoPath","");
    //HistoName="data" HistoPath=""
    //    book.add("title", titles[i]);
    //    book.add("<xmlattr>.id", i);
    //    book.add("pageCount", (i+1) * 234);

    ptree& serr_book = tree.add("Channel.StatErrorConfig","");
    serr_book.add("<xmlattr>.RelErrorThreshold","0.05");
    serr_book.add("<xmlattr>.ConstraintType","Poisson");

    ptree& sig_book = tree.add("Channel.Sample","");
    sig_book.add("<xmlattr>.Name","signal");
    sig_book.add("<xmlattr>.HistoPath","");
    sig_book.add("<xmlattr>.NormalizeByTheory","True");
    sig_book.add("<xmlattr>.HistoName","signal");

    sig_book.add("OverallSys.<xmlattr>.Name","Overall");
    sig_book.add("OverallSys.<xmlattr>.Low",sigErrDown);
    sig_book.add("OverallSys.<xmlattr>.High",sigErrUp);

    sig_book.add("NormFactor.<xmlattr>.Name","SigXsecOverSM");
    sig_book.add("NormFactor.<xmlattr>.Val","1.0");
    sig_book.add("NormFactor.<xmlattr>.Low","0.0");
    sig_book.add("NormFactor.<xmlattr>.High","10.0");
    sig_book.add("NormFactor.<xmlattr>.Const","True");

    ptree& bkg_book = tree.add("Channel.Sample","");
    bkg_book.add("<xmlattr>.Name","background");
    bkg_book.add("<xmlattr>.HistoPath","");
    bkg_book.add("<xmlattr>.NormalizeByTheory","False");
    bkg_book.add("<xmlattr>.HistoName","background");

    bkg_book.add("OverallSys.<xmlattr>.Name","Bkguncert");
    bkg_book.add("OverallSys.<xmlattr>.Low",bkgErrDown);
    bkg_book.add("OverallSys.<xmlattr>.High",bkgErrUp);

    // Note that starting with Boost 1.56, the template argument must be std::string
    // instead of char
    boost::property_tree::xml_writer_settings<char> settings('\t', 1);
    std::filebuf fb;
    fb.open (outputDirectory+"counting_exp_ratesys_channel_"+sample_name+".xml",std::ios::out);
    std::ostream output(&fb);

    output << "<?xml version=\"1.0\" encoding=\"";
    output << settings.encoding;
    output << "\"?>\n";
    output << "<!DOCTYPE Channel SYSTEM \'HistFactorySchema.dtd\'>\n";

    write_xml_element(output, std::basic_string<ptree::key_type::value_type>(), tree, -1, settings);
    fb.close();

    return;
}

int main(int argc, char **argv){

    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::string outputDirectory = std::string(argv[1]);
    std::cout << "Output XMLs will be put in folder: " << argv[1] << std::endl;
    TString model = TString(argv[2]);
    std::cout << "Making XMLs for model: " << model << std::endl;
    std::vector<std::string> benchmark, sample;
    if(model == "Stealth"){
        benchmark = {"mg250", "mg500", "mg800", "mg1200", "mg1500", "mg2000"};
        sample = {"250", "500", "800", "1200", "1500", "2000"};
    }
    else if(model == "Higgs"){
        benchmark = {"mH125mS5lt5","mH125mS8lt5","mH125mS15lt5","mH125mS25lt5","mH125mS40lt5","mH125mS55lt5"};
        sample = {"1255", "1258", "12515", "12525", "12540", "12555"};
    }
    else if(model == "Scalar"){
        benchmark = {"mH100mS8lt5", "mH100mS25lt5", "mH200mS8lt5", "mH200mS25lt5","mH200mS50lt5", "mH400mS50lt5", "mH400mS100lt5", "mH600mS50lt5", "mH600mS150lt5", "mH1000mS50lt5", "mH1000mS150lt5", "mH1000mS400lt5"};
        sample = {"1008", "10025", "2008", "20025", "20050", "40050","400100","60050","600150","100050","1000150","1000400"};
    }
    else {
        std::cout << "Model unknown :(. Exiting" << std::endl;
        return 0;
    }

    for(unsigned int i=0; i<benchmark.size(); i++){
        SampleDetails::setGlobalVariables(benchmark.at(i));
        makeXML(SampleDetails::global2VxError_up,SampleDetails::global2VxError_down,SampleDetails::bkg2VxError_up,SampleDetails::bkg2VxError_down,sample.at(i), outputDirectory);
    }
    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;
    std::cout << "Done creating xmls" << std::endl;
}
