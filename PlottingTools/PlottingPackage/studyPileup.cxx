void studyPileup(){
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c_1","c_1",800,600);
    c1->SetLogy(1);
    
    TChain* ch = new TChain("recoTree");
        ch->Add("*root*");
    
    TH1D* h[3];
    
    Int_t colorSig[4] = {kGray, kBlue-3, kViolet-6, kTeal-6};
    TLegend *leg = new TLegend(0.6,0.73,.92,0.91);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.03);
    TString label[3] = {"nominal","PRW__1up","PRW__1down"};
    
    for(int i=0;i<3;i++){
        h[i] = new TH1D(TString("n"+to_string(i)),TString("n"+to_string(i)),50,0,500);
        if(i==0) ch->Project(TString("n"+to_string(i)), "CalibJet_pT[0]","1.0*pileupEventWeight");
        else if(i==1) ch->Project(TString("n"+to_string(i)), "CalibJet_pT[0]","1.0*event_Pileup_PRW_DATASF__1up");
        else if (i==2) ch->Project(TString("n"+to_string(i)), "CalibJet_pT[0]","1.0*event_Pileup_PRW_DATASF__1down");
        h[i]->SetLineColor(colorSig[i]);
        h[i]->SetLineStyle(i+1);
        std::cout << "Number of events with pT > 50 GeV jet: " << h[i]->Integral(5,501) << std::endl;
        if(i==0){
            
            h[i]->GetXaxis()->SetTitle("Leading jet p_{T} [GeV]");
            h[i]->GetYaxis()->SetTitle("Number of events");
            h[i]->Draw("hist");
        }
        
        else{
            h[i]->Draw("HIST SAME");
        }
        leg->AddEntry(h[i],label[i],"l");
    }
    leg->Draw();
    gPad->RedrawAxis();
    ATLASLabel(0.19,0.88,"Simulation Internal", kBlack);

    
    c1->Print("leadingjets.pdf");
    
}
