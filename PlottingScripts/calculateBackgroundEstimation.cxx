//
//  calculateBackgroundEstimation.cpp
//  plotVertexParameters
//
//  Created by Heather Russell on 13/10/16.
//
//

void calculateBackgroundEstimation(){
    double btrig = 9974328.;
    double etrig = 3446945.;
    double bbtrig = 1002.;
    double eetrig = 164.;
    double betrig = 84.;

    double bvxtrig = 44782.;
    double evxtrig = 107427.;
    double n1vxtrig = bvxtrig+evxtrig;
    
    double bbvxtrig = 0.;
    double eevxtrig = 0;
    double bevxtrig = 0;
    double ebvxtrig = 0;

    double nvx_zb = 6.;
    double nevt_zb = 35673956.  ;
    double pvx_zb = nvx_zb/nevt_zb;
    double pvx_zb_e = sqrt(pvx_zb*(1-pvx_zb)/nevt_zb);
    
    double pbvx = bvxtrig/btrig;
    double pbvx_e = sqrt(pbvx*(1-pbvx) / btrig);
    
    double pevx = evxtrig/etrig;
    double pevx_e = sqrt(pevx*(1-pevx) / etrig);
    
    double nbtt = (bbvxtrig + ebvxtrig) * pbvx;
    double nbtt_e = nbtt * sqrt(1. / (bbvxtrig + ebvxtrig) + pbvx_e*pbvx_e);
    double nett = (eevxtrig + bevxtrig) * pevx;
    double nett_e = nett * sqrt(1. / (eevxtrig + bevxtrig) + pevx_e*pevx_e);

    
    double nt_zb = (n1vxtrig)*pvx_zb;
    double nt_zb_e = nt_zb*sqrt(1./n1vxtrig+(pvx_zb_e/pvx_zb)*(pvx_zb_e/pvx_zb));
    
    std::cout << "\\PMSVertex = " << pvx_zb << " \\pm " << pvx_zb_e << std::endl;
    std::cout << "\\PMSRecoBarrel = " << pbvx << " \\pm " << pbvx_e << std::endl;
    std::cout << "\\PMSRecoEndcaps = " << pevx << " \\pm " << pevx_e << std::endl;
    
    std::cout << "\\nCluVxBCluVx = " << nbtt << " \\pm " << nbtt_e << std::endl;
    std::cout << "\\nCluVxECluVx = " << nett << " \\pm " << nett_e << std::endl;
    std::cout << "\\nCluVxVx = " << nt_zb << " \\pm " << nt_zb_e << std::endl;
    
    std::cout << " total background : " << nbtt + nett + nt_zb << " \\pm " << sqrt(nbtt_e*nbtt_e + nett_e*nett_e + nt_zb_e*nt_zb_e) << std::endl;
}
