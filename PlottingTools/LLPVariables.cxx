//
//  LLPVariables.cpp
//  plotVertexParameters
//
//  Created by Heather Russell on 12/10/15.
//
//

#include "PlottingPackage/LLPVariables.h"
#include <iostream>
#include "TVector3.h"
void LLPVariables::setZeroVectors(){

    eta=0;
    phi=0;
    Lz=0;
    Lxy=0;
    Lxyz= new std::vector<double>;
    beta=0;
    gamma=0;
    pT=0;
//    px=0;
//    py=0;
    pz= new std::vector<double>;
    E=0;
    nMSeg = 0;
    bg = new std::vector<double>;
    theta = new std::vector<double>;
}
void LLPVariables::setBranchAddresses(TChain *chain){

    chain->SetBranchAddress("LLP_beta",&beta);
    chain->SetBranchAddress("LLP_gamma",&gamma);
    chain->SetBranchAddress("LLP_Lxy",&Lxy);
    chain->SetBranchAddress("LLP_Lz",&Lz);
    chain->SetBranchAddress("LLP_eta",&eta);
    chain->SetBranchAddress("LLP_phi",&phi);
    chain->SetBranchAddress("LLP_pT",&pT);
    chain->SetBranchAddress("LLP_E",&E);
    chain->SetBranchAddress("LLP_nMSegs_dR04", &nMSeg);
    
}
void LLPVariables::setToyBranchAddresses(TChain *chain){
//used in doLimitExtrapolation    
    chain->SetBranchAddress("LLP_E",&E);
    chain->SetBranchAddress("LLP_pT",&pT);
    chain->SetBranchAddress("LLP_eta",&eta);
    chain->SetBranchAddress("LLP_phi",&phi);
    chain->SetBranchAddress("LLP_beta",&beta);
    chain->SetBranchAddress("LLP_gamma",&gamma);

}
void LLPVariables::SetAllVariables(){
    for(unsigned int i=0; i< eta->size();i++){
        TVector3 tmp; tmp.SetPtEtaPhi(1.0,eta->at(i),phi->at(i));
        theta->push_back(tmp.Theta());
        bg->push_back(beta->at(i)*gamma->at(i));
        double pz_tmp = E->at(i) * ( exp(2.*eta->at(i)) - 1.0 )/ ( exp(2.*eta->at(i)) + 1.0 );
        pz->push_back(pz_tmp);
    }
}

void LLPVariables::clearAllVectors(){

    eta->clear();
    phi->clear();
    Lz->clear();
    Lxy->clear();
    Lxyz->clear();
    pT->clear();
   // px->clear();
   // py->clear();
    pz->clear();
    bg->clear();
    theta->clear();
    beta->clear();
    gamma->clear();
    E->clear();
    nMSeg->clear();
    
}
void LLPVariables::clearAllToyVectors(){
    eta->clear();
    phi->clear();
    E->clear();
    pz->clear();
    pT->clear();
    bg->clear();
    theta->clear();
    beta->clear();
    gamma->clear();
}
void LLPVariables::set_pz(){
    for(unsigned int i=0; i< eta->size();i++){
        double pz_tmp = E->at(i) * ( exp(2.*eta->at(i)) - 1.0 )/ ( exp(2.*eta->at(i)) + 1.0 ); 
        pz->push_back(pz_tmp);
    }
}
void LLPVariables::set_Lxyz(){
    for(unsigned int i=0; i< eta->size();i++){
        double lxyztmp = sqrt(Lxy->at(i)*Lxy->at(i) + Lz->at(i)*Lz->at(i));
        Lxyz->push_back(lxyztmp);
    }
}
