//
//  makeCombinedPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include "TMultiGraph.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"

using namespace std;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
TStopwatch timer;

TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){
    int numberOfSlashes = 0; int tmpSlashNumber = 0;
    int indexOf2ndLastSlash = -1;
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            numberOfSlashes++;
        }
    }
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            tmpSlashNumber++;
        }
        if(tmpSlashNumber == (numberOfSlashes - 1)){
            indexOf2ndLastSlash = charN;
            break;
        }
    }
	fileName = (TString)fileName(0+indexOf2ndLastSlash+1,fileName.Sizeof() - 18 - indexOf2ndLastSlash - 1);
    std::cout << "file name : " << fileName << std::endl;
	TString title = "";

	if(fileName.Contains("HChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        if(channel == "nubb") channel = "#nu b#bar{b}";
        TString mchi = (TString)fileName(fileName.First("5")+5,fileName.Sizeof());
        title = (TString) "H #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
		return title;
    } else if(fileName.Contains("WChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        TString mchi = (TString)fileName(fileName.First("m")+4,fileName.Sizeof());
        title = (TString) "W/Z #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    }else if(fileName.Contains("mS")){
		TString lt = "";
		TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
		TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
		if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
		if(mH == "125") title = (TString) "m_{H},m_{s}=[125,"+mS+"] GeV" + lt;
		else title = (TString) "m_{#Phi},m_{s}=["+mH+","+mS+"] GeV" + lt;
		return title;
	} else if(fileName.Contains("mg")){
		TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
		title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
	}
	return title;
}
void makeCombinedPlot(TFile *_sig[4], int nSIG, TString type, bool forThesis);
void makeCombinedPlot(TFile *_sig[4], int nSIG, TString type, bool forThesis = false){
bool add_ctau = false;    
    TString sigNames[6];
    std::cout << "file name: " << _sig[0]->GetName() << std::endl;
    for(int i=0; i<nSIG; i++){ 
        sigNames[i] = convertFiletoLabel(_sig[i]->GetName(), add_ctau);
        if(i == 2){
            sigNames[i] = "Dijets (LJ 600 < pT < 1000 GeV)";
        } else if(i == 3){
            sigNames[i] = "Dijets (LJ > 1000 GeV)";
        }
        
    }
    
    Int_t colorSig[16] = { kPink-3,kViolet-2, kAzure+5, kTeal-6,kTeal+2, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2};
    
    if(nSIG < 5){
        colorSig[0] = kTeal - 5;
        colorSig[1] = kPink-1;
        colorSig[2] = kBlue + 2;
        colorSig[3] = kAzure + 5;
    }
    Int_t markerSig[16] = {26,25,20,23,30,26,27,24,25,26,27,24,25,26};
    
    Int_t colors[20] = {1,2,3,4,6,1,2,3,4,6,1,2,3,4,6,1,2,3,4,6};
    
    TString names[12];
    names[0] = "eta1_nTracklets_";
    names[1] = "eta1_nMuonRoIs_";
    names[2] = "eta1_nAssocMSeg_";
    names[3] = "eta3_nTracklets_";
    names[4] = "eta3_nMuonRoIs_";
    names[5] = "eta3_nAssocMSeg_";
    
    TString sAdd[4]; sAdd[0] = "MSDecays";sAdd[1] = "MSDecays";sAdd[2] = "LJ";sAdd[3] = "LJ";
    
    //SetAtlasStyle();
    //gStyle->SetPalette(1);
    
    std::cout << "running on : " << _sig[0]->GetName() << std::endl;
    
    TCanvas* c = new TCanvas("c_1","c_1",800,600);
    c->SetLogy();
    
    //vertex reco
    for(unsigned int i=0;i<6; i++){
        if(i==1 || i==4) continue;
        std::cout << "plot: " << names[i] << std::endl;
        
        TH1D* h_sig[30];
        
        TLegend *legS = new TLegend(0.52,0.8,0.9,0.93);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.03);
        
        for(int j=0; j<nSIG; j++){
            h_sig[j] = (TH1D*) _sig[j]->Get(names[i]+sAdd[j]);
            if(!h_sig[j] ) continue;
            h_sig[j]->SetMarkerColor(colorSig[j]);
            h_sig[j]->SetLineColor(colorSig[j]);
            h_sig[j]->SetMarkerStyle(markerSig[j]);
            
            if(names[i].Contains("MuonRoI")){
                h_sig[j]->Scale(1./h_sig[j]->Integral());

                h_sig[j]->GetXaxis()->SetTitle("Number of L1 Muon RoIs in #Delta R = 0.4");
                h_sig[j]->GetYaxis()->SetRangeUser(0.001,1);
                //h_sig[j]->GetXaxis()->SetRangeUser(0,20);
            }
            else if(names[i].Contains("Assoc")){ 
                h_sig[j]->Rebin(5);
                h_sig[j]->Scale(1./h_sig[j]->Integral(h_sig[j]->FindBin(20),h_sig[j]->GetNbinsX()+1));

                h_sig[j]->GetXaxis()->SetTitle("Number of muon segments in #Delta R = 0.4");
                h_sig[j]->GetYaxis()->SetRangeUser(0.00001,2);
                h_sig[j]->GetXaxis()->SetRangeUser(20,180); 
            }
            else if(names[i].Contains("Tracklets")){
                h_sig[j]->Scale(1./h_sig[j]->Integral());

                h_sig[j]->GetYaxis()->SetRangeUser(0.0001,2);
                //h_sig[j]->GetXaxis()->SetRangeUser(0,1200);
            h_sig[j]->GetXaxis()->SetTitle("Number of tracklets in #Delta R = 0.4");
            }
            h_sig[j]->GetYaxis()->SetTitle("Fraction of decays (jets)");
            
            legS->AddEntry(h_sig[j],sigNames[j],"lp");
            
            
            if(j == 0){ h_sig[j]->Draw("");}
            else h_sig[j]->Draw("SAME");
            
            if(j == nSIG-1){
                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.045);
                latex.SetTextAlign(13);  //align at top
                double y_val_label = 0.91;
                if(!forThesis){
                    y_val_label = 0.86;
                    latex.DrawLatex(.2,.91,"#font[72]{ATLAS} Internal");
                }
                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.035);
                latex2.SetTextAlign(13);  //align at top
                if(names[i].Contains("eta1")){latex2.DrawLatex(.2,y_val_label,"#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}");}
                else if(names[i].Contains("eta3") ){latex2.DrawLatex(.2,y_val_label,"#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}");}
                
                legS->Draw();
                gPad->RedrawAxis();
                
                c->Print("thesisPlots_scaling/"+names[i]+"_"+type+"_1TeVEC.pdf");
                legS->Clear();
            }
            
        }
    }
}


int makeDijetSignalComparisonPlots(bool forThesis = false){


	//gROOT->LoadMacro("AtlasUtils.C");
	//gROOT->LoadMacro("AtlasLabels.C");
	//gROOT->LoadMacro("AtlasStyle.C");

	bool add_ctau = false;
    
	TFile *_sig[4];
    TString locn = "signalMC/";
    
    _sig[0] = new TFile(locn+"mg250/outputMSEff.root");
    _sig[1] = new TFile(locn+"mg2000/outputMSEff.root");
    _sig[2] = new TFile("bkgdJZ/JZAll/outputMSSystematics_20p7_j600to1or2.root");
    _sig[3] = new TFile("bkgdJZ/JZAll/outputMSSystematics_20p7_j1000.root");
    makeCombinedPlot(_sig, 4, "stealth", forThesis);

    
	return 314;

}
