//
//  plotParamsFromTTree.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/VertexHistograms.h"
#include "PlottingPackage/TriggerVariables.h"
#include "PlottingPackage/VertexVariables.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/PlottingUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/HistTools.h"

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>

using namespace std;
using namespace plotVertexParameters;
using namespace PlottingUtils;
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
bool isSignal; int isData;
double jetSliceWeights[13];
TStopwatch timer;


double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;
}
double scaleMeff(double Meff);
double scaleMeff(double Meff){
    if(Meff < 10.) return 0.0123803;
    else if(Meff < 40.){
        std::cout <<"this bin has zero entries, why are we calling it? " << std::endl;
        return 0.;
    }
    else if (Meff < 60.) return 0.0580279;
    else if (Meff < 90.) return 0.272705;
    else if (Meff < 120.) return 0.210008;
    else if (Meff < 150.) return 0.16783;
    else if (Meff < 180.) return 0.120115;
    else if (Meff < 210.) return 0.086823;
    else if (Meff < 300.) return 0.0438672;
    else if (Meff < 400.) return 0.0177901;
    else if (Meff < 500.) return 0.00771574;
    else if (Meff < 5000.) return 0.00273717;
    
    else {
        std::cout << "effective mass is: " << Meff << ", returning weight = 0" << std::endl;
        return 0.;
    }
    
}
double combVariable(double dR1, double dR2, double cut);
double combVariable(double dR1, double dR2, double cut){
    if(dR1 < cut || dR2 < cut){ return -1.0*sqrt(fabs((dR1 - cut)*(dR2 - cut)));}
    else return sqrt(fabs((dR1 - cut)*(dR2 - cut)));
}
int main(int argc, char **argv){
    double nBarrel_Vx_events = 0;
    double nBarrel_Vx2j150_events = 0;
    double nEC_Vx_events = 0;
    double nEC_Vx2j150_events = 0;
    
    std::cout << "running program!" << std::endl;
    
    //TFile *outFile = new TFile("outputHistograms.root","RECREATE");
    
    TChain *chain = new TChain("recoTree");
    
    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
    isSignal = (TString(argv[3]) == "true")? 1 : 0;
    std::cout << "Signal? : " << argv[3]  << ", " << isSignal << std::endl;
    
    InputFiles::AddFilesToChain(TString(argv[1]), chain);
    chain->SetBranchStatus("*",0);
    
    //Directory the plots will go in
    
    TString plotDirectory = TString(argv[2]);
    
    TFile *outputFile = new TFile(plotDirectory+"/outputDataSearch_goodJetEventFlags.root","RECREATE");
    
    std::cout << "added file!, will be saved in directory: " << plotDirectory << std::endl;
    
    TChain *vxChain = new TChain("recoTree");
    std::map<ULong64_t, int> msvxMap;
    ULong64_t eventNumber = 0;
    
    if(isSignal){
        InputFiles::AddVertexRerunFiles(TString(argv[1]), vxChain);
        vxChain->SetBranchStatus("*",0);
        msvxMap = CreateMap(vxChain);
        //have to have the next few lines here or it segfaults, because it's used in CreateMap.
        //I don't fully understand why SetBranchStatus("*",0) doesn't get rid of it.
        vxChain->SetBranchStatus("*",1);
        vxChain->SetBranchAddress("EventNumber", &eventNumber);
    }
    
    /*VertexHistograms *vxBIso = new VertexHistograms;
     vxIso->initializeHistograms("MSVx","BIso");
     
     VertexHistograms *vxBNoiso = new VertexHistograms;
     vxNoiso->initializeHistograms("MSVx","BNoiso");
     
     VertexHistograms *vxEIso = new VertexHistograms;
     vxIso->initializeHistograms("MSVx","EIso");
     
     VertexHistograms *vxENoiso = new VertexHistograms;
     vxNoiso->initializeHistograms("MSVx","ENoiso");
     */
    
    HistTools *histCR = new HistTools;
    histCR->addHist("logR_Jets_dR04",400,-10,10);
    histCR->addHist("ClosesetJet_logR_vs_dR",400,-10,10,400,0,10);
    
    HistTools *histIso = new HistTools;
    histIso->addHist("pVx_vs_nJets_iso",5,0,5);
    histIso->addHist("nJets_iso",10,0,10);
    histIso->addHist("eventHT_iso",500,0,5000);
    histIso->addHist("eventHTMiss_iso",100,0,1000);
    histIso->addHist("eventMeff_iso",500,0,5000);
    histIso->addHist("Vx_Trig_dR_iso",200,0,2);
    
    HistTools *histNoiso = new HistTools;
    histNoiso->addHist("pVx_vs_nJets_noiso",5,0,5);
    histNoiso->addHist("nJets_noiso",10,0,10);
    histNoiso->addHist("eventHT_noiso",500,0,5000);
    histNoiso->addHist("eventHTMiss_noiso",100,0,1000);
    histNoiso->addHist("eventMeff_noiso",500,0,5000);
    histNoiso->addHist("Vx_Trig_dR_noiso",200,0,2);
    
    //experiment 1 = e1
    //experiment 1 = e1
    HistTools *hist2JetSR = new HistTools;
    
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_1j50_b", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_2j50_b", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_1j50_1j150_b", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_2j150_b", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_1j100_1j200_b", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_2j200_b", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_1j100_1j250_b", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_2j250_b", 400,0,10,400,0,10);
    
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_1j50_ec", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_2j50_ec", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_1j50_1j150_ec", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_2j150_ec", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_1j100_1j200_ec", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_2j200_ec", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_1j100_1j250_ec", 400,0,10,400,0,10);
    hist2JetSR->addHist("ClosestJetdR_vs_ClosestTrackdR_2j250_ec", 400,0,10,400,0,10);
    
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_1j50_b",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_2j50_b",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_1j50_1j150_b",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_2j150_b",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_1j100_1j200_b",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_2j200_b",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_1j100_1j250_b",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_2j250_b",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_JetETs_1j100_1j250_b",100,0,1000,100,0,1000);
    hist2JetSR->addHist("e1_JetETs_2j250_b",100,0,1000,100,0,1000);
    
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_1j50_ec",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_2j50_ec",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_1j50_1j150_ec",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_2j150_ec",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_1j100_1j200_ec",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_2j200_ec",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_1j100_1j250_ec",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_ClosestdR_vs_nHits_2j250_ec",200,0,10,100,0,10000);
    hist2JetSR->addHist("e1_JetETs_1j100_1j250_ec",100,0,1000,100,0,1000);
    hist2JetSR->addHist("e1_JetETs_2j250_ec",100,0,1000,100,0,1000);
    
    hist2JetSR->addGAHists("1j50_ec","e1");
    hist2JetSR->addGAHists("2j50_ec","e1");
    hist2JetSR->addGAHists("1j50_1j150_ec","e1");
    hist2JetSR->addGAHists("2j150_ec","e1");
    hist2JetSR->addGAHists("1j100_1j200_ec","e1");
    hist2JetSR->addGAHists("2j200_ec","e1");
    hist2JetSR->addGAHists("1j100_1j250_ec","e1");
    hist2JetSR->addGAHists("2j250_ec","e1");
    
    hist2JetSR->addGAHists("1j50_b","e1");
    hist2JetSR->addGAHists("2j50_b","e1");
    hist2JetSR->addGAHists("1j50_1j150_b","e1");
    hist2JetSR->addGAHists("2j150_b","e1");
    hist2JetSR->addGAHists("1j100_1j200_b","e1");
    hist2JetSR->addGAHists("2j200_b","e1");
    hist2JetSR->addGAHists("1j100_1j250_b","e1");
    hist2JetSR->addGAHists("2j250_b","e1");
    
    VertexVariables *vxVar = new VertexVariables;
    TriggerVariables *trigVar = new TriggerVariables;
    CommonVariables *commonVar = new CommonVariables;
    
    bool isData = !isSignal;
    
    vxVar->setZeroVectors(isSignal);
    vxVar->setBranchAdresses(isSignal ? vxChain : chain, isSignal, false);
    std::cout << "got vertex variables" << std::endl;
    trigVar->setZeroVectors();
    trigVar->setBranchAdresses(chain,isSignal);
    std::cout << "got trigger variables" << std::endl;
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain,true,false,isData,isSignal,false);
    std::cout << "got common variables" << std::endl;
    
    TString inputFile = chain->GetFile()->GetName();
    
    std::cout << "" << std::endl;
    std::cout << "Going to run over " << chain->GetEntries()  << " events." << endl;
    std::cout << "First file is called " << inputFile << std::endl;
    
    setPrettyCanvasStyle();
    
    TH1::SetDefaultSumw2();
    
    setJetSliceWeights(jetSliceWeights);
    
    int jetSlice = -1;
    double finalWeight = 0;
    timer.Start();
    
    TH1F *h_time = new TH1F("htime","htime",200,0,200);
    
    for (int i_evt=0;i_evt<chain->GetEntries();i_evt++)
    {
        if(i_evt % 500000 == 0){
            timer.Stop();
            std::cout << "event " << i_evt  << " at time " << timer.RealTime() << std::endl;
            h_time->Fill(double(i_evt)/500000.,timer.RealTime());
            timer.Start();
        }
        
        chain->GetEntry(i_evt);
        commonVar->fixJetET();
        
	    inputFile = TString(chain->GetCurrentFile()->GetName());
        if(   inputFile.Contains("JZ")    ){
            isSignal = 0; isData = 0;
            if(inputFile.Contains("JZ0W")) jetSlice = 0;
            if(inputFile.Contains("JZ1W")) jetSlice = 1;
            if(inputFile.Contains("JZ2W")) jetSlice = 2;
            if(inputFile.Contains("JZ3W")) jetSlice = 3;
            if(inputFile.Contains("JZ4W")) jetSlice = 4;
            if(inputFile.Contains("JZ5W")) jetSlice = 5;
            if(inputFile.Contains("JZ6W")) jetSlice = 6;
            if(inputFile.Contains("JZ7W")) jetSlice = 7;
            if(inputFile.Contains("JZ8W")) jetSlice = 8;
            if(inputFile.Contains("JZ9W")) jetSlice = 9;
            if(inputFile.Contains("JZ10W")) jetSlice = 10;
            if(inputFile.Contains("JZ11W")) jetSlice = 11;
            if(inputFile.Contains("JZ12W")) jetSlice = 12;
            
            finalWeight = commonVar->pileupEventWeight*jetSliceWeights[jetSlice] * commonVar->eventWeight;
        } else if( isSignal ) finalWeight = commonVar->pileupEventWeight;
        else {
            isData = 1;
            finalWeight = 1.0;
        }
        
        //std::cout << "trig variables size? " << trigVar->eta->size() << std::endl;
        double trig_eta = -99;
        double trig_phi = -99;
        double trig_nJet = -1;
        
        if(isSignal) {
            //        std::cout << "vxVarMap[commonVar->eventNumber]: " << vxVarMap[commonVar->eventNumber] << std::endl;
            vxChain->GetEntry(msvxMap[commonVar->eventNumber]);
            //std::cout << "vertices? " << vxVar->eta->size() << std::endl;
            //make the scale-factored clusters!
            trigVar->reclusterRoIs();
            //std::cout << "number of new clusters: " << trig->cluSyst->size() << std::endl;
            
            for(unsigned int i=0; i<trigVar->cluEta->size(); i++){
                if(trigVar->cluSyst->at(i) == 0){
                    if( (TMath::Abs(trigVar->cluEta->at(i)) > 0.8  && TMath::Abs(trigVar->cluEta->at(i)) < 1.3)
                       || TMath::Abs(trigVar->cluEta->at(i)) > 2.5){ break;}
                    
                    trig_eta = trigVar->cluEta->at(i);
                    trig_phi = trigVar->cluPhi->at(i);
                    break;
                    //if(TMath::Abs(trigVar->cluEta->at(i)) < 1.0) trigRegion.push_back(1);
                    // else trigRegion.push_back(2);
                }
            }
            //std::cout << "trigger? " << trig_eta << ", " << trig_phi << std::endl;
            if(trig_eta > -90){
                commonVar->passMuvtx_noiso = true;
            } else {
                commonVar->passMuvtx_noiso = false;
                vxVar->clearAllVectors(false, isSignal);
                trigVar->clearAllVectors();
                commonVar->clearAllVectors(isSignal,true);
                continue;
                
            }
        } else {
            if(!commonVar->passMuvtx_noiso){
                vxVar->clearAllVectors(false, isSignal);
                trigVar->clearAllVectors();
                commonVar->clearAllVectors(isSignal,true);
                continue;
            }
            if(trigVar->eta->size() == 0){
                vxVar->clearAllVectors(false, isSignal);
                trigVar->clearAllVectors();
                commonVar->clearAllVectors(isSignal,true);
                continue;
            }
            if(!commonVar->isQualityEvent(isData)) {
                vxVar->clearAllVectors(false, isSignal);
                trigVar->clearAllVectors();
                commonVar->clearAllVectors(isSignal,true);
                continue;
            }
            trig_eta = trigVar->eta->at(0);
            trig_phi = trigVar->phi->at(0);
            trig_nJet = trigVar->nJet->at(0);
            if(commonVar->passMuvtx_noiso &&( (TMath::Abs(trig_eta) > 0.8 && TMath::Abs(trig_eta) < 1.3) || TMath::Abs(trig_eta) > 2.5)){
                vxVar->clearAllVectors(false, isSignal);
                trigVar->clearAllVectors();
                commonVar->clearAllVectors(isSignal,true);
                continue;
            }
        }
        if(isSignal){
            if(vxVar->eta->size() > 0){
                vxVar->performJetIsolation(commonVar->Jet_ET,commonVar->Jet_eta,commonVar->Jet_phi, commonVar->Jet_logRatio, commonVar->Jet_passJVT);
                vxVar->performTrackIsolation(commonVar->Track_pT,commonVar->Track_eta,commonVar->Track_phi);
                vxVar->testHitThresholds();
                vxVar->testIsGood(isSignal);
            }
        }
        
        if(commonVar->passMuvtx && trig_nJet > 0){
            std::cout << "contradiction: online passes iso, offline has " << trig_nJet << " jets" << std::endl;
        }
        
        
        bool isIsoVertexEvent = false;
        bool isNoisoVertexEvent = false;
        
        TString regString = "";
        int vxMatch_index = -1;
        double closestVertexdR = 99;
        
        int nVertices = 0;
        
        //std::cout << "vx variables size? " << vxVar->eta->size() << std::endl;
        bool isBarrelEvent = false;
        
        for (size_t i_vtx=0;i_vtx<vxVar->eta->size();i_vtx++){
            if(isSignal && vxVar->syst->at(i_vtx) != 0) continue;
            
            double msvx_eta = vxVar->eta->at(i_vtx);
            //don't consider vertices in the overlap region
            if( (TMath::Abs(msvx_eta) > 0.8  && TMath::Abs(msvx_eta) < 1.3) || TMath::Abs(msvx_eta) > 2.5){ continue;}
            
            double msvx_phi = vxVar->phi->at(i_vtx);
            //only consider vertices that pass the minimum hit threshold.
            if( !vxVar->passesHitThresholds->at(i_vtx) ){
                continue;
            }
            nVertices++;

        
            double deltaR2 = DeltaR2(trig_phi,msvx_phi,trig_eta,msvx_eta);
            //std::cout << "delta r between trig/vx: " << sqrt(deltaR2) << std::endl;
            if(deltaR2 > 0.4*0.4) continue;
            
            if(deltaR2 < closestVertexdR){
                closestVertexdR = deltaR2;
                vxMatch_index = i_vtx;
            }
            
            if(TMath::Abs(msvx_eta) <= 0.8) isBarrelEvent = true;
            
            //iso/noiso strings for kinematic variables plots (not ABCD plane)
            if(isBarrelEvent){
                if( vxVar->closestdR->at(i_vtx) <= 0.3){
                    isNoisoVertexEvent = true;
                    regString = "noiso";
                }
                else if( vxVar->closestdR->at(i_vtx)  > 0.3){//vxVar->isGood->at(i_vtx) ){
                    isIsoVertexEvent= true;
                    regString = "iso";
                }
            } else {
                if( vxVar->closestdR->at(i_vtx) <= 0.6){
                    isNoisoVertexEvent = true;
                    regString = "noiso";
                }
                else if( vxVar->closestdR->at(i_vtx)  > 0.6){//vxVar->isGood->at(i_vtx) ){
                    isIsoVertexEvent= true;
                    regString = "iso";
                }
            }
            
        }
        closestVertexdR = sqrt(closestVertexdR);
        
        if(nVertices > 1 && isData){
            if(isNoisoVertexEvent) std::cout << "noniso event: " << i_evt << " has " << nVertices << " good vertices" << std::endl;
            else if(isIsoVertexEvent) std::cout << "iso event: " << i_evt << " has " << nVertices << " good vertices" << std::endl;
        }
        if(nVertices > 1){
            vxVar->clearAllVectors(false, isSignal);
            trigVar->clearAllVectors();
            commonVar->clearAllVectors(isSignal,true);
            continue;
        }
        
        //use this if we only want to include events with trig+vertex.
        if( !isIsoVertexEvent && !isNoisoVertexEvent ){
            vxVar->clearAllVectors(false, isSignal);
            trigVar->clearAllVectors();
            commonVar->clearAllVectors(isSignal,true);
            continue;
        }
        
        std::vector<TString> exp; std::vector<TString> expIso;
        if(isIsoVertexEvent || isNoisoVertexEvent){
            exp.push_back("e1");  expIso.push_back(isIsoVertexEvent ? "iso" : "noiso");
        }
        
        TString reg = "";
        int numHits = 0;
        
        //if vertex matches a trigger...
        if( closestVertexdR < 0.4){
            commonVar->setGoodJetEventFlags();
            commonVar->set2DPlotVars(vxVar->eta->at(vxMatch_index), vxVar->phi->at(vxMatch_index));
            //commonVar->getVertexTrackdR(vxVar->eta->at(vxMatch_index), vxVar->phi->at(vxMatch_index));
            
            if(isBarrelEvent){
                reg = "_b";
                numHits = vxVar->nMDT->at(vxMatch_index) + vxVar->nRPC->at(vxMatch_index);
            } else {
                reg = "_ec";
                numHits = vxVar->nMDT->at(vxMatch_index) + vxVar->nTGC->at(vxMatch_index);
            }
        }
        TString srcr = "CR";
        if(numHits >= 2000) srcr = "SR";
        
        double weight = finalWeight;
        /*
         if(isIsoVertexEvent){
         commonVar->recalculateEvtProperties();
         
         histIso->fill("Vx_Trig_dR_iso",closestVertexdR, 1.0);
         histIso->fill("nJets_iso",commonVar->eventNJets,1);
         
         if(closestVertexdR < 0.4){
         
         histIso->fill("pVx_vs_nJets_iso",nJets,1.0);
         histIso->fill("eventHT_iso",commonVar->eventHT,1.0);
         histIso->fill("eventHTMiss_iso",commonVar->eventHTMiss,1.0);
         histIso->fill("eventMeff_iso",commonVar->eventMeff,1.0);
         
         vxIso->h_eta->Fill(vxVar->eta->at(vxMatch_index));
         vxIso->h_phi->Fill(vxVar->phi->at(vxMatch_index));
         vxIso->h_R->Fill(vxVar->R->at(vxMatch_index)*0.001);
         vxIso->h_z->Fill(vxVar->z->at(vxMatch_index)*0.001);
         vxIso->h_nTrks->Fill(vxVar->nTrks->at(vxMatch_index));
         vxIso->h_nMDT->Fill(vxVar->nMDT->at(vxMatch_index));
         if(TMath::Abs(vxVar->eta->at(vxMatch_index)) < 1.0) vxIso->h_nRPC->Fill(vxVar->nRPC->at(vxMatch_index));
         else if(TMath::Abs(vxVar->eta->at(vxMatch_index)) > 1.0) vxIso->h_nTGC->Fill(vxVar->nTGC->at(vxMatch_index));
         
         }
         }
         if(isNoisoVertexEvent){
         
         histNoiso->fill("Vx_Trig_dR_noiso",closestVertexdR, weight);
         histNoiso->fill("nJets_noiso",commonVar->eventNJets-1,weight);
         
         if(closestVertexdR < 0.4){
         if(commonVar->vertexJetdR > 0.3 && isBarrelEvent ) std::cout << "noniso event has closest jet within: " << commonVar->vertexJetdR << " of vertex... " << std::endl;
         
         commonVar->recalculateEvtProperties(vxVar->eta->at(vxMatch_index), vxVar->phi->at(vxMatch_index));
         weight = finalWeight*1.0;//scaleMeff(commonVar->eventMeff);
         
         histNoiso->fill("pVx_vs_nJets_noiso",commonVar->eventNJets,weight);
         histNoiso->fill("eventHT_noiso",commonVar->eventHT,weight);
         histNoiso->fill("eventHTMiss_noiso",commonVar->eventHTMiss,weight);
         histNoiso->fill("eventMeff_noiso",commonVar->eventMeff,weight);
         
         vxNoiso->h_eta->Fill(vxVar->eta->at(vxMatch_index),weight);
         vxNoiso->h_phi->Fill(vxVar->phi->at(vxMatch_index),weight);
         vxNoiso->h_R->Fill(vxVar->R->at(vxMatch_index)*0.001,weight);
         vxNoiso->h_z->Fill(vxVar->z->at(vxMatch_index)*0.001,weight);
         vxNoiso->h_nTrks->Fill(vxVar->nTrks->at(vxMatch_index),weight);
         vxNoiso->h_nMDT->Fill(vxVar->nMDT->at(vxMatch_index),weight);
         
         if(TMath::Abs(vxVar->eta->at(vxMatch_index)) < 1.0) vxNoiso->h_nRPC->Fill(vxVar->nRPC->at(vxMatch_index),weight);
         else if(TMath::Abs(vxVar->eta->at(vxMatch_index)) > 1.0) vxNoiso->h_nTGC->Fill(vxVar->nTGC->at(vxMatch_index),weight);
         
         
         }
         } */
        
        if(closestVertexdR < 0.4){
            double closestdR = vxVar->closestdR->at(vxMatch_index);
            if(isSignal && closestdR > 0.3 && numHits > 2000 && isBarrelEvent) nBarrel_Vx_events+= weight;
            if(isSignal && closestdR > 0.6 && numHits > 2500 && !isBarrelEvent) nEC_Vx_events+= weight;
            if(commonVar->CR_1j50){
                hist2JetSR->fill("ClosestJetdR_vs_ClosestTrackdR_1j50"+reg,vxVar->closestJetdR->at(vxMatch_index), vxVar->closestTrackdR->at(vxMatch_index), weight);
            }
            if(commonVar->CR_2j50){
                hist2JetSR->fill("ClosestJetdR_vs_ClosestTrackdR_2j50"+reg,vxVar->closestJetdR->at(vxMatch_index), vxVar->closestTrackdR->at(vxMatch_index),weight);
            }
            if(commonVar->CR_1j50_1j150){
                hist2JetSR->fill("ClosestJetdR_vs_ClosestTrackdR_1j50_1j150"+reg,vxVar->closestJetdR->at(vxMatch_index), vxVar->closestTrackdR->at(vxMatch_index),weight);
            }
            if(commonVar->CR_2j150){
                hist2JetSR->fill("ClosestJetdR_vs_ClosestTrackdR_2j150"+reg,vxVar->closestJetdR->at(vxMatch_index), vxVar->closestTrackdR->at(vxMatch_index),weight);
            }
            if(commonVar->CR_1j100_1j200){
                hist2JetSR->fill("ClosestJetdR_vs_ClosestTrackdR_1j100_1j200"+reg,vxVar->closestJetdR->at(vxMatch_index), vxVar->closestTrackdR->at(vxMatch_index),weight);
            }
            if(commonVar->CR_2j200){
                hist2JetSR->fill("ClosestJetdR_vs_ClosestTrackdR_2j200"+reg,vxVar->closestJetdR->at(vxMatch_index), vxVar->closestTrackdR->at(vxMatch_index),weight);
            }
            if(commonVar->CR_1j100_1j250){
                hist2JetSR->fill("ClosestJetdR_vs_ClosestTrackdR_1j100_1j250"+reg,vxVar->closestJetdR->at(vxMatch_index), vxVar->closestTrackdR->at(vxMatch_index),weight);
            }
            if(commonVar->CR_2j250){
                hist2JetSR->fill("ClosestJetdR_vs_ClosestTrackdR_2j250"+reg,vxVar->closestJetdR->at(vxMatch_index), vxVar->closestTrackdR->at(vxMatch_index),weight);
            }
            for(unsigned int iE=0;iE<exp.size();iE++){
                if(commonVar->CR_1j50){
                    hist2JetSR->fill(exp.at(iE)+"_ClosestdR_vs_nHits_1j50"+reg,closestdR,numHits,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxEta_1j50"+reg,vxVar->eta->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxPhi_1j50"+reg,vxVar->phi->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHT_1j50"+reg,commonVar->eventHT,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHTMiss_1j50"+reg,commonVar->eventHTMiss,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventMeff_1j50"+reg,commonVar->eventMeff,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxIso_1j50"+reg,closestdR,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxHits_1j50"+reg,numHits,weight);
                }
                if(commonVar->CR_2j50){
                    hist2JetSR->fill(exp.at(iE)+"_ClosestdR_vs_nHits_2j50"+reg,closestdR,numHits,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxEta_2j50"+reg,vxVar->eta->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxPhi_2j50"+reg,vxVar->phi->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHT_2j50"+reg,commonVar->eventHT,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHTMiss_2j50"+reg,commonVar->eventHTMiss,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventMeff_2j50"+reg,commonVar->eventMeff,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxIso_2j50"+reg,closestdR,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxHits_2j50"+reg,numHits,weight);
                }
                if(commonVar->CR_1j50_1j150){
                    hist2JetSR->fill(exp.at(iE)+"_ClosestdR_vs_nHits_1j50_1j150"+reg,closestdR,numHits,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxEta_1j50_1j150"+reg,vxVar->eta->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxPhi_1j50_1j150"+reg,vxVar->phi->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHT_1j50_1j150"+reg,commonVar->eventHT,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHTMiss_1j50_1j150"+reg,commonVar->eventHTMiss,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventMeff_1j50_1j150"+reg,commonVar->eventMeff,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxIso_1j50_1j150"+reg,closestdR,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxHits_1j50_1j150"+reg,numHits,weight);
                }
                if(commonVar->CR_2j150){
                    if(isSignal && closestdR > 0.3 && numHits > 2000 && isBarrelEvent) nBarrel_Vx2j150_events+= weight;
                    if(isSignal && closestdR > 0.6 && numHits > 2500 && !isBarrelEvent) nEC_Vx2j150_events+= weight;
                    hist2JetSR->fill(exp.at(iE)+"_ClosestdR_vs_nHits_2j150"+reg,closestdR,numHits,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxEta_2j150"+reg,vxVar->eta->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxPhi_2j150"+reg,vxVar->phi->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHT_2j150"+reg,commonVar->eventHT,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHTMiss_2j150"+reg,commonVar->eventHTMiss,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventMeff_2j150"+reg,commonVar->eventMeff,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxIso_2j150"+reg,closestdR,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxHits_2j150"+reg,numHits,weight);
                    if(closestdR > 0.3 && numHits > 2000 && isBarrelEvent){
                        std::cout << "Event passes all selection criteria, is in SR-A: " << commonVar->runNumber << ", " << commonVar->lumiBlock << ", " << commonVar->eventNumber << std::endl;
                    }
                }
                if(commonVar->CR_1j100_1j200){
                    hist2JetSR->fill(exp.at(iE)+"_ClosestdR_vs_nHits_1j100_1j200"+reg,closestdR,numHits,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxEta_1j100_1j200"+reg,vxVar->eta->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxPhi_1j100_1j200"+reg,vxVar->phi->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHT_1j100_1j200"+reg,commonVar->eventHT,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHTMiss_1j100_1j200"+reg,commonVar->eventHTMiss,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventMeff_1j100_1j200"+reg,commonVar->eventMeff,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxIso_1j100_1j200"+reg,closestdR,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxHits_1j100_1j200"+reg,numHits,weight);
                }
                if(commonVar->CR_2j200){
                    hist2JetSR->fill(exp.at(iE)+"_ClosestdR_vs_nHits_2j200"+reg,closestdR,numHits,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxEta_2j200"+reg,vxVar->eta->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxPhi_2j200"+reg,vxVar->phi->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHT_2j200"+reg,commonVar->eventHT,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHTMiss_2j200"+reg,commonVar->eventHTMiss,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventMeff_2j200"+reg,commonVar->eventMeff,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxIso_2j200"+reg,closestdR,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxHits_2j200"+reg,numHits,weight);
                }
                if(commonVar->CR_1j100_1j250){
                    hist2JetSR->fill(exp.at(iE)+"_ClosestdR_vs_nHits_1j100_1j250"+reg,closestdR,numHits,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxEta_1j100_1j250"+reg,vxVar->eta->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxPhi_1j100_1j250"+reg,vxVar->phi->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHT_1j100_1j250"+reg,commonVar->eventHT,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHTMiss_1j100_1j250"+reg,commonVar->eventHTMiss,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventMeff_1j100_1j250"+reg,commonVar->eventMeff,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxIso_1j100_1j250"+reg,closestdR,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxHits_1j100_1j250"+reg,numHits,weight);
                    hist2JetSR->fill(exp.at(iE)+"_JetETs_1j100_1j250"+reg,commonVar->Jet_pT->at(0), commonVar->Jet_pT->at(1),weight);
                }
                if(commonVar->CR_2j250){
                    hist2JetSR->fill(exp.at(iE)+"_ClosestdR_vs_nHits_2j250"+reg,closestdR,numHits,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxEta_2j250"+reg,vxVar->eta->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxPhi_2j250"+reg,vxVar->phi->at(vxMatch_index),weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHT_2j250"+reg,commonVar->eventHT,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventHTMiss_2j250"+reg,commonVar->eventHTMiss,weight);
                    hist2JetSR->fill(exp.at(iE)+"_eventMeff_2j250"+reg,commonVar->eventMeff,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxIso_2j250"+reg,closestdR,weight);
                    hist2JetSR->fill(exp.at(iE)+"_MSVxHits_2j250"+reg,numHits,weight);
                    hist2JetSR->fill(exp.at(iE)+"_JetETs_2j250"+reg,commonVar->Jet_pT->at(0), commonVar->Jet_pT->at(1),weight);
                }

            }
        }
        
        vxVar->clearAllVectors(false, isSignal);
        trigVar->clearAllVectors();
        commonVar->clearAllVectors(isSignal,true);
    }
    
    std::cout << "plotted hists!" << std::endl;
    outputFile->Write();
    
    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;
    
    if(isSignal){
        std::cout << "number of barrel events before 2j150: " << nBarrel_Vx_events << std::endl;
        std::cout << "number of barrel events after 2j150: " << nBarrel_Vx2j150_events << std::endl;
        std::cout << "number of endcap events before 2j150: " << nEC_Vx_events << std::endl;
        std::cout << "number of endcap events after 2j150: " << nEC_Vx2j150_events << std::endl;
    }
}
