void calculateMeffRescaling(){
    TFile *_file0 = new TFile("data/outputDataSearch_noTrigIso_unscaled.root");
    
    Double_t dbins[] = {0,10,40,60,90,120,150,180,210,300,400,500,5000};
    Float_t bins[] = {0,10,40,60,90,120,150,180,210,300,400,500,5000};
    Int_t  binnum = sizeof(bins)/sizeof(Float_t) - 1;
    TH1F* h_noiso = new TH1F("noiso","noiso", binnum, bins);
    TH1F* h_iso = new TH1F("iso","iso", binnum, bins);
    TH1F *h_noiso_1 = (TH1F*)_file0->Get("eventMeff_noiso");
    TH1F *h_iso_1 = (TH1F*)_file0->Get("eventMeff_iso");
    
    h_noiso->SetBinContent(1,h_noiso_1->GetBinContent(1));
    h_noiso->SetBinContent(2,h_noiso_1->GetBinContent(2)+h_noiso_1->GetBinContent(3)+h_noiso_1->GetBinContent(4));
    h_noiso->SetBinContent(3,h_noiso_1->GetBinContent(5)+h_noiso_1->GetBinContent(6));
    
    
    for(i=4;i<9;i++){h_noiso->SetBinContent(i,h_noiso_1->GetBinContent(3*i-5)+h_noiso_1->GetBinContent(3*i-4)+h_noiso_1->GetBinContent(3*i-3));h_noiso->SetBinError(i, sqrt(h_noiso_1->GetBinContent(3*i-5)+h_noiso_1->GetBinContent(3*i-4)+h_noiso_1->GetBinContent(3*i-3)));}
    
    double binval = 0;
    
    for(j=22; j < 31; j++){ binval += h_noiso_1->GetBinContent(j);}
    h_noiso->SetBinContent(9,binval);
    h_noiso->SetBinError(9, sqrt(binval));
    
    binval=0;
    for(j=31; j < 41; j++){ binval += h_noiso_1->GetBinContent(j);}
    h_noiso->SetBinContent(10,binval);
    h_noiso->SetBinError(10, sqrt(binval));
    binval=0;
    for(j=41; j < 51;j++){ binval += h_noiso_1->GetBinContent(j);}
    h_noiso->SetBinContent(11,binval);
    h_noiso->SetBinError(11, sqrt(binval));
    
    binval=0;
    for(j=51; j < 501;j++){ binval += h_noiso_1->GetBinContent(j);}
    h_noiso->SetBinContent(12,binval);
    h_noiso->SetBinError(12, sqrt(binval));
    
    h_iso->SetBinContent(1,h_iso_1->GetBinContent(1));
    h_iso->SetBinContent(2,h_iso_1->GetBinContent(2)+h_iso_1->GetBinContent(3)+h_iso_1->GetBinContent(4));
    h_iso->SetBinContent(3,h_iso_1->GetBinContent(5)+h_iso_1->GetBinContent(6));
    
    
    for(i=4;i<9;i++){h_iso->SetBinContent(i,h_iso_1->GetBinContent(3*i-5)+h_iso_1->GetBinContent(3*i-4)+h_iso_1->GetBinContent(3*i-3));h_iso->SetBinError(i, sqrt(h_iso_1->GetBinContent(3*i-5)+h_iso_1->GetBinContent(3*i-4)+h_iso_1->GetBinContent(3*i-3)));}
    
    binval = 0;
    
    for(j=22; j < 31; j++){ binval += h_iso_1->GetBinContent(j);}
    h_iso->SetBinContent(9,binval);
    h_iso->SetBinError(9, sqrt(binval));
    
    binval=0;
    for(j=31; j < 41; j++){ binval += h_iso_1->GetBinContent(j);}
    h_iso->SetBinContent(10,binval);
    h_iso->SetBinError(10, sqrt(binval));
    binval=0;
    for(j=41; j < 51;j++){ binval += h_iso_1->GetBinContent(j);}
    h_iso->SetBinContent(11,binval);
    h_iso->SetBinError(11, sqrt(binval));
    
    binval=0;
    for(j=51; j < 501;j++){ binval += h_iso_1->GetBinContent(j);}
    h_iso->SetBinContent(12,binval);
    h_iso->SetBinError(12, sqrt(binval));

    h_iso->Divide(h_noiso);
    h_iso->Scale(1./h_iso->Integral());
    h_iso->Draw();
    
    for(int i=1; i < h_iso->GetXaxis()->GetNbins()+1; i++){ std::cout << "else if (Meff < " << dbins[i] << ".) return " << h_iso->GetBinContent(i) << ";" << std::endl;}

}
