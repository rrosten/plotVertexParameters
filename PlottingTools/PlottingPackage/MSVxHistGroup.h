//
//  MSVxHistGroup.h
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#ifndef __plotVertexParameters__MSVxHistGroup__
#define __plotVertexParameters__MSVxHistGroup__


#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
#include "VertexHistograms.h"
#include "VertexVariables.h"

//#include "GVCHistograms.h"

struct MSVxHistGroup {
public:
    
    //all vertices
    VertexHistograms *hMSVxBar;
    VertexHistograms *hMSVxEC;
    VertexHistograms *hMSVxFull;
    
    //good vertices
    VertexHistograms *hMSVxBarGood;
    VertexHistograms *hMSVxECGood;
    VertexHistograms *hMSVxFullGood;
    
    //matched good vertices (valid for llps only):
    VertexHistograms *hMSVxBarGoodMatched;
    VertexHistograms *hMSVxECGoodMatched;
    VertexHistograms *hMSVxFullGoodMatched;
    
    //matched bad vertices (valid for llps only):
    VertexHistograms *hMSVxBarBadMatched;
    VertexHistograms *hMSVxECBadMatched;
    VertexHistograms *hMSVxFullBadMatched;
    
    //GVCHistograms *hGVCMSVxBar;
    //GVCHistograms *hGVCMSVxEC;
    
    void initialize(bool isSignal);
    //void fillVariables(VertexVariables* vxVar)
    void fillFromTree(TChain* tree, int isJZMC);

};

#endif /* defined(__plotVertexParameters__MSVxHistGroup__) */
