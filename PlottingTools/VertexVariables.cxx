//
//  VertexVariables.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#include <iostream>
#include "PlottingPackage/VertexVariables.h"
#include "TVector3.h"
#include "TMath.h"

double VertexVariables::DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double VertexVariables::DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double VertexVariables::wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;
    
}

void VertexVariables::setZeroVectors(bool isRerun){
    
    //MSVertex_passCutsJet=0;
    //MSVertex_passCutsTrk=0;
    region=0;
    syst=0;
    passReco=0;
    eta=0;
    phi=0;
    R=0;
    z=0;
    nTrks=0;
    nMDT=0;
    nRPC=0;
    nTGC=0;
    isGoodABCD = new std::vector<int>;
    if(isRerun){
        isGood = new std::vector<int>;
        indexLLP= new std::vector<int>;
        LLP_dR= new std::vector<double>;
        passesHitThresholds= new std::vector<int>;
        passesJetIso= new std::vector<int>;
        passesTrackIso= new std::vector<int>;
        closestdR= new std::vector<double>;
        closestJetdR= new std::vector<double>;
        closestTrackdR= new std::vector<double>;
        indexTrigRoI= new std::vector<int>;
    } else {
        isGood=0;
        indexLLP=0;
        LLP_dR=0;
        passesHitThresholds=0;
        passesJetIso=0;
        passesTrackIso=0;
        closestdR=0;
        closestJetdR=0;
        closestTrackdR=0;
        indexTrigRoI=0;
    }
    tracklet_eta = 0;
    tracklet_phi = 0;
    mseg_eta = 0;
    mseg_phi = 0;
    
    
}
void VertexVariables::setBranchAdresses(TChain *chain, bool isRerun, bool useTracklets){
    if(isRerun){
        chain->SetBranchAddress("Vertex_eta"           ,&eta);
        chain->SetBranchAddress("Vertex_phi"           ,&phi);
        chain->SetBranchAddress("Vertex_R"             ,&R);
        chain->SetBranchAddress("Vertex_z"             ,&z);
        chain->SetBranchAddress("Vertex_nTracklets"    ,&nTrks);
        chain->SetBranchAddress("Vertex_nMDT"          ,&nMDT);
        chain->SetBranchAddress("Vertex_nRPC"          ,&nRPC);
        chain->SetBranchAddress("Vertex_nTGC"          ,&nTGC);
        chain->SetBranchAddress("Vertex_pass"        ,&passReco);
        chain->SetBranchAddress("Vertex_syst", &syst);
        chain->SetBranchAddress("Vertex_region", &region);
        chain->SetBranchAddress("nTracklets", &nTracklets);
        
        if(useTracklets){
            chain->SetBranchAddress("Tracklet_eta"           ,&tracklet_eta);
            chain->SetBranchAddress("Tracklet_phi"           ,&tracklet_phi);
            //    chain->SetBranchAddress("MSeg_phi"           ,&mseg_phi);
            //    chain->SetBranchAddress("MSeg_eta"           ,&mseg_eta);
        }
    } else{
        chain->SetBranchAddress("MSVertex_eta"           ,&eta);
        chain->SetBranchAddress("MSVertex_phi"           ,&phi);
        chain->SetBranchAddress("MSVertex_R"             ,&R);
        chain->SetBranchAddress("MSVertex_z"             ,&z);
        chain->SetBranchAddress("MSVertex_nTrks"         ,&nTrks);
        chain->SetBranchAddress("MSVertex_nMDT"          ,&nMDT);
        chain->SetBranchAddress("MSVertex_nRPC"          ,&nRPC);
        chain->SetBranchAddress("MSVertex_nTGC"          ,&nTGC);
        chain->SetBranchAddress("MSVertex_indexLLP"      ,&indexLLP);
        chain->SetBranchAddress("MSVertex_LLP_dR"        ,&LLP_dR);
        chain->SetBranchAddress("MSVertex_passesHitThresholds" ,&passesHitThresholds);
        chain->SetBranchAddress("MSVertex_passesJetIso" ,&passesJetIso);
        chain->SetBranchAddress("MSVertex_passesTrackIso" ,&passesTrackIso);
        chain->SetBranchAddress("MSVertex_isGood"        ,&isGood);
        chain->SetBranchAddress("MSVertex_closestdR", &closestdR);
        chain->SetBranchAddress("MSVertex_closestJetdR", &closestJetdR);
        chain->SetBranchAddress("MSVertex_closestTrackdR", &closestTrackdR);
        chain->SetBranchAddress("MSVertex_indexTrigRoI", &indexTrigRoI);
        if(useTracklets){
            chain->SetBranchAddress("MSTracklet_eta"           ,&tracklet_eta);
            //    chain->SetBranchAddress("MSeg_eta"           ,&mseg_eta);
            //    chain->SetBranchAddress("MSeg_phi"           ,&mseg_phi);
            chain->SetBranchAddress("MSTracklet_phi"           ,&tracklet_phi);
        }
    }
}
int VertexVariables::countScaledTracklets(){
    double prob = 1;
    int ntrkls = 0;
    for(size_t it = 0; it < tracklet_eta->size(); it++){
        prob = rnd.Rndm();
        if(TMath::Abs(tracklet_eta->at(it)) < 1.0 && prob < m_bvx_sf) ntrkls++;
        else if(TMath::Abs(tracklet_eta->at(it)) > 1.0 && prob < m_evx_sf) ntrkls++;
    }
    return ntrkls;
}
void VertexVariables::FindGoodVertices(std::vector<double> &goodEta, std::vector<double> &goodPhi, std::vector<int> &goodRegion, std::vector<int> &index, int isSignal){
    
    for(size_t ll = 0; ll < eta->size(); ll++){
        if( isGood->at(ll) != 1 ) continue;
        if(isSignal){
            if(syst->at(ll) != 0) continue;
        }
        if(nMDT->at(ll) == 0){
            std::cout << "this shouldn't happen, this vertex isn't good!" << std::endl;
        }
        if( (TMath::Abs(eta->at(ll)) > 0.8  && TMath::Abs(eta->at(ll)) < 1.3) || TMath::Abs(eta->at(ll)) > 2.5){ continue;}
        goodEta.push_back(eta->at(ll));
        goodPhi.push_back(phi->at(ll));
        index.push_back(ll);
        if(TMath::Abs(eta->at(ll)) < 1.0) goodRegion.push_back(1);
        else goodRegion.push_back(2);
    }
}
void VertexVariables::testHitThresholds(){
    
    passesHitThresholds->assign(eta->size(),0);
    for(unsigned int i_vx=0; i_vx < eta->size(); i_vx++){
        int pass = 1;
        if( (nMDT->at(i_vx) < m_GVC_BE_nMDTMin)  || (nMDT->at(i_vx) > m_GVC_BE_nMDTMax) ) pass = 0;
        if( (fabs(eta->at(i_vx) ) <= 1.0) && (nRPC->at(i_vx) < m_GVC_BE_nTrigHits) ) pass = 0;
        if( (fabs(eta->at(i_vx) ) > 1.0) && (nTGC->at(i_vx) < m_GVC_BE_nTrigHits) ) pass = 0;
        passesHitThresholds->at(i_vx) = pass;
    }
}
void VertexVariables::performJetIsolation(std::vector<double> *Jet_ET,std::vector<double> *Jet_eta,std::vector<double> *Jet_phi,std::vector<double> *Jet_logRatio,std::vector<int> *Jet_passJVT)
{
    passesJetIso->assign(eta->size(),0);
    closestJetdR->assign(eta->size(), -99);
    for (unsigned int i_vx = 0; i_vx < eta->size(); i_vx++) {
        double mindR2 = 99;
        int pass = 1; int njets_pass = 0;
        for( unsigned int j_jet = 0; j_jet < Jet_ET->size(); j_jet++ ){
            
            if(Jet_ET->at(j_jet) < m_GVC_BE_jetIso_Et) continue; //only consider jets above Et threshold
            
            if(Jet_logRatio->at(j_jet) >= m_GVC_BE_jetIso_logRatio) continue; //don't isolate from jets with low EMF/high logRatio
            
            if(!Jet_passJVT->at(j_jet)) continue; //don't isolate from pileup jets
            
            double deltaR2 = DeltaR2(phi->at(i_vx),Jet_phi->at(j_jet),eta->at(i_vx),Jet_eta->at(j_jet));
            if(deltaR2 < mindR2) mindR2 = deltaR2;
            if(deltaR2 < m_GVC_jetIso_deltaR2[region->at(i_vx)] /*cut is different in barrel/endcap*/){
                njets_pass++;
                pass = 0; //fail if jet is within GVC determined deltaR of vertex (deltaR2 = deltaR^2)
            }
        }
        
        passesJetIso->at(i_vx) = pass;
        closestJetdR->at(i_vx) = sqrt(mindR2); // set msvx variables to be written out
    }
}
void VertexVariables::checkJetIsolation(std::vector<double> *Jet_ET,std::vector<double> *Jet_eta,std::vector<double> *Jet_phi,std::vector<double> *Jet_logRatio,std::vector<int> *Jet_passJVT)
{
    
    for (unsigned int i_vx = 0; i_vx < eta->size(); i_vx++) {
        double cJetdR = -99;
        double mindR2 = 99;
        for( unsigned int j_jet = 0; j_jet < Jet_ET->size(); j_jet++ ){
            
            if(Jet_ET->at(j_jet) < m_GVC_BE_jetIso_Et) continue; //only consider jets above Et threshold
            
            if(Jet_logRatio->at(j_jet) >= m_GVC_BE_jetIso_logRatio) continue; //don't isolate from jets with low EMF/high logRatio
            
            if(!Jet_passJVT->at(j_jet)) continue; //don't isolate from pileup jets
            
            double deltaR2 = DeltaR2(phi->at(i_vx),Jet_phi->at(j_jet),eta->at(i_vx),Jet_eta->at(j_jet));
            if(deltaR2 < mindR2) mindR2 = deltaR2;
        }
        cJetdR = sqrt(mindR2); // set msvx variable
        if(fabs(cJetdR - closestJetdR->at(i_vx)) > 0 ){
            std::cout << "PROBLEM IN JET ISOLATION!" << std::endl;
            std::cout << "calculated: " << cJetdR << ", ntuple has " << closestJetdR->at(i_vx) << std::endl;
            for( unsigned int j_jet = 0; j_jet < Jet_ET->size(); j_jet++ ){
                
                if(Jet_ET->at(j_jet) < m_GVC_BE_jetIso_Et) continue; //only consider jets above Et threshold
                
                if(Jet_logRatio->at(j_jet) >= m_GVC_BE_jetIso_logRatio) continue; //don't isolate from jets with low EMF/high logRatio
                
                if(!Jet_passJVT->at(j_jet)) continue; //don't isolate from pileup jets
                
                double deltaR2 = DeltaR2(phi->at(i_vx),Jet_phi->at(j_jet),eta->at(i_vx),Jet_eta->at(j_jet));
                std::cout << "jet ET: " << Jet_ET->at(j_jet)  << ", logR: " << Jet_logRatio->at(j_jet) << ", jvt: " << Jet_passJVT->at(j_jet) << ", eta: " << Jet_eta->at(j_jet) << ", phi: " <<Jet_phi->at(j_jet) << ", deltaR: " << sqrt(deltaR2) << std::endl;
            }
        }
    }
}
void VertexVariables::performTrackIsolation(std::vector<double> *Track_pT,std::vector<double> *Track_eta,std::vector<double> *Track_phi)
{
    passesTrackIso->assign(eta->size(),0);
    closestTrackdR->assign(eta->size(), -99);
    
    for (unsigned int i_vx = 0; i_vx < eta->size(); i_vx++) {
        int ntracks_pass = 0;
        int pass_sum = 1;
        int pass_highpt = 1;
        TVector3 sumTrackPt; sumTrackPt.SetXYZ(0.,0.,0.);
        
        double mindR2 = 99;
        for( unsigned int i_t=0; i_t < Track_eta->size(); i_t++ ){
            TVector3 tmpTrack; tmpTrack.SetPtEtaPhi(Track_pT->at(i_t),Track_eta->at(i_t),Track_phi->at(i_t));
            double deltaR2 = DeltaR2(phi->at(i_vx),Track_phi->at(i_t),eta->at(i_vx),Track_eta->at(i_t));
            
            if(deltaR2 < mindR2 &&  Track_pT->at(i_t) > m_GVC_BE_trackIso_pT ) mindR2 = deltaR2;
            
            if(deltaR2 > m_GVC_trackIso_deltaR2[region->at(i_vx)]) continue; //only consider tracks in GVC dR of vertex
            if(Track_pT->at(i_t) > m_GVC_BE_trackIso_pT){ //isolate from high-pT tracks
                pass_highpt = 0;
                ntracks_pass++;
            }
            if(deltaR2 < m_GVC_BE_sumTracks_deltaR2){ //isolate from a high density of low-pT tracks (sum-pT > threshold in small cone)
                sumTrackPt += tmpTrack;
            }
        }
        //set msvx variables to be written out
        if(sumTrackPt.Perp() >= m_GVC_BE_sumTracks_pT) pass_sum = 0;
        if(pass_sum && pass_highpt) passesTrackIso->at(i_vx) = 1;
        else passesTrackIso->at(i_vx) = 0;
        //sumPtTracksInCone->at(i_vx) = sumTrackPt.Perp();
        closestTrackdR->at(i_vx) = sqrt(mindR2);
    }
}
void VertexVariables::testIsGood(bool isRerun)
{
    //set a global flag "isGood" for a vertex passing all GVC criteria. also keep them separate for determining cutflow later.
    isGood->assign(eta->size(),0);
    isGoodABCD->assign(eta->size(), 0);
    closestdR->assign(eta->size(),-99);
    
    for (unsigned int i_vx = 0; i_vx < eta->size(); i_vx++) {
        if(isRerun){
            if(passReco->at(i_vx)!= 1){
                isGood->at(i_vx) = 0;
                isGoodABCD->at(i_vx) = 0;
                continue;
            }
        }
        if(passesTrackIso->at(i_vx) && passesJetIso->at(i_vx) && passesHitThresholds->at(i_vx)
           && (fabs(eta->at(i_vx)) < 0.8 || (fabs(eta->at(i_vx)) > 1.3 && fabs(eta->at(i_vx)) < 2.5) ) ){
            isGood->at(i_vx) = 1;
        } else {
            isGood->at(i_vx) = 0;
        }
        //isoVar->at(i_vx) = isolationVariable(closestTrackdR->at(i_vx), closestJetdR->at(i_vx), m_GVC_IsoCone_deltaR[region->at(i_vx)] );
        closestdR->at(i_vx) = std::min( closestTrackdR->at(i_vx), closestJetdR->at(i_vx) );
        if(fabs(eta->at(i_vx)) < 0.8){
            if(closestdR->at(i_vx) > 0.3 && ((nMDT->at(i_vx) + nRPC->at(i_vx)) > 2000) ){
                isGoodABCD->at(i_vx) = 1;
            }
        } else if(fabs(eta->at(i_vx)) > 1.3 && fabs(eta->at(i_vx)) < 2.5){
            if(closestdR->at(i_vx) > 0.6 && ((nMDT->at(i_vx) + nTGC->at(i_vx)) > 2500) ){
                isGoodABCD->at(i_vx) = 1;
            }
        }
        
    }
}
double VertexVariables::isolationVariable(double dR1, double dR2, double cut){
    if(dR1 < cut || dR2 < cut){ return -1.0*sqrt(fabs((dR1 - cut)*(dR2 - cut)));}
    else return sqrt(fabs((dR1 - cut)*(dR2 - cut)));
}
void VertexVariables::testTruthMatch(std::vector<double> *llp_eta,std::vector<double> *llp_phi) {
    
    //if (!TruthUtils::isLLP(theLLP)) { throw std::runtime_error("execute()  Tried to match an ms vertex to a non-llp. Exiting."); }
    indexLLP->assign(eta->size(), -1);
    LLP_dR->assign(eta->size(), -99);
    for(unsigned int i_llp = 0; i_llp < llp_eta->size(); i_llp++){
        TVector3 v_vx;
        TVector3 v_truth;
        v_truth.SetPtEtaPhi(1.,llp_eta->at(i_llp), llp_phi->at(i_llp));
        
        for (unsigned int i_vx = 0; i_vx < eta->size(); i_vx++) {
            double smallest_dR = LLP_dR->at(i_vx);
            
            v_vx.SetPtEtaPhi(1., eta->at(i_vx), phi->at(i_vx));
            double dR_pair = v_vx.DeltaR(v_truth);
            
            if (dR_pair < 0.4) {
                indexLLP->at(i_vx) = i_llp;
            }
            if (smallest_dR < 0 || dR_pair < smallest_dR){
                smallest_dR = dR_pair;
                LLP_dR->at(i_vx) = dR_pair;
            }
        }
    }
}
void VertexVariables::testRoIMatch(const std::vector<double> *muRoI_eta, const std::vector<double> *muRoI_phi){
    //std::cout << "test vx-roi match" << std::endl;
    indexTrigRoI->assign(eta->size(),-1);
    for (unsigned int i_vx = 0; i_vx < eta->size(); i_vx++) {
        if(muRoI_eta->size() == 0){
            //std::cout << "no rois..." << std::endl;
            indexTrigRoI->at(i_vx) = -1; return;
        }
        double mindR = 99;
        for(unsigned int i_t = 0; i_t <  muRoI_eta->size(); i_t++ ){
            double deltaR2 = DeltaR2(phi->at(i_vx),muRoI_phi->at(i_t),eta->at(i_vx),muRoI_eta->at(i_t));
            //std::cout << "vertex -- roi #" << i_t << " dR^2 = " << deltaR2 << std::endl;
            if( deltaR2 < 0.16){
                if(deltaR2 < mindR){
                    mindR = deltaR2;
                    //save the index of the closest matching trigger RoI
                    indexTrigRoI->at(i_vx) = i_t;
                }
            }
        }
    }
}
void VertexVariables::clearAllVectors(){
    eta->clear();
    phi->clear();
    R->clear();
    z->clear();
    nTrks->clear();
    nMDT->clear();
    nRPC->clear();
    nTGC->clear();
    isGood->clear();
    passesHitThresholds->clear();
    passesJetIso->clear();
    passesTrackIso->clear();
    LLP_dR->clear();
    indexLLP->clear();
    tracklet_phi->clear();
    tracklet_eta->clear();
    indexTrigRoI->clear();
    closestdR->clear();
    closestJetdR->clear();
    closestTrackdR->clear();
    //   mseg_phi->clear();
    //   mseg_eta->clear();
    
}
void VertexVariables::clearAllVectors(bool useTracklets, bool isRerun){
    if(isRerun){
        passReco->clear();
        syst->clear();
        region->clear();
    }
    eta->clear();
    phi->clear();
    R->clear();
    z->clear();
    nTrks->clear();
    nMDT->clear();
    nRPC->clear();
    nTGC->clear();
    isGood->clear();
    isGoodABCD->clear();
    passesHitThresholds->clear();
    passesJetIso->clear();
    passesTrackIso->clear();
    LLP_dR->clear();
    indexLLP->clear();
    indexTrigRoI->clear();
    closestdR->clear();
    closestJetdR->clear();
    closestTrackdR->clear();
    
    if(useTracklets){
        tracklet_eta->clear();
        tracklet_phi->clear();
    }
    //   mseg_phi->clear();
    //   mseg_eta->clear();
    
}
