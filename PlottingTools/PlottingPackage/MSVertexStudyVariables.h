//
//  MSVertexStudyVariables.h
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#ifndef plotVertexParameters_MSVertexStudyVariables_h
#define plotVertexParameters_MSVertexStudyVariables_h
#include <vector>
#include "TChain.h"

struct MSVertexStudyVariables {
    
public:
    
	  int eventNumber;
	  int nMDTs;
	  int nRPCs;
	  int nTGCs;
	  std::vector<std::string> *mdtChamber = new std::vector<std::string>;
	  std::vector<float> *mdtEta = new std::vector<float>;
	  std::vector<float> *mdtPhi = new std::vector<float>;
	  std::vector<float> *mdtR = new std::vector<float>;
	  std::vector<float> *mdtz = new std::vector<float>;
	  std::vector<float> *mdtAdc = new std::vector<float>;
	  std::vector<int> *mdtStatus = new std::vector<int>;
	  std::vector<float> *mdtRadius = new std::vector<float>;
	  std::vector<float> *mdtRadErr = new std::vector<float>;
	  std::vector<float> *TrackletSeeds_slope = new std::vector<float>;
	  std::vector<float> *TrackletSeeds_int = new std::vector<float>;
	  std::vector<float> *TrackletSeeds_mdt1_z = new std::vector<float>;
	  std::vector<float> *TrackletSeeds_mdt1_R = new std::vector<float>;
	  std::vector<float> *TrackletSeeds_mdt2_z = new std::vector<float>;
	  std::vector<float> *TrackletSeeds_mdt2_R = new std::vector<float>;
	  std::vector<float> *TrackletSeeds_mdt3_z = new std::vector<float>;
	  std::vector<float> *TrackletSeeds_mdt3_R = new std::vector<float>;
	  int n3HitTrackletSeeds;
	  int nTrackletSegs;
	  int nTrackSegments;
	  std::vector<float> *TrackSegmentDeltaB = new std::vector<float>;
	  std::vector<float> *TrackSegmentDeltaAlpha = new std::vector<float>;
	  std::vector<float> *Tracklet_eta = new std::vector<float>;
	  std::vector<float> *Tracklet_phi = new std::vector<float>;
	  std::vector<float> *Tracklet_R = new std::vector<float>;
	  std::vector<float> *Tracklet_z = new std::vector<float>;
	  std::vector<float> *Tracklet_ml1_d12 = new std::vector<float>;
	  std::vector<float> *Tracklet_ml1_d13 = new std::vector<float>;
	  std::vector<float> *Tracklet_ml2_d12 = new std::vector<float>;
	  std::vector<float> *Tracklet_ml2_d13 = new std::vector<float>;
	  std::vector<int> *Tracklet_ml1_nHitsOnTrack = new std::vector<int>;
	  std::vector<int> *Tracklet_ml2_nHitsOnTrack = new std::vector<int>;
	  std::vector<int> *Tracklet_nHitsOnTrack = new std::vector<int>;
	  std::vector<std::vector<float> > *Tracklet_Hits_ml1_R = new std::vector<std::vector<float> >;
	  std::vector<std::vector<float> > *Tracklet_Hits_ml1_z = new std::vector<std::vector<float> >;
	  std::vector<std::vector<float> > *Tracklet_Hits_ml1_eta = new std::vector<std::vector<float> >;
	  std::vector<std::vector<float> > *Tracklet_Hits_ml1_phi = new std::vector<std::vector<float> >;
	  std::vector<std::vector<float> > *Tracklet_Hits_ml2_R = new std::vector<std::vector<float> >;
	  std::vector<std::vector<float> > *Tracklet_Hits_ml2_z = new std::vector<std::vector<float> >;
	  std::vector<std::vector<float> > *Tracklet_Hits_ml2_eta = new std::vector<std::vector<float> >;
	  std::vector<std::vector<float> > *Tracklet_Hits_ml2_phi = new std::vector<std::vector<float> >;
	  std::vector<int> *Tracklet_chamber = new std::vector<int>;
	  std::vector<int> *Tracklet_inBarrel = new std::vector<int>;
	  std::vector<float> *TrackSegment_eta = new std::vector<float>;
	  std::vector<float> *TrackSegment_phi = new std::vector<float>;
	  std::vector<float> *TrackSegment_R = new std::vector<float>;
	  std::vector<float> *TrackSegment_z = new std::vector<float>;
	  std::vector<int> *LLP_inBarrel = new std::vector<int>;
	  std::vector<int> *LLP_inEndcap = new std::vector<int>;
	  std::vector<float> *LLP_R = new std::vector<float>;
	  std::vector<float> *LLP_z = new std::vector<float>;
	  std::vector<float> *LLP_eta = new std::vector<float>;
	  std::vector<float> *LLP_phi = new std::vector<float>;
	  std::vector<float> *LLP_gamma = new std::vector<float>;
	  std::vector<float> *LLP_deltar = new std::vector<float>;
	  std::vector<float> *LLP_nTrackSegs = new std::vector<float>;
	  std::vector<float> *LLP_nTracklets = new std::vector<float>;
	  std::vector<float> *LLP_nMDT = new std::vector<float>;
	  std::vector<float> *LLP_nRPC = new std::vector<float>;
	  std::vector<float> *LLP_nTGC = new std::vector<float>;
	  std::vector<float> *Cluster_eta = new std::vector<float>;
	  std::vector<float> *Cluster_phi = new std::vector<float>;
	  std::vector<float> *Cluster_region = new std::vector<float>;
	  std::vector<int> *Vertex_indexLLP = new std::vector<int>;
	  std::vector<float> *Vertex_LLP_dR = new std::vector<float>;
	  std::vector<float> *Vertex_Eta = new std::vector<float>;
	  std::vector<float> *Vertex_Phi = new std::vector<float>;
	  std::vector<float> *Vertex_z = new std::vector<float>;
	  std::vector<float> *Vertex_R = new std::vector<float>;
	  std::vector<float> *Vertex_chi2prob = new std::vector<float>;
	  std::vector<int> *Vertex_nMDT = new std::vector<int>;
	  std::vector<int> *Vertex_nRPC = new std::vector<int>;
	  std::vector<int> *Vertex_nTGC = new std::vector<int>;
	  std::vector<int> *Vertex_nHighOccChambers = new std::vector<int>;
	  std::vector<int> *Vertex_nTracklets = new std::vector<int>;
	  std::vector<int> *Vertex_pass = new std::vector<int>;
	  std::vector<int> *Vertex_region = new std::vector<int>;
	  int nTracklets;
	  int nClusters;	  
	  int nVertices;
	  int progressBar;
	  int progressBar_BVx;
	  int progressBar_EVx;
  
    void setZeroVectors();
    void setBranchAdresses(TChain *chain);
    void clearAllVectors();
};


#endif
