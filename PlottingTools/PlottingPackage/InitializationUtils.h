//
//  InitializationUtils.h
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#ifndef plotVertexParameters_InitializationUtils_h
#define plotVertexParameters_InitializationUtils_h

#include "TChain.h"
#include "TH1.h"

namespace plotVertexParameters{
    void setPrettyCanvasStyle();
    void setJetSliceWeights(double (&jetSliceWeights)[13]);
    void BinLogX(TH1F *h);
    std::map<ULong64_t,int> CreateMap(TChain * &ch);
};

#endif
