//
//  InitializationUtils.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#include "PlottingPackage/InitializationUtils.h"
#include "TColor.h"
#include "TStyle.h"
#include "TROOT.h"
#include <iostream>
#include "AtlasUtils.h"
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "TAxis.h"
#include "TMath.h"

using namespace plotVertexParameters;

    void plotVertexParameters::setPrettyCanvasStyle(){
        
    	  //gROOT->LoadMacro("AtlasUtils.C");
    	  //gROOT->LoadMacro("AtlasLabels.C");
    	  //gROOT->LoadMacro("AtlasStyle.C");

    	  //std::cout << "loaded macros"<<std::endl;
    	  
    	SetAtlasStyle();
    	
    	  std::cout << "setAtlasStyle"<<std::endl;
    	  
        gStyle->SetPalette(1);
        
        const Int_t NRGBs = 5;
        const Int_t NCont = 255;
        
        Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
        Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
        Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
        Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
        TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
        gStyle->SetNumberContours(NCont);
        return;
        
/*        Double_t stops[9] = { 0.0000, 0.1250, 0.2500, 0.3750, 0.5000, 0.6250, 0.7500, 0.8750, 1.0000};
        Double_t red[9]   = { 0./255., 32./255., 64./255., 96./255., 128./255., 160./255., 192./255., 224./255., 255./255.};
        Double_t green[9] = { 0./255., 32./255., 64./255., 96./255., 128./255., 160./255., 192./255., 224./255., 255./255.};
        Double_t blue[9]  = { 0./255., 32./255., 64./255., 96./255., 128./255., 160./255., 192./255., 224./255., 255./255.};
        TColor::CreateGradientColorTable(9, stops, red, green, blue, 255, 0.3);
                Double_t red2[9]   = { 0.2082, 0.0592, 0.0780, 0.0232, 0.1802, 0.5301, 0.8186, 0.9956, 0.9764};
                Double_t green2[9] = { 0.1664, 0.3599, 0.5041, 0.6419, 0.7178, 0.7492, 0.7328, 0.7862, 0.9832};
                Double_t blue2[9]  = { 0.5293, 0.8684, 0.8385, 0.7914, 0.6425, 0.4662, 0.3499, 0.1968, 0.0539};
  */  
    }
   
    void plotVertexParameters::setJetSliceWeights(double (&jetSliceWeights)[13]){
        double lumi_in_nb = 18857.4*1000. + 3193.68*1000.; //2016 MD2 + 2015 full repro. Needs to be the same as the prw!

        jetSliceWeights[0] = 1.0240     * 78420000.      * lumi_in_nb /100000.; //( Filter eff. ) * ( Cross-section [nb] ) * ( desired lumi [nb^-1] ) / ( Num Events )
        jetSliceWeights[1] = 0.00067198 * 78420000.      * lumi_in_nb /100000.; //JZ1W
        jetSliceWeights[2] = 0.00033264 * 2433400.       * lumi_in_nb /1982189.;
        jetSliceWeights[3] = 0.00031953 * 26454.         * lumi_in_nb /7874500.;
        jetSliceWeights[4] = 0.00053009 * 254.64         * lumi_in_nb /7979800.;
        jetSliceWeights[5] = 0.00092325 * 4.5536         * lumi_in_nb /7977600.;
        jetSliceWeights[6] = 0.00094016 * 0.25752        * lumi_in_nb /1893400.;
        jetSliceWeights[7] = 0.00039282 * 0.016214       * lumi_in_nb /1770200.; //JZ7W
        jetSliceWeights[8] = 0.010162   * 0.00062505     * lumi_in_nb /1743200.; //JZ8W
        jetSliceWeights[9] = 0.012054   * 0.00001964     * lumi_in_nb /1813200.; //JZ9W
        jetSliceWeights[10] = 0.0058935 * 0.0000011961   * lumi_in_nb /1996000.; //JZ10W
        jetSliceWeights[11] = 0.0027015 * 0.00000004226  * lumi_in_nb /1993200.; //JZ11W
        jetSliceWeights[12] = 0.00042502* 0.000000001037 * lumi_in_nb /1974600.; //JZ12W
        
        return;
    }
    void plotVertexParameters::BinLogX(TH1F *h)
    {

      TAxis *axis = h->GetXaxis();
      int bins = axis->GetNbins();

      Axis_t from = axis->GetXmin();
      Axis_t to = axis->GetXmax();
      Axis_t width = (to - from) / bins;
      Axis_t *new_bins = new Axis_t[bins + 1];

      for (int i = 0; i <= bins; i++) {
        new_bins[i] = TMath::Power(10, from + i * width);
      }
      axis->Set(bins, new_bins);
      delete new_bins;
      return;
    }
    
    std::map<ULong64_t,int> plotVertexParameters::CreateMap(TChain * &ch){
        std::map <ULong64_t, int> map;
        ch->SetBranchStatus("*",0);
        ULong64_t eventNumber = 0;
        ch->SetBranchAddress("EventNumber",&eventNumber);
        for(unsigned int i_evt=0; i_evt < ch->GetEntries(); i_evt++){
            ch->GetEntry(i_evt);
            map[eventNumber] = i_evt;
        }
        
        return map;
    }