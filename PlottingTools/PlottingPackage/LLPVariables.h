//
//  LLPVariables.h
//  plotVertexParameters
//
//  Created by Heather Russell on 12/10/15.
//
//

#ifndef __plotVertexParameters__LLPVariables__
#define __plotVertexParameters__LLPVariables__

#include <vector>
#include "TChain.h"

struct LLPVariables {
    
public:
    
    std::vector<double> *eta;
    std::vector<double> *phi;
    std::vector<double> *Lz;
    std::vector<double> *Lxy;
    std::vector<double> *Lxyz;
    std::vector<double> *beta;
    std::vector<double> *gamma;
    std::vector<double> *pT;
    std::vector<double> *pz;
    std::vector<double> *E;
    std::vector<int>	*nMSeg;
    std::vector<double> *bg;
    std::vector<double> *theta;
    void setZeroVectors();
    void SetAllVariables();
    void setBranchAddresses(TChain *chain);
    void setToyBranchAddresses(TChain *chain);
    void clearAllVectors();
    void clearAllToyVectors();
    void set_pz();
    void set_Lxyz();
};


#endif /* defined(__plotVertexParameters__LLPVariables__) */
