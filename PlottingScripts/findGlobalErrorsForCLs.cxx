#include "TFile.h"
#include "TH1D.h"
#include "TF1.h"
#include "TPad.h"
#include "TMath.h"
#include <cmath>
#include "TCanvas.h"
#include "../PlottingTools/PlottingPackage/SampleDetails.h"
double evaluate_novosibirsk( const double x, const double peak, const double width, const double tail )
{
    if (TMath::Abs(tail) < 1.e-7) {
        return TMath::Exp( -0.5 * TMath::Power( ( (x - peak) / width ), 2 ));
    }
    
    Double_t arg = 1.0 - ( x - peak ) * tail / width;
    
    if (arg < 1.e-9) {
        return 0.0;
    }
    Double_t log = TMath::Log(arg);
    
    const Double_t xi = 2.3548200450309494; // 2 Sqrt( Ln(4) )
    
    Double_t width_zero = ( 2.0 / xi ) * TMath::ASinH( tail * xi * 0.5 );
    Double_t width_zero2 = width_zero * width_zero;
    Double_t exponent = ( -0.5 / (width_zero2) * log * log ) - ( width_zero2 * 0.5 );
    
    return TMath::Exp(exponent) ;
}
double novosibirsk( double* x, double* par ){
    return par[0]*evaluate_novosibirsk( log10( x[0] ), par[1], par[2], par[3] );
}

void findGlobalErrorsForCLs(){
    TCanvas *c1 = new TCanvas("c1","c1",800,600);
    std::vector<TString> benchmark = {"mg250", "mg500", "mg800", "mg1200", "mg1500", "mg2000"};
    //std::vector<TString> benchmark = {"mH125mS5lt5","mH125mS8lt5","mH125mS15lt5","mH125mS25lt5","mH125mS40lt5","mH125mS55lt5"}
    //std::vector<TString> benchmark = {"mH100mS8lt5", "mH100mS25lt5", "mH200mS8lt5", "mH200mS25lt5","mH200mS50lt5", "mH400mS50lt5", "mH400mS100lt5", "mH600mS50lt5", "mH600mS150lt5", "mH1000mS50lt5", "mH1000mS150lt5", "mH1000mS400lt5"};
    std::vector<double> ctau;
    for(unsigned int i=0; i<benchmark.size(); i++){
        SampleDetails::setGlobalVariables(benchmark.at(i));
        ctau.push_back(SampleDetails::sim_ctau);
    }
    std::vector<double> uperr; std::vector<double> downerr;
    std::vector<double> uperrA; std::vector<double> downerrA;
    std::vector<double> uperrC; std::vector<double> downerrC;
    TFile* tfile[12];
    TH1D* thist[12];
    TH1D* thistA[12];
    TH1D* thist_up[12];
    TH1D* thistA_up[12];
    TH1D* thist_down[12];
    TH1D* thistA_down[12];
    TF1* fn[12];
    TF1* fn_up[12];
    TF1* fn_down[12];
    TF1* fnA[12];
    TF1* fnA_up[12];
    TF1* fnA_down[12];
    for(int i=0; i<6; i++){
        tfile[i] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/outputExtrapolation_"+benchmark.at(i)+"_dv17.root");
        thist[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx");
        thist_up[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_maxTotal");
        thist_down[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_minTotal");
        thistA[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_2jx50");
        thistA_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_2jx50_maxTotal");
        thistA_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_2jx50_minTotal");

	double maxVal = thist[i]->GetMaximum();
	double maxValA = thistA[i]->GetMaximum();
        fn[i] = new TF1("fn",novosibirsk,0.05,300,4);
        fn[i]->SetParameter(0,maxVal);
        fn[i]->SetParameter(1,-0.3); //fn->SetParLimits(1,-1,2);
        fn[i]->SetParameter(2,0.4); //fn->SetParLimits(2,0,2);
        fn[i]->SetParameter(3,-0.2); //fn->SetParLimits(3,0,20.0);
         

        fn_up[i] = new TF1("fn_up",novosibirsk,0.05,300,4);
        fn_up[i]->SetParameter(0,maxVal);
        fn_up[i]->SetParameter(1,-0.3); //fn->SetParLimits(1,-1,2);
        fn_up[i]->SetParameter(2,0.4); //fn->SetParLimits(2,0,2);
        fn_up[i]->SetParameter(3,-0.2); //fn->SetParLimits(3,0,20.0);
         
        
        fn_down[i] = new TF1("fn_down",novosibirsk,0.05,300,4);
        fn_down[i]->SetParameter(0,maxVal);
        fn_down[i]->SetParameter(1,-0.3); //fn->SetParLimits(1,-1,2);
        fn_down[i]->SetParameter(2,0.4); //fn->SetParLimits(2,0,2);
        fn_down[i]->SetParameter(3,-0.2); //fn->SetParLimits(3,0,20.0);
         
        thist[i]->Fit( fn[i], "WRV" );
        thist_up[i]->Fit( fn_up[i], "WRV+" );
        thist_down[i]->Fit( fn_down[i], "WRV" );

        //std::cout << "Values: " << fn_up[i]->Eval(ctau.at(i)) << " " << fn[i]->Eval(ctau.at(i)) << " " << fn_down[i]->Eval(ctau.at(i)) << std::endl;
        //std::cout << "Low/high: " <<  fn_up[i]->Eval(ctau.at(i))/fn[i]->Eval(ctau.at(i)) << ", " << fn_down[i]->Eval(ctau.at(i))/fn[i]->Eval(ctau.at(i)) << std::endl;
        uperr.push_back(fn_up[i]->Eval(ctau.at(i))/fn[i]->Eval(ctau.at(i)));
        downerr.push_back(fn_down[i]->Eval(ctau.at(i))/fn[i]->Eval(ctau.at(i)));
	double errTmpUp = fn_up[i]->Eval(ctau.at(i)) - fn[i]->Eval(ctau.at(i));
	double errTmpDown = fn_down[i]->Eval(ctau.at(i)) - fn[i]->Eval(ctau.at(i));
	double midVal = fn[i]->Eval(ctau.at(i));
	thist_up[i]->Draw();
	thist[i]->Draw("SAME");
	thist_down[i]->Draw("SAME");

	c1->Print("Expected_2vx_"+benchmark.at(i)+".root");	

        fn[i] = new TF1("fn",novosibirsk,0.05,300,4);
	fn[i]->SetParameter(0,maxValA);
        fn[i]->SetParameter(1,1); //fn->SetParLimits(1,-1,2);
        fn[i]->SetParameter(2,1); //fn->SetParLimits(2,0,2);
        fn[i]->SetParameter(3,1); //fn->SetParLimits(3,0,20.0);
        
        fn_up[i] = new TF1("fn_up",novosibirsk,0.05,300,4);
fn_up[i]->SetParameter(0,maxValA);
        fn_up[i]->SetParameter(1,1); //fn->SetParLimits(1,-1,2);
        fn_up[i]->SetParameter(2,1); //fn->SetParLimits(2,0,2);
        fn_up[i]->SetParameter(3,1); //fn->SetParLimits(3,0,20.0);
        
        fn_down[i] = new TF1("fn_down",novosibirsk,0.05,300,4);
fn_down[i]->SetParameter(0,maxValA);
        fn_down[i]->SetParameter(1,1); //fn->SetParLimits(1,-1,2);
        fn_down[i]->SetParameter(2,1); //fn->SetParLimits(2,0,2);
        fn_down[i]->SetParameter(3,1); //fn->SetParLimits(3,0,20.0);
 
	thistA[i]->Fit( fn[i], "WRV"); 
	thistA_up[i]->Fit( fn_up[i], "WRV+"); 
	thistA_down[i]->Fit( fn_down[i], "WRV"); 
	double errTmpUpA = fn_up[i]->Eval(ctau.at(i))-fn[i]->Eval(ctau.at(i));
	double errTmpDownA = fn_down[i]->Eval(ctau.at(i))-fn[i]->Eval(ctau.at(i));
 	midVal+= fn[i]->Eval(ctau.at(i));
        uperrA.push_back(fn_up[i]->Eval(ctau.at(i))/fn[i]->Eval(ctau.at(i)));
        downerrA.push_back(fn_down[i]->Eval(ctau.at(i))/fn[i]->Eval(ctau.at(i)));
 	uperrC.push_back(sqrt(errTmpUp*errTmpUp + errTmpUpA*errTmpUpA)/midVal + 1.);   
 	downerrC.push_back(1.-sqrt(errTmpDown*errTmpDown + errTmpDownA*errTmpDownA)/midVal);
   
        thistA_up[i]->Draw();
        thistA[i]->Draw("SAME");
        thistA_down[i]->Draw("SAME");

        c1->Print("Expected_ABCD_"+benchmark.at(i)+".root");
  
        //std::cout << "ABCD method values: " << fn_up[i]->Eval(ctau.at(i)) << " " << fn[i]->Eval(ctau.at(i)) << " " << fn_down[i]->Eval(ctau.at(i)) << std::endl;
        //std::cout << "ABCD Low/high: " <<  fn_up[i]->Eval(ctau.at(i))/fn[i]->Eval(ctau.at(i)) << ", " << fn_down[i]->Eval(ctau.at(i))/fn[i]->Eval(ctau.at(i)) << std::endl; 
	    //std::cout << errTmpUp << " " << errTmpUpA << " " << errTmpDown << " " << errTmpDownA << " " << midVal << ", " << fn[i]->Eval(ctau.at(i)) << std::endl;
 } 
    std::cout << "Low/high variations at sim_ctau: " << std::endl; 
    for(int i=0; i<6; i++){
        std::cout <<"global2VxError_up = "<< uperr.at(i) << "; global2VxError_down = " << downerr.at(i) << ";"<< std::endl;
        std::cout <<"globalABCDError_up = "<< uperrA.at(i) << "; globalABCDError_down = " << downerrA.at(i) << ";"<< std::endl;
        std::cout <<"globalCombError_up = "<< uperrC.at(i) << "; globalCombError_down = " << downerrC.at(i) << ";"<< std::endl;
        std::cout << std::endl;
    }
    

    return;
}
