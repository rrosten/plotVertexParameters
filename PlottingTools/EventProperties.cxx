//
//  EventProperties.cpp
//  plotVertexParameters
//
//  Created by Heather Russell on 12/10/15.
//
//

#include "PlottingPackage/EventProperties.h"
#include <iostream>

void EventProperties::setZeroVectors(){
    nTrigBarrel_1Trig=0;
    nTrigEndcap_1Trig=0;

    nTrig_BB=0;
    nTrig_BE=0;
    nTrig_EE=0;
    
    n2TrigEvents=0;
    
    n1TrigEvents=0;
    
    nMoreThan2TrigEvents=0;
    
    nVxMatchBarrel_BB=0;
    nVxMatchBarrel_BE=0;
    nVxMatchEndcap_BE=0;
    nVxMatchEndcap_EE=0;
    
    nVxMatchBarrel_1Trig=0;
    nVxMatchEndcap_1Trig=0;


    nVxMatch_1Trig=0;
    n1VxMatch_2Trig=0;
    
    nVxUnMatch_1Trig=0;
    nVxUnMatch_2Trig=0;

    //all the possible 2 vertex events!
    nVxMatchBarrel_1Trig_BVx=0;
    nVxMatchEndcap_1Trig_BVx=0;
    nVxMatchBarrel_1Trig_EVx=0;
    nVxMatchEndcap_1Trig_EVx=0;
    
    n2VxMatch_BB=0;
    n2VxMatch_BE=0;
    n2VxMatch_EE=0;
    
    n2VxMatch_2Trig=0;
    n2VxMatch_1Trig=0;
    
    //failed 2vx events b/c of matching.
    
    n2VxUnMatch_1Trig=0;
    //at least one doesn't match
    n2VxUnMatch_2Trig=0;
    
    
    for(int i=0;i<10;i++){eventsPassCuts[i] = 0;}

}

void EventProperties::add1TrigEvent(int trigRegion, int vx, int vxMatches, double weight){
	n1TrigEvents++;
	if(trigRegion == 1){
		nTrigBarrel_1Trig+= weight;
		if( vx  && vxMatches ){
			nVxMatchBarrel_1Trig+= weight;
			nVxMatch_1Trig+= weight;
		} else if(vx){
		    nVxUnMatch_1Trig+= weight;
		}
	} else if(trigRegion == 2){
		nTrigEndcap_1Trig+= weight;
		if( vx  && vxMatches ){
			nVxMatchEndcap_1Trig+= weight;
			nVxMatch_1Trig+= weight;
		} else if(vx){
		    nVxUnMatch_1Trig+= weight;
		}
	}
	
}
void EventProperties::add2Vertex1TrigEvent(int trigRegion, int vx1region, int vxMatches1, int vx2region, int vxMatches2, double weight){
    n1TrigEvents++;
    /*
    nVxMatchBarrel_1Trig_BVx=0;
    nVxMatchEndcap_1Trig_BVx=0;
    nVxMatchBarrel_1Trig_EVx=0;
    nVxMatchEndcap_1Trig_EVx=0;
     */
    if(trigRegion == 1){
        nTrigBarrel_1Trig+= weight;
        if( vx1region  && vxMatches1 && vx2region && !vxMatches2){
            if(vx2region == 1)nVxMatchBarrel_1Trig_BVx+= weight;
            else if(vx2region == 2) nVxMatchBarrel_1Trig_EVx += weight;
            n2VxMatch_1Trig+= weight;
        } else if( vx1region  && !vxMatches1 && vx2region && vxMatches2){
            if(vx1region == 1) nVxMatchBarrel_1Trig_BVx+= weight;
            else if(vx1region == 2) nVxMatchBarrel_1Trig_EVx += weight;
            n2VxMatch_1Trig+= weight;
        } else if( vx1region  && !vxMatches1 && vx2region && !vxMatches2){
            std::cout << "one trigger, two vertices, but neither vertex matches the trigger :( " << std::endl;
            n2VxUnMatch_1Trig+= weight;
        }
    } else if(trigRegion == 2){
        nTrigEndcap_1Trig+= weight;
        if( vx1region  && vxMatches1 && vx2region && !vxMatches2){
            if(vx2region == 1)nVxMatchEndcap_1Trig_BVx+= weight;
            else if(vx2region == 2) nVxMatchEndcap_1Trig_EVx += weight;
            n2VxMatch_1Trig+= weight;
        } else if( vx1region  && !vxMatches1 && vx2region && vxMatches2){
            if(vx1region == 1)nVxMatchEndcap_1Trig_BVx+= weight;
            else if(vx1region == 2) nVxMatchEndcap_1Trig_EVx += weight;
            n2VxMatch_1Trig+= weight;
        } else if( vx1region  && !vxMatches1 && vx2region && !vxMatches2){
            std::cout << "one trigger, two vertices, but neither vertex matches the trigger :( " << std::endl;
            n2VxUnMatch_1Trig+= weight;
        }
    }
    
}
void EventProperties::add2TrigEvent(int trigRegion, int vxRegion, int vxMatches, double weight){
	n2TrigEvents+= weight;
	if(trigRegion == 1){
		nTrig_BB+= weight;
		if(vxMatches){
			n1VxMatch_2Trig+= weight;
			if(vxRegion == 1){
				nVxMatchBarrel_BB+= weight;
			}
		}else{
			nVxUnMatch_2Trig+= weight;
		}
	} else if(trigRegion == 2){
		nTrig_BE+= weight;
		if(vxMatches){
			n1VxMatch_2Trig+= weight;
			if(vxRegion == 1){
				nVxMatchBarrel_BE+= weight;
			} else if (vxRegion == 2){
				nVxMatchEndcap_BE+= weight;
			}
		}else{
			nVxUnMatch_2Trig+= weight;
		}
	} else if(trigRegion == 3){
		nTrig_EE+= weight;
		if(vxMatches){
			n1VxMatch_2Trig+= weight;
			if(vxRegion == 2){
				nVxMatchEndcap_EE+= weight;
			}
		}else{
			nVxUnMatch_2Trig+= weight;
		}
	}
}
void EventProperties::add2Vertex2TrigEvent(int trigRegion, int verticesMatch, double weight){
    /* 
     //all the possible 2 vertex events!
     n2VxMatch_BB=0;
     n2VxMatch_BE=0;
     n2VxMatch_EE=0;
     
     n2VxMatch_2Trig=0;*/
    
    n2TrigEvents+= weight;
    if(trigRegion == 1){
        nTrig_BB+= weight;
        if(verticesMatch){
            n2VxMatch_2Trig+= weight;
            n2VxMatch_BB+= weight;
        }else{
            n2VxUnMatch_2Trig+= weight;
        }
    } else if(trigRegion == 2){
        nTrig_BE+= weight;
        if(verticesMatch){
            n2VxMatch_2Trig+= weight;
            n2VxMatch_BE+= weight;
        }else{
            n2VxUnMatch_2Trig+= weight;
        }
    } else if(trigRegion == 3){
        nTrig_EE+= weight;
        if(verticesMatch){
            n2VxMatch_2Trig+= weight;
            n2VxMatch_EE+= weight;
        }else{
            n2VxUnMatch_2Trig+= weight;
        }
    }
}

void EventProperties::addStrangeEvent(int nTrigs, double weight){
	if(nTrigs > 2) nMoreThan2TrigEvents+= weight;
}

void EventProperties::addEventToCutflow(enum EventProperties::Cutflow cf, double weight){
 
    eventsPassCuts[cf]+= weight;
    
}

void EventProperties::printResults(bool isBlind){
    if(isBlind){
        std::cout << "RUNNING BLINDED TO 2 VERTEX EVENTS!" << std::endl;
    }
	std::cout << std::fixed << "Total number of events with 1 Muon RoI Cluster (trigger): " << n1TrigEvents << std::endl;
	std::cout << "Total number of events with 2 Muon RoI Clusters (triggers): " << n2TrigEvents << std::endl;
	std::cout << "Total number of events with more than 2 Muon RoI Clusters (triggers): " << nMoreThan2TrigEvents << std::endl;
	
	std::cout << std::fixed << "number of events with 1 barrel trigger: " << nTrigBarrel_1Trig << std::endl;
	std::cout << std::fixed << "number of events with 1 endcap trigger: " << nTrigEndcap_1Trig << std::endl;
	std::cout << "number of events with 2 triggers in the barrel: " << nTrig_BB << std::endl;
	std::cout << "number of events with 2 triggers in the endcap: " << nTrig_EE << std::endl;
	std::cout << "number of events with 1 barrel trig, 1 endcap trig: " << nTrig_BE << std::endl;
	
	std::cout << std::fixed << "Global number of events with 1 trigger and 1 matching vertex: " << nVxMatch_1Trig << std::endl;
	std::cout << std::fixed << "Global number of events with 2 triggers and 1 matching vertex: " << n1VxMatch_2Trig << std::endl;
	
	std::cout << std::fixed << "number of events with 1 barrel trigger+vertex: " << nVxMatchBarrel_1Trig << std::endl;
	std::cout << std::fixed << "number of events with 1 endcap trigger+vertex: " << nVxMatchEndcap_1Trig << std::endl;
	std::cout << "number of events with 2 triggers in the barrel, one matches a vertex: " << nVxMatchBarrel_BB << std::endl;
	std::cout << "number of events with 2 triggers in the endcap, one matches a vertex: " << nVxMatchEndcap_EE << std::endl;
	std::cout << "number of events with 1 barrel trig, 1 endcap trig, barrel matches a vertex: " << nVxMatchBarrel_BE << std::endl;
	std::cout << "number of events with 1 barrel trig, 1 endcap trig, endcap matches a vertex: " << nVxMatchEndcap_BE << std::endl;
	
	std::cout << "number of events with 1 trigger and 1 vertex that don't match: " << nVxUnMatch_1Trig << std::endl;
	std::cout << "number of events with 2 triggers and 1 vertex that doesn't match: " << nVxUnMatch_2Trig << std::endl;
    if(!isBlind){
        std::cout << "**********************************************************************************" <<std::endl;
        std::cout << "*********************************UNBLINDED INFORMATION****************************" <<std::endl;
        std::cout << "number of events with 1 barrel trigger+vertex and one triggerless barrel vertex: " << nVxMatchBarrel_1Trig_BVx << std::endl;
        std::cout << "number of events with 1 barrel trigger+vertex and one triggerless endcap vertex: " << nVxMatchBarrel_1Trig_EVx << std::endl;
        std::cout << "number of events with 1 endcap trigger+vertex and one triggerless barrel: " << nVxMatchEndcap_1Trig_BVx << std::endl;
        std::cout << "number of events with 1 endcap trigger+vertex and one triggerless endcap: " << nVxMatchEndcap_1Trig_EVx << std::endl;
        std::cout << "number of events with 2 triggers in the barrel, both match a vertex: " << n2VxMatch_BB << std::endl;
        std::cout << "number of events with 2 triggers in the endcap, both match a vertex: " << n2VxMatch_EE << std::endl;
        std::cout << "number of events with 1 barrel trig, 1 endcap trig, both match a vertex: " << n2VxMatch_BE << std::endl;
        
        std::cout << "number of events with 1 trigger and 1 vertex that doesn't match, and one more vertex: " << n2VxUnMatch_1Trig << std::endl;
        std::cout << "number of events with 2 triggers and 2 vertices, at least one doesn't match: " << n2VxUnMatch_2Trig << std::endl;
        
    }
    
}
