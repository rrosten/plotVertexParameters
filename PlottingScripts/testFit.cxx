#include "TFile.h"
#include "TH1D.h"
#include "TF1.h"
#include "TPad.h"
#include "TMath.h"
#include <cmath>

double plaw( double* x, double* par ){
    double val =  x[0];
    return ( par[0]*std::pow( val, par[1] + par[2]*log( val ) ) );
}

/*
 // if ( val < par[3] ) return 0;
 
 //return ( par[0]*exp( -par[1]*val )*
 //         std::pow(par[2]*( val - par[3] ),par[4])/(1 + std::pow(par[2]*( val - par[3] ),par[4]) ) );
 */




double evaluate_cb( const double x, const double mu, const double sigma, const double a, const double n ){
    
    double val = (x - mu)/sigma ;
    
    if ( val <= a ){
        return exp( -0.5*val*val );
    }
    
    double aval = std::pow( n/fabs(a), n )*exp( -0.5*a*a );
    
    double bval = (n/fabs(a)) - a;
    
    return aval/std::pow( bval + val, n );
}

double evaluate_novosibirsk( const double x, const double peak, const double width, const double tail )
{
    if (TMath::Abs(tail) < 1.e-5) {
        return TMath::Exp( -0.5 * TMath::Power( ( (x - peak) / width ), 2 ));
    }
    
    Double_t arg = 1.0 - ( x - peak ) * tail / width;
    
    if (arg < 1.e-5) {
        return 0.0;
    }
    Double_t log = TMath::Log(arg);
    
    const Double_t xi = 2.3548200450309494; // 2 Sqrt( Ln(4) )
    
    Double_t width_zero = ( 2.0 / xi ) * TMath::ASinH( tail * xi * 0.5 );
    Double_t width_zero2 = width_zero * width_zero;
    Double_t exponent = ( -0.5 / (width_zero2) * log * log ) - ( width_zero2 * 0.5 );
    
    return TMath::Exp(exponent) ;
}


double cb( double* x, double* par ){
    return par[0]*evaluate_cb( log10( x[0] ), par[1], par[2], par[3], par[4] );
}

double novorsibirsk( double* x, double* par ){
    return par[0]*evaluate_novosibirsk( log10( x[0] ), par[1], par[2], par[3] );
}

void testFit(TString file){
    TFile* tfile = TFile::Open(file);
    TH1D* thist[6];
    TF1* fn[6];
    thist[0] = (TH1D*) tfile->Get("Expected_Tot2Vx");
    thist[1] = (TH1D*) tfile->Get("Expected_Tot2Vx_maxStat");
    thist[2] = (TH1D*) tfile->Get("Expected_Tot2Vx_minStat");
    thist[3] = (TH1D*) tfile->Get("Expected_Tot2Vx_maxSyst");
    thist[4] = (TH1D*) tfile->Get("Expected_Tot2Vx_minSyst");

    thist[5] = (TH1D*) tfile->Get("Expected_1BMSVx_2j150");
    TH1D* tmp = (TH1D*) tfile->Get("Expected_BB1MSVx_2j150");
    thist[5]->Add(tmp);
    tmp = (TH1D*) tfile->Get("Expected_BE1MSVx_2j150");
    thist[5]->Add(tmp);
    
    for(int i=0;i<6;i++){
    thist[i]->Draw();
    gPad->SetLogx();
    
    /*
    TF1* fn = new TF1("fn",cb,0.05,300,5);
    fn->SetParameter(0,70000);
    fn->SetParameter(1,1); fn->SetParLimits(1,-1,2);
    fn->SetParameter(2,1); fn->SetParLimits(2,0,2);
    fn->SetParameter(3,1); fn->SetParLimits(3,0.1,5.0);
    fn->SetParameter(4,10); fn->SetParLimits(4,0.1,30);
    */
    
    /*
    TF1* fn = new TF1("fn",plaw,0.05,300,5);
    fn->SetParameter(0,70000); fn->SetParLimits(0,0,100000);
    fn->SetParameter(1,0); fn->SetParLimits(1,0,100);
    fn->SetParameter(2,2);
    fn->SetParameter(3,1); fn->SetParLimits(3,0, 1000);
    fn->SetParameter(4,2);
    */
    
    /*
    TF1* fn = new TF1("fn",plaw,0.05,300,5);
    fn->SetParameter(0,70000);
    fn->SetParameter(1,-1);
    fn->SetParameter(2,-2);
    */
    
    fn[i] = new TF1("fn",novorsibirsk,0.05,300,4);
    fn[i]->SetParameter(0,50);
    fn[i]->SetParameter(1,1); //fn->SetParLimits(1,-1,2);
    fn[i]->SetParameter(2,1); //fn->SetParLimits(2,0,2);
    fn[i]->SetParameter(3,1); //fn->SetParLimits(3,0,20.0);
     
    thist[i]->Fit( fn[i], "WRV" );
    //fn->Draw();
    
    }
    fn[0]->SetLineColor(kBlack); fn[0]->SetLineWidth(3);
    fn[1]->SetLineColor(kAzure-5); fn[2]->SetLineColor(kAzure-5); fn[1]->SetLineStyle(7);fn[2]->SetLineStyle(7);
        fn[3]->SetLineColor(kGreen); fn[4]->SetLineColor(kGreen);fn[3]->SetLineStyle(2);fn[4]->SetLineStyle(2);
    fn[5]->SetLineColor(kRed+3); fn[5]->SetLineWidth(3);
    fn[0]->GetXaxis()->SetRangeUser(0.1,300);
    fn[0]->GetXaxis()->SetTitle("Singlino proper lifetime (c*#tau) [m]");
    fn[0]->GetYaxis()->SetRangeUser(0,600);
    fn[0]->GetYaxis()->SetTitle("Expected events in 22.1 fb^{-1}");
    fn[0]->Draw();
    for(int i=1; i<6; i++){
        fn[i]->Draw("LSAME");
    }

    TLegend *leg = new TLegend(0.55,0.75,0.9,0.9);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->AddEntry(fn[5],"Expected 1 BVx + 2j150 events","l");
    leg->AddEntry(fn[0],"Expected 2 vertex events","l");
    leg->AddEntry(fn[1],"#pm #sigma_{stat.}","l");
        leg->AddEntry(fn[3],"#pm #sigma_{syst.}","l");
    leg->Draw();
    
    return;
}