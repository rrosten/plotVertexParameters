//
//  EventHistograms.h
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#ifndef __plotVertexParameters__EventHistograms__
#define __plotVertexParameters__EventHistograms__

#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
#include "PlottingPackage/CommonVariables.h"

struct EventHistograms {
public:
    TH1D *h_Jet_pT;
    TH1D *h_Jet_Et;
    TH1D *h_Jet_eta;
    TH1D *h_Jet_phi;
    TH1D *h_Jet_nJetPerEvt;
    
    TH1D *h_HT;
    TH1D *h_HTMiss;
    TH1D *h_NJets;
    TH1D *h_Meff;
    
    TH1D *h_Jet_pT_vx;
    TH1D *h_Jet_Et_vx;
    TH1D *h_Jet_eta_vx;
    TH1D *h_Jet_phi_vx;
    TH1D *h_Jet_nJetPerEvt_vx;
    
    TH1D *h_HT_vx;
    TH1D *h_HTMiss_vx;
    TH1D *h_NJets_vx;
    TH1D *h_Meff_vx;
    
    TH1D *h_MSVertex_TrigMatched;
    TH1D *h_TrigPassed;
    
    TH1D *h_MSVertex_eta;
    TH1D *h_MSVertex_phi;
    TH1D *h_MSVertex_R;
    TH1D *h_MSVertex_z;
    TH1D *h_MSVertex_eta_good;
    TH1D *h_MSVertex_phi_good;
    TH1D *h_MSVertex_R_good;
    TH1D *h_MSVertex_z_good;
    TH1D *h_MSVertex_nTrks;
    TH1D *h_MSVertex_nMDT;
    TH1D *h_MSVertex_nRPC;
    TH1D *h_MSVertex_nTGC;
    
    //
    TH2D *h_MSVertex_phieta;
    TH2D *h_MSVertex_phieta_good;
    TH2D *h_MSVertex_phieta_matched;
    

    
    void initializeHistograms();
    void fillHistograms(CommonVariables *commonVar, bool vxMatched);

};



#endif /* defined(__plotVertexParameters__EventHistograms__) */
