
void makePunchThroughTriggerProbPlots(){
    //0.7 < |η| < 1.0 or 1.5 < |η| < 1.7

    TCanvas* c1 = new TCanvas("c1","c1",800,600);
    SetAtlasStyle();

    //c1->SetRightMargin(0.05);
    //c1->SetTopMargin(0.07);
    //c1->SetLeftMargin(0.15);
    //c1->SetBottomMargin(0.15);
    TPad *pad1 = new TPad("pad1","pad1",0,0.32,1,1);
    pad1->SetTopMargin(0.075);
    pad1->SetBottomMargin(0.04);
    pad1->SetLeftMargin(0.14);
    pad1->SetLogy();
    pad1->Draw();

    TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.32);
    pad2->SetTopMargin(0.05);
    pad2->SetBottomMargin(0.40);
    pad2->SetLeftMargin(0.14);
    pad2->Draw();



    //v13: TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/data/outputMSSystematics_20p7_j600.root");
    TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/data/outputMSSyst_dv14.root");

    //v13: TFile *_file1 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/bkgdJZ/JZAll/outputMSSystematics_20p7_j600.root");
    TFile *_file1 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/bkgdJZ/JZAll/outputMSSyst_dv14.root");


    TString plotDir = "../OutputPlots/PunchThroughProbabilities/";

    std::vector<TString> histNames;
    histNames.push_back("eta1_pT_pTrigAllJets");
    histNames.push_back("eta3_pT_pTrigAllJets");
    


    for (auto histName : histNames){

            pad1->cd(); 
            gStyle->SetPadTickX(2);
            gStyle->SetPadTickY(2);
            TH1D *h_MC =  (TH1D*)_file1->Get(histName);
            TH1D *h_Data = (TH1D*)_file0->Get(histName);
        h_Data->SetLineColor(kBlack);h_Data->SetMarkerColor(kBlack);h_Data->SetMarkerSize(1.2);h_Data->SetLineWidth(2);h_Data->SetMarkerStyle(20);
        h_MC->SetLineColor(kBlue+2);h_MC->SetMarkerColor(kBlue+2);h_MC->SetMarkerSize(1.2);h_MC->SetLineWidth(2);h_MC->SetMarkerStyle(4);  

        std::cout <<" entries data/mc: " << h_Data->GetEntries()  <<"/" <<h_MC->GetEntries() <<  std::endl;

        if(h_MC->GetEntries() == 0 || h_Data->GetEntries() == 0) continue;

        pad1->SetLogy(1); h_Data->SetMinimum(0.000001);

        if(histName.Contains("pT_p")){ h_Data->GetXaxis()->SetTitle("jet p_{T} [GeV]");
        h_Data->GetYaxis()->SetTitle("p(muon RoI cluster in jet cone)");
	}

        double maxVal = h_MC->GetMaximum(); if(h_Data->GetMaximum() > maxVal) maxVal = h_Data->GetMaximum();
        h_Data->SetMaximum(5.*maxVal);
        h_Data->SetTitle("");

        TString append = "";

        if(histName.Contains("LJ")) append = ", leading jets"; 
        else if(histName.Contains("AllJets")) append = ", leading and sub-leading jets"; 
        TString title = "";
        if(histName.Contains("full")) title = TString("|#eta| < 2.7") + append;
        else if(histName.Contains("eta1")) title = TString("|#eta| < 0.8") + append;
        else if(histName.Contains("eta3")) title = TString("1.3 < |#eta| < 2.5") + append;

        h_Data->GetYaxis()->SetLabelSize(0.070);
        h_Data->GetYaxis()->SetTitleSize(0.065);
        h_Data->GetYaxis()->SetTitleOffset(0.18);
        h_Data->GetXaxis()->SetTitleOffset(2.7);
        h_Data->GetXaxis()->SetLabelOffset(0.5);
        h_Data->GetYaxis()->SetTitleOffset(0.95);
        
        double maxxval = histName.Contains("eta1") ? 3500 : 2000;
        double minval = 0;
    	bool doHighpT=false;
	    if(doHighpT) minval = histName.Contains("eta1") ? 1500 : 800;
        h_Data->GetXaxis()->SetRangeUser(0,maxxval); 

        h_Data->Draw();
        h_MC->Draw("SAME");

        TLegend *leg = new TLegend(0.65,0.75,0.85,0.9);
        leg->SetFillColor(0);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.05);
        leg->AddEntry(h_MC,"Pythia JZXW MC","lp");
        leg->AddEntry(h_Data,"data","lp");
        leg->Draw();

        TLatex latex2;
        latex2.SetNDC();
        latex2.SetTextColor(kBlack);
        latex2.SetTextSize(0.06);
        latex2.SetTextAlign(31);  //align at bottom
        latex2.DrawLatex(0.95,.94,title);

        //c1->Print(plotDir+histName+".pdf");
        TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
        l.SetNDC();
        l.SetTextFont(72);
        l.SetTextSize(0.06);
        l.SetTextColor(kBlack);
        //l.DrawLatex(.72,.84,"ATLAS");
        TLatex p; 
        p.SetNDC();
        p.SetTextSize(0.06);
        p.SetTextFont(42);
        p.SetTextColor(kBlack);
        // p.DrawLatex(0.72+0.105,0.84,"Internal");

        c1->cd();

        pad2->cd();
        gStyle->SetPadTickX(1);
        gStyle->SetPadTickY(1);

        TH1D *p1_new = (TH1D*)h_Data->Clone("data_clone");

        p1_new->Divide(h_MC);
        /*for(int i=1;i<p1_new->GetNbinsX(); i++){
            p1_new->SetBinError(i+1,0.0001);
        }*/

        p1_new->SetMarkerStyle(8);
        p1_new->SetMarkerSize(0.8);
        p1_new->SetMarkerColor(kBlack);
        p1_new->SetMinimum(0.0);
        p1_new->SetMaximum(2.0);
        //p1_new->GetXaxis()->SetTitle("to do");
        p1_new->GetXaxis()->SetLabelFont(42);
        p1_new->GetXaxis()->SetLabelSize(0.16);
        p1_new->GetXaxis()->SetLabelOffset(0.05);
        p1_new->GetXaxis()->SetTitleFont(42);
        p1_new->GetXaxis()->SetTitleSize(0.14);
        p1_new->GetXaxis()->SetTitleOffset(1.3);
        p1_new->GetYaxis()->SetNdivisions(505);
        p1_new->GetYaxis()->SetTitle("#font[42]{Data/MC}");
        p1_new->GetYaxis()->SetLabelFont(42);
        p1_new->GetYaxis()->SetLabelSize(0.15);
        p1_new->GetYaxis()->SetTitleFont(42);
        p1_new->GetYaxis()->SetTitleSize(0.13);
        p1_new->GetYaxis()->SetTitleOffset(0.44); 
        p1_new->DrawCopy("eP");

        TLine* line = new TLine();
        line->DrawLine(p1_new->GetXaxis()->GetXmin(),1,p1_new->GetXaxis()->GetXmax(),1);

        TString fitVal = "";
        TString fitVal2 = "";

        TF1 *f1 = new TF1("fit1", "pol0", minval, maxxval);
        p1_new->Fit("fit1","R");
        p1_new->GetXaxis()->SetRangeUser(0,maxxval); 
        fitVal =  TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(0) ) ) 
                            + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(0) );
        //fitVal2 = TString("slope: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(1) ) ) + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(1) );     

        std::cout << fitVal << std::endl; 
        pad2->SetLogy(0);

        //latex2.DrawLatex(.15,.98,title + ", Data/MC");

        TLatex latex;
        latex.SetNDC();
        latex.SetTextColor(kBlack);
        latex.SetTextSize(0.1);
        latex.SetTextAlign(13);  //align at top
        latex.DrawLatex(.25,.92, fitVal);
        latex.DrawLatex(.25,.82, fitVal2);

        c1->Print(plotDir+"ratio_"+histName+"_pol0.pdf");
        c1->Print(plotDir+"ratio_"+histName+"_pol0.png");
        c1->Print(plotDir+"ratio_"+histName+"_pol0.root");
        
        delete h_MC; delete h_Data; delete p1_new;
    }


}
