#include "TFile.h"
#include "TH1D.h"
#include "TF1.h"
#include "TPad.h"
#include "TMath.h"
#include <cmath>

double evaluate_novosibirsk( const double x, const double peak, const double width, const double tail )
{
    if (TMath::Abs(tail) < 1.e-12) {
        return TMath::Exp( -0.5 * TMath::Power( ( (x - peak) / width ), 2 ));
    }

    Double_t arg = 1.0 - ( x - peak ) * tail / width;

    if (arg < 1.e-12) {
        return 0.0;
    }
    Double_t log = TMath::Log(arg);

    const Double_t xi = 2.3548200450309494; // 2 Sqrt( Ln(4) )

    Double_t width_zero = ( 2.0 / xi ) * TMath::ASinH( tail * xi * 0.5 );
    Double_t width_zero2 = width_zero * width_zero;
    Double_t exponent = ( -0.5 / (width_zero2) * log * log ) - ( width_zero2 * 0.5 );

    return TMath::Exp(exponent) ;
}
double novosibirsk( double* x, double* par ){
    return par[0]*evaluate_novosibirsk( log10( x[0] ), par[1], par[2], par[3] );
}
void plotGlobalEfficienciesStealth(){
    std::vector<TString> benchmark = {"250", "500", "800", "1200", "1500", "2000"};
    std::vector<double> scalings = {66.2841*396000., 1.54623*391000., 0.084412*389000., 0.00476892*396000.,  0.000794191*394000., 0.0000554713*390000.};
    std::cout << "SCALINGS ARE FOR OLD LUMI, THESE NEED UPDATING AS SOON AS LIMIT EXTRAPOLATIONS ARE REDONE" << std::endl;
    int colors[6] = {kViolet+7, kAzure+7, kTeal, kSpring-2, kOrange-3, kRed-4};

    TCanvas *c1 = new TCanvas("c1","c1",800,600);

    TFile* tfile[6];
    TH1D* thist[6];
    TH1D* thist2[6];
    TH1D *thist_up[6];
    TH1D *thist2_up[6];
    TH1D *thist_down[6];
    TH1D *thist2_down[6];
    TF1* fn_up[6];
    TF1* fn_down[6];
    TF1* fn2_up[6];
    TF1* fn2_down[6];
    TF1* fn[6];
    TF1* fn2[6];
    TGraph* fn3[6];
    TGraphAsymmErrors* g_err1[6];
    TGraphAsymmErrors* g_err2[6];
    TGraphAsymmErrors* g_err3[6];
    double maxVals[6] = {0,0,0,0,0,0};
    char name1[10];
    double maxValue = 0;

    for(int i=0; i<6; i++){
        tfile[i] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/outputExtrapolation_mg"+benchmark.at(i)+"_dv17.root");
        
        thist[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx");
        thist_up[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_maxTotal");
        thist_down[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_minTotal");
        thist2[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_2jx50");
        thist2_up[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_2jx50_maxTotal");
        thist2_down[i] = (TH1D*) tfile[i]->Get("Expected_ABCD1MSVx_2jx50_minTotal");
        /*
        thist[i]->Scale(1./scalings.at(i));
        thist_up[i]->Scale(1./scalings.at(i));
        thist_down[i]->Scale(1./scalings.at(i));
        thist2[i]->Scale(1./scalings.at(i));
        thist2_up[i]->Scale(1./scalings.at(i));
        thist2_down[i]->Scale(1./scalings.at(i));
        */
        sprintf(name1,"fn_%d",i);
        fn[i] = new TF1(name1,novosibirsk,0.05,300,4);
        fn[i]->SetParameter(0,thist[i]->GetMaximum());
        fn[i]->SetParameter(1,-0.3); //fn->SetParLimits(1,-1,2);
        fn[i]->SetParameter(2,0.4); //fn->SetParLimits(2,0,2);
        fn[i]->SetParameter(3,-0.2); //fn->SetParLimits(3,0,20.0);
        thist[i]->Fit( fn[i], "WRV" );

        sprintf(name1,"fn_up_%d",i);
        fn_up[i] = new TF1(name1,novosibirsk,0.05,300,4);
        fn_up[i]->SetParameter(0,thist_up[i]->GetMaximum());
        fn_up[i]->SetParameter(1,-0.3); //fn_up->SetParLimits(1,-1,2);
        fn_up[i]->SetParameter(2,0.4); //fn_up->SetParLimits(2,0,2);
        fn_up[i]->SetParameter(3,-0.2); //fn_up->SetParLimits(3,0,20.0);
        thist_up[i]->Fit( fn_up[i], "WRV" );
        sprintf(name1,"fn_down_%d",i);
        fn_down[i] = new TF1(name1,novosibirsk,0.05,300,4);
        fn_down[i]->SetParameter(0,thist_down[i]->GetMaximum());
        fn_down[i]->SetParameter(1,-0.3); //fn_down->SetParLimits(1,-1,2);
        fn_down[i]->SetParameter(2,0.4); //fn_down->SetParLimits(2,0,2);
        fn_down[i]->SetParameter(3,-0.2); //fn_down->SetParLimits(3,0,20.0);
        thist_down[i]->Fit( fn_down[i], "WRV" );

        sprintf(name1,"fn2_%d",i);
        fn2[i] = new TF1(name1,novosibirsk,0.05,300,4);
        fn2[i]->SetParameter(0,thist2[i]->GetMaximum());
        fn2[i]->SetParameter(1,-0.3); //fn->SetParLimits(1,-1,2);
        fn2[i]->SetParameter(2,0.4); //fn->SetParLimits(2,0,2);
        fn2[i]->SetParameter(3,-0.2); //fn->SetParLimits(3,0,20.0);
        thist2[i]->Fit( fn2[i], "WRV" );

        sprintf(name1,"fn2_up_%d",i);
        fn2_up[i] = new TF1(name1,novosibirsk,0.05,300,4);
        fn2_up[i]->SetParameter(0,thist2_up[i]->GetMaximum());
        fn2_up[i]->SetParameter(1,-0.3); //fn_up->SetParLimits(1,-1,2);
        fn2_up[i]->SetParameter(2,0.4); //fn_up->SetParLimits(2,0,2);
        fn2_up[i]->SetParameter(3,-0.2); //fn_up->SetParLimits(3,0,20.0);
        thist2_up[i]->Fit( fn2_up[i], "WRV" );
        sprintf(name1,"fn2_down_%d",i);
        fn2_down[i] = new TF1(name1,novosibirsk,0.05,300,4);
        fn2_down[i]->SetParameter(0,thist2_down[i]->GetMaximum());
        fn2_down[i]->SetParameter(1,-0.3); //fn_down->SetParLimits(1,-1,2);
        fn2_down[i]->SetParameter(2,0.4); //fn_down->SetParLimits(2,0,2);
        fn2_down[i]->SetParameter(3,-0.2); //fn_down->SetParLimits(3,0,20.0);
        thist2_down[i]->Fit( fn2_down[i], "WRV" );


        Double_t ctau[1000];
        Double_t eff[1000];
        Double_t eff1[1000];
        Double_t eff2[1000];
        Double_t errUp[1000];
        Double_t errDown[1000];
        Double_t errUp1[1000];
        Double_t errDown1[1000];
        Double_t errUp2[1000];
        Double_t errDown2[1000];
        int ntot = 0;
        TGraph *g_res = new TGraph(thist[i]);
        for (Int_t j = 0; j < 1000; j++) {
            Double_t life, nev, nev_up, nev_down, nev1_up, nev1_down, nev2_up, nev2_down;
            g_res->GetPoint(j, life, nev);
            nev = fn[i]->Eval(life) + fn2[i]->Eval(life);
            nev_up = sqrt( pow(fn_up[i]->Eval(life)-fn[i]->Eval(life), 2) + pow(fn2_up[i]->Eval(life) - fn2[i]->Eval(life), 2));
            nev_down = sqrt( pow(fn_down[i]->Eval(life)-fn[i]->Eval(life), 2) + pow(fn2_down[i]->Eval(life) - fn2[i]->Eval(life), 2));
            nev1_up = fn_up[i]->Eval(life)-fn[i]->Eval(life);
            nev1_down = fn[i]->Eval(life)-fn_down[i]->Eval(life);
            nev2_up = fn2_up[i]->Eval(life)-fn2[i]->Eval(life);
            nev2_down = fn2[i]->Eval(life)-fn2_down[i]->Eval(life);
            if(fn2_up[i]->Eval(life) > maxValue) maxValue = fn2_up[i]->Eval(life);
            if (nev == 0) {
                std::cout << "Continue because nev= " << nev << std::endl;
                continue;
            }
            if(nev > maxVals[i]) maxVals[i] = nev;
            eff[ntot] = nev;
            eff1[ntot] = fn[i]->Eval(life);
            eff2[ntot] = fn2[i]->Eval(life);
            errUp[ntot] = nev_up;
            errDown[ntot] = nev_down;
            errUp1[ntot] = nev1_up;
            errDown1[ntot] = nev1_down;
            errUp2[ntot] = nev2_up;
            errDown2[ntot] = nev2_down;
            ctau[ntot] = life;
            ntot++;
        }
        fn3[i] = new TGraph(ntot, ctau,eff);
        fn3[i]->SetLineColor(colors[i]);
        fn3[i]->SetLineStyle(i+1);
        fn3[i]->SetLineWidth(3);
        g_err3[i] = new TGraphAsymmErrors(ntot, ctau, eff);
        g_err1[i] = new TGraphAsymmErrors(ntot, ctau, eff1);
        g_err2[i] = new TGraphAsymmErrors(ntot, ctau, eff2);
        for(int j=0;j<ntot; j++){
            g_err3[i]->SetPointEYlow(j, errDown[j]);
            g_err3[i]->SetPointEYhigh(j, errUp[j]);
            g_err1[i]->SetPointEYlow(j, errDown1[j]);
            g_err1[i]->SetPointEYhigh(j, errUp1[j]);
            g_err2[i]->SetPointEYlow(j, errDown2[j]);
            g_err2[i]->SetPointEYhigh(j, errUp2[j]);
        }
        g_err1[i]->SetFillColorAlpha(colors[i], 0.15);
        g_err2[i]->SetFillColorAlpha(colors[i], 0.15);
        g_err3[i]->SetFillColorAlpha(colors[i], 0.15);
        g_err1[i]->SetLineWidth(0);
        g_err2[i]->SetLineWidth(0);
        g_err3[i]->SetLineWidth(0);
    }
    fn3[5]->SetLineColor(98);
    c1->SetLogx();
    fn3[0]->Draw("AL");
    fn3[0]->GetYaxis()->SetTitleOffset(1.6);
    fn3[0]->GetXaxis()->SetLimits(0.05,100);
    fn3[0]->GetXaxis()->SetTitle("Singlino proper lifetime (c*#tau) [m]");
    fn3[0]->GetYaxis()->SetRangeUser(0,maxValue*1.5);
    fn3[0]->GetYaxis()->SetTitle("Global expected events");
    fn3[0]->Draw("AL");
    g_err3[0]->Draw("c3 SAME");

    c1->Update();
    for(int i=1; i<6; i++){
        g_err3[i]->Draw("c3 SAME");
        fn3[i]->Draw("LSAME");

    }

    TLegend *leg = new TLegend(0.66,0.66,0.99,0.91);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->AddEntry(fn3[0],"m_{#tilde{g}} = 250 GeV","l");
    leg->AddEntry(fn3[1],"m_{#tilde{g}} = 500 GeV","l");
    leg->AddEntry(fn3[2],"m_{#tilde{g}} = 800 GeV","l");
    leg->AddEntry(fn3[3],"m_{#tilde{g}} = 1200 GeV","l");
    leg->AddEntry(fn3[4],"m_{#tilde{g}} = 1500 GeV","l");
    leg->AddEntry(fn3[5],"m_{#tilde{g}} = 2000 GeV","l");
    leg->Draw();

    TLatex latex2;
    latex2.SetNDC();
    latex2.SetTextColor(kBlack);
    latex2.SetTextSize(0.05);
    latex2.SetTextAlign(13);  //align at top
    latex2.DrawLatex(.21,.92,"#font[72]{ATLAS}  Internal");

    c1->Print("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/GlobalExpected_Combined_stealth_err.pdf");
    c1->Print("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/GlobalExpected_Combined_stealth_err.root");


    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.045);
    latex.SetTextAlign(13);  //align at top

    for(int i=0;i<6;i++){
        fn[i]->SetLineColor(kRed+1);
        fn2[i]->SetLineColor(kBlue-3);
        fn3[i]->SetLineColor(kViolet-6);
        g_err3[i]->SetFillColorAlpha(kViolet-6,0.2);
        g_err1[i]->SetFillColorAlpha(kRed+1,0.2);
        g_err2[i]->SetFillColorAlpha(kBlue-3,0.2);
        fn[i]->SetLineStyle(2); fn2[i]->SetLineStyle(7);
        fn[i]->SetLineWidth(3); fn2[i]->SetLineWidth(3);
        fn3[i]->SetLineStyle(1);

        //maxVals[0] = 0.006/1.5;
        fn3[i]->GetYaxis()->SetRangeUser(0,1.5*maxVals[i]);
        fn3[i]->Draw("AL");
        fn3[i]->GetYaxis()->SetTitleOffset(1.6);
        fn3[i]->GetXaxis()->SetLimits(0.05,100);
        fn3[i]->GetXaxis()->SetTitle("Singlino proper lifetime (c*#tau) [m]");
        fn3[i]->GetYaxis()->SetRangeUser(0,1.5*maxVals[i]);
        fn3[i]->GetYaxis()->SetLimits(0,1.5*maxVals[i]);
        fn3[i]->GetYaxis()->SetTitle("Global efficiency");

        fn3[i]->Draw("AL");
        g_err3[i]->Draw("c3 same");
        fn3[i]->Draw("L SAME");
        c1->Update();
        g_err1[i]->Draw("c3 same");
        fn[i]->Draw("LSAME");
        g_err2[i]->Draw("c3 same");
        fn2[i]->Draw("LSAME");
        TLegend *leg2 = new TLegend(0.58,0.76,0.95,0.91);
        leg2->SetFillStyle(0);
        leg2->SetBorderSize(0);
        leg2->AddEntry(fn[i],"2 MS vertices","l");
        leg2->AddEntry(fn2[i],"1 MS vertex + 2 jets","l");
        leg2->AddEntry(fn3[i],"Sum","l");
        leg2->Draw();
        latex.DrawLatex(0.2,0.87,"m_{#tilde{g}} = "+benchmark.at(i) + " GeV");
        latex2.DrawLatex(.2,.92,"#font[72]{ATLAS}  Internal");
        c1->Print("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/GlobalExpected_Channel_"+benchmark.at(i)+"_err.pdf");
        c1->Print("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/GlobalExpected_Channel_"+benchmark.at(i)+"_err.root");
    }


    return;
}
