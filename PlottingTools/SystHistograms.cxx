//
//  SystHistograms.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 24/02/16.
//
//

#include "PlottingPackage/SystHistograms.h"

#include <iostream>
#include "TCanvas.h"

void SystHistograms::initializeMSHistograms(TString obj, TString str){
    
    std::cout << "initializing systematics histograms...  " << std::endl;

    int nBins=50;
    
    h_eta            = new TH1D(obj+"_eta_"+str  ,obj+"_eta_"+str   ,nBins,-3.0,3.0);
    h_phi            = new TH1D(obj+"_phi_"+str  ,obj+"_phi_"+str   ,nBins,-3.2,3.2);
    h_width          = new TH1D(obj + "_width_"+str,obj + "_width_"+str, 100,0,0.5 );

    h_width_nAssocMSeg   = new TH1D(obj + "_width_nAssocMSeg_"+str,obj + "_width_nAssocMSeg_"+str, 100,0,0.5 );
    h_width_nMSTracklets = new TH1D(obj + "_width_nMSTracklets_"+str,obj + "_width_nMSTracklets_"+str, 100,0,0.5 );
    h_width_nMuonRoIs    = new TH1D(obj + "_width_nMuonRoIs_"+str,obj + "_width_nMuonRoIs_"+str, 100,0,0.5 );
    
    h_eta_nAssocMSeg = new TH1D(obj+"_eta_nAssocMSeg"+str  ,obj+"_eta_nAssocMSeg"+str   ,nBins,-3.0,3.0);
    h_eta_nMSTracklets = new TH1D(obj+"_eta_nMSTracklets"+str  ,obj+"_eta_nMSTracklets"+str   ,nBins,-3.0,3.0);
    h_eta_nMuonRoIs    = new TH1D(obj+"_eta_nMuonRoIs"+str  ,obj+"_eta_nMuonRoIs"+str   ,nBins,-3.0,3.0);
    h_eta_width          = new TH1D(obj + "_eta_width_"+str,obj + "_eta_width_"+str, nBins,-3.0,3.0 );

    h_pT_nAssocMSeg   = new TH1D(obj+"_pT_nAssocMSeg"+str  ,obj+"_pT_nAssocMSeg"+str   ,50,0,5000);
    h_pT_nMSTracklets = new TH1D(obj+"_pT_nMSTracklets"+str  ,obj+"_pT_nMSTracklets"+str   ,50,0,5000);
    h_pT_nMuonRoIs    = new TH1D(obj+"_pT_nMuonRoIs"+str  ,obj+"_pT_nMuonRoIs"+str   ,50,0,5000);
    h_pT_width       = new TH1D(obj+"_pT_width"+str  ,obj+"_pT_width"+str   ,50,0,5000);

    h_pT_nAssocMSeg_num   = new TH1D(obj+"_pT_nAssocMSeg_num"+str  ,obj+"_pT_nAssocMSeg"+str   ,50,0,5000);
    h_pT_nMSTracklets_num = new TH1D(obj+"_pT_nMSTracklets_num"+str  ,obj+"_pT_nMSTracklets"+str   ,50,0,5000);
    h_pT_nMuonRoIs_num    = new TH1D(obj+"_pT_nMuonRoIs_num"+str  ,obj+"_pT_nMuonRoIs"+str   ,50,0,5000);
    
    h_pT_nAssocMSeg_sq   = new TH1D(obj+"_pT_nAssocMSeg_sq"+str  ,obj+"_pT_nAssocMSeg)sq"+str   ,50,0,5000);
    h_pT_nMSTracklets_sq = new TH1D(obj+"_pT_nMSTracklets_sq"+str  ,obj+"_pT_nMSTracklets_sq"+str   ,50,0,5000);
    h_pT_nMuonRoIs_sq    = new TH1D(obj+"_pT_nMuonRoIs_sq"+str  ,obj+"_pT_nMuonRoIs_sq"+str   ,50,0,5000);
    
    h_nMSeg_nMSTracklets = new TH1D(obj+"_nMSeg_nMSTracklets"+str  ,obj+"_nMSeg_nMSTracklets"+str   ,20,0,200);
    h_nMSeg_nMuonRoIs    = new TH1D(obj+"_nMSeg_nMuonRoIs"+str  ,obj+"_nMSeg_nMuonRoIs"+str   ,20,0,200);
    

    h_eta_denom            = new TH1D(obj+"_eta_denom_"+str  ,obj+"_eta_denom_"+str   ,25,0,2.5);
    h_eta_pMSVx = new TH1D(obj+"_eta_pMSVx"+str  ,obj+"_eta_pMSVx"+str   ,25,0,2.5);
    h_eta_pTrig    = new TH1D(obj+"_eta_pTrig"+str  ,obj+"_eta_pTrig"+str   ,25,0,2.5);
    h_pT_pMSVx = new TH1D(obj+"_pT_pMSVx"+str  ,obj+"_pT_pMSVx"+str   , 50,0,5000);
    h_pT_pTrig    = new TH1D(obj+"_pT_pTrig"+str  ,obj+"_pT_pTrig"+str   ,50,0,5000);
    
    h_pT_denom_all             = new TH1D(obj+"_pT_denom_all_"+str    ,obj+"_pT_denom_all_"+str     ,50,0,5000);
    h_pT_denom             = new TH1D(obj+"_pT_denom_"+str    ,obj+"_pT_denom_"+str     ,50,0,5000);
    h_nMSeg_denom             = new TH1D(obj+"_nMSeg_denom_"+str    ,obj+"_nMSeg_denom_"+str ,20,0,200);
    h_nMSeg_pMSVx             = new TH1D(obj+"_nMSeg_pMSVx_"+str    ,obj+"_nMSeg_pMSVx_"+str ,20,0,200);
    h_nMSeg_pTrig             = new TH1D(obj+"_nMSeg_pTrig_"+str    ,obj+"_nMSeg_pTrig_"+str ,20,0,200);

    h_pT_eta_pMSVx = new TH2D(obj+"_pT_eta_pMSVx"+str  ,obj+"_pT_eta_pMSVx"+str   , 50 ,0,5000,25,0,2.5);
    h_pT_eta_pTrig    = new TH2D(obj+"_pT_eta_pTrig"+str  ,obj+"_pT_eta_pTrig"+str   ,50 ,0,5000, 25,0,2.5);
    h_pT_eta_denom    = new TH2D(obj+"_pT_eta_denom"+str  ,obj+"_pT_eta_denom"+str   ,50 ,0,5000, 25,0,2.5);
    
    h_MSegVsTracklets = new TH2D(obj + "_nMSegVsnTrk" + str, obj+"_nMSegVsNTrk"+str, 200,0,200,20,0,20);
    
    h_nAssocMSeg     = new TH1D(obj+"_nAssocMSeg_"+str  ,obj+"_nAssocMSeg_"+str   ,200,0,200);
    h_nMSTracklets   = new TH1D(obj+"_nTracklets_"+str  ,obj+"_nTracklets_"+str     ,20,0,20);
    h_cone_avgTracklets = new TH1D(obj+"_cone_avgTracklets_"+str , obj+"_cone_avgTracklets_"+str , 40,0,2);
    h_nMuonRoIs      = new TH1D(obj+"_nMuonRoIs_"+str  ,obj+"_nMuonRoIs_"+str   ,10,0,10);
    h_pT             = new TH1D(obj+"_pT_"+str    ,obj+"_pT_"+str     ,nBins*2,0,5000);
    h_ET             = new TH1D(obj+"_ET_"+str    ,obj+"_ET_"+str     ,nBins*2,0,5000);
    h_logRatio       = new TH1D(obj+"_logRatio_"+str,obj+"_logRatio_"+str ,60,-3.,3.);
    h_nMSVx          = new TH1D(obj+"_nMSVx_"+str ,obj+"_nMSVx_"+str  ,3,0,3);
    h_MSVx_nMDT      = new TH1D(obj+"_MSVx_nMDT_"+str ,obj+"_MSVx_nMDT_"+str  ,nBins,0,5000);
    
    h_width->Sumw2(); h_eta_width->Sumw2(); h_pT_width->Sumw2(); h_width_nAssocMSeg->Sumw2(); h_width_nMSTracklets->Sumw2(); h_width_nMuonRoIs->Sumw2();
    h_pT_denom_all->Sumw2();
    h_eta->Sumw2();h_phi->Sumw2();h_eta_nAssocMSeg->Sumw2();h_eta_nMSTracklets->Sumw2();h_nAssocMSeg->Sumw2();h_nMSTracklets->Sumw2(); 
    h_nMuonRoIs->Sumw2();h_pT->Sumw2();h_ET->Sumw2();h_logRatio->Sumw2();h_nMSVx->Sumw2();h_MSVx_nMDT->Sumw2();
    h_eta_nMuonRoIs->Sumw2(); 
    h_pT_nAssocMSeg->Sumw2(); h_pT_nMSTracklets->Sumw2(); h_pT_nMuonRoIs->Sumw2();
    h_pT_nAssocMSeg_num->Sumw2(); h_pT_nMSTracklets_num->Sumw2(); h_pT_nMuonRoIs_num->Sumw2();
    h_pT_nAssocMSeg_sq->Sumw2(); h_pT_nMSTracklets_sq->Sumw2(); h_pT_nMuonRoIs_sq->Sumw2();
    h_nMSeg_nMSTracklets->Sumw2(); h_nMSeg_nMuonRoIs->Sumw2();
    h_eta_pMSVx->Sumw2();h_eta_pTrig->Sumw2(); h_pT_pMSVx->Sumw2(); h_pT_pTrig->Sumw2();
    h_nMSeg_denom->Sumw2();       h_nMSeg_pMSVx->Sumw2(); h_nMSeg_pTrig->Sumw2();
    h_eta_denom->Sumw2(); h_pT_denom->Sumw2(); h_pT_eta_denom->Sumw2(); h_pT_eta_pMSVx->Sumw2(); h_pT_eta_pTrig->Sumw2();//h_nMSegCone->Sumw2();
    
    h_CR_BDT = new TH1D(obj + "_CR_BDT_"+str,obj + "_CR_BDT_"+str, 100,-1,1 );
    h_CR_nTrk = new TH1D(obj + "_CR_nTrk_"+str,obj + "_CR_nTrk_"+str, 20,0,20 );
    h_CR_sumTrkpT = new TH1D(obj + "_CR_sumTrkpT_"+str,obj + "_CR_sumTrkpT_"+str, 100,0,50 );
    h_CR_maxTrkpT = new TH1D(obj + "_CR_maxTrkpT_"+str,obj + "_CR_maxTrkpT_"+str, 100,0,20 );
    h_CR_width = new TH1D(obj + "_CR_width_"+str,obj + "_CR_width_"+str, 100,0,0.5 );
    h_CR_jetDRTo2GeVTrack = new TH1D(obj + "_CR_jetdRTrk_"+str,obj + "_CR_jetdRTrk_"+str, 100,0,1 );
    h_CR_l1frac = new TH1D(obj + "_CR_l1frac_"+str,obj + "_CR_l1frac_"+str, 100,0,1 );
    h_CR_c_firenden =  new TH1D(obj + "_CR_c_firenden_"+str,obj + "_CR_c_firenden_"+str, 100,0,1 );
    h_CR_c_lat = new TH1D(obj + "_CR_c_lat_"+str,obj + "_CR_c_lat_"+str, 100,0,1 );
    h_CR_c_long= new TH1D(obj + "_CR_c_long_"+str,obj + "_CR_c_lat_"+str, 100,0,1 );
    h_CR_c_r = new TH1D(obj + "_CR_c_r_"+str,obj + "_CR_c_r_"+str, 200,0,8000 );
    h_CR_c_lambda = new TH1D(obj + "_CR_c_lambda_"+str,obj + "_CR_c_lambda_"+str, 200,0,5000 );
    h_CR_logR = new TH1D(obj + "_CR_logR_"+str,obj + "_CR_logR_"+str, 200,-5,5 );
    
    h_CR_logR_BDT = new TH1D(obj + "_CR_logR_BDT_"+str,obj + "_CR_logR_BDT_"+str, 200,-5,5 );
    h_CR_logR_nTrk = new TH1D(obj + "_CR_logR_nTrk_"+str,obj + "_CR_logR_nTrk_"+str, 200,-5,5 );
    h_CR_logR_sumTrkpT = new TH1D(obj + "_CR_logR_sumTrkpT_"+str,obj + "_CR_logR_sumTrkpT_"+str, 200,-5,5 );
    h_CR_logR_maxTrkpT = new TH1D(obj + "_CR_logR_maxTrkpT_"+str,obj + "_CR_logR_maxTrkpT_"+str, 200,-5,5 );
    h_CR_logR_width = new TH1D(obj + "_CR_logR_width_"+str,obj + "_CR_logR_width_"+str,200,-5,5 );
    h_CR_logR_jetDRTo2GeVTrack = new TH1D(obj + "_CR_logR_jetdRTrk_"+str,obj + "_CR_logR_jetdRTrk_"+str, 200,-5,5 );
    h_CR_logR_l1frac = new TH1D(obj + "_CR_logR_l1frac_"+str,obj + "_CR_logR_l1frac_"+str, 200,-5,5 );
    h_CR_logR_c_firenden =  new TH1D(obj + "_CR_logR_c_firenden_"+str,obj + "_CR_logR_c_firenden_"+str, 200,-5,5 );
    h_CR_logR_c_lat = new TH1D(obj + "_CR_logR_c_lat_"+str,obj + "_CR_logR_c_lat_"+str, 200,-5,5 );
    h_CR_logR_c_long= new TH1D(obj + "_CR_logR_c_long_"+str,obj + "_CR_logR_c_lat_"+str,200,-5,5 );
    h_CR_logR_c_r = new TH1D(obj + "_CR_logR_c_r_"+str,obj + "_CR_logR_c_r_"+str, 200,-5,5 );
    h_CR_logR_c_lambda = new TH1D(obj + "_CR_logR_c_lambda_"+str,obj + "_CR_logR_c_lambda_"+str, 200,-5,5 );
    
    h_CR_BDT->Sumw2();
    h_CR_nTrk->Sumw2();
    h_CR_sumTrkpT->Sumw2();
    h_CR_maxTrkpT->Sumw2();
    h_CR_width->Sumw2();
    h_CR_jetDRTo2GeVTrack->Sumw2();
    h_CR_l1frac->Sumw2();
    h_CR_c_firenden->Sumw2();
    h_CR_c_lat->Sumw2();
    h_CR_c_long->Sumw2();
    h_CR_c_r->Sumw2();
    h_CR_c_lambda->Sumw2();
    h_CR_logR->Sumw2();
    h_CR_logR_BDT->Sumw2();
    h_CR_logR_nTrk->Sumw2();
    h_CR_logR_sumTrkpT->Sumw2();
    h_CR_logR_maxTrkpT->Sumw2();
    h_CR_logR_width->Sumw2();
    h_CR_logR_jetDRTo2GeVTrack->Sumw2();
    h_CR_logR_l1frac->Sumw2();
    h_CR_logR_c_firenden->Sumw2();
    h_CR_logR_c_lat->Sumw2();
    h_CR_logR_c_long->Sumw2();
    h_CR_logR_c_r->Sumw2();
    h_CR_logR_c_lambda->Sumw2();
    
    return;
    
}
void SystHistograms::initializeCRHistograms(TString obj, TString str){
    
    std::cout << "initializing systematics histograms...  " << std::endl;
    
    h_CR_BDT = new TH1D(obj + "_CR_BDT_"+str,obj + "_CR_BDT_"+str, 100,-1,1 );
    h_CR_nTrk = new TH1D(obj + "_CR_nTrk_"+str,obj + "_CR_nTrk_"+str, 20,0,20 );
    h_CR_sumTrkpT = new TH1D(obj + "_CR_sumTrkpT_"+str,obj + "_CR_sumTrkpT_"+str, 100,0,50 );
    h_CR_maxTrkpT = new TH1D(obj + "_CR_maxTrkpT_"+str,obj + "_CR_maxTrkpT_"+str, 100,0,20 );
    h_CR_width = new TH1D(obj + "_CR_width_"+str,obj + "_CR_width_"+str, 100,0,0.5 );
    h_CR_jetDRTo2GeVTrack = new TH1D(obj + "_CR_jetdRTrk_"+str,obj + "_CR_jetdRTrk_"+str, 100,0,1 );
    h_CR_l1frac = new TH1D(obj + "_CR_l1frac_"+str,obj + "_CR_l1frac_"+str, 100,0,1 );
    h_CR_c_firenden =  new TH1D(obj + "_CR_c_firenden_"+str,obj + "_CR_c_firenden_"+str, 100,0,1 );
    h_CR_c_lat = new TH1D(obj + "_CR_c_lat_"+str,obj + "_CR_c_lat_"+str, 100,0,1 );
    h_CR_c_long= new TH1D(obj + "_CR_c_lat_"+str,obj + "_CR_c_lat_"+str, 100,0,1 );
    h_CR_c_r = new TH1D(obj + "_CR_c_r_"+str,obj + "_CR_c_r_"+str, 200,0,8000 );
    h_CR_c_lambda = new TH1D(obj + "_CR_c_lambda_"+str,obj + "_CR_c_lambda_"+str, 200,0,5000 );
    h_CR_logR = new TH1D(obj + "_CR_logR_"+str,obj + "_CR_logR_"+str, 200,-5,5 );
    
    h_CR_logR_BDT = new TH1D(obj + "_CR_logR_BDT_"+str,obj + "_CR_logR_BDT_"+str, 200,-5,5 );
    h_CR_logR_nTrk = new TH1D(obj + "_CR_logR_nTrk_"+str,obj + "_CR_logR_nTrk_"+str, 200,-5,5 );
    h_CR_logR_sumTrkpT = new TH1D(obj + "_CR_logR_sumTrkpT_"+str,obj + "_CR_logR_sumTrkpT_"+str,200,-5,5 );
    h_CR_logR_maxTrkpT = new TH1D(obj + "_CR_logR_maxTrkpT_"+str,obj + "_CR_logR_maxTrkpT_"+str, 200,-5,5 );
    h_CR_logR_width = new TH1D(obj + "_CR_logR_width_"+str,obj + "_CR_logR_width_"+str, 200,-5,5 );
    h_CR_logR_jetDRTo2GeVTrack = new TH1D(obj + "_CR_logR_jetdRTrk_"+str,obj + "_CR_logR_jetdRTrk_"+str,200,-5,5 );
    h_CR_logR_l1frac = new TH1D(obj + "_CR_logR_l1frac_"+str,obj + "_CR_logR_l1frac_"+str, 200,-5,5 );
    h_CR_logR_c_firenden =  new TH1D(obj + "_CR_logR_c_firenden_"+str,obj + "_CR_logR_c_firenden_"+str, 200,-5,5 );
    h_CR_logR_c_lat = new TH1D(obj + "_CR_logR_c_lat_"+str,obj + "_CR_logR_c_lat_"+str, 200,-5,5 );
    h_CR_logR_c_long= new TH1D(obj + "_CR_logR_c_lat_"+str,obj + "_CR_logR_c_lat_"+str, 200,-5,5 );
    h_CR_logR_c_r = new TH1D(obj + "_CR_logR_c_r_"+str,obj + "_CR_logR_c_r_"+str, 200,-5,5 );
    h_CR_logR_c_lambda = new TH1D(obj + "_CR_logR_c_lambda_"+str,obj + "_CR_logR_c_lambda_"+str, 200,-5,5 );
    
    h_CR_BDT->Sumw2();
    h_CR_nTrk->Sumw2();
    h_CR_sumTrkpT->Sumw2();
    h_CR_maxTrkpT->Sumw2();
    h_CR_width->Sumw2();
    h_CR_jetDRTo2GeVTrack->Sumw2();
    h_CR_l1frac->Sumw2();
    h_CR_c_firenden->Sumw2();
    h_CR_c_lat->Sumw2();
    h_CR_c_long->Sumw2();
    h_CR_c_r->Sumw2();
    h_CR_c_lambda->Sumw2();
    h_CR_logR->Sumw2();
    h_CR_logR_BDT->Sumw2();
    h_CR_logR_nTrk->Sumw2();
    h_CR_logR_sumTrkpT->Sumw2();
    h_CR_logR_maxTrkpT->Sumw2();
    h_CR_logR_width->Sumw2();
    h_CR_logR_jetDRTo2GeVTrack->Sumw2();
    h_CR_logR_l1frac->Sumw2();
    h_CR_logR_c_firenden->Sumw2();
    h_CR_logR_c_lat->Sumw2();
    h_CR_logR_c_long->Sumw2();
    h_CR_logR_c_r->Sumw2();
    h_CR_logR_c_lambda->Sumw2();
    
    return;
    
}
/*void SystHistograms::fill(TString name, double value, double weight){
    
    
    return;
    
}*/
