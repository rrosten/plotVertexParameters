

#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/LLPVariables.h"
#include "PlottingPackage/CommonVariables.h"
#include "PlottingPackage/VertexVariables.h"
#include "PlottingPackage/TriggerVariables.h"
#include "PlottingPackage/EfficiencyHistograms.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/SystHistograms.h"
#include "PlottingPackage/HistTools.h"
#include "PlottingPackage/RPCScaling.h"

#include "TVector3.h"
//#include "PlottingPackage/PlottingUtils.h"
//#include "CommonUtils/MathUtils.h"
#include "TChain.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "TLorentzVector.h"
#include <TString.h>

using namespace std;
using namespace plotVertexParameters;

double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;

}
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

int isSignal; int isData;
double jetSliceWeights[13];
TStopwatch timer;
int nEvt_passedNoIsoOffIso;
int nEvt_passedIso;
int nEvt_passedNoIsoOffIso_Barrel;
int nEvt_passedIso_Barrel;
int nEvt_passedNoIsoOffIso_EndCaps;
int nEvt_passedIso_EndCaps;
//
double nMSVx;
double nBMSVx;
double nEMSVx;
double nMSVx_passHitQual;
double nMSVx_passJetIso;
double nMSVx_passTrkIso;
double nMSVx_passGVC;
double nBMSVx_passJetIso03;
double nBMSVx_passJetIso06;
double nBMSVx_passJetIso10;
double nEMSVx_passJetIso03;
double nEMSVx_passJetIso06;
double nEMSVx_passJetIso10;
//
int nEvt_MoreThanOneVtx;
int nEvt_MoreThanOneVtx_noiso;

double nEventsPassTrigger;

int main(int argc, char **argv){

    std::cout << "running program!" << std::endl;

    TChain *chain = new TChain("recoTree");

    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
    std::cout << "Output File name is: " << argv[3] << std::endl;
    bool applyTriggerScaleFactor = (TString(argv[4]) == "true") ? true : false;
    std::cout << "Apply trigger scale factor? " << argv[4] << " (" << applyTriggerScaleFactor << ")" << std::endl;
    if(!applyTriggerScaleFactor){
        std::cout << "WARNING: SCALE FACTOR NOT APPLIED! CAREFUL WITH THE RESULTS!" << std::endl;
        std::cout << "Also, the tracklet cut is not implemented" << std::endl;
    }
    std::cout << "Systematics are -1/0/+1? " << argv[5] << std::endl;
    bool doPDFSystematics = (TString(argv[6]) == "true") ? true : false;
    std::cout << "PDF Systematics are: " << argv[6] << " ("<< doPDFSystematics << ")" << std::endl;
    int nPDFs = doPDFSystematics ? 101 : 0;
    bool doPileupSystematics = (TString(argv[7]) == "true") ? true : false;
    std::cout << "Pileup systematics are: " << argv[7] << " (" << doPileupSystematics << ")" << std::endl;
    bool applyVertexScaleFactor = (TString(argv[8]) == "true") ? true : false;
    std::cout << "Apply vertex scale factor? " << argv[8] << " (" << applyVertexScaleFactor << ")" << std::endl;
    int SYST_VAL = atoi(argv[5]);
    
    InputFiles::AddFilesToChain(TString(argv[1]), chain);

    //Directory the plots will go in

    TString plotDirectory = TString(argv[2]);
    TFile *outputFile = new TFile(plotDirectory+"/"+TString(argv[3]),"RECREATE");

    TriggerVariables *trig = new TriggerVariables;
    CommonVariables *commonVar = new CommonVariables;
    VertexVariables *vxVar = new VertexVariables;
    LLPVariables *llpVar = new LLPVariables;

    std::map<ULong64_t, int> msvxMap;
    ULong64_t eventNumber = 0;

    TChain *vxChain = new TChain("recoTree");
    if(applyVertexScaleFactor){
        InputFiles::AddVertexRerunFiles(TString(argv[1]), vxChain);
        vxChain->SetBranchStatus("*",0);
        msvxMap = CreateMap(vxChain);
        //have to have the next few lines here or it segfaults, because it's used in CreateMap.
        //I don't fully understand why SetBranchStatus("*",0) doesn't get rid of it.
        vxChain->SetBranchStatus("*",1);
        vxChain->SetBranchAddress("EventNumber", &eventNumber);
    }
    EfficiencyHistograms *effHistMSTrig = new EfficiencyHistograms;
    RPCScaling rpcSc;
    setPrettyCanvasStyle();

    std::cout << "set canvas style" << std::endl;

    TH1::SetDefaultSumw2();

    isSignal = -1;
    chain->SetBranchStatus("*",0);
    trig->setZeroVectors();
    trig->setBranchAdresses(chain, applyTriggerScaleFactor);
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain, true, false, false ,true, doPDFSystematics);
    llpVar->setZeroVectors();
    llpVar->setBranchAddresses(chain);
    vxVar->setZeroVectors(true);
    vxVar->setBranchAdresses(applyVertexScaleFactor ? vxChain : chain, applyVertexScaleFactor, true);

    //



    char name[40];
    if(doPDFSystematics){
        for(int i=0;i < nPDFs; i++){
            sprintf(name,"%s%d","MSTrig_1B_Lxy_",i);
            effHistMSTrig->addHist(TString(name),40,0,10);
            sprintf(name,"%s%d","MSTrig_1E_Lz_",i);
            effHistMSTrig->addHist(name,60,0,15);
        }
    }
    if(doPileupSystematics){
        effHistMSTrig->addHist("MSTrig_1B_Lxy_PRW_1up",40,0,10);
        effHistMSTrig->addHist("MSTrig_1B_Lxy_PRW_1down",40,0,10);
        effHistMSTrig->addHist("MSTrig_1E_Lz_PRW_1up",60,0,15);
        effHistMSTrig->addHist("MSTrig_1E_Lz_PRW_1down",60,0,15);
    }

    effHistMSTrig->addHist("MSTrig_1B_Lxy",40,0,10);
    effHistMSTrig->addHist("MSTrig_1E_Lz",60,0,15);

    effHistMSTrig->addHist("MSTrig_BB",10,0,10,10,0,10);
    effHistMSTrig->addHist("MSTrig_BE",15,0,15,10,0,10);
    effHistMSTrig->addHist("MSTrig_EE",15,0,15,15,0,15);

    HistTools *hists = new HistTools;
    hists->addHist("LLP_trig_dR_B",200,0,2);
    hists->addHist("LLP_trig_dR_E",200,0,2);
    hists->addHist("Trig_jet_dR_B",200,0,5);
    hists->addHist("Trig_jet_dR_E",200,0,5);
    hists->addHist("Trig_trk_dR_B",200,0,5);
    hists->addHist("Trig_trk_dR_E",200,0,5);
    hists->addHist("LLP_closestJet_dR_Lxy",50,0,5,200,0,1);

    nEventsPassTrigger=0;

    timer.Start();

    setJetSliceWeights(jetSliceWeights);

    int jetSlice = -1;
    double finalWeight = 0;
    bool debug = false;


    for (int l=0;l<chain->GetEntries();l++)
    {

        if(l % 50000 == 0){
            timer.Stop();
            std::cout << "event " << l  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }
        if(l == 74424 && TString(argv[1]) == "mH100mS8lt5") continue;

        if(applyVertexScaleFactor) {
            vxChain->GetEntry(msvxMap[commonVar->eventNumber]);
        }
        chain->GetEntry(l);

        TString inputFile = TString(chain->GetCurrentFile()->GetName());

        //TString inputFile = chain->GetFile()->GetName();
        if( l % 100000 == 0 ) std::cout << "running over file: " << inputFile << std::endl;
        if(   inputFile.Contains("JZ")    ){
            isSignal = 0; isData = 0;
            if(inputFile.Contains("JZ0W")) jetSlice = 0;
            if(inputFile.Contains("JZ1W")) jetSlice = 1;
            if(inputFile.Contains("JZ2W")) jetSlice = 2;
            if(inputFile.Contains("JZ3W")) jetSlice = 3;
            if(inputFile.Contains("JZ4W")) jetSlice = 4;
            if(inputFile.Contains("JZ5W")) jetSlice = 5;
            if(inputFile.Contains("JZ6W")) jetSlice = 6;
            if(inputFile.Contains("JZ7W")) jetSlice = 7;
            if(inputFile.Contains("JZ8W")) jetSlice = 8;
            if(inputFile.Contains("JZ9W")) jetSlice = 9;
            if(inputFile.Contains("JZ10W")) jetSlice = 10;
            if(inputFile.Contains("JZ11W")) jetSlice = 11;
            if(inputFile.Contains("JZ12W")) jetSlice = 12;
        }
        if( inputFile.Contains("LongLived") || inputFile.Contains("_LLP") || inputFile.Contains("ChiChi") || inputFile.Contains("Stealth")){ isSignal = 1; isData = 0; }
        if( inputFile.Contains("data15_13TeV")){ isSignal=0; isData = 1;}
        if(!isData){
            if(isSignal == 0 ) finalWeight = jetSliceWeights[jetSlice] * commonVar->eventWeight;
            else if( isSignal == 1 ) finalWeight = commonVar->pileupEventWeight;
            else std::cout << "what on earth are you running on? Input file is " << inputFile << std::endl;
            if(l==0) std::cout << "isSignal: " << isSignal << std::endl;
        }
        else if(isData) finalWeight = 1.0;

        int nBarrel=0; int nEndcap = 0; int nID = 0;
        int bIndex = -1; int eIndex = -1;
        std::vector<int> llpMatching;
        if(applyVertexScaleFactor){
            if(l==0){
                vxVar->rnd.SetSeed(llpVar->pT->at(1));
            }
        }
        //fill denominators
        bool eventHasLT40_Tracklets = true;
        if(applyVertexScaleFactor){
            if(vxVar->countScaledTracklets() > 40) eventHasLT40_Tracklets = false;
        } else {
            if(vxVar->tracklet_eta->size() > 40) eventHasLT40_Tracklets = false;
        }

        bool is2J150Event = commonVar->eventIs2j150();
        if(debug) std::cout << "is this a 2J150 event? " << is2J150Event << std::endl;

        if(isSignal){
            llpVar->set_pz();llpVar->set_Lxyz();
            for(unsigned int j=0; j<llpVar->eta->size(); j++){

                llpMatching.push_back(0);
                if( (fabs(llpVar->eta->at(j)) < 0.8) && llpVar->Lxy->at(j) > 3000. && llpVar->Lxy->at(j) < 8000.){
                    nBarrel++; bIndex = j;
                }
                else if( (fabs(llpVar->eta->at(j)) > 1.3) &&(fabs(llpVar->eta->at(j))< 2.5)&& llpVar->Lz->at(j) > 5000. && llpVar->Lz->at(j) < 15000. && llpVar->Lxy->at(j) < 10000.){
                    nEndcap++;eIndex = j;
                }
                else if( fabs(llpVar->eta->at(j)) < 2.5 && llpVar->Lxy->at(j) < 275. && llpVar->Lz->at(j) < 840.){
                    nID++;
                }
            }
            //std::cout << "made it through llp loop" << std::endl;
            //if(Lxy->size() == 2){
            if(nBarrel == 1 && nEndcap ==0 ){
                effHistMSTrig->fill("MSTrig_1B_Lxy_denom",llpVar->Lxy->at(bIndex)*0.001,finalWeight);
                if(doPDFSystematics) {
                    for(int i_pdf=0;i_pdf < nPDFs; i_pdf++){
                        sprintf(name,"%s%d%s","MSTrig_1B_Lxy_",i_pdf,"_denom");
                        effHistMSTrig->fill(TString(name),llpVar->Lxy->at(bIndex)*0.001,finalWeight*commonVar->pdfWeights->at(i_pdf));
                    }
                }
                if(doPileupSystematics){
                    effHistMSTrig->fill("MSTrig_1B_Lxy_PRW_1up_denom",llpVar->Lxy->at(bIndex)*0.001,commonVar->prw_1up);
                    effHistMSTrig->fill("MSTrig_1B_Lxy_PRW_1down_denom",llpVar->Lxy->at(bIndex)*0.001,commonVar->prw_1down);
                }
            }
            else if(nBarrel == 1 && nEndcap == 1){
                effHistMSTrig->fill("MSTrig_BE_denom",fabs(llpVar->Lz->at(eIndex))*0.001,fabs(llpVar->Lxy->at(bIndex))*0.001,finalWeight);

            }
            else if(nBarrel == 2 ){
                if(llpVar->Lxy->at(0) < llpVar->Lxy->at(1)) {
                    effHistMSTrig->fill("MSTrig_BB_denom",llpVar->Lxy->at(1)*0.001,llpVar->Lxy->at(0)*0.001,finalWeight);
                }
                else {
                    effHistMSTrig->fill("MSTrig_BB_denom",llpVar->Lxy->at(0)*0.001,llpVar->Lxy->at(1)*0.001,finalWeight);
                }
            }
            else if(nBarrel == 0 && nEndcap ==1){
                effHistMSTrig->fill("MSTrig_1E_Lz_denom",fabs(llpVar->Lz->at(eIndex))*0.001,finalWeight);
                if(doPDFSystematics) {
                    for(int i_pdf=0;i_pdf < nPDFs; i_pdf++){
                        sprintf(name,"%s%d%s","MSTrig_1E_Lz_",i_pdf,"_denom");
                        effHistMSTrig->fill(TString(name),fabs(llpVar->Lz->at(eIndex))*0.001,finalWeight*commonVar->pdfWeights->at(i_pdf));
                    }
                }
                if(doPileupSystematics){
                    effHistMSTrig->fill("MSTrig_1E_Lz_PRW_1up_denom",fabs(llpVar->Lz->at(eIndex))*0.001,commonVar->prw_1up);
                    effHistMSTrig->fill("MSTrig_1E_Lz_PRW_1down_denom",fabs(llpVar->Lz->at(eIndex))*0.001,commonVar->prw_1down);
                }
            }
            else if(nEndcap == 2){
                if(llpVar->Lz->at(0) < llpVar->Lz->at(1)) {
                    effHistMSTrig->fill("MSTrig_EE_denom",fabs(llpVar->Lz->at(1))*0.001,fabs(llpVar->Lz->at(0))*0.001,finalWeight);
                }
                else {
                    effHistMSTrig->fill("MSTrig_EE_denom",fabs(llpVar->Lz->at(0))*0.001,fabs(llpVar->Lz->at(1))*0.001,finalWeight);
                }
            }

        }//end signal loop

        //std::cout << " filled denominators " << std::endl;
        //make some trigger study plots
        //fill some trigger isolation variables with OFFLINE TRACKS AND JETS as a schematic, for now
        if(trig->eta->size() > 0){
            commonVar->fixJetET();
            //trig->testTruthMatch(llpVar->eta, llpVar->phi);
            trig->performJetIsolation(commonVar->Jet_ET, commonVar->Jet_eta, commonVar->Jet_phi, commonVar->Jet_logRatio);
            trig->performTrackIsolation(commonVar->Track_pT, commonVar->Track_eta, commonVar->Track_phi);
        }
        for(unsigned int i_trig = 0; i_trig < trig->eta->size(); i_trig++){
            if(TMath::Abs(trig->eta->at(i_trig)) < 1.0){
                hists->fill("LLP_trig_dR_B",trig->LLP_dR->at(i_trig), finalWeight);
                hists->fill("Trig_jet_dR_B",trig->jetdR->at(i_trig), finalWeight);
                hists->fill("Trig_trk_dR_B",trig->trackdR->at(i_trig), finalWeight);
            } else {
                hists->fill("LLP_trig_dR_E",trig->LLP_dR->at(i_trig), finalWeight);
                hists->fill("Trig_jet_dR_E",trig->jetdR->at(i_trig), finalWeight);
                hists->fill("Trig_trk_dR_E",trig->trackdR->at(i_trig), finalWeight);
            }
        }

        //make the scale-factored clusters!
        double trig_eta =-99;
        double trig_phi = -99;//trig->phi->at(0);
        if(applyTriggerScaleFactor){
            trig->reclusterRoIs();
            //std::cout << "number of new clusters: " << trig->cluSyst->size() << std::endl;

            for(unsigned int i=0; i<trig->cluEta->size(); i++){
                if(trig->cluSyst->at(i) == SYST_VAL){
                    trig_eta = trig->cluEta->at(i);
                    trig_phi = trig->cluPhi->at(i);
                    break; //pick out the FIRST in the list - what the trigger sees....
                }
            }       
        } else if(commonVar->passMuvtx_noiso){
            trig_eta = trig->eta->at(0);
            trig_phi = trig->phi->at(0);
        }
        if(trig_eta > -90){ //skip events where the trigger doesn't actually pass
            nEventsPassTrigger+=finalWeight;
            /*int isEndcap = 0;
            int isBarrel = 0;
            if ( fabs(trig_eta)<1.0 ){
                isBarrel = 1;
                //std::cout << "trigger in barrel! " <<std::endl;
            }
            else if ( fabs(trig_eta)>= 1.0 && fabs(trig_eta) < 2.5 ){
                isEndcap = 1;// for endcaps
                //std::cout << "Trigger in endcaps!" << std::endl;
            }
            else{
                //std::cout << "Trigger found, but in neither barrel nor endcaps" << std::endl;
                continue;
            }*/
            int nTriggerLLPMatches = 0;
            double rpcScaleFactor[2] = {1.0,1.0};
            double rpcUnscaleFactor[2] = {1.0,1.0};
            for(unsigned int j=0; j<llpVar->Lxy->size(); j++){
                double dR2_trig_llp = DeltaR2(llpVar->phi->at(j),trig_phi,llpVar->eta->at(j),trig_eta);
                if (dR2_trig_llp > 0.16) continue;

                //we know this trigger is matched to an LLP: figure out where it is!
                nTriggerLLPMatches++;
                bool isllpBarrel = false; bool isllpEndcap = false;

                rpcScaleFactor[j] = rpcSc.scaleMCtoData(llpVar->Lxyz->at(j), llpVar->beta->at(j));
                rpcScaleFactor[j] = rpcSc.unscaleMC(llpVar->Lxyz->at(j), llpVar->beta->at(j));
                if((fabs(llpVar->eta->at(j)) < 0.8) && llpVar->Lxy->at(j) > 3000. && llpVar->Lxy->at(j) < 8000.){ isllpBarrel=true;

                }
                if( (fabs(llpVar->eta->at(j))> 1.3) &&(fabs(llpVar->eta->at(j))< 2.5) && llpVar->Lz->at(j)>5000.
                        && llpVar->Lz->at(j)<15000. && llpVar->Lxy->at(j)<10000.){
                    isllpEndcap=true;
                }
                //std::cout << "llp matched to trigger in b/e: " << isllpBarrel << "/" << isllpEndcap << std::endl;
                if(nBarrel == 1 && nEndcap == 0 && isllpBarrel){
                    effHistMSTrig->fill("MSTrig_1B_Lxy_ALL",llpVar->Lxy->at(j)*0.001,finalWeight);
                    if(doPDFSystematics) {
                        for(int i_pdf=0;i_pdf < nPDFs; i_pdf++){
                            sprintf(name,"%s%d","MSTrig_1B_Lxy_",i_pdf);
                            effHistMSTrig->fill(TString(name),llpVar->Lxy->at(j)*0.001,rpcScaleFactor[j]*finalWeight*commonVar->pdfWeights->at(i_pdf));
                        }
                    }
                    if(doPileupSystematics){
                        effHistMSTrig->fill("MSTrig_1B_Lxy_PRW_1up",llpVar->Lxy->at(j)*0.001,rpcScaleFactor[j]*commonVar->prw_1up);
                        effHistMSTrig->fill("MSTrig_1B_Lxy_PRW_1down",llpVar->Lxy->at(j)*0.001,rpcScaleFactor[j]*commonVar->prw_1down);
                    }
                    if(eventHasLT40_Tracklets){
                        effHistMSTrig->fill("MSTrig_1B_Lxy",llpVar->Lxy->at(j)*0.001,rpcScaleFactor[j]*finalWeight);
                    }
                }
                else if(nBarrel == 0 && nEndcap ==1 && isllpEndcap){
                    effHistMSTrig->fill("MSTrig_1E_Lz_ALL",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                    if(eventHasLT40_Tracklets){ 
                        effHistMSTrig->fill("MSTrig_1E_Lz",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                    }
                    if(doPDFSystematics) {
                        for(int i_pdf=0;i_pdf < nPDFs; i_pdf++){
                            sprintf(name,"%s%d","MSTrig_1E_Lz_",i_pdf);
                            effHistMSTrig->fill(TString(name),llpVar->Lz->at(j)*0.001,finalWeight*commonVar->pdfWeights->at(i_pdf));
                        }
                    }
                    if(doPileupSystematics){
                        effHistMSTrig->fill("MSTrig_1E_Lz_PRW_1up",fabs(llpVar->Lz->at(j))*0.001,commonVar->prw_1up);
                        effHistMSTrig->fill("MSTrig_1E_Lz_PRW_1down",fabs(llpVar->Lz->at(j))*0.001,commonVar->prw_1down);
                    }
                }
            }
            if(nTriggerLLPMatches && eventHasLT40_Tracklets){
                if(nBarrel == 1 && nEndcap == 1){
                    effHistMSTrig->fill("MSTrig_BE",fabs(llpVar->Lz->at(eIndex))*0.001,fabs(llpVar->Lxy->at(bIndex))*0.001,finalWeight);
                }
                else if(nBarrel == 2 ){
                    double minRPCTime = std::min(rpcUnscaleFactor[0],rpcUnscaleFactor[1]);
                    if(llpVar->Lxy->at(0) < llpVar->Lxy->at(1)) {
                        effHistMSTrig->fill("MSTrig_BB",llpVar->Lxy->at(1)*0.001,llpVar->Lxy->at(0)*0.001,minRPCTime*finalWeight);
                    }
                    else {
                        effHistMSTrig->fill("MSTrig_BB",llpVar->Lxy->at(0)*0.001,llpVar->Lxy->at(1)*0.001,minRPCTime*finalWeight);
                    }
                }
                else if(nEndcap == 2){
                    if(llpVar->Lz->at(0) < llpVar->Lz->at(1)) {
                        effHistMSTrig->fill("MSTrig_EE",fabs(llpVar->Lz->at(1))*0.001,fabs(llpVar->Lz->at(0))*0.001,finalWeight);
                    }
                    else {
                        effHistMSTrig->fill("MSTrig_EE",fabs(llpVar->Lz->at(0))*0.001,fabs(llpVar->Lz->at(1))*0.001,finalWeight);
                    }
                }
            }
            if(debug && nTriggerLLPMatches > 1){
                std::cout << " this trigger matched an LLP : " << nTriggerLLPMatches << " times " << std::endl;
                std::cout << " trigger eta,phi: " << trig_eta << ", " << trig_phi << ")" << std::endl;
                std::cout << " LLP 1 eta,phi: " << llpVar->eta->at(0) << ", " << llpVar->phi->at(0) << ")" << std::endl;
                std::cout << " LLP 2 eta,phi: " << llpVar->eta->at(0) << ", " << llpVar->phi->at(1) << ")" << std::endl;
            }
        }
        if(debug) std::cout << "done triggers" << std::endl;
        vxVar->clearAllVectors(true, applyVertexScaleFactor);
        commonVar->clearAllVectors(true, true, doPDFSystematics);
        llpVar->clearAllVectors();
        trig->clearAllVectors(applyTriggerScaleFactor);
        if(debug) std::cout << "vectors are cleared" << std::endl;
    }


    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;

    cout << "" << endl;
    cout << " nEntries                 = " << chain->GetEntries() << endl;
    cout << "" << endl;
    cout << "" << endl;
    cout << " nEventsPassTrigger = " << nEventsPassTrigger << endl;
    cout << "" << endl;

    effHistMSTrig->plotEfficiencies(plotDirectory);
    outputFile->Write();
}

