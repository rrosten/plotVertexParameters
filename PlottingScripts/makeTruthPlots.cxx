
TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){
    int numberOfSlashes = 0; int tmpSlashNumber = 0;
    int indexOf2ndLastSlash = -1;
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            numberOfSlashes++;
        }
    }
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            tmpSlashNumber++;
        }
        if(tmpSlashNumber == (numberOfSlashes - 1)){
            indexOf2ndLastSlash = charN;
            break;
        }
    }
	fileName = (TString)fileName(0+indexOf2ndLastSlash+1,fileName.Sizeof() - 24 - indexOf2ndLastSlash - 1);
    std::cout << "file name : " << fileName << std::endl;
	TString title = "";

	if(fileName.Contains("HChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        if(channel == "nubb") channel = "#nu b#bar{b}";
        TString mchi = (TString)fileName(fileName.First("5")+5,fileName.Sizeof());
        title = (TString) "H #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
		return title;
    } else if(fileName.Contains("WChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        TString mchi = (TString)fileName(fileName.First("m")+4,fileName.Sizeof());
        title = (TString) "W/Z #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    }else if(fileName.Contains("mS")){
		TString lt = "";
		TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
		TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
		if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
		if(mH == "125") title = (TString) "m_{H},m_{s}=[125,"+mS+"] GeV" + lt;
		else title = (TString) "m_{#Phi},m_{s}=["+mH+","+mS+"] GeV" + lt;
		return title;
	} else if(fileName.Contains("mg")){
		TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
		title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
	}
	return title;
}
void makeTruthPlots(){
    TString locn = "../OutputPlots/signalMC/";
    TFile* _sig[3];
    /*_sig[0] = new TFile(locn+"mg250/outputTruthParams.root");
    _sig[1] = new TFile(locn+"mg800/outputTruthParams.root");
    _sig[2] = new TFile(locn+"mg2000/outputTruthParams.root");*/
    _sig[0] = new TFile(locn+"mH125mS5lt5/outputTruthParams.root");
    _sig[1] = new TFile(locn+"mH125mS15lt5/outputTruthParams.root");
    _sig[2] = new TFile(locn+"mH125mS55lt5/outputTruthParams.root");
    bool add_ctau = false;    
    TString sigNames[3];
    int nSIG=3;
    std::cout << "file name: " << _sig[0]->GetName() << std::endl;
    for(int i=0; i<3; i++){ sigNames[i] = convertFiletoLabel(_sig[i]->GetName(), add_ctau);}

    Int_t markerSig[3] = {24,25,26};
    Int_t colorSig[3] = {kViolet -3, kTeal - 6, kBlue -3};
    if(nSIG < 4){
        colorSig[0] = kBlue - 3;
        colorSig[1] = kAzure + 5;
        colorSig[2] = kTeal - 6;
    }
        
    TString names[3];
    names[0] = "LLPdR";
    names[1] = "LLPdPhi";
    names[2] = "LLPdEta";

  TCanvas* c = new TCanvas("c_1","c_1",800,600);
    //vertex reco
    for(unsigned int i=0;i<3; i++){
        
        std::cout << "plot: " << names[i] << std::endl;
        
        TH1D* h_sig[30];
        
        TLegend *legS = new TLegend(0.2,0.8,0.5,0.9);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.04);
        
        for(int j=0; j<nSIG; j++){
            h_sig[j] = (TH1D*) _sig[j]->Get(names[i]);
            if(!h_sig[j] ) continue;
            h_sig[j]->SetMarkerColor(colorSig[j]);
            h_sig[j]->SetLineColor(colorSig[j]);
            h_sig[j]->SetMarkerStyle(markerSig[j]);
	    h_sig[j]->GetYaxis()->SetTitle("Fraction of events");
	    h_sig[j]->Rebin(2); 
	    h_sig[j]->Scale(1./h_sig[j]->Integral());
	   
            if(i==0)  h_sig[j]->GetXaxis()->SetTitle("LLP #Delta R");
            if(i==1)  h_sig[j]->GetXaxis()->SetTitle("LLP #Delta #phi");
            if(i==2)  h_sig[j]->GetXaxis()->SetTitle("LLP #Delta #eta");
            legS->AddEntry(h_sig[j],sigNames[j],"lp");
            if(j == 0){ 
		h_sig[j]->SetMaximum(1.2*h_sig[j]->GetMaximum());		
		h_sig[j]->Draw("");
	    }
            else h_sig[j]->Draw("SAME");

	}
	legS->Draw();
        gPad->RedrawAxis();
           
        c->Print("../OutputPlots/thesisPlots/"+names[i]+"_hss.pdf");
        legS->Clear();
    }


}
