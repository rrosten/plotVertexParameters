//
//  RPCScaling.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 06/10/16.
//
//

#include "RPCScaling.h"
#include "TMath.h"


double RPCScaling::effFunc(double *x,double *par)
{
    double efficiency = 0.5*(1.0 - TMath::Erf((x[0] - par[0])/(sqrt(2)*par[1])));
    return efficiency;
}
double RPCScaling::ratioFunc(double *x,double *par)
{
    double efficiencyUP = 0.5*(1.0 - TMath::Erf((x[0] - par[0])/(sqrt(2)*par[1])));
    double efficiencyDown = 0.5*(1.0 - TMath::Erf((x[0] - par[2])/(sqrt(2)*par[3])));
    return efficiencyUP/efficiencyDown;
}

double RPCScaling::getDeltaT(double Lxyz, double beta){
    return ( Lxyz/299.792 * (1.0/beta-1.0) );
}

double RPCScaling::scaleMCtoData(double deltaT){
    double xVal[1] = {deltaT};
    return ratioFunc(xVal,params);
}

double RPCScaling::unscaleMC(double deltaT){
    double xVal[1] = {deltaT};
    return 1./effFunc(xVal, paramsMC);
}

double RPCScaling::scaletoData(double deltaT){
    double xVal[1] = {deltaT};
    return effFunc(xVal, paramsData);
}

double RPCScaling::scaletoMC(double deltaT){
     double xVal[1] = {deltaT};
     return effFunc(xVal, paramsMC);
}
