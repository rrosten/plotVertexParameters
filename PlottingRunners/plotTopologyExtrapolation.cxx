//
//  plotTopologyExtrapolation.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 09/02/16.
//
//

#include "Riostream.h"
#include <iostream>


#include "PlottingPackage/LLPVariables.h"
#include "PlottingPackage/CommonVariables.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/ExtrapolationUtils.h"
#include <TRandom3.h>
#include "TVector3.h"

//#include "PlottingPackage/PlottingUtils.h"

//#include "CommonUtils/MathUtils.h"

#include "TChain.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "TLorentzVector.h"

using namespace std;
using namespace plotVertexParameters;

TRandom3 rnd;

double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;
    
}
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

int isSignal;
double jetSliceWeights[13];
TStopwatch timer;
int nEvt_passedNoIsoOffIso;
int nEvt_passedIso;
int nEvt_passedNoIsoOffIso_Barrel;
int nEvt_passedIso_Barrel;
int nEvt_passedNoIsoOffIso_EndCaps;
int nEvt_passedIso_EndCaps;
//
double nMSVx;
double nBMSVx;
double nEMSVx;
double nMSVx_passHitQual;
double nMSVx_passJetIso;
double nMSVx_passTrkIso;
double nMSVx_passGVC;
double nEvents;
double nEvents_VH;
double nEvents_VBF;
//
int nEvt_MoreThanOneVtx;
int nEvt_MoreThanOneVtx_noiso;

TH1F* decayPositions[6];

int main(int argc, char **argv){
    
    std::cout << "running program!" << std::endl;
    
    TChain *chain = new TChain("recoTree");
    
    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
    
    InputFiles::AddFilesToChain(TString(argv[1]), chain);
    
    //Directory the plots will go in
    
    TFile *scaleWeight = new TFile("ZHScaleFactors.root");
    TH2F *scaleHist = (TH2F*) scaleWeight->Get("higgsBetaEta");

    TFile *scaleWeightVBF = new TFile("VBFScaleFactors.root");
    TH2F *scaleHistVBF = (TH2F*) scaleWeightVBF->Get("higgsBetaEta");
    
    TString plotDirectory = TString(argv[2]);
    TFile *outputFile = new TFile(plotDirectory+"/outputExtrapolation.root","RECREATE");
    
    CommonVariables *commonVar = new CommonVariables;
    LLPVariables *llpVar = new LLPVariables;
    
    decayPositions[0] = new TH1F("prompt","prompt",90,-6,3);
    decayPositions[1] = new TH1F("1DV","1DV",90,-6,3);
    decayPositions[2] = new TH1F("2DV","2DV",90,-6,3);
    decayPositions[5] = new TH1F("MSDV","MSDV",90,-6,3); //strage overlap, ignore for now. 1DV MS = MET?!?
    decayPositions[3] = new TH1F("invisibleVH","invisibleVH",90,-6,3);
    decayPositions[4] = new TH1F("invisibleVBF","invisibleVBF",90,-6,3);
    
    for(int i=0;i<6;i++){
        BinLogX(decayPositions[i]);
        decayPositions[i]->Sumw2();
    }
    
    setPrettyCanvasStyle();
    
    std::cout << "set canvas style" << std::endl;
    
    TH1::SetDefaultSumw2();
    
    isSignal = -1;
    
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain);
    llpVar->setZeroVectors();
    llpVar->setBranchAdresses(chain);
    
    
    timer.Start();
    
    setJetSliceWeights(jetSliceWeights);
    
    int jetSlice = -1;
    double finalWeight = 0;
    nEvents = 0.; nEvents_VBF= 0.; nEvents_VH=0.;
    for (int l=0;l<chain->GetEntries();l++)
    {
        
        if(l % 10000 == 0){
            timer.Stop();
            std::cout << "event " << l  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }
        
        chain->GetEntry(l);
        
         nEvents += 1.0;   
        //if(MSVertex_eta->size() == 0) continue;
        
        TString inputFile = TString(chain->GetCurrentFile()->GetName());
        
        //TString inputFile = chain->GetFile()->GetName();
        //if( l % 10000 == 0 ) std::cout << "running over file: " << inputFile << std::endl;

        if( inputFile.Contains("LongLived") || inputFile.Contains("LLP") ) isSignal = 1;
        
        
        //if( nMSVx > 4) continue;
        
        if(isSignal == 0 ) std::cout <<" not running on signal, what is being extrapolated? "<< std::endl;
        else if( isSignal == 1 ) finalWeight = 1.0;
        else std::cout << "what on earth are you running on? Input file is " << inputFile << std::endl;
        if(l==0) std::cout << "isSignal: " << isSignal << std::endl;
        
        if(isSignal){
            if(l == 0) rnd.SetSeed(llpVar->beta->at(0));
            
            double bsq_1 = llpVar->beta->at(0) * llpVar->beta->at(0);
            double bg_1 = sqrt(1.0 / (1.0/bsq_1 - 1.0));
            
            double bsq_2 = llpVar->beta->at(1) * llpVar->beta->at(1);
            double bg_2 = sqrt(1.0 / (1.0/bsq_1 - 1.0));
            
            TVector3 llp1; llp1.SetPtEtaPhi(1.0,llpVar->eta->at(0),llpVar->phi->at(0));
            TVector3 llp2; llp2.SetPtEtaPhi(1.0,llpVar->eta->at(1),llpVar->phi->at(1));
            
            TLorentzVector p1; p1.SetPtEtaPhiE(llpVar->pT->at(0),llpVar->eta->at(0),llpVar->phi->at(0),llpVar->E->at(0));
            TLorentzVector p2; p2.SetPtEtaPhiE(llpVar->pT->at(1),llpVar->eta->at(1),llpVar->phi->at(1),llpVar->E->at(1));
            TLorentzVector higgs = p1+p2;//std::cout << "higgs 4-vec: " << higgs.E() << ", " << higgs.Beta() << ", " << higgs.Eta() << ", " << higgs.M() << std::endl;
            double weightZH = 1.0; double weightVBF = 1.0;
            if(TMath::Abs(higgs.Eta ()) < 5){
                int scaleBin = scaleHist->FindBin(higgs.Beta(), TMath::Abs(higgs.Eta()));
                weightZH = scaleHist->GetBinContent(scaleBin);// std::cout << "weight: " << weight << std::endl;
                weightVBF = scaleHistVBF->GetBinContent(scaleBin);
            }
            nEvents_VBF += weightVBF;
            nEvents_VH += weightZH;
            ExtrapolationUtils::fillLifetimeExtrapolations(rnd,bg_1,llp1,bg_2,llp2, decayPositions,weightVBF,weightZH);
            
        }
        commonVar->clearAllVectors();
    }
    
    
    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;
    
    cout << "" << endl;
    cout << " nEntries                 = " << chain->GetEntries() << endl;
    cout << "" << endl;
    cout << " nMS in total, pre any criteria  (b,e)    = " << nMSVx <<
    ", (" << nBMSVx << ", " << nEMSVx << ") " << endl;
    cout << " nMS passing all criteria      = " << nMSVx_passGVC << endl;
    cout << " nMS passing ~track isolation      = " << nMSVx_passTrkIso << endl;
    cout << " nMS passing jet isolation     = " << nMSVx_passJetIso << endl;
    
    cout << " nMS passing Hit quality = " << nMSVx_passHitQual              << endl;
    //
    cout << " nEvt with > 1 vtx                = " << nEvt_MoreThanOneVtx       << endl;
    cout << " nEvt with > 1 vtx (no isolation) = " << nEvt_MoreThanOneVtx_noiso << endl;
    
    
    // ** Make plot **
    //
    
    decayPositions[0]->SetLineColor(kBlack);decayPositions[0]->SetLineWidth(2);decayPositions[0]->SetLineStyle(2);
    decayPositions[1]->SetLineColor(kRed+3);decayPositions[1]->SetLineWidth(2);decayPositions[1]->SetLineStyle(1);
    decayPositions[2]->SetLineColor(kBlue-2);decayPositions[2]->SetLineWidth(2);decayPositions[2]->SetLineStyle(9);
    decayPositions[3]->SetLineColor(kGreen-3);decayPositions[3]->SetLineWidth(2);decayPositions[3]->SetLineStyle(7);
    decayPositions[4]->SetLineColor(kGreen+3);decayPositions[4]->SetLineWidth(2);decayPositions[4]->SetLineStyle(4);
        decayPositions[5]->SetLineColor(kCyan-6);decayPositions[5]->SetLineWidth(2);decayPositions[5]->SetLineStyle(2);
    
    for(int i=0;i<3;i++)decayPositions[i]->Scale(1./nEvents);
    decayPositions[3]->Scale(1./nEvents_VH);
    decayPositions[4]->Scale(1./nEvents_VBF);
    decayPositions[5]->Scale(1./nEvents);
    
    
    TString name = "n";
    TCanvas* c = new TCanvas("c_"+name,"c_"+name,800,600);
    decayPositions[0]->Draw("hist"); for(int i=1;i<6;i++) decayPositions[i]->Draw("hist same");
    decayPositions[0]->SetMaximum(10);
    decayPositions[0]->SetMinimum(0.0001);
    decayPositions[0]->GetXaxis()->SetRangeUser(0.00001,1000);
    decayPositions[0]->GetXaxis()->SetTitle("Proper lifetime (c*#tau) [m]");
    decayPositions[0]->GetYaxis()->SetTitle("Fiducial acceptance * #sigma_{prod} / #sigma_{prod,ggF}");
    decayPositions[0]->GetXaxis()->SetTitleOffset(1.2);
    decayPositions[0]->GetYaxis()->SetTitleOffset(1.2);
    decayPositions[0]->GetXaxis()->SetTitleSize(0.04);
    decayPositions[0]->GetYaxis()->SetTitleSize(0.04);
    c->SetLogy(); c->SetLogx();
    
    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.038);
    latex.SetTextAlign(13);  //align at top
    //
//    latex.DrawLatex(.2,.91,"#font[72]{ATLAS}  Internal");
    //latex.DrawLatex(.43,.86,"#sqrt{s} = 13 TeV, 78 pb^{-1}");
    //
    TLegend *leg = new TLegend(0.40,0.8,0.7,0.92);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.038);
    TLegend *leg2 = new TLegend(0.70,0.8,0.92,0.92);
    leg2->SetFillColor(0);
    leg2->SetBorderSize(0);
    leg2->SetTextSize(0.038);

    leg->AddEntry(decayPositions[0],"#font[42]{Prompt search}","l");
    leg->AddEntry(decayPositions[1],"#font[42]{1 DV + prompt}","l");
    leg->AddEntry(decayPositions[2],"#font[42]{2 DV}","l");
    leg2->AddEntry(decayPositions[5],"#font[42]{1 MSDV + inv}","l");
    leg2->AddEntry(decayPositions[3],"#font[42]{VH, H -> inv}","l");
    leg2->AddEntry(decayPositions[4],"#font[42]{VBF + H -> inv}","l");
    
    leg->Draw();
    leg2->Draw();
    
    gPad->RedrawAxis();
    
    //
    //c->Print(dir+"/"+name+".eps");
    c->Print(plotDirectory+"/extrap.png");
    c->Print(plotDirectory+"/extrap.pdf");
    c->Print(plotDirectory+"/extrap.root");

    
    outputFile->Write();
}
