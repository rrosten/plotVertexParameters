//
//  VertexVariables.h
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#ifndef plotVertexParameters_VertexVariables_h
#define plotVertexParameters_VertexVariables_h
#include <vector>
#include "TChain.h"
#include "TRandom3.h"
struct VertexVariables {
    
public:
    

    ////////////////////////////////////////////////////
    //*    Good vertexing criteria cuts in common    *//
    //* for vertices in the barrel or in the endcap *//
    const double m_GVC_BE_nMDTMin          = 300;
    const double m_GVC_BE_nMDTMax          = 2999;
    const double m_GVC_BE_nTrigHits        = 250; //RPC for barrel, TGC for endcaps
    const double m_GVC_BE_jetIso_Et        = 30;  // GeV
    const double m_GVC_BE_jetIso_logRatio  = 0.5;
    const double m_GVC_BE_sumTracks_deltaR2= 0.04;
    const double m_GVC_BE_trackIso_pT      = 5;   // GeV
    const double m_GVC_BE_sumTracks_pT      = 10;  // GeV

    ////////////////////////////////////
    //* Good vertexing criteria cuts *//
    //*  for vertices in the barrel,endcaps  *//
    const double m_GVC_jetIso_deltaR2[2]     = {0.09,0.36};
    const double m_GVC_trackIso_deltaR2[2]   = {0.09,0.36};
    const double m_GVC_IsoCone_deltaR[2]     = {0.3,0.6};
    
    TRandom3 rnd;
    const double m_bvx_sf = 0.55;
    const double m_evx_sf = 0.63;
    
    std::vector<double> *eta;
    std::vector<double> *phi;
    std::vector<double> *R;
    std::vector<double> *z;
    std::vector<int>    *nTrks;
    std::vector<int>    *nMDT;
    std::vector<int>    *nRPC;
    std::vector<int>    *nTGC;
    std::vector<int>    *passesHitThresholds;
    std::vector<int>    *passesJetIso;
    std::vector<int>    *passesTrackIso;
    std::vector<double> *closestdR;
    std::vector<double> *closestJetdR;
    std::vector<double> *closestTrackdR;
    std::vector<int>    *indexTrigRoI;
    std::vector<int>    *isGood;
    std::vector<int>    *isGoodABCD;
    std::vector<int>    *indexLLP;
    std::vector<double> *LLP_dR;
    std::vector<int>    *passReco;
    std::vector<int>    *region;
    std::vector<int>    *syst;
    
    std::vector<double> *tracklet_phi;
    std::vector<double> *tracklet_eta;
    std::vector<double> *mseg_phi;
    std::vector<double> *mseg_eta;
    
    int nTracklets;
    int countScaledTracklets();
    void FindGoodVertices(std::vector<double> &goodEta, std::vector<double> &goodPhi, std::vector<int> &goodRegion, std::vector<int> &index, int isSignal);
    void testHitThresholds();
    void performJetIsolation(std::vector<double> *Jet_ET,std::vector<double> *Jet_eta,std::vector<double> *Jet_phi,std::vector<double> *Jet_logRatio,std::vector<int> *Jet_passJVT);
    void checkJetIsolation(std::vector<double> *Jet_ET,std::vector<double> *Jet_eta,std::vector<double> *Jet_phi,std::vector<double> *Jet_logRatio,std::vector<int> *Jet_passJVT);
    void performTrackIsolation(std::vector<double> *Track_pT,std::vector<double> *Track_eta,std::vector<double> *Track_phi);
    void testIsGood(bool isRerun);
    double isolationVariable(double dR1, double dR2, double cut);
    void testTruthMatch(std::vector<double> *llp_eta, std::vector<double> *llp_phi);
    void testRoIMatch(const std::vector<double> *muRoI_eta, const std::vector<double> *muRoI_phi);
    void setZeroVectors(bool isRerun);
    void setZeroVectors(){ setZeroVectors(false);}
    void setBranchAdresses(TChain *chain, bool isRerun, bool useTracklets);
    void clearAllVectors();
    void clearAllVectors(bool useTracklets, bool isRerun);
    double wrapPhi(double phi);
    double DeltaR2(double phi1, double phi2, double eta1, double eta2);
    double DeltaR(double phi1, double phi2, double eta1, double eta2);

};


#endif
