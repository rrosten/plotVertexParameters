void makePosterSLJetPlot(){
	SetAtlasStyle();
        TCanvas* c1 = new TCanvas("c_1","c_1",800,600);
	c1->SetLogy(1);

	TChain* ch[4]; ch[1] = new TChain("recoTree");
ch[2] = new TChain("recoTree");
ch[3] = new TChain("recoTree");
	ch[1]->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v14/user.hrussell.mc15_13TeV.304903.MadGraphPythia8EvtGen_StealthSUSY_mG250.AOD_DVAna.r7772_dv14b_hist/*root*");
	ch[2]->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v14/user.hrussell.mc15_13TeV.304905.MadGraphPythia8EvtGen_StealthSUSY_mG800.AOD_DVAna.r7772_dv14b_hist/*root*");
	ch[3]->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v14/user.hrussell.mc15_13TeV.304907.MadGraphPythia8EvtGen_StealthSUSY_mG1500.AOD_DVAna.r7772_dv14b_hist/*root*");
	//ch[0] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/signalMC/mg1500/outputDataSearch_noTrigIso_unscaled_smallestdR.root");

 	TH1D* h[2][4];
    
    Int_t colorSig[4] = {kGray, kBlue-3, kViolet-6, kTeal-6};
	TLegend *leg = new TLegend(0.6,0.73,.92,0.91);
	leg->SetFillStyle(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.03);
TString label[4] = {"","m_{#tilde{g}} = 250 GeV","m_{#tilde{g}} = 800 GeV","m_{#tilde{g}} = 1500 GeV"};

	for(int i=1;i<4;i++){
		h[0][i] = new TH1D(TString("n"+to_string(i)),TString("n"+to_string(i)),50,0,1000);
		ch[i]->Project(TString("n"+to_string(i)), "CalibJet_pT[1]","CalibJet_isGoodStand[1] && Length$(MSVertex_indexTrigRoI > -1) > 0 && TMath::Abs(MSVertex_eta) < 0.8");		
		h[0][i]->Scale(1./h[0][i]->Integral());
		h[0][i]->SetLineColor(colorSig[i]);
		h[0][i]->SetLineStyle(i+1);
	
		if(i==1){ 

			h[0][i]->GetXaxis()->SetTitle("Subleading jet p_{T} [GeV]");
			h[0][i]->GetYaxis()->SetTitle("Fraction of events with an MS vertex");
//			h[0][i]->GetXaxis()->SetRangeUser(0,);
			h[0][i]->GetYaxis()->SetRangeUser(0.0001,1);

			h[0][i]->Draw("hist");
		}

		else{
		 h[0][i]->Draw("HIST SAME");
        	}
leg->AddEntry(h[0][i],label[i],"l");
	}
	leg->Draw();
	gPad->RedrawAxis();
	//ATLASLabel(0.19,0.88,"Work in progress", kBlack);
    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    //latex.SetTextSize(0.04);
    latex.SetTextAlign(13);  //align at top
    //latex.DrawLatex(.19,.87,"Simulation");
    
	c1->Print("subleadingjets_barrel.pdf");

}
