std::vector<std::pair<double, double>> findScaleFactors_oneBin(TString sample, TString type){
    std::vector<std::pair<double, double>> systematic;
    TString plotDir = TString("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/")+sample;

    std::vector<TString> histNames;

    if(type == "MSVx") {
        histNames.push_back("MSVx_Barrel_Lxy");
        histNames.push_back("MSVx_Endcap_Lz");
    } else if(type.Contains( "Trig")){
        histNames.push_back("MSTrig_1B_Lxy");
        histNames.push_back("MSTrig_1E_Lz");
    }
    TString mod = ""; if(type.Contains("Trig")) mod = "_40TrackletCut";
    TFile *f[3];
    f[0] = TFile::Open(TString("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/")+sample+"/output"+type+"Eff"+mod+".root");
    f[1] = TFile::Open(TString("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/")+sample+"/output"+type+"Eff_1up.root");
    f[2] = TFile::Open(TString("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/")+sample+"/output"+type+"Eff_1down.root");
    double b_up = 0; double b_down = 0; double e_up = 0; double e_down = 0;
    for (auto histName : histNames){

        TH1D *err_n[2];       TH1D *err_d[2];
        err_n[0] =  (TH1D*)f[1]->Get(histName);
        err_d[0] = (TH1D*) f[1]->Get(histName+"_denom");
        err_n[1] =  (TH1D*)f[2]->Get(histName);
        err_d[1] =  (TH1D*)f[2]->Get(histName+"_denom");
        TH1D *h1[2];
        h1[0] = (TH1D*)f[0]->Get(histName);
        h1[1] = (TH1D*)f[0]->Get(histName+"_denom");

        std::cout << "syst up: " << err_n[0]->Integral() / err_d[0]->Integral() << "/" << h1[0]->Integral() / h1[1]->Integral() << " - 1= " << (err_n[0]->Integral() / err_d[0]->Integral())/(h1[0]->Integral() / h1[1]->Integral()) - 1 << std::endl;
        std::cout << "syst down: 1 - " << err_n[1]->Integral() / err_d[1]->Integral() << "/" << h1[0]->Integral() / h1[1]->Integral() << " = " << 1 - (err_n[1]->Integral() / err_d[1]->Integral())/(h1[0]->Integral() / h1[1]->Integral()) << std::endl;

        if(histName.Contains("Lxy")){

            b_up =(err_n[0]->Integral() / err_d[0]->Integral())/(h1[0]->Integral() / h1[1]->Integral()) - 1 ;
            b_down = 1 - (err_n[1]->Integral() / err_d[1]->Integral())/(h1[0]->Integral() / h1[1]->Integral());
        } else {

            e_up =(err_n[0]->Integral() / err_d[0]->Integral())/(h1[0]->Integral() / h1[1]->Integral()) - 1 ;
            e_down = 1 - (err_n[1]->Integral() / err_d[1]->Integral())/(h1[0]->Integral() / h1[1]->Integral());
        }
    }        
    /*std::cout << "***** for pasting ******" << std::endl;
        std::cout << b_up << " " << e_up << std::endl;
        std::cout << b_down << " " << e_down << std::endl;
     */
    std::pair<double, double> bsysts = std::make_pair(b_up,b_down);
    std::pair<double, double> esysts = std::make_pair(e_up,e_down);
    systematic.push_back(bsysts);
    systematic.push_back(esysts);
    return systematic;
}

