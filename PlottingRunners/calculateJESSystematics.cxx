#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/LLPVariables.h"
#include "PlottingPackage/CommonVariables.h"
#include "PlottingPackage/VertexVariables.h"
#include "PlottingPackage/TriggerVariables.h"
#include "PlottingPackage/EfficiencyHistograms.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/SystHistograms.h"
#include "PlottingPackage/HistTools.h"

#include "TVector3.h"
//#include "PlottingPackage/PlottingUtils.h"
//#include "CommonUtils/MathUtils.h"
#include "TChain.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "TLorentzVector.h"
#include <TString.h>

using namespace std;
using namespace plotVertexParameters;

double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;

}
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

int isSignal; 
TStopwatch timer;

int main(int argc, char **argv){

    std::cout << "running program!" << std::endl;

    TChain *chain = new TChain("recoTree");

    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
    std::cout << "Output File name is: " << argv[3] << std::endl;
    bool applyVertexSF = (TString(argv[4]) == "true") ? true : false;
    std::cout << "Apply scale factor? " << argv[4] << " (" << applyVertexSF << ")" << std::endl;
    std::cout << "Systematics are -1/0/+1? " << argv[5] << std::endl;
    bool doPDFSystematics = (TString(argv[6]) == "true") ? true : false;
    std::cout << "PDF Systematics are: " << argv[6] << " ("<< doPDFSystematics << ")" << std::endl;
    int nPDFs = doPDFSystematics ? 101 : 0;
    int SYST_VAL = atoi(argv[5]);
    bool doPileupSystematics = (TString(argv[7]) == "true") ? true : false;
    std::cout << "Pileup systematics are: " << argv[7] << " (" << doPileupSystematics << ")" << std::endl;
    bool doJESSystematics = (TString(argv[7]) == "true") ? true : false;
    std::cout << "JES systematics are: " << argv[8] << " (" << doJESSystematics << ")" << std::endl;

    InputFiles::AddFilesToChain(TString(argv[1]), chain);

    std::vector<TString> jesTrees = {"JET_EtaIntercalibration_NonClosure__1up","JET_EtaIntercalibration_NonClosure__1down","JET_GroupedNP_1__1up","JET_GroupedNP_1__1down","JET_GroupedNP_2__1up",
            "JET_GroupedNP_2__1down","JET_GroupedNP_3__1up","JET_GroupedNP_3__1down","JET_JER_SINGLE_NP__1up"};        
    std::vector<TString> jesTypes = {"JESEta_1up","JESEta_1down","NP1_1up","NP1_1down","NP2_1up",
            "NP2_1down","NP3_1up","NP3_1down","JER_1up"};
    TChain *jesChain[9]; 
    CommonVariables *jesVar[9];
    for(unsigned int i=0; i < 9;i++){
        jesChain[i] = new TChain(jesTrees.at(i));
        InputFiles::AddFilesToChain(TString(argv[1]),jesChain[i]);
        jesChain[i]->SetBranchStatus("*",0);
        jesVar[i] = new CommonVariables;
        jesVar[i]->setZeroJESVectors();
        jesVar[i]->setJESBranchAddresses(jesChain[i]);
    }
    //Directory the plots will go in

    TString plotDirectory = TString(argv[2]);
    TFile *outputFile = new TFile(plotDirectory+"/"+TString(argv[3]),"RECREATE");

    TriggerVariables *trig = new TriggerVariables;
    VertexVariables *msvx = new VertexVariables;
    CommonVariables *commonVar = new CommonVariables;
    LLPVariables *llpVar = new LLPVariables;

    TChain *vxChain = new TChain("recoTree");
    std::map<ULong64_t, int> msvxMap;
    ULong64_t eventNumber = 0;
    if(applyVertexSF){
        InputFiles::AddVertexRerunFiles(TString(argv[1]), vxChain);
        vxChain->SetBranchStatus("*",0);
        msvxMap = CreateMap(vxChain);
        //have to have the next few lines here or it segfaults, because it's used in CreateMap.
        //I don't fully understand why SetBranchStatus("*",0) doesn't get rid of it.
        vxChain->SetBranchStatus("*",1);
        vxChain->SetBranchAddress("EventNumber", &eventNumber);
    }
    EfficiencyHistograms *effHistLLPJet = new EfficiencyHistograms;

    setPrettyCanvasStyle();

    std::cout << "set canvas style" << std::endl;

    TH1::SetDefaultSumw2();

    isSignal = -1;
    chain->SetBranchStatus("*",0);
    trig->setZeroVectors();
    trig->setBranchAdresses(chain,true);
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain, true, false, false ,true, doPDFSystematics);
    llpVar->setZeroVectors();
    llpVar->setBranchAddresses(chain);
    msvx->setZeroVectors(applyVertexSF);
    msvx->setBranchAdresses(applyVertexSF ? vxChain : chain, applyVertexSF, true);

    //
    Double_t ptbins[10] = {0,50,100,150,200,250,300,350,400,500};
    Double_t pzbins[10] = {0,100,200,300,400,500,600,800,1000,1200};
    Double_t Lxbins[10] = {0,0.5,1,1.5,2,2.5,3,3.5,4,5};
    Double_t Lzbins[11] = {0,0.5,1,1.5,2,2.5,3,3.5,4,6,8};

    effHistLLPJet->addHist("LLPJetEffB_pT_Lxy",9,Lxbins,9, ptbins);
    effHistLLPJet->addHist("LLPJetEffB_pz_Lz",10,Lzbins,9,pzbins);

    effHistLLPJet->addHist("LLPJetEffB_pT",9, ptbins);
    effHistLLPJet->addHist("LLPJetEffB_pz",9,pzbins);

    effHistLLPJet->addHist("LLPJetEffB_Lxy",9,Lxbins);
    effHistLLPJet->addHist("LLPJetEffB_Lz",10,Lzbins);

    effHistLLPJet->addHist("LLPJetEffE_pT_Lxy",9,Lxbins,9, ptbins);
    effHistLLPJet->addHist("LLPJetEffE_pz_Lz",10,Lzbins,9,pzbins);

    effHistLLPJet->addHist("LLPJetEffE_pT",9, ptbins);
    effHistLLPJet->addHist("LLPJetEffE_pz",9,pzbins);

    effHistLLPJet->addHist("LLPJetEffE_Lxy",9,Lxbins);
    effHistLLPJet->addHist("LLPJetEffE_Lz",10,Lzbins);


    char name[40];
    if(doPDFSystematics){
        for(int i=0;i < nPDFs; i++){
            sprintf(name,"%s%d","LLPJetEffB_pT_",i);
            effHistLLPJet->addHist(TString(name),9,ptbins);
            sprintf(name,"%s%d","LLPJetEffB_pz_",i);
            effHistLLPJet->addHist(TString(name),9, pzbins);
            sprintf(name,"%s%d","LLPJetEffB_Lxy_",i);
            effHistLLPJet->addHist(TString(name),9,Lxbins);
            sprintf(name,"%s%d","LLPJetEffB_Lz_",i);
            effHistLLPJet->addHist(TString(name),10, Lzbins);
        }
    }
    if(doPileupSystematics){
        effHistLLPJet->addHist("LLPJetEffB_pT_PRW_1up",9, ptbins);
        effHistLLPJet->addHist("LLPJetEffB_pT_PRW_1down",9, ptbins);
        effHistLLPJet->addHist("LLPJetEffB_pz_PRW_1up",9, pzbins);
        effHistLLPJet->addHist("LLPJetEffB_pz_PRW_1down",9, pzbins);
        effHistLLPJet->addHist("LLPJetEffB_Lxy_PRW_1up",9, Lxbins);
        effHistLLPJet->addHist("LLPJetEffB_Lxy_PRW_1down",9, Lxbins);
        effHistLLPJet->addHist("LLPJetEffB_Lz_PRW_1up",10, Lzbins);
        effHistLLPJet->addHist("LLPJetEffB_Lz_PRW_1down",10, Lzbins);
    }

    if(doJESSystematics){
        for(auto jes : jesTypes){
            effHistLLPJet->addHist("LLPJetEffB_pT_"+jes,9,ptbins);
            effHistLLPJet->addHist("LLPJetEffB_Lxy_"+jes,9,Lxbins);
            effHistLLPJet->addHist("LLPJetEffB_pz_"+jes,9,pzbins);
            effHistLLPJet->addHist("LLPJetEffB_Lz_"+jes,10,Lzbins);
        }

    }
    if(doPDFSystematics){
        for(int i=0;i < nPDFs; i++){
            sprintf(name,"%s%d","LLPJetEffE_pT_",i);
            effHistLLPJet->addHist(TString(name),9,ptbins);
            sprintf(name,"%s%d","LLPJetEffE_pz_",i);
            effHistLLPJet->addHist(TString(name),9, pzbins);
            sprintf(name,"%s%d","LLPJetEffE_Lxy_",i);
            effHistLLPJet->addHist(TString(name),9,Lxbins);
            sprintf(name,"%s%d","LLPJetEffE_Lz_",i);
            effHistLLPJet->addHist(TString(name),10, Lzbins);
        }
    }
    if(doPileupSystematics){
        effHistLLPJet->addHist("LLPJetEffE_pT_PRW_1up",9, ptbins);
        effHistLLPJet->addHist("LLPJetEffE_pT_PRW_1down",9, ptbins);
        effHistLLPJet->addHist("LLPJetEffE_pz_PRW_1up",9, pzbins);
        effHistLLPJet->addHist("LLPJetEffE_pz_PRW_1down",9, pzbins);
        effHistLLPJet->addHist("LLPJetEffE_Lxy_PRW_1up",9, Lxbins);
        effHistLLPJet->addHist("LLPJetEffE_Lxy_PRW_1down",9, Lxbins);
        effHistLLPJet->addHist("LLPJetEffE_Lz_PRW_1up",10, Lzbins);
        effHistLLPJet->addHist("LLPJetEffE_Lz_PRW_1down",10, Lzbins);
    }

    if(doJESSystematics){
        for(auto jes : jesTypes){
            effHistLLPJet->addHist("LLPJetEffE_pT_"+jes,9,ptbins);
            effHistLLPJet->addHist("LLPJetEffE_Lxy_"+jes,9,Lxbins);
            effHistLLPJet->addHist("LLPJetEffE_pz_"+jes,9,pzbins);
            effHistLLPJet->addHist("LLPJetEffE_Lz_"+jes,10,Lzbins);
        }

    }
    timer.Start();

    double finalWeight = 0;
    bool debug = false;

    for (int l=0;l<chain->GetEntries();l++)
    {

        if(l % 50000 == 0){
            timer.Stop();
            std::cout << "event " << l  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }

        chain->GetEntry(l);
        if(doJESSystematics){
            for(unsigned int iJES=0;iJES<9;iJES++){jesChain[iJES]->GetEntry(l);}
        }
        if(applyVertexSF) {
            vxChain->GetEntry(msvxMap[commonVar->eventNumber]);
        }
        if(debug) std::cout << "does this event have vertices? " << msvx->eta->size() << std::endl;
        //if(MSVertex_eta->size() == 0) continue;

        TString inputFile = TString(chain->GetCurrentFile()->GetName());

        //TString inputFile = chain->GetFile()->GetName();
        if( l % 100000 == 0 ) std::cout << "running over file: " << inputFile << std::endl;

        finalWeight = commonVar->pileupEventWeight;

        int nBarrel=0; int nEndcap = 0; int nID = 0;
        std::vector<int> llpMatching;

        bool is2J150Event = false;
        bool is2J250Event = false;

        bool eventHasLT40_Tracklets = false;
        if(applyVertexSF){ 
            if(msvx->countScaledTracklets() <=40) eventHasLT40_Tracklets = true;
        } else {
            if(msvx->tracklet_eta->size() <= 40) eventHasLT40_Tracklets = true;
        }


        //make the scale-factored clusters!
        trig->reclusterRoIs();
        //std::cout << "number of new clusters: " << trig->cluSyst->size() << std::endl;
        double trig_eta =-99;
        double trig_phi = -99;//trig->phi->at(0);
        for(unsigned int i=0; i<trig->cluEta->size(); i++){
            if(trig->cluSyst->at(i) == SYST_VAL){
                trig_eta = trig->cluEta->at(i);
                trig_phi = trig->cluPhi->at(i);
                break; //pick out the FIRST in the list - what the trigger sees....
            }
        }
        if(trig_eta > -90){
            commonVar->passMuvtx_noiso = true;
        } else{
            commonVar->passMuvtx_noiso = false;
        }
        bool triggerMatch = false;
        if(isSignal){
            llpVar->set_pz();
            //double deltaR2LLPs = DeltaR2(llpVar->phi->at(0),llpVar->phi->at(1),llpVar->eta->at(0),llpVar->eta->at(1));
            //bool LLPsAreSeparated = (deltaR2LLPs > 1.0) ? true : false;
            //if(!LLPsAreSeparated) std::cout << "LLP dR: " << sqrt(deltaR2LLPs) << std::endl;
            for(unsigned int j=0; j<llpVar->eta->size(); j++){
                double dR2_trig_llp = DeltaR2(llpVar->phi->at(j),trig_phi,llpVar->eta->at(j),trig_eta);
                if (dR2_trig_llp > 0.16) triggerMatch = true;

                llpMatching.push_back(0);

                if( (fabs(llpVar->eta->at(j)) < 0.8) && llpVar->Lxy->at(j) > 3000. && llpVar->Lxy->at(j) < 8000.) nBarrel++;
                else if( (fabs(llpVar->eta->at(j)) > 1.3) &&(fabs(llpVar->eta->at(j))< 2.5)&& llpVar->Lz->at(j) > 5000. && llpVar->Lz->at(j) < 15000. && llpVar->Lxy->at(j) < 10000.) nEndcap++;
                else if( fabs(llpVar->eta->at(j)) < 2.5 && llpVar->Lxy->at(j) < 275. && llpVar->Lz->at(j) < 840.) nID++;

            }
        }
        //std::cout << " filled denominators " << std::endl;

        if(debug) std::cout << "there are: " << msvx->eta->size() << " vertices" << std::endl;
        if(applyVertexSF && msvx->eta->size() > 0){
            msvx->performJetIsolation(commonVar->Jet_ET,commonVar->Jet_eta,commonVar->Jet_phi, commonVar->Jet_logRatio, commonVar->Jet_passJVT);
            msvx->performTrackIsolation(commonVar->Track_pT,commonVar->Track_eta,commonVar->Track_phi);
            msvx->testHitThresholds();
            msvx->testTruthMatch(llpVar->eta, llpVar->phi);
            msvx->testIsGood(applyVertexSF);
            msvx->testRoIMatch(trig->eta,trig->phi);
        }
        if(debug) std::cout << "performed isolation" << std::endl;

        //find a better way to do all this bookkeeping.
        int nABCD_SRvx = 0; int nABCD_SRvx_matched = 0; int ABCD_SRvx_matched_index = -1; int vx_index_ABCDMatched = -1;
        int ABCDVertex_region = 0;
        std::vector<double> vx_abcd_eta; std::vector<double> vx_abcd_phi; std::vector<int> vx_abcd_region;

        for (size_t i_vx=0;i_vx<msvx->eta->size();i_vx++)
        {
            if(applyVertexSF){
                if(msvx->syst->at(i_vx) != SYST_VAL){
                    continue;
                }
                if(!msvx->passReco->at(i_vx)) continue; //chi2 cut, minimum hit cut internal to algorithm
            }
            if(msvx->nMDT->at(i_vx)==0) continue; //skip dummy vertices (when too many tracklets in event)

            if(debug) std::cout << "vertex: " << i_vx << std::endl;

            double msvx_eta = msvx->eta->at(i_vx);
            double msvx_phi = msvx->phi->at(i_vx);

            int isEndcap = 0;
            int isBarrel = 0;
            if ( fabs(msvx_eta)<0.8 ){
                isBarrel = 1;
                //if(msvx->nTrks->at(i_vx) < 6) isGood = false;
                if(debug) std::cout << "vertex in barrel! " <<std::endl;
            }
            else if ( fabs(msvx_eta)> 1.3 && fabs(msvx_eta) < 2.5 ){
                isEndcap = 1;// for endcaps
                //if(msvx->nTrks->at(i_vx) < 5) isGood = false;
                if(debug) std::cout << "Vertex in endcaps!" << std::endl;
            }
            else{
                if(debug) std::cout << "Vertex found, but in neither barrel nor endcaps" << std::endl;
                continue;
            }
            //
            bool goodABCDHits = false;
            if(isEndcap && ( msvx->nMDT->at(i_vx) + msvx->nTGC->at(i_vx) )> 2500 && msvx->nMDT->at(i_vx) < 3000) goodABCDHits= true;
            else if(isBarrel && ( msvx->nMDT->at(i_vx) + msvx->nRPC->at(i_vx) ) > 2000 &&  msvx->nMDT->at(i_vx) < 3000) goodABCDHits= true;

            bool isGoodABCD = false;
            if(isBarrel) isGoodABCD = (msvx->closestdR->at(i_vx) > 0.3 && msvx->indexTrigRoI->at(i_vx) > -1 && goodABCDHits);
            else if(isEndcap) isGoodABCD = (msvx->closestdR->at(i_vx) > 0.4 && msvx->indexTrigRoI->at(i_vx) > -1 && goodABCDHits);

            if(isGoodABCD){
                vx_abcd_eta.push_back(msvx_eta);
                vx_abcd_phi.push_back(msvx_phi);
                vx_abcd_region.push_back((isBarrel? 1 : 2));
                ABCDVertex_region = isBarrel ? 1 : 2;
                nABCD_SRvx++;
            } else{
                continue;
            }
            int nVertexLLPMatches = 0;
            if(debug) {
                std::cout << "is good for ABCD? " << isGoodABCD << std::endl;
            }
            for(unsigned int j=0; j<llpVar->Lxy->size(); j++){
                double dR2_vx_llp = DeltaR2(llpVar->phi->at(j),msvx_phi,llpVar->eta->at(j),msvx_eta);
                if (dR2_vx_llp > 0.16) continue;
                if(llpMatching.at(j) ){
                    if(debug){
                        std::cout << "This LLP is already matched to a vertex - ignore the 2nd time (hist already filled)" << std::endl;

                        for (size_t l2=0;l2<msvx->eta->size();l2++){ std::cout << "Vertex " << l2 << "(R,z,eta,phi): (" <<
                            msvx->R->at(l2)*0.001 << ", " <<msvx->z->at(l2)*0.001 << ", " <<
                            msvx->eta->at(l2) << ", " << msvx->phi->at(l2) << ")" << std::endl;
                        }
                    }
                    continue;
                }
                llpMatching.at(j)++;
                if(isGoodABCD){
                    nABCD_SRvx_matched++;
                    ABCD_SRvx_matched_index = j;
                    vx_index_ABCDMatched = i_vx;
                }
                nVertexLLPMatches++;
            }

        }
        if(debug) std::cout << " done vertices" << std::endl;

        //all that ABCD stuff...
        if(nABCD_SRvx == 1 && nABCD_SRvx_matched == 1  && commonVar->passMuvtx_noiso && eventHasLT40_Tracklets && triggerMatch){
            commonVar->setGoodJetEventFlags();
            is2J150Event =  commonVar->eventIs2j150(msvx->eta->at(vx_index_ABCDMatched), msvx->phi->at(vx_index_ABCDMatched));
            is2J250Event =  commonVar->eventIs2j250(msvx->eta->at(vx_index_ABCDMatched), msvx->phi->at(vx_index_ABCDMatched));
            if(is2J150Event && !is2J150Event) std::cout << "method isn't going to work.... " << std::endl;
            if(debug) std::cout << "is this a 2J150 event? " << is2J150Event << std::endl;

            int indexNonVxMatch = -1;
            if(ABCD_SRvx_matched_index == 0){ indexNonVxMatch = 1;}
            else{ indexNonVxMatch = 0;}

            if(TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 1.5 ) {
                double lxy = (llpVar->Lxy->at(indexNonVxMatch)*0.001 < 5) ? llpVar->Lxy->at(indexNonVxMatch)*0.001 : 4.5; //fill all overflow into last bin
                double llppt = (llpVar->pT->at(indexNonVxMatch)*0.001 < 500) ? llpVar->pT->at(indexNonVxMatch)*0.001 : 450.;
                effHistLLPJet->fill("LLPJetEffB_pT_Lxy_denom",lxy,llppt, finalWeight );
                effHistLLPJet->fill("LLPJetEffB_pT_denom", llppt,finalWeight);
                effHistLLPJet->fill("LLPJetEffB_Lxy_denom", lxy,finalWeight);
                effHistLLPJet->fill("LLPJetEffE_pT_Lxy_denom",lxy,llppt, finalWeight );
                effHistLLPJet->fill("LLPJetEffE_pT_denom", llppt,finalWeight);
                effHistLLPJet->fill("LLPJetEffE_Lxy_denom", lxy,finalWeight);
                if(doPDFSystematics) {
                    for(int i_pdf=0;i_pdf < nPDFs; i_pdf++){
                        sprintf(name,"%s%d%s","LLPJetEffB_pT_",i_pdf,"_denom");
                        effHistLLPJet->fill(TString(name),llppt,finalWeight*commonVar->pdfWeights->at(i_pdf));
                        sprintf(name,"%s%d%s","LLPJetEffB_Lxy_",i_pdf,"_denom");
                        effHistLLPJet->fill(TString(name),lxy,finalWeight*commonVar->pdfWeights->at(i_pdf));
                        sprintf(name,"%s%d%s","LLPJetEffE_pT_",i_pdf,"_denom");
                        effHistLLPJet->fill(TString(name),llppt,finalWeight*commonVar->pdfWeights->at(i_pdf));
                        sprintf(name,"%s%d%s","LLPJetEffE_Lxy_",i_pdf,"_denom");
                        effHistLLPJet->fill(TString(name),lxy,finalWeight*commonVar->pdfWeights->at(i_pdf));
                    }
                }
                if(doPileupSystematics){
                    effHistLLPJet->fill("LLPJetEffB_pT_PRW_1up_denom", llppt,commonVar->prw_1up);
                    effHistLLPJet->fill("LLPJetEffB_pT_PRW_1down_denom", llppt,commonVar->prw_1down);
                    effHistLLPJet->fill("LLPJetEffB_Lxy_PRW_1up_denom", lxy,commonVar->prw_1up);
                    effHistLLPJet->fill("LLPJetEffB_Lxy_PRW_1down_denom", lxy,commonVar->prw_1down);
                    effHistLLPJet->fill("LLPJetEffE_pT_PRW_1up_denom", llppt,commonVar->prw_1up);
                    effHistLLPJet->fill("LLPJetEffE_pT_PRW_1down_denom", llppt,commonVar->prw_1down);
                    effHistLLPJet->fill("LLPJetEffE_Lxy_PRW_1up_denom", lxy,commonVar->prw_1up);
                    effHistLLPJet->fill("LLPJetEffE_Lxy_PRW_1down_denom", lxy,commonVar->prw_1down);
                }
            } else if (TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 3.2)  {
                double lz = (llpVar->Lz->at(indexNonVxMatch)*0.001 < 8) ? llpVar->Lz->at(indexNonVxMatch)*0.001 : 7.;
                double llppz = (TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 < 1200) ? TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 : 1100.;
                effHistLLPJet->fill("LLPJetEffB_pz_Lz_denom",lz,llppz, finalWeight );
                effHistLLPJet->fill("LLPJetEffB_pz_denom", llppz,finalWeight);
                effHistLLPJet->fill("LLPJetEffB_Lz_denom", lz,finalWeight);
                effHistLLPJet->fill("LLPJetEffE_pz_Lz_denom",lz,llppz, finalWeight );
                effHistLLPJet->fill("LLPJetEffE_pz_denom", llppz,finalWeight);
                effHistLLPJet->fill("LLPJetEffE_Lz_denom", lz,finalWeight);
                if(doPDFSystematics) {
                    for(int i_pdf=0;i_pdf < nPDFs; i_pdf++){
                        sprintf(name,"%s%d%s","LLPJetEffB_pz_",i_pdf,"_denom");
                        effHistLLPJet->fill(TString(name),llppz,finalWeight*commonVar->pdfWeights->at(i_pdf));
                        sprintf(name,"%s%d%s","LLPJetEffB_Lz_",i_pdf,"_denom");
                        effHistLLPJet->fill(TString(name),lz,finalWeight*commonVar->pdfWeights->at(i_pdf));
                        sprintf(name,"%s%d%s","LLPJetEffE_pz_",i_pdf,"_denom");
                        effHistLLPJet->fill(TString(name),llppz,finalWeight*commonVar->pdfWeights->at(i_pdf));
                        sprintf(name,"%s%d%s","LLPJetEffE_Lz_",i_pdf,"_denom");
                        effHistLLPJet->fill(TString(name),lz,finalWeight*commonVar->pdfWeights->at(i_pdf));
                    }
                }
                if(doPileupSystematics){
                    effHistLLPJet->fill("LLPJetEffB_pz_PRW_1up_denom", llppz,commonVar->prw_1up);
                    effHistLLPJet->fill("LLPJetEffB_pz_PRW_1down_denom", llppz,commonVar->prw_1down);
                    effHistLLPJet->fill("LLPJetEffB_Lz_PRW_1up_denom", lz,commonVar->prw_1up);
                    effHistLLPJet->fill("LLPJetEffB_Lz_PRW_1down_denom", lz,commonVar->prw_1down);
                    effHistLLPJet->fill("LLPJetEffE_pz_PRW_1up_denom", llppz,commonVar->prw_1up);
                    effHistLLPJet->fill("LLPJetEffE_pz_PRW_1down_denom", llppz,commonVar->prw_1down);
                    effHistLLPJet->fill("LLPJetEffE_Lz_PRW_1up_denom", lz,commonVar->prw_1up);
                    effHistLLPJet->fill("LLPJetEffE_Lz_PRW_1down_denom", lz,commonVar->prw_1down);
                }
            }
            if(is2J150Event){
                if(TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 1.5) {
                    double lxy = (llpVar->Lxy->at(indexNonVxMatch)*0.001 < 5) ? llpVar->Lxy->at(indexNonVxMatch)*0.001 : 4.5; //fill all overflow into last bin
                    double llppt = (llpVar->pT->at(indexNonVxMatch)*0.001 < 500) ? llpVar->pT->at(indexNonVxMatch)*0.001 : 450.;
                    effHistLLPJet->fill("LLPJetEffB_pT_Lxy",lxy, llppt, finalWeight );
                    effHistLLPJet->fill("LLPJetEffB_pT", llppt,finalWeight);
                    effHistLLPJet->fill("LLPJetEffB_Lxy", lxy,finalWeight);
                    if(is2J250Event){
                        effHistLLPJet->fill("LLPJetEffE_pT_Lxy",lxy, llppt, finalWeight );
                        effHistLLPJet->fill("LLPJetEffE_pT", llppt,finalWeight);
                        effHistLLPJet->fill("LLPJetEffE_Lxy", lxy,finalWeight);
                    }
                    if(doPDFSystematics) {
                        for(int i_pdf=0;i_pdf < nPDFs; i_pdf++){
                            sprintf(name,"%s%d","LLPJetEffB_pT_",i_pdf);
                            effHistLLPJet->fill(TString(name),llppt,finalWeight*commonVar->pdfWeights->at(i_pdf));
                            sprintf(name,"%s%d","LLPJetEffB_Lxy_",i_pdf);
                            effHistLLPJet->fill(TString(name),lxy,finalWeight*commonVar->pdfWeights->at(i_pdf));
                            if(is2J250Event){
                                sprintf(name,"%s%d","LLPJetEffE_pT_",i_pdf);
                                effHistLLPJet->fill(TString(name),llppt,finalWeight*commonVar->pdfWeights->at(i_pdf));
                                sprintf(name,"%s%d","LLPJetEffE_Lxy_",i_pdf);
                                effHistLLPJet->fill(TString(name),lxy,finalWeight*commonVar->pdfWeights->at(i_pdf));
                            }
                        }
                    }
                    if(doPileupSystematics){
                        effHistLLPJet->fill("LLPJetEffB_pT_PRW_1up", llppt,commonVar->prw_1up);
                        effHistLLPJet->fill("LLPJetEffB_pT_PRW_1down", llppt,commonVar->prw_1down);
                        effHistLLPJet->fill("LLPJetEffB_Lxy_PRW_1up", lxy,commonVar->prw_1up);
                        effHistLLPJet->fill("LLPJetEffB_Lxy_PRW_1down", lxy,commonVar->prw_1down);
                        if(is2J250Event){
                            effHistLLPJet->fill("LLPJetEffE_pT_PRW_1up", llppt,commonVar->prw_1up);
                            effHistLLPJet->fill("LLPJetEffE_pT_PRW_1down", llppt,commonVar->prw_1down);
                            effHistLLPJet->fill("LLPJetEffE_Lxy_PRW_1up", lxy,commonVar->prw_1up);
                            effHistLLPJet->fill("LLPJetEffE_Lxy_PRW_1down", lxy,commonVar->prw_1down);
                        }
                    }
                } else if (TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 3.2)  {
                    double lz = (llpVar->Lz->at(indexNonVxMatch)*0.001 < 8) ? llpVar->Lz->at(indexNonVxMatch)*0.001 : 7.;
                    double llppz = (TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 < 1200) ? TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 : 1100.;
                    effHistLLPJet->fill("LLPJetEffB_pz_Lz",lz,llppz, finalWeight );
                    effHistLLPJet->fill("LLPJetEffB_pz", llppz,finalWeight);
                    effHistLLPJet->fill("LLPJetEffB_Lz", lz,finalWeight);
                    if(is2J250Event){
                        effHistLLPJet->fill("LLPJetEffE_pz_Lz",lz,llppz, finalWeight );
                        effHistLLPJet->fill("LLPJetEffE_pz", llppz,finalWeight);
                        effHistLLPJet->fill("LLPJetEffE_Lz", lz,finalWeight);
                    }
                    if(doPDFSystematics) {
                        for(int i_pdf=0;i_pdf < nPDFs; i_pdf++){
                            sprintf(name,"%s%d","LLPJetEffB_pz_",i_pdf);
                            effHistLLPJet->fill(TString(name),llppz,finalWeight*commonVar->pdfWeights->at(i_pdf));
                            sprintf(name,"%s%d","LLPJetEffB_Lz_",i_pdf);
                            effHistLLPJet->fill(TString(name),lz,finalWeight*commonVar->pdfWeights->at(i_pdf));
                            if(is2J250Event){
                                sprintf(name,"%s%d","LLPJetEffE_pz_",i_pdf);
                                effHistLLPJet->fill(TString(name),llppz,finalWeight*commonVar->pdfWeights->at(i_pdf));
                                sprintf(name,"%s%d","LLPJetEffE_Lz_",i_pdf);
                                effHistLLPJet->fill(TString(name),lz,finalWeight*commonVar->pdfWeights->at(i_pdf)); 
                            }
                        }
                    }
                    if(doPileupSystematics){
                        effHistLLPJet->fill("LLPJetEffB_pz_PRW_1up", llppz,commonVar->prw_1up);
                        effHistLLPJet->fill("LLPJetEffB_pz_PRW_1down", llppz,commonVar->prw_1down);
                        effHistLLPJet->fill("LLPJetEffB_Lz_PRW_1up", lz,commonVar->prw_1up);
                        effHistLLPJet->fill("LLPJetEffB_Lz_PRW_1down", lz,commonVar->prw_1down);
                        if(is2J250Event){
                            effHistLLPJet->fill("LLPJetEffE_pz_PRW_1up", llppz,commonVar->prw_1up);
                            effHistLLPJet->fill("LLPJetEffE_pz_PRW_1down", llppz,commonVar->prw_1down);
                            effHistLLPJet->fill("LLPJetEffE_Lz_PRW_1up", lz,commonVar->prw_1up);
                            effHistLLPJet->fill("LLPJetEffE_Lz_PRW_1down", lz,commonVar->prw_1down);
                        }
                    }
                }
            }
            for(unsigned int iJES = 0; iJES < 9; iJES++){
                commonVar->Jet_eta->swap(*jesVar[iJES]->Jet_eta);
                commonVar->Jet_phi->swap(*jesVar[iJES]->Jet_phi);
                commonVar->Jet_pT->swap(*jesVar[iJES]->Jet_pT);

                commonVar->setGoodJetEventFlags();
                is2J150Event =  commonVar->eventIs2j150(msvx->eta->at(vx_index_ABCDMatched), msvx->phi->at(vx_index_ABCDMatched));
                is2J250Event =  commonVar->eventIs2j250(msvx->eta->at(vx_index_ABCDMatched), msvx->phi->at(vx_index_ABCDMatched));
                int indexNonVxMatch = -1;
                if(ABCD_SRvx_matched_index == 0){ indexNonVxMatch = 1;}
                else{ indexNonVxMatch = 0;}

                if(TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 1.5 ) {
                    double lxy = (llpVar->Lxy->at(indexNonVxMatch)*0.001 < 5) ? llpVar->Lxy->at(indexNonVxMatch)*0.001 : 4.5; //fill all overflow into last bin
                    double llppt = (llpVar->pT->at(indexNonVxMatch)*0.001 < 500) ? llpVar->pT->at(indexNonVxMatch)*0.001 : 450.;
                    effHistLLPJet->fill("LLPJetEffB_Lxy_"+jesTypes.at(iJES)+"_denom",lxy, finalWeight );
                    effHistLLPJet->fill("LLPJetEffB_pT_"+jesTypes.at(iJES)+"_denom",llppt, finalWeight );
                    effHistLLPJet->fill("LLPJetEffE_Lxy_"+jesTypes.at(iJES)+"_denom",lxy, finalWeight );
                    effHistLLPJet->fill("LLPJetEffE_pT_"+jesTypes.at(iJES)+"_denom",llppt, finalWeight );

                } else if (TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 3.2)  {
                    double lz = (llpVar->Lz->at(indexNonVxMatch)*0.001 < 8) ? llpVar->Lz->at(indexNonVxMatch)*0.001 : 7.;
                    double llppz = (TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 < 1200) ? TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 : 1100.;
                    effHistLLPJet->fill("LLPJetEffB_Lz_"+jesTypes.at(iJES)+"_denom",lz, finalWeight );
                    effHistLLPJet->fill("LLPJetEffB_pz_"+jesTypes.at(iJES)+"_denom",llppz, finalWeight );
                    effHistLLPJet->fill("LLPJetEffE_Lz_"+jesTypes.at(iJES)+"_denom",lz, finalWeight );
                    effHistLLPJet->fill("LLPJetEffE_pz_"+jesTypes.at(iJES)+"_denom",llppz, finalWeight );

                }
                if(is2J150Event){
                    if(TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 1.5) {
                        double lxy = (llpVar->Lxy->at(indexNonVxMatch)*0.001 < 5) ? llpVar->Lxy->at(indexNonVxMatch)*0.001 : 4.5; //fill all overflow into last bin
                        double llppt = (llpVar->pT->at(indexNonVxMatch)*0.001 < 500) ? llpVar->pT->at(indexNonVxMatch)*0.001 : 450.;
                        effHistLLPJet->fill("LLPJetEffB_Lxy_"+jesTypes.at(iJES),lxy, finalWeight );
                        effHistLLPJet->fill("LLPJetEffB_pT_"+jesTypes.at(iJES), llppt, finalWeight );
                        if(is2J250Event){
                            effHistLLPJet->fill("LLPJetEffE_Lxy_"+jesTypes.at(iJES),lxy, finalWeight );
                            effHistLLPJet->fill("LLPJetEffE_pT_"+jesTypes.at(iJES), llppt, finalWeight );
                        }
                    } else if (TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 3.2)  {
                        double lz = (llpVar->Lz->at(indexNonVxMatch)*0.001 < 8) ? llpVar->Lz->at(indexNonVxMatch)*0.001 : 7.;
                        double llppz = (TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 < 1200) ? TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 : 1100.;
                        effHistLLPJet->fill("LLPJetEffB_Lz_"+jesTypes.at(iJES),lz, finalWeight );
                        effHistLLPJet->fill("LLPJetEffB_pz_"+jesTypes.at(iJES),llppz, finalWeight );
                        if(is2J250Event){
                            effHistLLPJet->fill("LLPJetEffE_Lz_"+jesTypes.at(iJES),lz, finalWeight );
                            effHistLLPJet->fill("LLPJetEffE_pz_"+jesTypes.at(iJES),llppz, finalWeight );
                        }
                    }

                }
            }

        } //end if ABCD event

        //std::cout << "done jets" << std::endl;

        msvx->clearAllVectors(true,applyVertexSF);
        commonVar->clearAllVectors(true, true, doPDFSystematics);
        for(int iJES=0;iJES < 9; iJES++){jesVar[iJES]->clearJESVectors();}
        llpVar->clearAllVectors();
        trig->clearAllVectors();
        //std::cout << "vectors are cleared" << std::endl;
    }


    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;

    std::cout << " nEntries                 = " << chain->GetEntries() << std::endl;

    effHistLLPJet->plotEfficiencies(plotDirectory);

    outputFile->Write();
}
