//
//  plotGVCHistograms.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 11/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/LLPVariables.h"
#include "PlottingPackage/CommonVariables.h"
#include "PlottingPackage/VertexVariables.h"
#include "PlottingPackage/GVCHistograms.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/PlottingUtils.h"
#include "PlottingPackage/InputFiles.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>


using namespace std;
using namespace plotVertexParameters;
using namespace PlottingUtils;
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
int isData;
double jetSliceWeights[13];
TStopwatch timer;
int nEvt_passedNoIsoOffIso;
int nEvt_passedIso;
int nEvt_passedNoIsoOffIso_Barrel;
int nEvt_passedIso_Barrel;
int nEvt_passedNoIsoOffIso_EndCaps;
int nEvt_passedIso_EndCaps;
//
double nCRJet;

double nMSVx;
double nBMSVx;
double nEMSVx;
double nMSVx_unweighted;
double nBMSVx_unweighted;
double nEMSVx_unweighted;
double nMSTrig;
double nBMSTrig;
double nEMSTrig;

double nMSVx_passHitQual;
double nMSVx_passJetIso;
double nMSVx_passTrkIso;
double nMSVx_passGVC;

//
int nEvt_MoreThanOneVtx;
int nEvt_MoreThanOneVtx_noiso;



int main(int argc, char **argv){
    
    std::cout << "running program!" << std::endl;
    
    //TFile *outFile = new TFile("outputHistograms.root","RECREATE");
    
    TChain *chain = new TChain("recoTree");
    
    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
//    std::cout << "Output file name is: " << argv[3] << std::endl;
 
    InputFiles::AddFilesToChain(TString(argv[1]), chain);
    
    //Directory the plots will go in
    
    TString plotDirectory = TString(argv[2]);
    
    TFile *outputFile = new TFile(plotDirectory+"/outputGVC.root","RECREATE");
    
    std::cout << "added file!, will be saved in directory: " << plotDirectory << std::endl;
    
    VertexVariables *vxVar = new VertexVariables;
    CommonVariables *commonVar = new CommonVariables;
    
    //one set of histograms for barrel, one for endcaps,
    //one for the full detector together (nice for 2D maps)
    GVCHistograms *hMSVxBar = new GVCHistograms;
    GVCHistograms *hMSVxEC = new GVCHistograms;
    //GVCHistograms *hCRJetFull = new GVCHistograms;
    
    setPrettyCanvasStyle();
    
    TH1::SetDefaultSumw2();
    
    isSignal = -1;
    chain->SetBranchStatus("*",0);
    
    vxVar->setZeroVectors();
    vxVar->setBranchAdresses(chain,false,true);
    std::cout << "got vertex/trig variables" << std::endl;
    commonVar->setZeroVectors();       //jets,truthjets,isdata,tracks
    commonVar->setBranchAdresses(chain,true,false,false,true);
    
    std::cout << "got common variables" << std::endl;
    
    hMSVxBar->initializeHistograms("MSVx","bar");
    hMSVxEC->initializeHistograms("MSVx","ec");
    //hCRJetFull->initializeHistograms("CRJet","full");
    
    std::cout << "initialized histograms" << std::endl;
    
    //
    nCRJet=0;
    
    nMSVx=0;
    nBMSVx=0;
    nEMSVx=0;
    nMSVx_unweighted =0 ;
    nBMSVx_unweighted =0 ;
    nEMSVx_unweighted =0 ;
    timer.Start();
    
    setJetSliceWeights(jetSliceWeights);
    
    int jetSlice = -1;
    double finalWeight = 0;
    
    std::cout << "You are about to run over: " << chain->GetEntries() << " events." << std::endl;
    
    for (int l=0;l<chain->GetEntries();l++)
    {
        
        if(l % 100000 == 0){
            timer.Stop();
            std::cout << "event " << l  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }
        
        chain->GetEntry(l);
        
        
        if(vxVar->eta->size() == 0) continue;
        
        TString inputFile = TString(chain->GetCurrentFile()->GetName());
        
        //TString inputFile = chain->GetFile()->GetName();
        if( l % 100000 == 0 ) std::cout << "running over file: " << inputFile << std::endl;
        if(   inputFile.Contains("JZ")    ){
            isSignal = 0; isData = 0;
            if(inputFile.Contains("JZ0W")) jetSlice = 0;
            if(inputFile.Contains("JZ1W")) jetSlice = 1;
            if(inputFile.Contains("JZ2W")) jetSlice = 2;
            if(inputFile.Contains("JZ3W")) jetSlice = 3;
            if(inputFile.Contains("JZ4W")) jetSlice = 4;
            if(inputFile.Contains("JZ5W")) jetSlice = 5;
            if(inputFile.Contains("JZ6W")) jetSlice = 6;
            if(inputFile.Contains("JZ7W")) jetSlice = 7;
            if(inputFile.Contains("JZ8W")) jetSlice = 8;
            if(inputFile.Contains("JZ9W")) jetSlice = 9;
            if(inputFile.Contains("JZ10W")) jetSlice = 10;
            if(inputFile.Contains("JZ11W")) jetSlice = 11;
            if(inputFile.Contains("JZ12W")) jetSlice = 12;
        }
        if(inputFile.Contains("Stealth")||  inputFile.Contains("LongLived") || inputFile.Contains("_LLP") || inputFile.Contains("Chi")){ isSignal = 1; isData = 0; }
        if( inputFile.Contains("data15_13TeV")){ isSignal=0; isData = 1;}
        if(!isData){
            if(isSignal == 0 ) finalWeight = jetSliceWeights[jetSlice] * commonVar->eventWeight*commonVar->pileupEventWeight;
            else if( isSignal == 1 ) finalWeight = commonVar->pileupEventWeight;
            else std::cout << "what on earth are you running on? Input file is " << inputFile << std::endl;
            if(l==0) std::cout << "isSignal: " << isSignal << std::endl;
        }
        else if(isData) finalWeight = 1.0;
        
        vector<int> crIndex; int nCalRatio = 0;
        bool doCRTest = false;
        if(doCRTest){
            //run for CalRatio track iso
            for ( size_t i_jet=0;i_jet<commonVar->Jet_ET->size();i_jet++){
                //only look at jets with log ratio > 1, ET > 40, and eta <= 2.5 for the track isolation!
                if(commonVar->Jet_ET->at(i_jet) < 40. || fabs(commonVar->Jet_eta->at(i_jet)) > 2.5
                   || commonVar->Jet_logRatio->at(i_jet) < 1.){
                    continue;
                }
                
                //in signal, only consider low EMF jets that match to an LLP (is this what we want?)
                if(isSignal && nCalRatio > 0){
                    if(std::find(crIndex.begin(), crIndex.end(), commonVar->Jet_indexLLP->at(i_jet)) == crIndex.end() ){ continue;}
                }
                //could add an efficiency plot for dijet MC for jet pT vs logR?
                nCRJet++;
                
                //Make high-pT and sum(pT) track isolation plots
                //PlottingUtils::make_trkIso_GVC(commonVar, commonVar->Jet_eta->at(i_jet), commonVar->Jet_phi->at(i_jet), hCRJetFull, 0.4,0.2,5.,10., finalWeight);
                
            }//end calratio part
        }
        
        for (size_t ll=0;ll<vxVar->eta->size();ll++) {
            
            double msvx_eta = vxVar->eta->at(ll);
            double msvx_phi = vxVar->phi->at(ll);
            int isEndcap = 0;
            int isBarrel = 0;
            if( isSignal && vxVar->indexLLP->at(ll) < 0 ){ continue; } //only put matched vertices into GVC for signal.
            if (vxVar->nMDT->at(ll) == 0) continue; //skip the vertices that weren't reconstructed because too many tracklets
            nMSVx+=finalWeight;
            nMSVx_unweighted ++;
            if ( fabs(msvx_eta)<0.8 ){
                isBarrel = 1;
                nBMSVx+=finalWeight;
                nBMSVx_unweighted++;
            }
            else if ( fabs(msvx_eta)>1.3 && fabs(msvx_eta) < 2.5 ){
                nEMSVx+=finalWeight;
                nEMSVx_unweighted++;
                isEndcap = 1;
            }
            else { continue;}
            
            int mdtBin1d =  vxVar->nMDT->at(ll)*0.02+1;
            int rpcBin1d =  vxVar->nRPC->at(ll)*0.02+1;
            int tgcBin1d =  vxVar->nTGC->at(ll)*0.02+1;
            
            //Make MDT, TGC/RPC hit GVC plots. These are simpler because only one hit count/vertex!
            if(isBarrel){
                int mdt_bin = vxVar->nMDT->at(ll)*0.1+1; //x-bin value
                int rpc_bin = vxVar->nRPC->at(ll)*0.1+1; //y-bin value
                if( mdt_bin > 101){ mdt_bin = 101;}
                if( rpc_bin > 101){ rpc_bin = 101;}
                
                std::vector<std::pair<int,int> > b_hits_by_vertices;
                std::pair<int,int> tmp_pair = std::make_pair(mdt_bin,rpc_bin);
                b_hits_by_vertices.push_back(tmp_pair);
                PlottingUtils::make_2d_gvc_hits(b_hits_by_vertices,hMSVxBar->h_nMDT_vs_nRPC_fPass,10.,10.,finalWeight);
            } else if(isEndcap){
                int mdt_bin = vxVar->nMDT->at(ll)*0.1+1; //x-bin value
                int tgc_bin = vxVar->nTGC->at(ll)*0.1+1; //y-bin value
                if( mdt_bin > 101){ mdt_bin = 101;}
                if( tgc_bin > 101){ tgc_bin = 101;}
                
                std::vector<std::pair<int,int> > e_hits_by_vertices;
                std::pair<int,int> tmp_pair = std::make_pair(mdt_bin,tgc_bin);
                e_hits_by_vertices.push_back(tmp_pair);
                PlottingUtils::make_2d_gvc_hits(e_hits_by_vertices,hMSVxEC->h_nMDT_vs_nTGC_fPass,10.,10.,finalWeight);
            }
            
            //
            
            if(isBarrel){
                PlottingUtils::fillAllBins(finalWeight, hMSVxBar->h_denom_Pt);
                PlottingUtils::fillAllBins(finalWeight, hMSVxBar->h_denom_Et);
                PlottingUtils::fillAllBins(finalWeight, hMSVxBar->h_denom_dR);
                PlottingUtils::fillAllBins(finalWeight, hMSVxBar->h_denom_sumPt); 
                PlottingUtils::fillAllBins(finalWeight, hMSVxBar->h_denom_trks);  
                PlottingUtils::fillAllBins(finalWeight, hMSVxBar->h_denom_hits);

                PlottingUtils::make_jetIso_GVC(commonVar, msvx_eta, msvx_phi, hMSVxBar, 0.3, 0.5, 30., finalWeight);
                PlottingUtils::make_trkIso_GVC(commonVar, msvx_eta, msvx_phi, hMSVxBar, 0.3, 0.2, 5., 10., finalWeight);
                
                PlottingUtils::make_1d_gvc_plot(mdtBin1d, hMSVxBar->h_nMDT_fPass, finalWeight, false);
                PlottingUtils::make_1d_gvc_plot(rpcBin1d, hMSVxBar->h_nRPC_fPass, finalWeight, false);
                PlottingUtils::make_1d_gvc_plot(mdtBin1d, hMSVxBar->h_nMDT_up_fPass, finalWeight, true);
                
            } else if(isEndcap){
                
                PlottingUtils::fillAllBins(finalWeight, hMSVxEC->h_denom_Pt);
                PlottingUtils::fillAllBins(finalWeight, hMSVxEC->h_denom_Et);
                PlottingUtils::fillAllBins(finalWeight, hMSVxEC->h_denom_dR);
                PlottingUtils::fillAllBins(finalWeight, hMSVxEC->h_denom_sumPt);

                PlottingUtils::fillAllBins(finalWeight, hMSVxEC->h_denom_trks);
                PlottingUtils::fillAllBins(finalWeight, hMSVxEC->h_denom_hits);
                PlottingUtils::make_jetIso_GVC(commonVar, msvx_eta, msvx_phi, hMSVxEC, 0.6, 0.5, 30., finalWeight);
                PlottingUtils::make_trkIso_GVC(commonVar, msvx_eta, msvx_phi, hMSVxEC, 0.6, 0.2, 5., 10., finalWeight);

                PlottingUtils::make_1d_gvc_plot(mdtBin1d, hMSVxEC->h_nMDT_fPass, finalWeight, false);
                PlottingUtils::make_1d_gvc_plot(tgcBin1d, hMSVxEC->h_nTGC_fPass, finalWeight, false);
                PlottingUtils::make_1d_gvc_plot(mdtBin1d, hMSVxEC->h_nMDT_up_fPass, finalWeight, true);

            }
            //
            
        } //end loop over vertices
        
        commonVar->clearAllVectors(true,true);
        vxVar->clearAllVectors();
        
    }//end event loop
    
    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;
    
    cout << "" << endl;
    cout << " nEntries                 = " << chain->GetEntries() << endl;
    cout << "" << endl;
    cout << "Total MS vertices, pre any criteria  (b,e)    = " << nMSVx <<
    ", (" << nBMSVx << ", " << nEMSVx << ") " << endl;
    cout << "Total unweighted MS vertices, pre any criteria  (b,e)    = " << nMSVx_unweighted <<
    ", (" << nBMSVx_unweighted << ", " << nEMSVx_unweighted << ") " << endl;
    
    // ** Make plots **
    //
    
    std::vector<double> xVyP = { 0.1,0.3,0.5,0.7 };
    std::vector<double> yVxP_jet = { 30,60,90 };
    std::vector<double> yVxP_trk = { 3,5,7 };
    std::vector<double> yVxP_trkS = { 5,10,15 };
    
    PlottingUtils::plot2D("BMSVertex_JetdR_vs_Et_highestE"    ,plotDirectory,"Jet E_{T} [GeV]","Jet #DeltaR",hMSVxBar->h_JetdR_vs_Et_highestE);
    PlottingUtils::plot2DGVC("BMSVertex_JetdR_vs_Et_fPass"    ,plotDirectory,"Jet E_{T} [GeV]","Jet #DeltaR",hMSVxBar->h_JetdR_vs_Et_fPass,true,(double)nBMSVx,1,20,100,0,1, yVxP_jet,xVyP);
    PlottingUtils::plot2D("EMSVertex_JetdR_vs_Et_highestE"    ,plotDirectory,"Jet E_{T} [GeV]","Jet #DeltaR",hMSVxEC->h_JetdR_vs_Et_highestE);
    PlottingUtils::plot2DGVC("EMSVertex_JetdR_vs_Et_fPass"    ,plotDirectory,"Jet E_{T} [GeV]","Jet #DeltaR",hMSVxEC->h_JetdR_vs_Et_fPass,true,(double)nEMSVx,1,20,100,0,1, yVxP_jet,xVyP);
    
    
    PlottingUtils::plot2D("BMSVertex_TrackdR_vs_Pt_highestPt"    ,plotDirectory,"Track p_{T} [GeV]","Track #DeltaR",hMSVxBar->h_TrackdR_vs_Pt_highestPt);
    PlottingUtils::plot2DGVC("BMSVertex_TrackdR_vs_Pt_fPass"    ,plotDirectory,"Track p_{T} [GeV]","Track #DeltaR",hMSVxBar->h_TrackdR_vs_Pt_fPass,true,(double)nBMSVx,2,1,20,0,1,yVxP_trk,xVyP);
    PlottingUtils::plot2DGVC("BMSVertex_TrackdR_vs_sumPt_fPass"    ,plotDirectory,"#Sigma Track p_{T} [GeV]","Track #DeltaR",hMSVxBar->h_TrackdR_vs_sumPt_fPass,true,(double)nBMSVx,3,5,20,0,1,yVxP_trkS,xVyP);
    
    PlottingUtils::plot2D("EMSVertex_TrackdR_vs_Pt_highestPt"    ,plotDirectory,"Track p_{T} [GeV]","Track #DeltaR",hMSVxEC->h_TrackdR_vs_Pt_highestPt);
    PlottingUtils::plot2DGVC("EMSVertex_TrackdR_vs_Pt_fPass"    ,plotDirectory,"Track p_{T} [GeV]","Track #DeltaR",hMSVxEC->h_TrackdR_vs_Pt_fPass,true,(double)nEMSVx,2,1,20,0,1,yVxP_trk,xVyP);
    PlottingUtils::plot2DGVC("EMSVertex_TrackdR_vs_sumPt_fPass"    ,plotDirectory,"#Sigma Track p_{T} [GeV]","Track #DeltaR",hMSVxEC->h_TrackdR_vs_sumPt_fPass,true,(double)nEMSVx,3,5,20,0,1,yVxP_trkS,xVyP);
    
    
    std::vector<double> projTrig = {250};
    std::vector<double> projMDT {300};
    
    PlottingUtils::plot2DGVC("BMSVertex_nMDT_vs_nRPC_fPass"    ,plotDirectory,"nMDT hits","nRPC hits",hMSVxBar->h_nMDT_vs_nRPC_fPass,true,(double)nBMSVx,4,300,1000,250,1000,projMDT,projTrig);
    PlottingUtils::plot2DGVC("EMSVertex_nMDT_vs_nTGC_fPass"    ,plotDirectory,"nMDT hits","nTGC hits",hMSVxEC->h_nMDT_vs_nTGC_fPass,true,(double)nEMSVx,4,300,1000,250,1000,projMDT,projTrig);
    
    
    //PlottingUtils::plot2DGVC("CRJet_TrackdR_vs_Pt_fPass"    ,plotDirectory,"Track p_{T} [GeV]","Track #DeltaR",hCRJetFull->h_TrackdR_vs_Pt_fPass,true,(double)nCRJet,2,1,20,0,1,yVxP_trk,xVyP);
    //PlottingUtils::plot2DGVC("CRJet_TrackdR_vs_sumPt_fPass"    ,plotDirectory,"#Sigma Track p_{T} [GeV]","Track #DeltaR",hCRJetFull->h_TrackdR_vs_sumPt_fPass,true,(double)nCRJet,3,5,20,0,1,yVxP_trkS,xVyP);
    
    
    
    hMSVxBar->g_jetIso_vsEt_fPass = PlottingUtils::plotEfficiency("BMSVertex_JetIso_vs_Et_fPass"   ,plotDirectory,"Jet E_{T} [GeV]","Acceptance", hMSVxBar->h_jetIso_vsEt_fPass,hMSVxBar->h_denom_Et,0,200);
    
    hMSVxBar->g_jetIso_vsdR_fPass = PlottingUtils::plotEfficiency("BMSVertex_JetIso_vs_dR_fPass"    ,plotDirectory,"Jet #DeltaR","Acceptance", hMSVxBar->h_jetIso_vsdR_fPass,hMSVxBar->h_denom_dR,0,1);
    
    hMSVxBar->g_trkIso_vsPt_fPass = PlottingUtils::plotEfficiency("BMSVertex_TrackIso_vs_Pt_fPass"    ,plotDirectory,"Track p_{T} [GeV]","Acceptance", hMSVxBar->h_trkIso_vsPt_fPass,hMSVxBar->h_denom_Pt,0,20);
    
    hMSVxBar->g_trkIso_vsdR_fPass = PlottingUtils::plotEfficiency("BMSVertex_TrackIso_vs_dR_fPass"    ,plotDirectory, "Track #DeltaR","Acceptance", hMSVxBar->h_trkIso_vsdR_fPass,hMSVxBar->h_denom_dR,0,1);
    
    hMSVxBar->g_trkSumIso_vsPt_fPass = PlottingUtils::plotEfficiency("BMSVertex_TrackSumIso_vs_sumPt_fPass",plotDirectory, "#Sigma Track p_{T} [GeV]","Acceptance", hMSVxBar->h_trkSumIso_vsPt_fPass, hMSVxBar->h_denom_sumPt,0,50);
    hMSVxBar->g_trkSumIso_vsdR_fPass = PlottingUtils::plotEfficiency("BMSVertex_TrackSumIso_vs_sumdR_fPass",plotDirectory, "Track #Delta R","Acceptance", hMSVxBar->h_trkSumIso_vsdR_fPass, hMSVxBar->h_denom_dR,0,1);
    
    hMSVxEC->g_jetIso_vsEt_fPass = PlottingUtils::plotEfficiency("EMSVertex_JetIso_vs_Et_fPass"   ,plotDirectory,"Jet E_{T} [GeV]","Acceptance", hMSVxEC->h_jetIso_vsEt_fPass,hMSVxEC->h_denom_Et,0,200);
    hMSVxEC->g_jetIso_vsdR_fPass = PlottingUtils::plotEfficiency("EMSVertex_JetIso_vs_dR_fPass"    ,plotDirectory,"Jet #DeltaR","Acceptance", hMSVxEC->h_jetIso_vsdR_fPass,hMSVxEC->h_denom_dR,0,1);
    hMSVxEC->g_trkIso_vsPt_fPass = PlottingUtils::plotEfficiency("EMSVertex_TrackIso_vs_Pt_fPass"    ,plotDirectory,"Track p_{T} [GeV]","Acceptance", hMSVxEC->h_trkIso_vsPt_fPass,hMSVxEC->h_denom_Pt,0,20);
    hMSVxEC->g_trkIso_vsdR_fPass = PlottingUtils::plotEfficiency("EMSVertex_TrackIso_vs_dR_fPass"    ,plotDirectory, "Track #DeltaR","Acceptance", hMSVxEC->h_trkIso_vsdR_fPass,hMSVxEC->h_denom_dR,0,1);
    hMSVxEC->g_trkSumIso_vsPt_fPass = PlottingUtils::plotEfficiency("EMSVertex_TrackSumIso_vs_sumPt_fPass"    ,plotDirectory, "#Sigma Track p_{T} [GeV]","Acceptance", hMSVxEC->h_trkSumIso_vsPt_fPass, hMSVxEC->h_denom_sumPt,0,50);
    hMSVxEC->g_trkSumIso_vsdR_fPass = PlottingUtils::plotEfficiency("EMSVertex_TrackSumIso_vs_sumPt_fPass"    ,plotDirectory, "Track #Delta R","Acceptance", hMSVxEC->h_trkSumIso_vsdR_fPass, hMSVxEC->h_denom_dR,0,1);
    
    hMSVxBar->g_jetIso_vsEt_fPass->Write();
    hMSVxBar->g_jetIso_vsdR_fPass->Write();
    hMSVxBar->g_trkIso_vsPt_fPass->Write();
    hMSVxBar->g_trkIso_vsdR_fPass->Write();
    hMSVxBar->g_trkSumIso_vsPt_fPass->Write();
    hMSVxBar->g_trkSumIso_vsdR_fPass->Write();
    hMSVxEC->g_jetIso_vsEt_fPass->Write();
    hMSVxEC->g_jetIso_vsdR_fPass->Write();
    hMSVxEC->g_trkIso_vsPt_fPass->Write();
    hMSVxEC->g_trkIso_vsdR_fPass->Write();
    hMSVxEC->g_trkSumIso_vsPt_fPass->Write();
    hMSVxEC->g_trkSumIso_vsdR_fPass->Write();
    
    hMSVxBar->g_nMDT_fPass = PlottingUtils::plotEfficiency("BMSVertex_nMDT_fPass", plotDirectory, "Number of MDT hits","Acceptance (lower cut)", hMSVxBar->h_nMDT_fPass, hMSVxBar->h_denom_hits,0,5000);
    hMSVxBar->g_nMDT_up_fPass = PlottingUtils::plotEfficiency("BMSVertex_nMDT_up_fPass", plotDirectory, "Number of MDT hits","Acceptance (upper cut)", hMSVxBar->h_nMDT_up_fPass, hMSVxBar->h_denom_hits,0,5000);
    hMSVxEC->g_nMDT_fPass = PlottingUtils::plotEfficiency("EMSVertex_nMDT_fPass", plotDirectory, "Number of MDT hits","Acceptance (lower cut)", hMSVxEC->h_nMDT_fPass, hMSVxEC->h_denom_hits,0,5000);
    hMSVxEC->g_nMDT_up_fPass = PlottingUtils::plotEfficiency("EMSVertex_nMDT_up_fPass", plotDirectory, "Number of MDT hits","Acceptance (upper cut)", hMSVxEC->h_nMDT_up_fPass, hMSVxEC->h_denom_hits,0,5000);
    hMSVxBar->g_nRPC_fPass = PlottingUtils::plotEfficiency("BMSVertex_nRPC_fPass", plotDirectory, "Number of RPC hits","Acceptance", hMSVxBar->h_nRPC_fPass, hMSVxBar->h_denom_hits,0,5000);
    hMSVxEC->g_nTGC_fPass = PlottingUtils::plotEfficiency("EMSVertex_nTGC_fPass", plotDirectory, "Number of TGC hits","Acceptance", hMSVxEC->h_nTGC_fPass, hMSVxEC->h_denom_hits,0,5000);
    
    outputFile->Write();
    //
    //
    
    
}
