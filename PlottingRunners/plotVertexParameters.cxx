
#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/LLPVariables.h"
#include "PlottingPackage/CommonVariables.h"
#include "PlottingPackage/VertexVariables.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/PlottingUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/MSVxHistGroup.h"

#include "TChain.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>

using namespace std;
using namespace plotVertexParameters;
using namespace PlottingUtils;
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal, isData;
double jetSliceWeights[13];
TStopwatch timer;

int main(int argc, char **argv){

    std::cout << "running program!" << std::endl;
    TChain *chain = new TChain("recoTree");

    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
    bool isSignal = (TString(argv[3]) == "true")? 1 : 0;
    std::cout << "Signal? : " << argv[3]  << ", " << isSignal << std::endl;
    std::cout << "WARNING: If running on signal, this plots vertices BEFORE scale factors are applied" << std::endl;
    InputFiles::AddFilesToChain(TString(argv[1]), chain);

    //Directory the plots will go in

    TString plotDirectory = TString(argv[2]);
    TFile *outputFile = new TFile(plotDirectory+"/outputMSVertexParams.root","RECREATE");

    VertexVariables *vxVar = new VertexVariables;
    CommonVariables *commonVar = new CommonVariables;
    LLPVariables *llpVar = new LLPVariables;
    setPrettyCanvasStyle();

    TH1::SetDefaultSumw2();

    chain->SetBranchStatus("*",0);
    vxVar->setZeroVectors(false);
    vxVar->setBranchAdresses(chain, false, false);
    std::cout << "got vertex/trig variables" << std::endl;
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain,false, false, (isSignal == 0 ? true : false), false, false);
    std::cout << "got common variables" << std::endl;
    if(isSignal){
        llpVar->setZeroVectors();
        llpVar->setBranchAddresses(chain);
    }
    //one set of histograms for barrel, one for endcaps,
    //one for the full detector together (nice for 2D maps)
    MSVxHistGroup* msvxHists = new MSVxHistGroup;
    msvxHists->initialize(isSignal);

    timer.Start();

    setJetSliceWeights(jetSliceWeights);

    int jetSlice = -1;
    double finalWeight = 0;

    for (int l=0;l<chain->GetEntries();l++)
    {

        if(l % 500000 == 0){
            timer.Stop();
            std::cout << "event " << l  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }

        chain->GetEntry(l);

        if(!isSignal) {
            if(!commonVar->passMuvtx_noiso){
                commonVar->clearAllVectors(false, false, false);
                vxVar->clearAllVectors(false, false);
                continue;
            }
        }

        //if(MSVertex_eta->size() == 0) continue;

        //TString inputFile = "LongLived";
        TString inputFile = TString(chain->GetCurrentFile()->GetName());

        if(   inputFile.Contains("JZ")    ){
            isSignal = 0; isData = 0;
            if(inputFile.Contains("JZ3W")) jetSlice = 3;
            else if(inputFile.Contains("JZ4W")) jetSlice = 4;
            else if(inputFile.Contains("JZ5W")) jetSlice = 5;
            else if(inputFile.Contains("JZ6W")) jetSlice = 6;
            else if(inputFile.Contains("JZ7W")) jetSlice = 7;
            else if(inputFile.Contains("JZ8W")) jetSlice = 8;
            else if(inputFile.Contains("JZ9W")) jetSlice = 9;
            else if(inputFile.Contains("JZ10W")) jetSlice = 10;
            else if(inputFile.Contains("JZ11W")) jetSlice = 11;
            else if(inputFile.Contains("JZ12W")) jetSlice = 12;
            else if(inputFile.Contains("JZ0W")) jetSlice = 0;
            else if(inputFile.Contains("JZ1W")) jetSlice = 1;
            else if(inputFile.Contains("JZ2W")) jetSlice = 2;
        }
        if( inputFile.Contains("LongLived") || inputFile.Contains("_LLP") || inputFile.Contains("Stealth") ){ isSignal = 1; isData = 0; }
        if( inputFile.Contains("data15_13TeV")){ isSignal=0; isData = 1;}

        if(!isData){
            if(isSignal == 0 ) finalWeight = jetSliceWeights[jetSlice] * commonVar->eventWeight * commonVar->pileupEventWeight;
            else if( isSignal == 1 ) finalWeight = 1.0;
            else std::cout << "what on earth are you running on? Input file is " << inputFile << std::endl;
            if(l==0) std::cout << "isSignal: " << isSignal << std::endl;
        }
        else if(isData) finalWeight = 1.0;
        if(finalWeight > 10) finalWeight = 1.0;  
        //std::cout << jetSliceWeights[jetSlice] <<" * " << commonVar->eventWeight << " =  " << finalWeight << std::endl;

        int nBarrel = 0; int nEndcap = 0;
        if(isSignal){
            for(unsigned int j=0; j<llpVar->eta->size(); j++){
                if( (fabs(llpVar->eta->at(j)) < 0.8) && llpVar->Lxy->at(j) < 8000. && llpVar->Lxy->at(j) > 3000.){ nBarrel++;}
                else if( (fabs(llpVar->eta->at(j)) > 1.3) &&(fabs(llpVar->eta->at(j))< 2.5)&& llpVar->Lz->at(j) > 5000. && llpVar->Lz->at(j) < 15000. && llpVar->Lxy->at(j) < 10000.){nEndcap++;}
            }
        } //end isSignal

        for (size_t iVx=0;iVx<vxVar->eta->size();iVx++)
        {

            double msvx_eta = vxVar->eta->at(iVx);
            double msvx_phi = vxVar->phi->at(iVx);
            double msvx_R = vxVar->R->at(iVx);
            double msvx_z = vxVar->z->at(iVx);

            int isEndcap = 0;
            int isBarrel = 0;

            if ( fabs(msvx_eta)<0.8){
                isBarrel = 1;
                //std::cout << "vertex in barrel! " <<std::endl;
            }
            else if ( fabs(msvx_eta)>=1.3 && fabs(msvx_eta)<=2.5 ){
                isEndcap = 1;// for endcaps
                //std::cout << "Vertex in endcaps!" << std::endl;
            }
            else{
                //std::cout << "Vertex found, but in neither barrel nor endcaps" << std::endl;
                continue;
            }
            if(vxVar->nMDT->at(iVx)==0) continue;
            
            msvxHists->hMSVxFull->h_eta->Fill(msvx_eta, finalWeight);
            msvxHists->hMSVxFull->h_phi->Fill(msvx_phi, finalWeight);
            msvxHists->hMSVxFull->h_R->Fill(msvx_R, finalWeight);
            msvxHists->hMSVxFull->h_z->Fill(msvx_z, finalWeight);
            msvxHists->hMSVxFull->h_nTrks->Fill(vxVar->nTrks->at(iVx), finalWeight);
            msvxHists->hMSVxFull->h_nMDT->Fill(vxVar->nMDT->at(iVx), finalWeight);
            msvxHists->hMSVxFull->h_nRPC->Fill(vxVar->nRPC->at(iVx), finalWeight);
            msvxHists->hMSVxFull->h_nTGC->Fill(vxVar->nTGC->at(iVx), finalWeight);

            if(isBarrel){
                msvxHists->hMSVxBar->h_eta->Fill(msvx_eta, finalWeight);
                msvxHists->hMSVxBar->h_phi->Fill(msvx_phi, finalWeight);
                msvxHists->hMSVxBar->h_R->Fill(msvx_R, finalWeight);
                msvxHists->hMSVxBar->h_z->Fill(msvx_z, finalWeight);
                msvxHists->hMSVxBar->h_nTrks->Fill(vxVar->nTrks->at(iVx), finalWeight);
                msvxHists->hMSVxBar->h_nMDT->Fill(vxVar->nMDT->at(iVx), finalWeight);
                msvxHists->hMSVxBar->h_nRPC->Fill(vxVar->nRPC->at(iVx), finalWeight);
                msvxHists->hMSVxBar->h_nTGC->Fill(vxVar->nTGC->at(iVx), finalWeight);
            } else if(isEndcap){
                msvxHists->hMSVxEC->h_eta->Fill(msvx_eta, finalWeight);
                msvxHists->hMSVxEC->h_phi->Fill(msvx_phi, finalWeight);
                msvxHists->hMSVxEC->h_R->Fill(msvx_R, finalWeight);
                msvxHists->hMSVxEC->h_z->Fill(msvx_z, finalWeight);
                msvxHists->hMSVxEC->h_nTrks->Fill(vxVar->nTrks->at(iVx), finalWeight);
                msvxHists->hMSVxEC->h_nMDT->Fill(vxVar->nMDT->at(iVx), finalWeight);
                msvxHists->hMSVxEC->h_nRPC->Fill(vxVar->nRPC->at(iVx), finalWeight);
                msvxHists->hMSVxEC->h_nTGC->Fill(vxVar->nTGC->at(iVx), finalWeight); 
            }
            if(vxVar->isGood->at(iVx)){
                msvxHists->hMSVxFullGood->h_eta->Fill(msvx_eta, finalWeight);
                msvxHists->hMSVxFullGood->h_phi->Fill(msvx_phi, finalWeight);
                msvxHists->hMSVxFullGood->h_R->Fill(msvx_R, finalWeight);
                msvxHists->hMSVxFullGood->h_z->Fill(msvx_z, finalWeight);
                msvxHists->hMSVxFullGood->h_nTrks->Fill(vxVar->nTrks->at(iVx), finalWeight);
                msvxHists->hMSVxFullGood->h_nMDT->Fill(vxVar->nMDT->at(iVx), finalWeight);
                msvxHists->hMSVxFullGood->h_nRPC->Fill(vxVar->nRPC->at(iVx), finalWeight);
                msvxHists->hMSVxFullGood->h_nTGC->Fill(vxVar->nTGC->at(iVx), finalWeight);

                if(isBarrel){
                    msvxHists->hMSVxBarGood->h_eta->Fill(msvx_eta, finalWeight);
                    msvxHists->hMSVxBarGood->h_phi->Fill(msvx_phi, finalWeight);
                    msvxHists->hMSVxBarGood->h_R->Fill(msvx_R, finalWeight);
                    msvxHists->hMSVxBarGood->h_z->Fill(msvx_z, finalWeight);
                    msvxHists->hMSVxBarGood->h_nTrks->Fill(vxVar->nTrks->at(iVx), finalWeight);
                    msvxHists->hMSVxBarGood->h_nMDT->Fill(vxVar->nMDT->at(iVx), finalWeight);
                    msvxHists->hMSVxBarGood->h_nRPC->Fill(vxVar->nRPC->at(iVx), finalWeight);
                    msvxHists->hMSVxBarGood->h_nTGC->Fill(vxVar->nTGC->at(iVx), finalWeight);
                } else if(isEndcap){
                    msvxHists->hMSVxECGood->h_eta->Fill(msvx_eta, finalWeight);
                    msvxHists->hMSVxECGood->h_phi->Fill(msvx_phi, finalWeight);
                    msvxHists->hMSVxECGood->h_R->Fill(msvx_R, finalWeight);
                    msvxHists->hMSVxECGood->h_z->Fill(msvx_z, finalWeight);
                    msvxHists->hMSVxECGood->h_nTrks->Fill(vxVar->nTrks->at(iVx), finalWeight);
                    msvxHists->hMSVxECGood->h_nMDT->Fill(vxVar->nMDT->at(iVx), finalWeight);
                    msvxHists->hMSVxECGood->h_nRPC->Fill(vxVar->nRPC->at(iVx), finalWeight);
                    msvxHists->hMSVxECGood->h_nTGC->Fill(vxVar->nTGC->at(iVx), finalWeight); 
                }
            }

        } //end loop over vertices


        commonVar->clearAllVectors(false, false, false);
        if(isSignal) llpVar->clearAllVectors();
        vxVar->clearAllVectors(false, false);

    }
    bool log_scale = false;

    PlottingUtils::plot("MSVertex_barrel_eta",plotDirectory ,"MS vertex #eta" , "Number of vertices",msvxHists->hMSVxBar->h_eta,
            msvxHists->hMSVxBarGood->h_eta, log_scale, "|#eta| < 0.8","|#eta| < 0.8, Good");
    
    PlottingUtils::plot("MSVertex_barrel_phi",plotDirectory ,"MS vertex #phi" , "Number of vertices",msvxHists->hMSVxBar->h_phi,
            msvxHists->hMSVxBarGood->h_phi, log_scale,"|#eta| < 0.8","|#eta| < 0.8, Good");
    
    PlottingUtils::plot("MSVertex_barrel_R",plotDirectory ,"MS vertex L_{xy} [m]" , "Number of vertices",msvxHists->hMSVxBar->h_R,
            msvxHists->hMSVxBarGood->h_R, log_scale,"|#eta| < 0.8","|#eta| < 0.8, sGood");
    
    PlottingUtils::plot("MSVertex_barrel_z",plotDirectory ,"MS vertex L_{z} [m]" , "Number of vertices",msvxHists->hMSVxBar->h_z,
            msvxHists->hMSVxBarGood->h_z, log_scale, "|#eta| < 0.8","|#eta| < 0.8, Good");
    
    PlottingUtils::plot("MSVertex_barrel_nMDT",plotDirectory ,"MS vertex nMDT hits" , "Number of vertices",msvxHists->hMSVxBar->h_nMDT,
                msvxHists->hMSVxBarGood->h_nMDT, log_scale, "|#eta| < 0.8","|#eta| < 0.8, Good");
        
    PlottingUtils::plot("MSVertex_barrel_nRPC",plotDirectory ,"MS vertex nRPC hits" , "Number of vertices",msvxHists->hMSVxBar->h_nRPC,
                msvxHists->hMSVxBarGood->h_nRPC, log_scale, "|#eta| < 0.8","|#eta| < 0.8, Good");
       
    
    PlottingUtils::plot("MSVertex_Endcap_eta",plotDirectory ,"MS vertex #eta" , "Number of vertices",msvxHists->hMSVxEC->h_eta,
            msvxHists->hMSVxECGood->h_eta, log_scale, "1.3 < |#eta| < 2.5","1.3 < |#eta| < 2.5, Good");
    
    PlottingUtils::plot("MSVertex_Endcap_phi",plotDirectory ,"MS vertex #phi" , "Number of vertices",msvxHists->hMSVxEC->h_phi,
            msvxHists->hMSVxECGood->h_phi, log_scale, "1.3 < |#eta| < 2.5","1.3 < |#eta| < 2.5, Good");
    
    PlottingUtils::plot("MSVertex_Endcap_R",plotDirectory ,"MS vertex L_{xy} [m]" , "Number of vertices",msvxHists->hMSVxEC->h_R,
            msvxHists->hMSVxECGood->h_R, log_scale, "1.3 < |#eta| < 2.5","1.3 < |#eta| < 2.5, Good");
    
    PlottingUtils::plot("MSVertex_Endcap_z",plotDirectory ,"MS vertex L_{z} [m]" , "Number of vertices",msvxHists->hMSVxEC->h_z,
            msvxHists->hMSVxECGood->h_z, log_scale,"1.3 < |#eta| < 2.5","1.3 < |#eta| < 2.5, Good");
    
    PlottingUtils::plot("MSVertex_Endcap_nMDT",plotDirectory ,"MS vertex nMDT hits" , "Number of vertices",msvxHists->hMSVxEC->h_nMDT,
                msvxHists->hMSVxECGood->h_nMDT,log_scale, "1.3 < |#eta| < 2.5","1.3 < |#eta| < 2.5, Good");        
     
    PlottingUtils::plot("MSVertex_Endcap_nTGC",plotDirectory ,"MS vertex nTGC hits" , "Number of vertices",msvxHists->hMSVxEC->h_nTGC,
                msvxHists->hMSVxECGood->h_nTGC, log_scale, "1.3 < |#eta| < 2.5","1.3 < |#eta| < 2.5, Good");
        
    PlottingUtils::plot2D("MSVertex_all_phieta",plotDirectory, "MS vertex #phi", "MS vertex #eta", msvxHists->hMSVxFull->h_phieta);
    PlottingUtils::plot2D("MSVertex_all_zR",plotDirectory,"MS vertex Lxy [m]", "MS vertex Lz [m]", msvxHists->hMSVxFull->h_zR);

    PlottingUtils::plot2D("MSVertex_Barrel_MDTvsRPC",plotDirectory,"MS vertex nRPC hits", "MS vertex nMDT hits", msvxHists->hMSVxBar->h_MDTvsTrig);
    PlottingUtils::plot2D("MSVertex_Endcap_MDTvsTGC",plotDirectory,"MS vertex nTGC hits", "MS vertex nMDT hits", msvxHists->hMSVxEC->h_MDTvsTrig);

    
    std::cout << "plotted hists!" << std::endl;

    outputFile->Write();

    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;

    std::cout << "" << std::endl;
    std::cout << " Ran over = " << chain->GetEntries() << " events." << std::endl;



    //
    //


}
