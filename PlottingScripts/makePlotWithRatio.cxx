void makePlotWithRatio(std::vector<TString> inFiles, std::vector<TString> labels, TString plotDir, TString histName, TString xTitle, TString yTitle){
    //0.7 < |η| < 1.0 or 1.5 < |η| < 1.7

    TString fileDir = "/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/";
    
    TCanvas* c1 = new TCanvas("c1","c1",800,600);
    SetAtlasStyle();

    TPad *pad1 = new TPad("pad1","pad1",0,0.32,1,1);
    pad1->SetTopMargin(0.075);
    pad1->SetBottomMargin(0.04);
    pad1->SetLeftMargin(0.14);
    pad1->SetLogy();
    pad1->Draw();

    TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.32);
    pad2->SetTopMargin(0.05);
    pad2->SetBottomMargin(0.40);
    pad2->SetLeftMargin(0.14);
    pad2->Draw();
    
    pad1->SetLogy(0); pad1->cd();
    
    Int_t colorSig[6] = {kBlue-3,kBlue-3,kAzure+5, kAzure+5, kTeal-6,kTeal-6};
    Int_t markerSig[6] = {20,24,22,26,21,25};

    std::cout << "input sizes: " << inFiles.size() << ", " << labels.size() << std::endl;
    
    std::vector<TH1D *> hists;
    for(auto fileName: inFiles){
        TFile * file = TFile::Open(fileDir+fileName);
        TH1D *tmp = (TH1D*)file->Get(histName);
        hists.push_back( tmp );
    }

    std::cout << "hist size: " << hists.size() << std::endl;
    
    double maxVal = 0;
    TLegend *leg = new TLegend(0.6,0.7,0.85,0.9);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.045);

    for(int i=0; i<hists.size(); i++){
        hists.at(i)->SetLineColor(colorSig[i]);
        hists.at(i)->SetMarkerColor(colorSig[i]);
        hists.at(i)->SetMarkerStyle(markerSig[i]);
        hists.at(i)->SetMarkerSize(1.2);
        leg->AddEntry(hists.at(i),labels.at(i),"lp");
        hists.at(i)->Draw();
        if(hists.at(i)->GetMaximum() > maxVal) maxVal = hists.at(i)->GetMaximum();
    }

    
    hists.at(0)->SetMinimum(0);
    hists.at(0)->GetYaxis()->SetRangeUser(0,1.2*maxVal);

    hists.at(0)->GetYaxis()->SetLabelSize(0.070);
    hists.at(0)->GetYaxis()->SetTitleSize(0.065);
    hists.at(0)->GetYaxis()->SetTitleOffset(0.18);
    hists.at(0)->GetXaxis()->SetTitleOffset(2.7);
    hists.at(0)->GetXaxis()->SetLabelOffset(0.5);
    hists.at(0)->GetYaxis()->SetTitleOffset(0.95);
    hists.at(0)->GetYaxis()->SetTitle(yTitle);
    hists.at(0)->Draw();
    for(int i=1; i<hists.size(); i++) hists.at(i)->Draw("SAME");

    leg->Draw();

    TLatex latex2;
    latex2.SetNDC();
    latex2.SetTextColor(kBlack);
    latex2.SetTextSize(0.06);
    latex2.SetTextAlign(31);  //align at bottom
    //latex2.DrawLatex(0.95,.94,title);

    //c1->Print(plotDir+histName+".pdf");
    TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
    l.SetNDC();
    l.SetTextFont(72);
    l.SetTextSize(0.06);
    l.SetTextColor(kBlack);
    //l.DrawLatex(.72,.84,"ATLAS");
    TLatex p; 
    p.SetNDC();
    p.SetTextSize(0.06);
    p.SetTextFont(42);
    p.SetTextColor(kBlack);
    // p.DrawLatex(0.72+0.105,0.84,"Internal");

    c1->cd();

    pad2->cd();
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);

    std::vector<TH1D*> ratios;
    for(int i=1; i< hists.size(); i+=2){
        TH1D* tmp = (TH1D*)hists.at(i)->Clone("h0_clone");
        tmp->Divide(hists.at(i-1));
        ratios.push_back(tmp);
    }

    ratios.at(0)->GetXaxis()->SetLabelFont(42);
    ratios.at(0)->GetXaxis()->SetLabelSize(0.16);
    ratios.at(0)->GetXaxis()->SetLabelOffset(0.05);
    ratios.at(0)->GetXaxis()->SetTitleFont(42);
    ratios.at(0)->GetXaxis()->SetTitleSize(0.14);
    ratios.at(0)->GetXaxis()->SetTitleOffset(1.3);
    ratios.at(0)->GetXaxis()->SetTitle(xTitle);
    ratios.at(0)->GetYaxis()->SetNdivisions(505);
    ratios.at(0)->GetYaxis()->SetTitle("#font[42]{SF/Nominal}");
    ratios.at(0)->GetYaxis()->SetLabelFont(42);
    ratios.at(0)->GetYaxis()->SetLabelSize(0.15);
    ratios.at(0)->GetYaxis()->SetTitleFont(42);
    ratios.at(0)->GetYaxis()->SetTitleSize(0.13);
    ratios.at(0)->GetYaxis()->SetTitleOffset(0.44); 
    ratios.at(0)->DrawCopy("eP");

    for(int i=0;i<ratios.size(); i++) ratios.at(i)->Draw("PSAME");

    TLine* line = new TLine();
    line->DrawLine(ratios.at(0)->GetXaxis()->GetXmin(),1,ratios.at(0)->GetXaxis()->GetXmax(),1);

    pad2->SetLogy(0);

    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.1);
    latex.SetTextAlign(13);  //align at top
    //latex.DrawLatex(.25,.92, fitVal);
    //latex.DrawLatex(.25,.82, fitVal2);

    c1->Print(plotDir+"/ratio_"+histName+".pdf");
    c1->Print(plotDir+"/ratio_"+histName+".png");

}
