//
//  CutflowCounter.h
//  plotVertexParameters
//
//  Created by Heather Russell on 03/09/16.
//
//

#ifndef __plotVertexParameters__CutflowCounter__
#define __plotVertexParameters__CutflowCounter__

#include <stdio.h>

class CutflowCounter
{
public:
    std::vector< std::pair<std::string, double> > m_counters;
    int m_nVariables;
    
    CutflowCounter(); // The object we'll create
    ~CutflowCounter(){
        delete m_counters;
    };
    CutflowCounter(const CutflowCounter&) = delete;
    CutflowCounter& operator=(const CutflowCounter&) = delete;
    
    void initialize()
};


#endif /* defined(__plotVertexParameters__CutflowCounter__) */
