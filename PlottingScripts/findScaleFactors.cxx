void findScaleFactors(TString sample, TString type){

    TCanvas* c1 = new TCanvas("c1","c1",800,600);
    
    SetAtlasStyle();

    //c1->SetRightMargin(0.05);
    //c1->SetTopMargin(0.07);
    //c1->SetLeftMargin(0.15);
    //c1->SetBottomMargin(0.15);
    TPad *pad1 = new TPad("pad1","pad1",0,0.32,1,1);
    pad1->SetTopMargin(0.075);
    pad1->SetBottomMargin(0.04);
    pad1->SetLeftMargin(0.14);
    pad1->SetLogy();
    pad1->Draw();

    TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.32);
    pad2->SetTopMargin(0.05);
    pad2->SetBottomMargin(0.40);
    pad2->SetLeftMargin(0.14);
    pad2->Draw();
    
    TString plotDir = TString("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/")+sample;

    std::vector<TString> histNames;
    if(type == "MSVx") {
        histNames.push_back("MSVx_Barrel_Lxy_eff");
        histNames.push_back("MSVx_Endcap_Lz_eff");
    } else if(type == "Trig"){
        histNames.push_back("MSTrig_1B_Lxy_eff");
        histNames.push_back("MSTrig_1E_Lz_eff");
    }
    TString mod = ""; if(type.Contains("Trig")) mod = "_40TrackletCut";

    TFile *f[3];
    f[0] = TFile::Open(TString("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/")+sample+"/output"+type+"Eff"+mod+".root");
    f[1] = TFile::Open(TString("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/")+sample+"/output"+type+"Eff_1up.root");
    f[2] = TFile::Open(TString("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/")+sample+"/output"+type+"Eff_1down.root");
    
    for (auto histName : histNames){

        pad1->cd(); 
        gStyle->SetPadTickX(2);
        gStyle->SetPadTickY(2);
        TH1D *hdown =  (TH1D*)f[2]->Get(histName);
        TH1D *hup =  (TH1D*)f[1]->Get(histName);
        TH1D *hN = (TH1D*)f[0]->Get(histName);
        
        pad1->SetLogy(0); hN->SetMinimum(0);
        hN->SetLineColor(kBlack);hN->SetMarkerColor(kBlack);hN->SetMarkerSize(1.2);hN->SetLineWidth(2);hN->SetMarkerStyle(20);
        hup->SetLineColor(kBlue+1);hup->SetMarkerColor(kBlue+1);hup->SetMarkerSize(1.2);hup->SetLineWidth(2);hup->SetMarkerStyle(21);  
        hdown->SetLineColor(kRed+1);hdown->SetMarkerColor(kRed+1);hdown->SetMarkerSize(1.2);hdown->SetLineWidth(2);hdown->SetMarkerStyle(22);  
        
        if(histName.Contains("Lxy")){ hN->GetXaxis()->SetTitle("L_{xy} [m]");}
        else        if(histName.Contains("Lz")){ hN->GetXaxis()->SetTitle("L_{z} [m]");}
        if(type == "MSVx"){
            hN->GetYaxis()->SetTitle("MS vertex reco eff");
        } else if(type == "Trig"){
            hN->GetYaxis()->SetTitle("Muvtx noiso eff");
        }
        double maxVal = hup->GetMaximum(); if(hN->GetMaximum() > maxVal) maxVal = hN->GetMaximum();
        if(hdown->GetMaximum() > maxVal) maxVal = hdown->GetMaximum();
        hN->SetMaximum(1.3*maxVal);
        hN->SetTitle("");
  
        hN->GetYaxis()->SetLabelSize(0.070);
        hN->GetYaxis()->SetTitleSize(0.065);
        hN->GetYaxis()->SetTitleOffset(0.18);
        hN->GetXaxis()->SetTitleOffset(2.7);
        hN->GetXaxis()->SetLabelOffset(0.5);
        hN->GetYaxis()->SetTitleOffset(0.95);

        hN->Draw();
        hup->Draw("SAME");
        hdown->Draw("SAME");

        TLegend *leg = new TLegend(0.18,0.7,0.4,0.9);
        leg->SetFillStyle(0);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.05);
        leg->AddEntry(hup,"+1 #sigma","lp");
        leg->AddEntry(hN,"nominal","lp");
        leg->AddEntry(hdown,"-1 #sigma","lp");
        leg->Draw();

        //c1->Print(plotDir+histName+".pdf");
        TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
        l.SetNDC();
        l.SetTextFont(72);
        l.SetTextSize(0.06);
        l.SetTextColor(kBlack);
        //l.DrawLatex(.72,.84,"ATLAS");
        TLatex p; 
        p.SetNDC();
        p.SetTextSize(0.06);
        p.SetTextFont(42);
        p.SetTextColor(kBlack);
        // p.DrawLatex(0.72+0.105,0.84,"Internal");

        c1->cd();

        pad2->cd();
        gStyle->SetPadTickX(1);
        gStyle->SetPadTickY(1);

        TH1D *p1_up = (TH1D*)hup->Clone("nominal_clone");
        TH1D *p1_down = (TH1D*)hdown->Clone("nominal_clone");

        p1_up->Divide(hN);
        p1_down->Divide(hN);
        /*for(int i=1;i<p1_up->GetNbinsX(); i++){
            p1_up->SetBinError(i+1,0.0001);
        }*/

        p1_up->SetMarkerStyle(21);
        p1_up->SetMarkerSize(0.8);
        p1_up->SetMarkerColor(kBlue+1);
        p1_down->SetMarkerStyle(22);
        p1_down->SetMarkerSize(0.8);
        p1_down->SetMarkerColor(kRed+1);
        p1_up->SetMinimum(0.75);
        p1_up->SetMaximum(1.25);

        p1_up->GetXaxis()->SetLabelFont(42);
        p1_up->GetXaxis()->SetLabelSize(0.16);
        p1_up->GetXaxis()->SetLabelOffset(0.05);
        p1_up->GetXaxis()->SetTitleFont(42);
        p1_up->GetXaxis()->SetTitleSize(0.14);
        p1_up->GetXaxis()->SetTitleOffset(1.3);
        
        if(histName.Contains("Lxy")){ p1_up->GetXaxis()->SetTitle("L_{xy} [m]");}
        else if(histName.Contains("Lz")){ p1_up->GetXaxis()->SetTitle("L_{z} [m]");}
        
        p1_up->GetYaxis()->SetNdivisions(505);
        p1_up->GetYaxis()->SetTitle("#font[42]{#pm #sigmal / nominal}");
        p1_up->GetYaxis()->SetLabelFont(42);
        p1_up->GetYaxis()->SetLabelSize(0.12);
        p1_up->GetYaxis()->SetTitleFont(42);
        p1_up->GetYaxis()->SetTitleSize(0.13);
        p1_up->GetYaxis()->SetTitleOffset(0.44); 
        p1_up->DrawCopy("eP");
        p1_down->DrawCopy("SAME");

        TLine* line = new TLine();
        line->DrawLine(p1_up->GetXaxis()->GetXmin(),1,p1_up->GetXaxis()->GetXmax(),1);

        TString fitVal = "";
        TString fitVal2 = "";

        TF1 *fup = new TF1("fitup", "pol0");
        TF1 *fdown = new TF1("fitdown", "pol0");
        p1_up->Fit("fitup");
        p1_down->Fit("fitdown");
        
        p1_up->DrawCopy("eP");
        p1_down->DrawCopy("SAME");        
        char buffer [100];
        int nc = sprintf(buffer, "Fit up: %1.2f +/- %1.2f", p1_up->GetFunction("fitup")->GetParameter(0), p1_up->GetFunction("fitup")->GetParError(0));
        fitVal =  TString(buffer);
        nc = sprintf(buffer, "Fit down: %1.2f +/- %1.2f", p1_down->GetFunction("fitdown")->GetParameter(0), p1_down->GetFunction("fitdown")->GetParError(0));

        fitVal2 =  TString(buffer);
        
        std::cout << fitVal << std::endl; 
        std::cout << fitVal2 << std::endl; 
        pad2->SetLogy(0);

        //latex2.DrawLatex(.15,.98,title + ", Data/MC");

        TLatex latex;
        latex.SetNDC();
        latex.SetTextColor(kBlack);
        latex.SetTextSize(0.1);
        latex.SetTextAlign(13);  //align at top
        latex.DrawLatex(.16,.92, fitVal);
        latex.DrawLatex(.16,.82, fitVal2);
        
        c1->Print(plotDir+"/ratio_"+histName+"_pol0.pdf");
        c1->Print(plotDir+"/ratio_"+histName+"_pol0.png");
        c1->Print(plotDir+"/ratio_"+histName+"_pol0.root");
        delete hN; delete hup; delete hdown;
        
    }

}

