//
//  countABCDSignalEvents.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 21/10/16.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/VertexHistograms.h"
#include "PlottingPackage/TriggerVariables.h"
#include "PlottingPackage/VertexVariables.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/PlottingUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/HistTools.h"
#include "PlottingPackage/ExtrapolationUtils.h"
#include "PlottingPackage/LLPVariables.h"

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>

using namespace std;
using namespace plotVertexParameters;
using namespace PlottingUtils;
using namespace ExtrapolationUtils;
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal; int isData;
TStopwatch timer;

double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;
}
int main(int argc, char **argv){
    bool debug = false;
    double n1B_Trig, n1E_Trig, nBB_Trig, nBE_Trig, nEE_Trig;
    double nBB_TrigA_0vx, nBE_TrigA_0vx, nEE_TrigA_0vx, n1B_Trig_0vx, n1E_Trig_0vx;
    double nBB_TrigA_1vx, nBE_TrigA_1vx,nEB_TrigA_1vx, nEE_TrigA_1vx, n1B_Trig_1vx, n1E_Trig_1vx;
    double nBB_TrigA_2j150, nBE_TrigA_2j150,nEB_TrigA_2j150, nEE_TrigA_2j150, n1B_Trig_2j150, n1E_Trig_2j150;
    double nBB_Decays, n1B_Decays;
    double nBE_Decays, n1E_Decays;
    double nEE_Decays;
    double nBarrel_Vx_events = 0;
    double nBarrel_Vx1j50_1j150_events = 0;
    double nBarrel_Vx2j150_events = 0; 
    double nBarrel_Vx1j100_1j250_events = 0; 
    double nBarrel_Vx2j250_events = 0; 
    double nEC_Vx_events = 0;
    double nEC_Vx2j150_events = 0; 
    double nEC_Vx1j100_1j250_events = 0; 
    double nEC_Vx2j250_events = 0; 
    nBB_Decays=0; n1B_Decays=0; nBE_Decays=0; n1E_Decays=0; nEE_Decays=0;
    nBB_Trig=0; n1B_Trig=0; nBE_Trig=0; n1E_Trig=0; nEE_Trig=0;

    n1B_Trig_0vx=0;
    n1E_Trig_0vx=0;
    n1B_Trig_1vx=0;
    n1E_Trig_1vx=0;
    n1B_Trig_2j150=0;
    n1E_Trig_2j150=0;

    nBB_TrigA_0vx=0;
    nBE_TrigA_0vx=0;
    nEE_TrigA_0vx=0;
    nBB_TrigA_1vx=0;
    nBE_TrigA_1vx=0;
    nEB_TrigA_1vx=0;
    nEE_TrigA_1vx=0;
    nBB_TrigA_2j150=0;
    nBE_TrigA_2j150=0;
    nEB_TrigA_2j150=0;
    nEE_TrigA_2j150=0;
    double nTriggeredEvents = 0;
    double nVerticesPostTrigger=0;
    double nVerticesPostTrigger_notCrack_passHits_matchTrig = 0; 
    double nVerticesPostTrigger_notCrack_passHits = 0;
    double nVerticesPostTrigger_notCrack = 0;
    double nVertices_Good = 0;
    std::cout << "running program!" << std::endl;

    //TFile *outFile = new TFile("outputHistograms.root","RECREATE");

    TChain *chain = new TChain("recoTree");

    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;

    InputFiles::AddFilesToChain(TString(argv[1]), chain);

    
/*    TChain *vxChain = new TChain("recoTree");
    std::map<ULong64_t, int> msvxMap;
    ULong64_t eventNumber = 0;
    InputFiles::AddVertexRerunFiles(TString(argv[1]), vxChain);
    vxChain->SetBranchStatus("*",0);
    msvxMap = CreateMap(vxChain);
    //have to have the next few lines here or it segfaults, because it's used in CreateMap.
    //I don't fully understand why SetBranchStatus("*",0) doesn't get rid of it.
    vxChain->SetBranchStatus("*",1);
    vxChain->SetBranchAddress("EventNumber", &eventNumber);
*/

    VertexVariables *vxVar = new VertexVariables;
    TriggerVariables *trigVar = new TriggerVariables;
    CommonVariables *commonVar = new CommonVariables;
    LLPVariables *llpVar = new LLPVariables;

    chain->SetBranchStatus("*",0);

    vxVar->setZeroVectors(true);
    //vxVar->setBranchAdresses(vxChain, true, false);
    vxVar->setBranchAdresses(chain,false,false);
    std::cout << "got vertex variables" << std::endl;
    trigVar->setZeroVectors();
    trigVar->setBranchAdresses(chain,true);
    std::cout << "got trigger variables" << std::endl;
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain,true,false,false,true,false);
    std::cout << "got common variables" << std::endl;

    llpVar->setZeroVectors();
    llpVar->setBranchAddresses(chain);

    std::cout << "" << std::endl;
    std::cout << "Number of events = " << chain->GetEntries() << std::endl;
    std::cout << "" << std::endl;

    setPrettyCanvasStyle();

    TH1::SetDefaultSumw2();

    isSignal = 0;

    double finalWeight = 0;
    timer.Start();

    for (int i_evt=0;i_evt<chain->GetEntries();i_evt++)
    {
        if(i_evt % 100000 == 0){
            timer.Stop();
            std::cout << "event " << i_evt  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }
        chain->GetEntry(i_evt);
        commonVar->fixJetET(); 
        finalWeight = commonVar->pileupEventWeight;

        std::vector<TVector3> llp_pos;
        llpVar->SetAllVariables();

        for(unsigned int i=0; i<llpVar->eta->size(); ++i) {

            ////////////////////////////////
            //* Generating v-pion decays *//O
            TVector3 tmp_pos;
            double Lxyz;
            //use original
            Lxyz = sqrt(pow(llpVar->Lxy->at(i),2) + pow(llpVar->Lz->at(i),2));
            tmp_pos.SetMagThetaPhi(Lxyz,llpVar->theta->at(i),llpVar->phi->at(i));
            llp_pos.push_back(tmp_pos);

        }
        if(isBB_Event(llp_pos)){nBB_Decays+= finalWeight;}
        else if(isBE_Event(llp_pos)){nBE_Decays+= finalWeight;}
        else if(isEE_Event(llp_pos)){nEE_Decays+= finalWeight;}
        else if(is1B_Event(llp_pos)){n1B_Decays+= finalWeight;}
        else if(is1E_Event(llp_pos)){n1E_Decays+= finalWeight;}
        /*else {
            llpVar->clearAllVectors();
            trigVar->clearAllVectors(true);
            commonVar->clearAllVectors(true,true);
            continue;
        }*/

        if(debug) std::cout << "done event topo" << std::endl;
        //std::cout << "trig variables size? " << trigVar->eta->size() << std::endl;
        double trig_eta = -99;
        double trig_phi = -99;

        trigVar->reclusterRoIs();

        for(unsigned int i=0; i<trigVar->cluEta->size(); i++){
            if(trigVar->cluSyst->at(i) == 0){
                if( (TMath::Abs(trigVar->cluEta->at(i)) > 0.8  && TMath::Abs(trigVar->cluEta->at(i)) < 1.3)
                        || TMath::Abs(trigVar->cluEta->at(i)) > 2.5){ break;}
                trig_eta = trigVar->cluEta->at(i);
                trig_phi = trigVar->cluPhi->at(i);
                break;
            }
        }
        if(debug) std::cout << "done trigger reclustering" << std::endl;

        //std::cout << "trigger? " << trig_eta << ", " << trig_phi << std::endl;
        if(trig_eta > -90){
            commonVar->passMuvtx_noiso = true; //passed trigger, can count topologies here! Missing 40 tracklet cut, but that will come with the vertices...
            nTriggeredEvents+= finalWeight;
            if(isBB_Event(llp_pos)){nBB_Trig+= finalWeight;}
            else if(isBE_Event(llp_pos)){nBE_Trig+= finalWeight;}
            else if(isEE_Event(llp_pos)){nEE_Trig+= finalWeight;}
            else if(is1B_Event(llp_pos)){n1B_Trig+= finalWeight;}
            else if(is1E_Event(llp_pos)){n1E_Trig+= finalWeight;}
        } else {
            llpVar->clearAllVectors();
            trigVar->clearAllVectors(true);
            commonVar->clearAllVectors(true,true);
            continue;

        }
        if(debug) std::cout << "done trigger topo" << std::endl;

        //vxChain->GetEntry(msvxMap[commonVar->eventNumber]);

        if(vxVar->eta->size() > 0){
           // vxVar->performJetIsolation(commonVar->Jet_ET,commonVar->Jet_eta,commonVar->Jet_phi, commonVar->Jet_logRatio, commonVar->Jet_passJVT);
           // vxVar->performTrackIsolation(commonVar->Track_pT,commonVar->Track_eta,commonVar->Track_phi);
           // vxVar->testHitThresholds();
            vxVar->testIsGood(false);
        }
        if(debug) std::cout << "done vertex re-isolating" << std::endl;

        int vxMatch_index = -1;
        int nVertices = 0;

        //std::cout << "vx variables size? " << vxVar->eta->size() << std::endl;
        bool isBarrelEvent = false;
        double vxResidual = 10;

        for (size_t i_vtx=0;i_vtx<vxVar->eta->size();i_vtx++){

            //if((vxVar->syst->at(i_vtx) != 0) || (vxVar->passReco->at(i_vtx) != 1) ){ continue;}
            nVerticesPostTrigger+= finalWeight;
            //don't consider vertices in the overlap region
            double msvx_eta = vxVar->eta->at(i_vtx);
            if( (TMath::Abs(msvx_eta) > 0.8  && TMath::Abs(msvx_eta) < 1.3) || TMath::Abs(msvx_eta) > 2.5){ continue;}
            nVerticesPostTrigger_notCrack += finalWeight;

            if(!vxVar->passesHitThresholds->at(i_vtx)){
                continue;
            }
            nVerticesPostTrigger_notCrack_passHits += finalWeight;
            double msvx_phi = vxVar->phi->at(i_vtx);
            double deltaR2 = DeltaR2(trig_phi,msvx_phi,trig_eta,msvx_eta);

            if(deltaR2 < 0.4*0.4){
                nVerticesPostTrigger_notCrack_passHits_matchTrig+= finalWeight;
                if(vxVar->isGoodABCD->at(i_vtx)  == 1) nVertices_Good+=finalWeight;
            }

            if( vxVar->isGoodABCD->at(i_vtx) != 1) continue;

            if(deltaR2 > 0.4*0.4) continue;
            nVertices++;
            vxResidual = sqrt(deltaR2);
            //at this point, only use the index if there is one good vertex so should be ok!
            vxMatch_index = i_vtx;
            if(TMath::Abs(msvx_eta) <= 0.8) isBarrelEvent = true;
            if(commonVar->eventNumber == 132715){
                std::cout << "Vertex R, z, eta, phi: ";
                std::cout << vxVar->R->at(i_vtx) << ", ";
                std::cout << vxVar->z->at(i_vtx) << ", ";
                std::cout << msvx_eta << ", " << vxVar->phi->at(i_vtx) << std::endl;
            }

        }
        if(debug) std::cout << "done vertex counting" << std::endl;

        if(nVertices > 1){
            llpVar->clearAllVectors();
            vxVar->clearAllVectors(false, false);
            trigVar->clearAllVectors();
            commonVar->clearAllVectors(true,true);
            continue;
        }

        if(nVertices == 0 || (nVertices == 1 && vxMatch_index == -1)) {
            if(isBB_Event(llp_pos)){nBB_TrigA_0vx+= finalWeight;}
            else if(isBE_Event(llp_pos)){nBE_TrigA_0vx+= finalWeight;}
            else if(isEE_Event(llp_pos)){nEE_TrigA_0vx+= finalWeight;}
            else if(is1B_Event(llp_pos)){n1B_Trig_0vx+= finalWeight;}
            else if(is1E_Event(llp_pos)){n1E_Trig_0vx+= finalWeight;}

            llpVar->clearAllVectors();
            vxVar->clearAllVectors(false, false);
            trigVar->clearAllVectors(true);
            commonVar->clearAllVectors(true,true);
            continue;
        }
        else if(nVertices == 1 && vxMatch_index > -1 &&  commonVar->passMuvtx_noiso){
            if(debug) std::cout << "this event has one vertex" << std::endl;
            if(isBB_Event(llp_pos)){nBB_TrigA_1vx+= finalWeight;}
            else if(isBE_Event(llp_pos)){
                if(isBarrelEvent) nBE_TrigA_1vx+= finalWeight;
                else nEB_TrigA_1vx+= finalWeight;
            }
            else if(isEE_Event(llp_pos)){nEE_TrigA_1vx+= finalWeight;}
            else if(is1B_Event(llp_pos)){n1B_Trig_1vx+= finalWeight;}
            else if(is1E_Event(llp_pos)){n1E_Trig_1vx+= finalWeight;}

            if(isBarrelEvent) nBarrel_Vx_events+=finalWeight;
            else nEC_Vx_events +=finalWeight;
            commonVar->setGoodJetEventFlags();
            commonVar->set2DPlotVars(vxVar->eta->at(vxMatch_index), vxVar->phi->at(vxMatch_index));
            if(debug) std::cout << "this event has one vertex and 2j150 tested..." << std::endl;
            if(isBarrelEvent && commonVar->CR_2j150 && vxResidual < 0.1 && ((commonVar->Jet_eta->size() == 2 && commonVar->Jet_pT->at(1) > 150.) || (commonVar->Jet_eta->size() > 2 && commonVar->Jet_pT->at(2) <50.))){
                //std::cout << "event number: " << commonVar->eventNumber << std::endl;
            }
            if(commonVar->CR_2j150){

                if(isBB_Event(llp_pos)){

                    nBB_TrigA_2j150+= finalWeight;
                }
                else if(isBE_Event(llp_pos)){
                    if(isBarrelEvent ){
                        nBE_TrigA_2j150+= finalWeight;
                        //std::cout << "BE event number: " << commonVar->eventNumber << std::endl;
                    }
                    else nEB_TrigA_2j150+=finalWeight;
                }
                else if(isEE_Event(llp_pos)){nEE_TrigA_2j150+= finalWeight;}
                else if(is1B_Event(llp_pos)){
                    //                    std::cout << "event number: " << commonVar->eventNumber << std::endl;
                    /*if(commonVar->eventNumber == 132715){
                        for(int iJet = 0; iJet < commonVar->Jet_eta->size(); iJet++){
                            std::cout << "eta, phi, pT, logRatio, isGoodStandard: ";
                            std::cout << commonVar->Jet_eta->at(iJet) << ", ";
                            std::cout << commonVar->Jet_phi->at(iJet) << ", ";
                            std::cout << commonVar->Jet_pT->at(iJet) << ", ";
                            std::cout << commonVar->Jet_logRatio->at(iJet) << ", ";
                            std::cout << commonVar->Jet_isGoodStand->at(iJet) << std::endl;
                        }
                        for(int iLLP = 0; iLLP < llpVar->eta->size(); iLLP++){
                            std::cout << "LLP R, |z|, eta, phi:";
                            std::cout << llpVar->Lxy->at(iLLP) << ", ";
                            std::cout << llpVar->Lz->at(iLLP) << ", ";
                            std::cout << llpVar->eta->at(iLLP) << ", ";
                            std::cout << llpVar->phi->at(iLLP) << std::endl;
                        }

                    }*/
                    n1B_Trig_2j150+= finalWeight;
                }
                else if(is1E_Event(llp_pos)){n1E_Trig_2j150+= finalWeight;}

                if(isBarrelEvent) nBarrel_Vx2j150_events+=finalWeight;
                else nEC_Vx2j150_events +=finalWeight;
            }
            if(isBarrelEvent && commonVar->CR_1j50_1j150 && !commonVar->CR_2j150) nBarrel_Vx1j50_1j150_events += finalWeight;
            if(commonVar->CR_1j100_1j250 && !commonVar->CR_2j250){
                if(isBarrelEvent) nBarrel_Vx1j100_1j250_events += finalWeight;
                else nEC_Vx1j100_1j250_events += finalWeight;
            } else if(commonVar->CR_2j250){
                if(isBarrelEvent) nBarrel_Vx2j250_events += finalWeight;
                else nEC_Vx2j250_events += finalWeight;
            }
        }

        llpVar->clearAllVectors();
        vxVar->clearAllVectors(false, false);
        trigVar->clearAllVectors(true);
        commonVar->clearAllVectors(true,true);
    }

    std::cout << " ************************************************** " << std::endl;
    std::cout << " ******************* ABCD RESULTS ***************** " << std::endl;
    cout << "nBB, BE, EE decays"  << endl;
    cout << nBB_Decays << ", " << nBE_Decays << ", " << nEE_Decays << endl;
    std::cout << " n1B, n1E decays" <<std::endl;
    std::cout << n1B_Decays << " " << n1E_Decays << std::endl;
    std::cout << " n1B , n1E trig events: " << std::endl;
    std::cout << n1B_Trig << " " << n1E_Trig << std::endl;
    std::cout << "nBB, BE, EE trig events:  " << std::endl;
    std::cout << nBB_Trig << " " << nBE_Trig << " " << nEE_Trig << std::endl;
    std::cout << " Trig + vx: 1B, 1E, BB, BE, EB, EE: " << std::endl;
    std::cout << n1B_Trig_1vx << " " << n1E_Trig_1vx << " " << nBB_TrigA_1vx << " ";
    std::cout << nBE_TrigA_1vx << " " << nEB_TrigA_1vx << " " <<nEE_TrigA_1vx << std::endl;
    std::cout << " Trig + vx + 2j150: 1B, 1E, BB, BE, EB, EE: " << std::endl;
    std::cout << n1B_Trig_2j150 << " " << n1E_Trig_2j150 << " " << nBB_TrigA_2j150 << " ";
    std::cout << nBE_TrigA_2j150 << " " << nEB_TrigA_2j150 << " " << nEE_TrigA_2j150 << std::endl;
    std::cout << "TOTAL events passing ABCD selection with 1 barrel vertex: " << n1B_Trig_2j150+nBB_TrigA_2j150+nBE_TrigA_2j150 << std::endl;
    std::cout << "TOTAL events passing ABCD selection with 1 endcap vertex: " << n1E_Trig_2j150+nEE_TrigA_2j150+nEB_TrigA_2j150 << std::endl;
    std::cout << " ************************************************** " << std::endl;
    std::cout << "triggered events in total? " << nTriggeredEvents << std::endl;
    std::cout << nVerticesPostTrigger << " " << nVerticesPostTrigger_notCrack << " " << nVerticesPostTrigger_notCrack_passHits << " ";
    std::cout << nVerticesPostTrigger_notCrack_passHits_matchTrig << " " << nVertices_Good <<  std::endl;
    std::cout << " ************************************************** " << std::endl;
    std::cout << "Without truth cuts" << std::endl;
    std::cout << "number of barrel events before 2j150: " << nBarrel_Vx_events << std::endl;
    std::cout << "number of barrel events in VR 1j50, 1j150: " << nBarrel_Vx1j50_1j150_events << std::endl;
    std::cout << "number of barrel events after 2j150: " << nBarrel_Vx2j150_events << std::endl;
    std::cout << "number of endcap events before 2j150: " << nEC_Vx_events << std::endl;
    std::cout << "number of endcap events after 2j150: " << nEC_Vx2j150_events << std::endl;
    std::cout << " ************************************************** " << std::endl;
    std::cout << "Barrel, 1j100_2j250, 2j250: " << nBarrel_Vx1j100_1j250_events << ", " << nBarrel_Vx2j250_events << std::endl;
    std::cout << "Endcap, 1j100_2j250, 2j250: " << nEC_Vx1j100_1j250_events << ", " << nEC_Vx2j250_events << std::endl;
    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;

}

