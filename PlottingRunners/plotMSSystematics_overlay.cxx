
#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/CommonVariables.h"
#include "PlottingPackage/VertexVariables.h"
#include "PlottingPackage/TriggerVariables.h"
#include "PlottingPackage/SystHistograms.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/CalRatioVariables.h"
#include "TVector3.h"

#include "PlottingPackage/PlottingUtils.h"

//#include "CommonUtils/MathUtils.h"

#include "TChain.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "TLorentzVector.h"

using namespace std;
using namespace plotVertexParameters;

double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;

}
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

int isSignal; int isData;
double jetSliceWeights[13];
TStopwatch timer;
//



int main(int argc, char **argv){

    std::cout << "running program!" << std::endl;

    TChain *chain = new TChain("recoTree");

    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
    std::cout << "Output File name is: " << argv[3] << std::endl;
    bool doCRVariables = (TString(argv[4]) == "true") ? true : false;
    std::cout << "do CR variables: " << doCRVariables << std::endl;

    InputFiles::AddFilesToChain(TString(argv[1]), chain);

    //Directory the plots will go in

    TString plotDirectory = TString(argv[2]);
    TFile *outputFile = new TFile(plotDirectory+"/"+TString(argv[3]),"RECREATE");

    TriggerVariables *trigVar = new TriggerVariables;
    VertexVariables *variables = new VertexVariables;
    CommonVariables *commonVar = new CommonVariables;

    CalRatioVariables *crVar = new CalRatioVariables;

    SystHistograms *histEtaFull = new SystHistograms;

    SystHistograms *histEtaFullLJ = new SystHistograms;
    SystHistograms *histEtaFullSLJ = new SystHistograms;

    SystHistograms *histEta[5]; //SystHistograms *histEtaCR[5];
    for(int i=0;i<5;i++){
        histEta[i] = new SystHistograms;
        //histEtaCR[i] = new SystHistograms;
    }
    SystHistograms *histEtaLJ[5]; //SystHistograms *histEtaLJCR[5];
    for(int i=0;i<5;i++){
        histEtaLJ[i] = new SystHistograms;
        //histEtaLJCR[i] = new SystHistograms;
    }
    setPrettyCanvasStyle();

    std::cout << "set canvas style" << std::endl;

    TH1::SetDefaultSumw2();

    isSignal = -1; isData = 0;
    if(TString(argv[1]).Contains("data")) isData = 1;

    chain->SetBranchStatus("*",0);
    
    trigVar->setZeroVectors();
    trigVar->setBranchAdresses(chain);
    variables->setZeroVectors();
    variables->setBranchAdresses(chain,false,true);

    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain, true, false, isData, false);
    if(doCRVariables){
        crVar->setZeroVectors();
        crVar->setBranchAdresses(chain);
    }
    
    histEtaFull->initializeMSHistograms("full","AllJets");
    histEtaFullLJ->initializeMSHistograms("full","LJ");
    histEtaFullSLJ->initializeMSHistograms("full","SLJ");
    //histEtaFull->initializeCRHistograms("full","AllJets");
    //histEtaFullLJ->initializeCRHistograms("full","LJ");
    //histEtaFullSLJ->initializeCRHistograms("full","SLJ");

    histEta[0]->initializeMSHistograms("eta1","AllJets");
    histEta[1]->initializeMSHistograms("eta2","AllJets");
    histEta[2]->initializeMSHistograms("eta3","AllJets");
    histEta[3]->initializeMSHistograms("eta4","AllJets");
    histEta[4]->initializeMSHistograms("eta5","AllJets");

    histEtaLJ[0]->initializeMSHistograms("eta1","LJ");
    histEtaLJ[1]->initializeMSHistograms("eta2","LJ");
    histEtaLJ[2]->initializeMSHistograms("eta3","LJ");
    histEtaLJ[3]->initializeMSHistograms("eta4","LJ");
    histEtaLJ[4]->initializeMSHistograms("eta5","LJ");

    /*histEtaCR[0]->initializeCRHistograms("eta1","AllJets");
    histEtaCR[1]->initializeCRHistograms("eta2","AllJets");
    histEtaCR[2]->initializeCRHistograms("eta3","AllJets");
    histEtaCR[3]->initializeCRHistograms("eta4","AllJets");
    histEtaCR[4]->initializeCRHistograms("eta5","AllJets");

    histEtaLJCR[0]->initializeCRHistograms("eta1","LJ");
    histEtaLJCR[1]->initializeCRHistograms("eta2","LJ");
    histEtaLJCR[2]->initializeCRHistograms("eta3","LJ");
    histEtaLJCR[3]->initializeCRHistograms("eta4","LJ");
    histEtaLJCR[4]->initializeCRHistograms("eta5","LJ");*/

    //

    timer.Start();

    setJetSliceWeights(jetSliceWeights);

    int jetSlice = -1;
    double finalWeight = 0;
    int nEvents = chain->GetEntries();
    for (int l=0;l<chain->GetEntries();l++)
    {
        if(l % 500000 == 0){
            timer.Stop();
            std::cout << "event " << l  << "/"<< nEvents << ", " << timer.RealTime() << " s." << std::endl;
            timer.Start();
        }

        chain->GetEntry(l);

        TString inputFile = TString(chain->GetCurrentFile()->GetName());

        //TString inputFile = chain->GetFile()->GetName();
        if( l % 500000 == 0 ) std::cout << "running over file: " << inputFile << std::endl;

/*        if(   inputFile.Contains("JZ")    ){
            isSignal = 0; isData = 0;
            if(inputFile.Contains("JZ0W")) jetSlice = 0;
            if(inputFile.Contains("JZ1W")) jetSlice = 1;
            if(inputFile.Contains("JZ2W")) jetSlice = 2;
            if(inputFile.Contains("JZ3W")) jetSlice = 3;
            if(inputFile.Contains("JZ4W")) jetSlice = 4;
            if(inputFile.Contains("JZ5W")) jetSlice = 5;
            if(inputFile.Contains("JZ6W")){
                std::cout << "is jz6w!" << std::endl;
                jetSlice = 6;
            }
            if(inputFile.Contains("JZ7W")) jetSlice = 7;
            if(inputFile.Contains("JZ8W")) jetSlice = 8;
            if(inputFile.Contains("JZ9W")) jetSlice = 9;
            if(inputFile.Contains("JZ10W")) jetSlice = 10;
            if(inputFile.Contains("JZ11W")) jetSlice = 11;
            if(inputFile.Contains("JZ12W")) jetSlice = 12;
        }
        if( inputFile.Contains("LongLived") || inputFile.Contains("_LLP") || inputFile.Contains("Stealth") ){ isSignal = 1; isData = 0; }
        if( inputFile.Contains("data15_13TeV")){ isSignal=0; isData = 1;}

        if(!isData){
            if(isSignal == 0 ) finalWeight = jetSliceWeights[jetSlice] * commonVar->eventWeight * commonVar->pileupEventWeight;
            else if( isSignal == 1 ) finalWeight = 1.0;
            else std::cout << "what on earth are you running on? Input file is " << inputFile << std::endl;
            if(l==0) std::cout << "isSignal: " << isSignal << std::endl;
        }
        else if(isData)*/ finalWeight = 1.0;
        //if(finalWeight > 10) finalWeight = 1.0;  
        //std::cout << jetSliceWeights[jetSlice] <<" * " << commonVar->eventWeight << " =  " << finalWeight << std::endl;

        //see if event is a good dijet event
        int indexLJ = 0;
        int indexSLJ = 1;

        if(commonVar->Jet_eta->size() < 2){
            trigVar->clearAllVectors();
            variables->clearAllVectors();
            commonVar->clearAllVectors(false,true);
            if(doCRVariables) crVar->clearAllVectors();
            continue;
        }
        commonVar->fixJetET();
        commonVar->calcMHT();

        /*if(!commonVar->isGoodDijetEvent()){
            trigVar->clearAllVectors();
            variables->clearAllVectors();
            commonVar->clearAllVectors(false, true);
            if(doCRVariables) crVar->clearAllVectors();
            continue;
        }*/
        
        /*std::cout << "subleading/leading jet?!? " << indexLJ << ", " << indexSLJ << std::endl;
    	std::cout << "eta/phi size: " << commonVar->Jet_eta->size()<<", " << commonVar->Jet_phi->size() << std::endl;
    	std::cout << "LJ: " << commonVar->Jet_eta->at(indexLJ)<<", " << commonVar->Jet_phi->at(indexLJ) << std::endl;
    	std::cout << "SLJ: " << commonVar->Jet_eta->at(indexSLJ)<<", " << commonVar->Jet_phi->at(indexSLJ) << std::endl;
         */

        // if(commonVar->Jet_isGoodLLP->at(indexLJ) == 0 || commonVar->Jet_isGoodLLP->at(indexSLJ) == 0) continue; //one of hte jets is bad! 

        if(TMath::Abs(wrapPhi(commonVar->Jet_phi->at(indexLJ) - commonVar->Jet_phi->at(indexSLJ))) < 3.0) continue; //need two back-to-back jets
        if(TMath::Abs(commonVar->Jet_eta->at(indexLJ)) > 2.7 || TMath::Abs(commonVar->Jet_eta->at(indexSLJ)) > 2.7){
            trigVar->clearAllVectors();
            variables->clearAllVectors();
            commonVar->clearAllVectors(false,true);
            if(doCRVariables) crVar->clearAllVectors();
            continue; //dont want forward jets
        }


        int i2 = -1; int i1 = -1;
        if(TMath::Abs(commonVar->Jet_eta->at(indexLJ)) < 0.8) i1 = 0;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexLJ)) < 1.3) i1 = 1;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexLJ)) < 2.5) i1 = 2;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexLJ)) < 1.3) i1 = 3;
        //else if(TMath::Abs(commonVar->Jet_eta->at(indexLJ)) < 2.7) i1 = 4;
        else i1 = 4;//continue;
        if(TMath::Abs(commonVar->Jet_eta->at(indexSLJ)) < 0.8) i2 = 0;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexSLJ)) < 1.3) i2 = 1;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexSLJ)) < 2.5) i2 = 2;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexSLJ)) < 1.3) i2 = 3;
        //else if(TMath::Abs(commonVar->Jet_eta->at(indexSLJ)) < 2.7) i2 = 4;
        else i2 = 4;//continue;

        int pt1 = 0; int pt2 = 0;
        if(commonVar->Jet_nAssocMSeg->at(indexLJ) > 20) pt1 = 1;
        if(commonVar->Jet_nAssocMSeg->at(indexSLJ) > 20) pt2 = 1;


        double ljeta = commonVar->Jet_eta->at(indexLJ);
        double ljphi = commonVar->Jet_phi->at(indexLJ);
        double sljeta = commonVar->Jet_eta->at(indexSLJ);
        double sljphi = commonVar->Jet_phi->at(indexSLJ);

        int nInConeLJ[40]; for(int i=0;i<40;i++) nInConeLJ[i]=0;
        int nInConeSLJ[40]; for(int i=0;i<40;i++) nInConeSLJ[i]=0;
        double aa = 0.05;

        for(size_t tt = 0; tt < variables->tracklet_eta->size(); tt++){
            double eta = variables->tracklet_eta->at(tt);
            double phi = variables->tracklet_phi->at(tt);
            double dR_lj = DeltaR(phi,ljphi,eta,ljeta);
            double dR_slj = DeltaR(phi,sljphi,eta,sljeta);
            for(int i=1;i<41;i++){
                if(dR_lj < aa * double(i) ) nInConeLJ[i-1]++;
                if(dR_slj < aa * double(i) ) nInConeSLJ[i-1]++;
            }
        }

        // std::cout << "good dijet event, with eta regions : " << i1 <<" , " << i2 << std::endl;

        int nTrig_LJ = 0; int nTrig_SLJ = 0;
        for( size_t i_t=0; i_t < trigVar->eta->size(); i_t++){

            double trig_eta = trigVar->eta->at(i_t);
            double trig_phi = trigVar->phi->at(i_t);
            double trig_nRoI = trigVar->nRoI->at(i_t);

            if(TMath::Abs(trig_eta) < 1.0 && trig_nRoI >= 3){
                if(DeltaR2(trig_phi,ljphi,trig_eta,ljeta) < 0.4*0.4  ){
                    nTrig_LJ++;
                }
        if(DeltaR2(trig_phi,sljphi,trig_eta,sljeta) < 0.4*0.4){
            nTrig_SLJ++;
        }
            } else if(TMath::Abs(trig_eta) < 2.5 && trig_nRoI >= 4){     
                if(DeltaR2(trig_phi,ljphi,trig_eta,ljeta) < 0.4*0.4  ){
                    nTrig_LJ++;
        }
        if(DeltaR2(trig_phi,sljphi,trig_eta,sljeta) < 0.4*0.4  ){
                    nTrig_SLJ++;
                }
            }
        }

        int nMSVx_LJ=0; double MSVx_nMDT_LJ = 0;
        int nMSVx_SLJ=0; double MSVx_nMDT_SLJ = 0;

        //std::cout << "Event has " << variables->isGood->size() << " vertices " << std::endl;  
        for (size_t ll=0;ll<variables->eta->size();ll++)
        {
            //std::cout << "start looking at vertex " << ll << std::endl;
            if(variables->nMDT->at(ll)==0) continue; //skip dummy vertices (when too many tracklets in cluster/event
            int msvx_nMDT = variables->nMDT->at(ll);
            double msvx_eta = variables->eta->at(ll);
            double msvx_phi = variables->phi->at(ll);
            //bool   isGood = variables->isGood->at(ll);

            /*if ( fabs(msvx_eta)<0.8 ){
                isBarrel = 1;
                nBMSVx+=finalWeight;
                //std::cout << "vertex in barrel! " <<std::endl;
            }
            else if ( fabs(msvx_eta)> 1.3 && fabs(msvx_eta) < 2.5 ){
                nEMSVx+=finalWeight;
                isEndcap = 1;// for endcaps
                //std::cout << "Vertex in endcaps!" << std::endl;
            }
            else{
                //std::cout << "Vertex found, but in neither barrel nor endcaps" << std::endl;
                continue;
            }*/
            //
            //std::cout << "in vertex loop: vertex " << ll << ". jet indices: " << indexLJ <<", " << indexSLJ<< std::endl;

            //std::cout << "LJ: " << ljeta<<", " << ljphi << ", deltaR " << DeltaR(msvx_phi,ljphi,msvx_eta,ljeta) << std::endl;
            //std::cout << "SLJ: " << sljeta<<", " << sljphi	 << ", deltaR " << DeltaR(msvx_phi,sljphi,msvx_eta,sljeta) << std::endl;
            if( DeltaR2(msvx_phi,ljphi,msvx_eta,ljeta) < 0.4*0.4 ){
                //std::cout << "vertex match LJ " << std::endl;

                MSVx_nMDT_LJ += msvx_nMDT;
                nMSVx_LJ++;
            }
            if( DeltaR2(msvx_phi,sljphi,msvx_eta,sljeta) < 0.4*0.4 ){
                //std::cout << "vertex match SLJ " << std::endl;

                MSVx_nMDT_SLJ += msvx_nMDT;
                nMSVx_SLJ++;
            }
            //std::cout << "finished looking at vertex " << ll << std::endl;

        }
        //std::cout << "after vertex loop " << std::endl;


        histEtaFull->h_nMSeg_denom->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
        histEtaFull->h_nMSeg_denom->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);
        histEtaFullLJ->h_nMSeg_denom->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_nMSeg_denom->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);

        histEtaFull->h_nMSeg_nMuonRoIs->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ)); 
        histEtaFull->h_nMSeg_nMuonRoIs->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ));
        histEtaFullLJ->h_nMSeg_nMuonRoIs->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ));
        histEtaFullSLJ->h_nMSeg_nMuonRoIs->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ));

        histEtaFull->h_nMSeg_nMSTracklets->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight*nInConeLJ[7]); 
        histEtaFull->h_nMSeg_nMSTracklets->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight*nInConeSLJ[7]);
        histEtaFullLJ->h_nMSeg_nMSTracklets->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight*nInConeLJ[7]);
        histEtaFullSLJ->h_nMSeg_nMSTracklets->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight*nInConeSLJ[7]);
        //std::cout << "filled ms histograms 1" << std::endl;

        if(nMSVx_LJ){
            histEtaFull->h_nMSeg_pMSVx->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
            histEtaFullLJ->h_nMSeg_pMSVx->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
        }
        if(nMSVx_SLJ ){
            histEtaFull->h_nMSeg_pMSVx->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);
            histEtaFullSLJ->h_nMSeg_pMSVx->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);
        } 
        if(nTrig_LJ){
            histEtaFull->h_nMSeg_pTrig->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
            histEtaFullLJ->h_nMSeg_pTrig->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
        }
        if(nTrig_SLJ ){
            histEtaFull->h_nMSeg_pTrig->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);
            histEtaFullSLJ->h_nMSeg_pTrig->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);
        }         
        //std::cout << "filled ms histograms 2" << std::endl;

        histEtaFull->h_nMSVx->Fill(nMSVx_LJ,finalWeight); histEtaFull->h_nMSVx->Fill(nMSVx_SLJ,finalWeight);
        histEtaFull->h_MSVx_nMDT->Fill(MSVx_nMDT_LJ,finalWeight); histEtaFull->h_MSVx_nMDT->Fill(MSVx_nMDT_SLJ,finalWeight);

        if(nMSVx_LJ && pt1){
            histEtaFull->h_eta_pMSVx->Fill(fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
            histEtaFull->h_pT_pMSVx->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
            histEtaFullLJ->h_eta_pMSVx->Fill(fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
            histEtaFullLJ->h_pT_pMSVx->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
            histEtaFull->h_pT_eta_pMSVx->Fill(commonVar->Jet_pT->at(indexLJ),
                    fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
            histEtaFullLJ->h_pT_eta_pMSVx->Fill(commonVar->Jet_pT->at(indexLJ),
                    fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
        }
        if(nMSVx_SLJ && pt2){
            histEtaFull->h_eta_pMSVx->Fill(fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
            histEtaFull->h_pT_pMSVx->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);
            histEtaFullSLJ->h_eta_pMSVx->Fill(fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
            histEtaFullSLJ->h_pT_pMSVx->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);
            histEtaFull->h_pT_eta_pMSVx->Fill(commonVar->Jet_pT->at(indexSLJ),
                    fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
            histEtaFullSLJ->h_pT_eta_pMSVx->Fill(commonVar->Jet_pT->at(indexSLJ),
                    fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
        }
        //std::cout << "filled ms histograms 3" << std::endl;

        if(nTrig_LJ && pt1){
            histEtaFull->h_eta_pTrig->Fill(fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
            histEtaFull->h_pT_pTrig->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
            histEtaFullLJ->h_eta_pTrig->Fill(fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
            histEtaFullLJ->h_pT_pTrig->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
            histEtaFull->h_pT_eta_pTrig->Fill(commonVar->Jet_pT->at(indexLJ),
                    fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
            histEtaFullLJ->h_pT_eta_pTrig->Fill(commonVar->Jet_pT->at(indexLJ),
                    fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
        }
        if(nTrig_SLJ && pt2){
            histEtaFull->h_eta_pTrig->Fill(fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
            histEtaFull->h_pT_pTrig->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);
            histEtaFullSLJ->h_eta_pTrig->Fill(fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
            histEtaFullSLJ->h_pT_pTrig->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);
            histEtaFull->h_pT_eta_pTrig->Fill(commonVar->Jet_pT->at(indexSLJ),
                    fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
            histEtaFullSLJ->h_pT_eta_pTrig->Fill(commonVar->Jet_pT->at(indexSLJ),
                    fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
        }
        //std::cout << "filled ms histograms 4" << std::endl;

        histEtaFullLJ->h_nMSVx->Fill(nMSVx_LJ,finalWeight);
        histEtaFullLJ->h_MSVx_nMDT->Fill(MSVx_nMDT_LJ,finalWeight);

        histEtaFullSLJ->h_nMSVx->Fill(nMSVx_SLJ,finalWeight);
        histEtaFullSLJ->h_MSVx_nMDT->Fill(MSVx_nMDT_SLJ,finalWeight);


        histEtaFull->h_eta->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight);
        histEtaFull->h_eta->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight);
        histEtaFullLJ->h_eta->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_eta->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight);
        //std::cout << "filled ms histograms 5" << std::endl;

        if(pt1){
            histEtaFull->h_eta_denom->Fill(fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
            histEtaFullLJ->h_eta_denom->Fill(fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
            histEtaFull->h_pT_eta_denom->Fill(commonVar->Jet_pT->at(indexLJ),
                    fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
            histEtaFullLJ->h_pT_eta_denom->Fill(commonVar->Jet_pT->at(indexLJ),
                    fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
            histEtaFull->h_pT_denom->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
            histEtaFullLJ->h_pT_denom->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
        }
        if(pt2){
            histEtaFull->h_eta_denom->Fill(fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
            histEtaFullSLJ->h_eta_denom->Fill(fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
            histEtaFull->h_pT_eta_denom->Fill(commonVar->Jet_pT->at(indexSLJ),
                    fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
            histEtaFullSLJ->h_pT_eta_denom->Fill(commonVar->Jet_pT->at(indexSLJ),
                    fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
            histEtaFull->h_pT_denom->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);
            histEtaFullSLJ->h_pT_denom->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);

        }
        //std::cout << "filled ms histograms 6" << std::endl;

        histEtaFull->h_phi->Fill(commonVar->Jet_phi->at(indexLJ),finalWeight); histEtaFull->h_phi->Fill(commonVar->Jet_phi->at(indexSLJ),finalWeight);
        histEtaFullLJ->h_phi->Fill(commonVar->Jet_phi->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_phi->Fill(commonVar->Jet_phi->at(indexSLJ),finalWeight);

        histEtaFull->h_pT->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight); histEtaFull->h_pT->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);
        histEtaFullLJ->h_pT->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_pT->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);

        //std::cout << "filled ms histograms 7" << std::endl;

        histEtaFull->h_ET->Fill(commonVar->Jet_ET->at(indexLJ),finalWeight); 
        histEtaFull->h_ET->Fill(commonVar->Jet_ET->at(indexSLJ),finalWeight);
        histEtaFullLJ->h_ET->Fill(commonVar->Jet_ET->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_ET->Fill(commonVar->Jet_ET->at(indexSLJ),finalWeight);

        histEtaFull->h_logRatio->Fill(commonVar->Jet_logRatio->at(indexLJ),finalWeight); 
        histEtaFull->h_logRatio->Fill(commonVar->Jet_logRatio->at(indexSLJ),finalWeight);
        histEtaFullLJ->h_logRatio->Fill(commonVar->Jet_logRatio->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_logRatio->Fill(commonVar->Jet_logRatio->at(indexSLJ),finalWeight);

        histEtaFull->h_nAssocMSeg->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight); 
        histEtaFull->h_nAssocMSeg->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);
        histEtaFullLJ->h_nAssocMSeg->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_nAssocMSeg->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);

        //std::cout << "filled ms histograms 8" << std::endl;

        //histEtaFull->h_nMSegCone->Fill(commonVar->Jet_nMSegCone->at(indexLJ),finalWeight); histEtaFull->h_nMSegCone->Fill(commonVar->Jet_nMSegCone->at(indexSLJ),finalWeight);
        //histEtaFullLJ->h_nMSegCone->Fill(commonVar->Jet_nMSegCone->at(indexLJ),finalWeight);
        //histEtaFullSLJ->h_nMSegCone->Fill(commonVar->Jet_nMSegCone->at(indexSLJ),finalWeight);
        if(doCRVariables){
        histEtaFull->h_CR_BDT->Fill(crVar->BDT->at(indexLJ), finalWeight);
        histEtaFull->h_CR_nTrk->Fill(crVar->nTrk->at(indexLJ), finalWeight);
        histEtaFull->h_CR_sumTrkpT->Fill(crVar->sumTrkpT->at(indexLJ), finalWeight);
        histEtaFull->h_CR_maxTrkpT->Fill(crVar->maxTrkpT->at(indexLJ), finalWeight);
        histEtaFull->h_CR_width->Fill(crVar->width->at(indexLJ), finalWeight);
        histEtaFull->h_CR_jetDRTo2GeVTrack->Fill(crVar->minDRTrkpt2->at(indexLJ), finalWeight);
        histEtaFull->h_CR_l1frac->Fill(crVar->l1frac->at(indexLJ), finalWeight);
        histEtaFull->h_CR_c_firenden->Fill(crVar->c_firenden->at(indexLJ), finalWeight);
        histEtaFull->h_CR_c_lat->Fill(crVar->c_lat->at(indexLJ), finalWeight);
        histEtaFull->h_CR_c_long->Fill(crVar->c_long->at(indexLJ), finalWeight);
        histEtaFull->h_CR_c_r->Fill(crVar->c_r->at(indexLJ), finalWeight);
        histEtaFull->h_CR_c_lambda->Fill(crVar->c_lambda->at(indexLJ), finalWeight);
        histEtaFull->h_CR_logR->Fill(commonVar->Jet_logRatio->at(indexLJ), finalWeight);

        //std::cout << "filled ms histograms 9" << std::endl;

        histEtaFull->h_CR_BDT->Fill(crVar->BDT->at(indexSLJ), finalWeight);
        histEtaFull->h_CR_nTrk->Fill(crVar->nTrk->at(indexSLJ), finalWeight);
        histEtaFull->h_CR_sumTrkpT->Fill(crVar->sumTrkpT->at(indexSLJ), finalWeight);
        histEtaFull->h_CR_maxTrkpT->Fill(crVar->maxTrkpT->at(indexSLJ), finalWeight);
        histEtaFull->h_CR_width->Fill(crVar->width->at(indexSLJ), finalWeight);
        histEtaFull->h_CR_jetDRTo2GeVTrack->Fill(crVar->minDRTrkpt2->at(indexSLJ), finalWeight);
        histEtaFull->h_CR_l1frac->Fill(crVar->l1frac->at(indexSLJ), finalWeight);
        histEtaFull->h_CR_c_firenden->Fill(crVar->c_firenden->at(indexSLJ), finalWeight);
        histEtaFull->h_CR_c_lat->Fill(crVar->c_lat->at(indexSLJ), finalWeight);
        histEtaFull->h_CR_c_long->Fill(crVar->c_long->at(indexSLJ), finalWeight);
        histEtaFull->h_CR_c_r->Fill(crVar->c_r->at(indexSLJ), finalWeight);
        histEtaFull->h_CR_c_lambda->Fill(crVar->c_lambda->at(indexSLJ), finalWeight);
        histEtaFull->h_CR_logR->Fill(commonVar->Jet_logRatio->at(indexSLJ), finalWeight);
        //std::cout << "filled ms histograms 10" << std::endl;

        histEtaFullLJ->h_CR_BDT->Fill(crVar->BDT->at(indexLJ), finalWeight);
        histEtaFullLJ->h_CR_nTrk->Fill(crVar->nTrk->at(indexLJ), finalWeight);
        histEtaFullLJ->h_CR_sumTrkpT->Fill(crVar->sumTrkpT->at(indexLJ), finalWeight);
        histEtaFullLJ->h_CR_maxTrkpT->Fill(crVar->maxTrkpT->at(indexLJ), finalWeight);
        histEtaFullLJ->h_CR_width->Fill(crVar->width->at(indexLJ), finalWeight);
        histEtaFullLJ->h_CR_jetDRTo2GeVTrack->Fill(crVar->minDRTrkpt2->at(indexLJ), finalWeight);
        histEtaFullLJ->h_CR_l1frac->Fill(crVar->l1frac->at(indexLJ), finalWeight);
        histEtaFullLJ->h_CR_c_firenden->Fill(crVar->c_firenden->at(indexLJ), finalWeight);
        histEtaFullLJ->h_CR_c_lat->Fill(crVar->c_lat->at(indexLJ), finalWeight);
        histEtaFullLJ->h_CR_c_long->Fill(crVar->c_long->at(indexLJ), finalWeight);
        histEtaFullLJ->h_CR_c_r->Fill(crVar->c_r->at(indexLJ), finalWeight);
        histEtaFullLJ->h_CR_c_lambda->Fill(crVar->c_lambda->at(indexLJ), finalWeight);
        histEtaFullLJ->h_CR_logR->Fill(commonVar->Jet_logRatio->at(indexLJ), finalWeight);
        //std::cout << "filled ms histograms 11" << std::endl;

        histEtaFullSLJ->h_CR_BDT->Fill(crVar->BDT->at(indexSLJ), finalWeight);
        histEtaFullSLJ->h_CR_nTrk->Fill(crVar->nTrk->at(indexSLJ), finalWeight);
        histEtaFullSLJ->h_CR_sumTrkpT->Fill(crVar->sumTrkpT->at(indexSLJ), finalWeight);
        histEtaFullSLJ->h_CR_maxTrkpT->Fill(crVar->maxTrkpT->at(indexSLJ), finalWeight);
        histEtaFullSLJ->h_CR_width->Fill(crVar->width->at(indexSLJ), finalWeight);
        histEtaFullSLJ->h_CR_jetDRTo2GeVTrack->Fill(crVar->minDRTrkpt2->at(indexSLJ), finalWeight);
        histEtaFullSLJ->h_CR_l1frac->Fill(crVar->l1frac->at(indexSLJ), finalWeight);
        histEtaFullSLJ->h_CR_c_firenden->Fill(crVar->c_firenden->at(indexSLJ), finalWeight);
        histEtaFullSLJ->h_CR_c_lat->Fill(crVar->c_lat->at(indexSLJ), finalWeight);
        histEtaFullSLJ->h_CR_c_long->Fill(crVar->c_long->at(indexSLJ), finalWeight);
        histEtaFullSLJ->h_CR_c_r->Fill(crVar->c_r->at(indexSLJ), finalWeight);
        histEtaFullSLJ->h_CR_c_lambda->Fill(crVar->c_lambda->at(indexSLJ), finalWeight);
        histEtaFullSLJ->h_CR_logR->Fill(commonVar->Jet_logRatio->at(indexSLJ), finalWeight);

        //ones of avg. of variable vs. logR:
        histEtaFull->h_CR_logR_BDT->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->BDT->at(indexLJ)*finalWeight);
        histEtaFull->h_CR_logR_nTrk->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->nTrk->at(indexLJ)*finalWeight);
        histEtaFull->h_CR_logR_sumTrkpT->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->sumTrkpT->at(indexLJ)*finalWeight);
        histEtaFull->h_CR_logR_maxTrkpT->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->maxTrkpT->at(indexLJ)*finalWeight);
        histEtaFull->h_CR_logR_width->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->width->at(indexLJ)*finalWeight);
        histEtaFull->h_CR_logR_jetDRTo2GeVTrack->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->minDRTrkpt2->at(indexLJ)*finalWeight);
        histEtaFull->h_CR_logR_l1frac->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->l1frac->at(indexLJ)*finalWeight);
        histEtaFull->h_CR_logR_c_firenden->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->c_firenden->at(indexLJ)*finalWeight);
        histEtaFull->h_CR_logR_c_lat->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->c_lat->at(indexLJ)*finalWeight);
        histEtaFull->h_CR_logR_c_long->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->c_long->at(indexLJ)*finalWeight);
        histEtaFull->h_CR_logR_c_r->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->c_r->at(indexLJ)*finalWeight);
        histEtaFull->h_CR_logR_c_lambda->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->c_lambda->at(indexLJ)*finalWeight);

        //std::cout << "filled ms histograms 9" << std::endl;

        histEtaFull->h_CR_logR_BDT->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->BDT->at(indexSLJ)*finalWeight);
        histEtaFull->h_CR_logR_nTrk->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->nTrk->at(indexSLJ)*finalWeight);
        histEtaFull->h_CR_logR_sumTrkpT->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->sumTrkpT->at(indexSLJ)*finalWeight);
        histEtaFull->h_CR_logR_maxTrkpT->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->maxTrkpT->at(indexSLJ)*finalWeight);
        histEtaFull->h_CR_logR_width->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->width->at(indexSLJ)*finalWeight);
        histEtaFull->h_CR_logR_jetDRTo2GeVTrack->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->minDRTrkpt2->at(indexSLJ)*finalWeight);
        histEtaFull->h_CR_logR_l1frac->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->l1frac->at(indexSLJ)*finalWeight);
        histEtaFull->h_CR_logR_c_firenden->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->c_firenden->at(indexSLJ)*finalWeight);
        histEtaFull->h_CR_logR_c_lat->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->c_lat->at(indexSLJ)*finalWeight);
        histEtaFull->h_CR_logR_c_long->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->c_long->at(indexSLJ)*finalWeight);
        histEtaFull->h_CR_logR_c_r->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->c_r->at(indexSLJ)*finalWeight);
        histEtaFull->h_CR_logR_c_lambda->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->c_lambda->at(indexSLJ)*finalWeight);
        //std::cout << "filled ms histograms 10" << std::endl;

        histEtaFullLJ->h_CR_logR_BDT->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->BDT->at(indexLJ)*finalWeight);
        histEtaFullLJ->h_CR_logR_nTrk->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->nTrk->at(indexLJ)*finalWeight);
        histEtaFullLJ->h_CR_logR_sumTrkpT->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->sumTrkpT->at(indexLJ)*finalWeight);
        histEtaFullLJ->h_CR_logR_maxTrkpT->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->maxTrkpT->at(indexLJ)*finalWeight);
        histEtaFullLJ->h_CR_logR_width->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->width->at(indexLJ)*finalWeight);
        histEtaFullLJ->h_CR_logR_jetDRTo2GeVTrack->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->minDRTrkpt2->at(indexLJ)*finalWeight);
        histEtaFullLJ->h_CR_logR_l1frac->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->l1frac->at(indexLJ)*finalWeight);
        histEtaFullLJ->h_CR_logR_c_firenden->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->c_firenden->at(indexLJ)*finalWeight);
        histEtaFullLJ->h_CR_logR_c_lat->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->c_lat->at(indexLJ)*finalWeight);
        histEtaFullLJ->h_CR_logR_c_long->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->c_long->at(indexLJ)*finalWeight);
        histEtaFullLJ->h_CR_logR_c_r->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->c_r->at(indexLJ)*finalWeight);
        histEtaFullLJ->h_CR_logR_c_lambda->Fill(commonVar->Jet_logRatio->at(indexLJ), crVar->c_lambda->at(indexLJ)*finalWeight);
        //std::cout << "filled ms histograms 11" << std::endl;

        histEtaFullSLJ->h_CR_logR_BDT->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->BDT->at(indexSLJ)*finalWeight);
        histEtaFullSLJ->h_CR_logR_nTrk->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->nTrk->at(indexSLJ)*finalWeight);
        histEtaFullSLJ->h_CR_logR_sumTrkpT->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->sumTrkpT->at(indexSLJ)*finalWeight);
        histEtaFullSLJ->h_CR_logR_maxTrkpT->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->maxTrkpT->at(indexSLJ)*finalWeight);
        histEtaFullSLJ->h_CR_logR_width->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->width->at(indexSLJ)*finalWeight);
        histEtaFullSLJ->h_CR_logR_jetDRTo2GeVTrack->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->minDRTrkpt2->at(indexSLJ)*finalWeight);
        histEtaFullSLJ->h_CR_logR_l1frac->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->l1frac->at(indexSLJ)*finalWeight);
        histEtaFullSLJ->h_CR_logR_c_firenden->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->c_firenden->at(indexSLJ)*finalWeight);
        histEtaFullSLJ->h_CR_logR_c_lat->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->c_lat->at(indexSLJ)*finalWeight);
        histEtaFullSLJ->h_CR_logR_c_long->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->c_long->at(indexSLJ)*finalWeight);
        histEtaFullSLJ->h_CR_logR_c_r->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->c_r->at(indexSLJ)*finalWeight);
        histEtaFullSLJ->h_CR_logR_c_lambda->Fill(commonVar->Jet_logRatio->at(indexSLJ), crVar->c_lambda->at(indexSLJ)*finalWeight);
        }
        //std::cout << "filled ms histograms 12" << std::endl;

        if(pt1) histEtaFull->h_nMSTracklets->Fill(nInConeLJ[7],finalWeight);
        if(pt1) histEtaFullLJ->h_nMSTracklets->Fill(nInConeLJ[7],finalWeight);
        if(pt2) histEtaFull->h_nMSTracklets->Fill(nInConeLJ[7],finalWeight);
        if(pt2) histEtaFullSLJ->h_nMSTracklets->Fill(nInConeSLJ[7],finalWeight);


        if(pt1) histEtaFull->h_nMuonRoIs->Fill(commonVar->Jet_nMuonRoIs->at(indexLJ),finalWeight);
        if(pt2) histEtaFull->h_nMuonRoIs->Fill(commonVar->Jet_nMuonRoIs->at(indexSLJ),finalWeight);
        if(pt1) histEtaFullLJ->h_nMuonRoIs->Fill(commonVar->Jet_nMuonRoIs->at(indexLJ),finalWeight);
        if(pt2) histEtaFullSLJ->h_nMuonRoIs->Fill(commonVar->Jet_nMuonRoIs->at(indexSLJ),finalWeight);

        //std::cout << "filled ms histograms 13" << std::endl;

        histEtaFull->h_eta_nAssocMSeg->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ)); 
        histEtaFull->h_eta_nAssocMSeg->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ));
        histEtaFullLJ->h_eta_nAssocMSeg->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ));
        histEtaFullSLJ->h_eta_nAssocMSeg->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ));

        histEtaFull->h_eta_nMSTracklets->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*nInConeLJ[7]); 
        histEtaFull->h_eta_nMSTracklets->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight*nInConeSLJ[7]);
        histEtaFullLJ->h_eta_nMSTracklets->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*nInConeLJ[7]);
        histEtaFullSLJ->h_eta_nMSTracklets->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight*nInConeSLJ[7]);

        histEtaFull->h_eta_nMuonRoIs->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ)); 
        histEtaFull->h_eta_nMuonRoIs->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ));
        histEtaFullLJ->h_eta_nMuonRoIs->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ));
        histEtaFullSLJ->h_eta_nMuonRoIs->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ));

        //std::cout << "filled ms histograms 14" << std::endl;

        histEtaFull->h_pT_nAssocMSeg->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ)); 
        histEtaFull->h_pT_nAssocMSeg->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ));
        histEtaFullLJ->h_pT_nAssocMSeg->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ));
        histEtaFullSLJ->h_pT_nAssocMSeg->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ));

        histEtaFull->h_pT_nMSTracklets->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*nInConeLJ[7]); 
        histEtaFull->h_pT_nMSTracklets->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*nInConeSLJ[7]);
        histEtaFullLJ->h_pT_nMSTracklets->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*nInConeLJ[7]);
        histEtaFullSLJ->h_pT_nMSTracklets->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*nInConeSLJ[7]);

        histEtaFull->h_pT_nMuonRoIs->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ)); 
        histEtaFull->h_pT_nMuonRoIs->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ));
        histEtaFullLJ->h_pT_nMuonRoIs->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ));
        histEtaFullSLJ->h_pT_nMuonRoIs->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ));

        histEtaFull->h_pT_nAssocMSeg_num->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ)); 
        histEtaFull->h_pT_nAssocMSeg_num->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ));
        histEtaFullLJ->h_pT_nAssocMSeg_num->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ));
        histEtaFullSLJ->h_pT_nAssocMSeg_num->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ));

        histEtaFull->h_pT_nMSTracklets_num->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*nInConeLJ[7]); 
        histEtaFull->h_pT_nMSTracklets_num->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*nInConeSLJ[7]);
        histEtaFullLJ->h_pT_nMSTracklets_num->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*nInConeLJ[7]);
        histEtaFullSLJ->h_pT_nMSTracklets_num->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*nInConeSLJ[7]);

        histEtaFull->h_pT_nMuonRoIs_num->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ)); 
        histEtaFull->h_pT_nMuonRoIs_num->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ));
        histEtaFullLJ->h_pT_nMuonRoIs_num->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ));
        histEtaFullSLJ->h_pT_nMuonRoIs_num->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ));
        
        //for RMS error
        
        histEtaFull->h_pT_nAssocMSeg_sq->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ)*commonVar->Jet_nAssocMSeg->at(indexLJ)); 
        histEtaFull->h_pT_nAssocMSeg_sq->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ)*commonVar->Jet_nAssocMSeg->at(indexSLJ));
        histEtaFullLJ->h_pT_nAssocMSeg_sq->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ)*commonVar->Jet_nAssocMSeg->at(indexLJ));
        histEtaFullSLJ->h_pT_nAssocMSeg_sq->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ)*commonVar->Jet_nAssocMSeg->at(indexSLJ));

        histEtaFull->h_pT_nMSTracklets_sq->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*nInConeLJ[7]*nInConeLJ[7]); 
        histEtaFull->h_pT_nMSTracklets_sq->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*nInConeSLJ[7]*nInConeSLJ[7]);
        histEtaFullLJ->h_pT_nMSTracklets_sq->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*nInConeLJ[7]*nInConeLJ[7]);
        histEtaFullSLJ->h_pT_nMSTracklets_sq->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*nInConeSLJ[7]*nInConeSLJ[7]);

        histEtaFull->h_pT_nMuonRoIs_sq->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ)*commonVar->Jet_nMuonRoIs->at(indexLJ)); 
        histEtaFull->h_pT_nMuonRoIs_sq->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ)*commonVar->Jet_nMuonRoIs->at(indexSLJ));
        histEtaFullLJ->h_pT_nMuonRoIs_sq->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ)*commonVar->Jet_nMuonRoIs->at(indexLJ));
        histEtaFullSLJ->h_pT_nMuonRoIs_sq->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ)*commonVar->Jet_nMuonRoIs->at(indexSLJ));
        
        //std::cout << "filled ms histograms 15" << std::endl;



        for(int i=0;i<40;i++){
            double xVal = aa*double(i) + 0.005;
            histEtaFull->h_cone_avgTracklets->Fill(xVal, finalWeight*nInConeLJ[i]);  
            histEtaFull->h_cone_avgTracklets->Fill(xVal, finalWeight*nInConeSLJ[i]);   
            histEtaFullLJ->h_cone_avgTracklets->Fill(xVal, finalWeight*nInConeLJ[i]);  
            histEtaFullSLJ->h_cone_avgTracklets->Fill(xVal, finalWeight*nInConeSLJ[i]);   
            if(pt1) histEta[i1]->h_cone_avgTracklets->Fill(xVal, finalWeight*nInConeLJ[i]);
            if(pt1) histEtaLJ[i1]->h_cone_avgTracklets->Fill(xVal, finalWeight*nInConeLJ[i]);
            if(pt2) histEta[i2]->h_cone_avgTracklets->Fill(xVal, finalWeight*nInConeSLJ[i]);
        }
        //std::cout << "filled ms histograms 16" << std::endl;

        if(pt1){
            histEta[i1]->h_eta_denom->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight);
            histEta[i1]->h_pT_denom->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
            histEta[i1]->h_pT_eta_denom->Fill(commonVar->Jet_pT->at(indexLJ),
                    fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
            histEtaLJ[i1]->h_eta_denom->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight);
            histEtaLJ[i1]->h_pT_denom->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
            histEtaLJ[i1]->h_pT_eta_denom->Fill(commonVar->Jet_pT->at(indexLJ),
                    fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
            histEtaLJ[i1]->h_nMSVx->Fill(nMSVx_LJ,finalWeight);
            histEta[i1]->h_nMSVx->Fill(nMSVx_LJ,finalWeight);
            if(nMSVx_LJ){
                histEtaLJ[i1]->h_eta_pMSVx->Fill(fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
                histEta[i1]->h_eta_pMSVx->Fill(fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
                histEtaLJ[i1]->h_pT_pMSVx->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
                histEta[i1]->h_pT_pMSVx->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
                histEtaLJ[i1]->h_pT_eta_pMSVx->Fill(commonVar->Jet_pT->at(indexLJ),
                        fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
                histEta[i1]->h_pT_eta_pMSVx->Fill(commonVar->Jet_pT->at(indexLJ),
                        fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);

            }
        }
        if(pt2){
            histEta[i2]->h_eta_denom->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight);
            histEta[i2]->h_pT_denom->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);
            histEta[i2]->h_pT_eta_denom->Fill(commonVar->Jet_pT->at(indexSLJ),
                    fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);       
            histEta[i2]->h_nMSVx->Fill(nMSVx_SLJ,finalWeight);

            if(nMSVx_SLJ){
                histEta[i2]->h_eta_pMSVx->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight);
                histEta[i2]->h_pT_pMSVx->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);
                histEta[i2]->h_pT_eta_pMSVx->Fill(commonVar->Jet_pT->at(indexSLJ),
                        fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
            }
        }
        //std::cout << "filled ms histograms 17" << std::endl;

        if(pt1){
            if(nTrig_LJ){
                histEtaLJ[i1]->h_eta_pTrig->Fill(fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
                histEta[i1]->h_eta_pTrig->Fill(fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
                histEtaLJ[i1]->h_pT_pTrig->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
                histEta[i1]->h_pT_pTrig->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
                histEtaLJ[i1]->h_pT_eta_pTrig->Fill(commonVar->Jet_pT->at(indexLJ),
                        fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);
                histEta[i1]->h_pT_eta_pTrig->Fill(commonVar->Jet_pT->at(indexLJ),
                        fabs(commonVar->Jet_eta->at(indexLJ)),finalWeight);

            }
        }
        if(pt2){
            if(nTrig_SLJ){
                histEta[i2]->h_eta_pTrig->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight);
                histEta[i2]->h_pT_pTrig->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);
                histEta[i2]->h_pT_eta_pTrig->Fill(commonVar->Jet_pT->at(indexSLJ),
                        fabs(commonVar->Jet_eta->at(indexSLJ)),finalWeight);
            }
        }
        //std::cout << "filled ms histograms 18" << std::endl;

        if(pt1) histEtaLJ[i1]->h_MSVx_nMDT->Fill(MSVx_nMDT_LJ,finalWeight);
        if(pt1) histEta[i1]->h_MSVx_nMDT->Fill(MSVx_nMDT_LJ,finalWeight);
        if(pt2) histEta[i2]->h_MSVx_nMDT->Fill(MSVx_nMDT_SLJ,finalWeight);

        if(pt1) histEtaLJ[i1]->h_eta->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight);
        if(pt1) histEta[i1]->h_eta->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_eta->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight);

        if(pt1) histEtaLJ[i1]->h_phi->Fill(commonVar->Jet_phi->at(indexLJ),finalWeight);
        if(pt1) histEta[i1]->h_phi->Fill(commonVar->Jet_phi->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_phi->Fill(commonVar->Jet_phi->at(indexSLJ),finalWeight);


        if(pt1) histEtaLJ[i1]->h_pT->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
        if(pt1) histEta[i1]->h_pT->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_pT->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);

        if(pt1) histEtaLJ[i1]->h_ET->Fill(commonVar->Jet_ET->at(indexLJ),finalWeight);
        if(pt1) histEta[i1]->h_ET->Fill(commonVar->Jet_ET->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_ET->Fill(commonVar->Jet_ET->at(indexSLJ),finalWeight);

        if(pt1) histEtaLJ[i1]->h_logRatio->Fill(commonVar->Jet_logRatio->at(indexLJ),finalWeight);
        if(pt1) histEta[i1]->h_logRatio->Fill(commonVar->Jet_logRatio->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_logRatio->Fill(commonVar->Jet_logRatio->at(indexSLJ),finalWeight);

        //std::cout << "filled ms histograms 19" << std::endl;

        if(pt1) histEtaLJ[i1]->h_nAssocMSeg->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
        if(pt1) histEta[i1]->h_nAssocMSeg->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_nAssocMSeg->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);


        //if(pt1) histEtaLJ[i1]->h_nMSegCone->Fill(commonVar->Jet_nMSegCone->at(indexLJ),finalWeight);
        //if(pt1) histEta[i1]->h_nMSegCone->Fill(commonVar->Jet_nMSegCone->at(indexLJ),finalWeight);
        //if(pt2) histEta[i2]->h_nMSegCone->Fill(commonVar->Jet_nMSegCone->at(indexSLJ),finalWeight);


        if(pt1) histEtaLJ[i1]->h_nMSTracklets->Fill(nInConeLJ[7],finalWeight);
        if(pt1) histEta[i1]->h_nMSTracklets->Fill(nInConeLJ[7],finalWeight);
        if(pt2) histEta[i2]->h_nMSTracklets->Fill(nInConeLJ[7],finalWeight);



        if(pt1) histEtaLJ[i1]->h_nMuonRoIs->Fill(commonVar->Jet_nMuonRoIs->at(indexLJ),finalWeight);
        if(pt1) histEta[i1]->h_nMuonRoIs->Fill(commonVar->Jet_nMuonRoIs->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_nMuonRoIs->Fill(commonVar->Jet_nMuonRoIs->at(indexSLJ),finalWeight);


        if(pt1) histEtaLJ[i1]->h_eta_nAssocMSeg->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ));
        if(pt1) histEta[i1]->h_eta_nAssocMSeg->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ));
        if(pt2) histEta[i2]->h_eta_nAssocMSeg->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ));

        if(pt1) histEtaLJ[i1]->h_eta_nMSTracklets->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*nInConeLJ[7]);
        if(pt1) histEta[i1]->h_eta_nMSTracklets->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*nInConeLJ[7]);
        if(pt2) histEta[i2]->h_eta_nMSTracklets->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight*nInConeSLJ[7]);

        if(pt1) histEtaLJ[i1]->h_eta_nMuonRoIs->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ));
        if(pt1) histEta[i1]->h_eta_nMuonRoIs->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ));
        if(pt2) histEta[i2]->h_eta_nMuonRoIs->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ));

        if(pt1) histEtaLJ[i1]->h_pT_nAssocMSeg_num->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ));
        if(pt1) histEta[i1]->h_pT_nAssocMSeg_num->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ));
        if(pt2) histEta[i2]->h_pT_nAssocMSeg_num->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ));

        if(pt1) histEtaLJ[i1]->h_pT_nMSTracklets_num->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*nInConeLJ[7]);
        if(pt1) histEta[i1]->h_pT_nMSTracklets_num->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*nInConeLJ[7]);
        if(pt2) histEta[i2]->h_pT_nMSTracklets_num->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*nInConeSLJ[7]);

        if(pt1) histEtaLJ[i1]->h_pT_nMuonRoIs_num->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ));
        if(pt1) histEta[i1]->h_pT_nMuonRoIs_num->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ));
        if(pt2) histEta[i2]->h_pT_nMuonRoIs_num->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ));

        //for RMS error
        
        if(pt1) histEtaLJ[i1]->h_pT_nAssocMSeg_sq->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ)*commonVar->Jet_nAssocMSeg->at(indexLJ));
        if(pt1) histEta[i1]->h_pT_nAssocMSeg_sq->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ)*commonVar->Jet_nAssocMSeg->at(indexLJ));
        if(pt2) histEta[i2]->h_pT_nAssocMSeg_sq->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ)*commonVar->Jet_nAssocMSeg->at(indexSLJ));

        if(pt1) histEtaLJ[i1]->h_pT_nMSTracklets_sq->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*nInConeLJ[7]*nInConeLJ[7]);
        if(pt1) histEta[i1]->h_pT_nMSTracklets_sq->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*nInConeLJ[7]*nInConeLJ[7]);
        if(pt2) histEta[i2]->h_pT_nMSTracklets_sq->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*nInConeSLJ[7]*nInConeSLJ[7]);

        if(pt1) histEtaLJ[i1]->h_pT_nMuonRoIs_sq->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ)*commonVar->Jet_nMuonRoIs->at(indexLJ));
        if(pt1) histEta[i1]->h_pT_nMuonRoIs_sq->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ)*commonVar->Jet_nMuonRoIs->at(indexLJ));
        if(pt2) histEta[i2]->h_pT_nMuonRoIs_sq->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ)*commonVar->Jet_nMuonRoIs->at(indexSLJ));
        
        if(pt1) histEtaLJ[i1]->h_pT_nAssocMSeg->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ));
        if(pt1) histEta[i1]->h_pT_nAssocMSeg->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ));
        if(pt2) histEta[i2]->h_pT_nAssocMSeg->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ));

        if(pt1) histEtaLJ[i1]->h_pT_nMSTracklets->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*nInConeLJ[7]);
        if(pt1) histEta[i1]->h_pT_nMSTracklets->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*nInConeLJ[7]);
        if(pt2) histEta[i2]->h_pT_nMSTracklets->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*nInConeSLJ[7]);

        if(pt1) histEtaLJ[i1]->h_pT_nMuonRoIs->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ));
        if(pt1) histEta[i1]->h_pT_nMuonRoIs->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ));
        if(pt2) histEta[i2]->h_pT_nMuonRoIs->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ));


        histEta[i1]->h_nMSeg_denom->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
        histEtaLJ[i1]->h_nMSeg_denom->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
        histEta[i2]->h_nMSeg_denom->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);

        histEta[i1]->h_nMSeg_nMuonRoIs->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ));
        histEta[i2]->h_nMSeg_nMuonRoIs->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexSLJ));
        histEtaLJ[i1]->h_nMSeg_nMuonRoIs->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight*commonVar->Jet_nMuonRoIs->at(indexLJ));

        histEta[i1]->h_nMSeg_nMSTracklets->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight*nInConeLJ[7]);
        histEta[i2]->h_nMSeg_nMSTracklets->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight*nInConeSLJ[7]);
        histEtaLJ[i1]->h_nMSeg_nMSTracklets->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight*nInConeLJ[7]);

        if(nMSVx_LJ){
            histEta[i1]->h_nMSeg_pMSVx->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
            histEtaLJ[i1]->h_nMSeg_pMSVx->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
        }
        if(nMSVx_SLJ ){
            histEta[i2]->h_nMSeg_pMSVx->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);
        }
        if(nTrig_LJ){
            histEta[i1]->h_nMSeg_pTrig->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
            histEtaLJ[i1]->h_nMSeg_pTrig->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
        }
        if(nTrig_SLJ ){
            histEta[i2]->h_nMSeg_pTrig->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);
        } 

        // ** Fill variable for counting number of events with more than one vertex **
        //std::cout << "filled ms histograms 20" << std::endl;

        trigVar->clearAllVectors();
        variables->clearAllVectors();
        commonVar->clearAllVectors(false,true);
        if(doCRVariables) crVar->clearAllVectors();
    }


    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;

    cout << "" << endl;
    cout << " nEntries                 = " << chain->GetEntries() << endl;
    cout << "" << endl;

    histEtaFull->h_eta_nAssocMSeg->Divide(histEtaFull->h_eta);
    histEtaFullLJ->h_eta_nAssocMSeg->Divide(histEtaFullLJ->h_eta);
    histEtaFullSLJ->h_eta_nAssocMSeg->Divide(histEtaFullSLJ->h_eta);

    histEtaFull->h_eta_nMSTracklets->Divide(histEtaFull->h_eta);
    histEtaFullLJ->h_eta_nMSTracklets->Divide(histEtaFullLJ->h_eta);
    histEtaFullSLJ->h_eta_nMSTracklets->Divide(histEtaFullSLJ->h_eta);

    histEtaFull->h_eta_nMuonRoIs->Divide(histEtaFull->h_eta);
    histEtaFullLJ->h_eta_nMuonRoIs->Divide(histEtaFullLJ->h_eta);
    histEtaFullSLJ->h_eta_nMuonRoIs->Divide(histEtaFullSLJ->h_eta);

    histEtaFull->h_pT_nAssocMSeg->Divide(histEtaFull->h_pT_denom);
    histEtaFullLJ->h_pT_nAssocMSeg->Divide(histEtaFullLJ->h_pT_denom);
    histEtaFullSLJ->h_pT_nAssocMSeg->Divide(histEtaFullSLJ->h_pT_denom);

    histEtaFull->h_pT_nMSTracklets->Divide(histEtaFull->h_pT_denom);
    histEtaFullLJ->h_pT_nMSTracklets->Divide(histEtaFullLJ->h_pT_denom);
    histEtaFullSLJ->h_pT_nMSTracklets->Divide(histEtaFullSLJ->h_pT_denom);

    histEtaFull->h_pT_nMuonRoIs->Divide(histEtaFull->h_pT_denom);
    histEtaFullLJ->h_pT_nMuonRoIs->Divide(histEtaFullLJ->h_pT_denom);
    histEtaFullSLJ->h_pT_nMuonRoIs->Divide(histEtaFullSLJ->h_pT_denom);

    histEtaFull->h_cone_avgTracklets->Scale(1./histEtaFull->h_eta->Integral());
    histEtaFullLJ->h_cone_avgTracklets->Scale(1./histEtaFullLJ->h_eta->Integral());
    histEtaFullSLJ->h_cone_avgTracklets->Scale(1./histEtaFullSLJ->h_eta->Integral());

    histEtaFull->h_nMSeg_nMSTracklets->Divide(histEtaFull->h_nMSeg_denom);
    histEtaFull->h_nMSeg_nMuonRoIs->Divide(histEtaFull->h_nMSeg_denom);
    histEtaFull->h_nMSeg_pTrig->Divide(histEtaFull->h_nMSeg_denom);
    histEtaFull->h_nMSeg_pMSVx->Divide(histEtaFull->h_nMSeg_denom);
    histEtaFull->h_pT_pMSVx->Divide(histEtaFull->h_pT_denom);
    histEtaFull->h_pT_pTrig->Divide(histEtaFull->h_pT_denom);
    histEtaFull->h_eta_pMSVx->Divide(histEtaFull->h_eta_denom);
    histEtaFull->h_eta_pTrig->Divide(histEtaFull->h_eta_denom);
    histEtaFull->h_pT_eta_pMSVx->Divide(histEtaFull->h_pT_eta_denom);
    histEtaFull->h_pT_eta_pTrig->Divide(histEtaFull->h_pT_eta_denom);

    histEtaFullLJ->h_nMSeg_nMSTracklets->Divide(histEtaFullLJ->h_nMSeg_denom);
    histEtaFullLJ->h_nMSeg_nMuonRoIs->Divide(histEtaFullLJ->h_nMSeg_denom);
    histEtaFullLJ->h_nMSeg_pTrig->Divide(histEtaFullLJ->h_nMSeg_denom);
    histEtaFullLJ->h_nMSeg_pMSVx->Divide(histEtaFullLJ->h_nMSeg_denom);

    histEtaFullLJ->h_pT_pMSVx->Divide(histEtaFullLJ->h_pT_denom);
    histEtaFullLJ->h_pT_pTrig->Divide(histEtaFullLJ->h_pT_denom);
    histEtaFullLJ->h_eta_pMSVx->Divide(histEtaFullLJ->h_eta_denom);
    histEtaFullLJ->h_eta_pTrig->Divide(histEtaFullLJ->h_eta_denom);
    histEtaFullLJ->h_pT_eta_pMSVx->Divide(histEtaFullLJ->h_pT_eta_denom);
    histEtaFullLJ->h_pT_eta_pTrig->Divide(histEtaFullLJ->h_pT_eta_denom);

    histEtaFullSLJ->h_nMSeg_nMSTracklets->Divide(histEtaFullSLJ->h_nMSeg_denom);
    histEtaFullSLJ->h_nMSeg_nMuonRoIs->Divide(histEtaFullSLJ->h_nMSeg_denom);
    histEtaFullSLJ->h_nMSeg_pTrig->Divide(histEtaFullSLJ->h_nMSeg_denom);
    histEtaFullSLJ->h_nMSeg_pMSVx->Divide(histEtaFullSLJ->h_nMSeg_denom);

    histEtaFullSLJ->h_pT_pMSVx->Divide(histEtaFullSLJ->h_pT_denom);
    histEtaFullSLJ->h_pT_pTrig->Divide(histEtaFullSLJ->h_pT_denom);
    histEtaFullSLJ->h_eta_pMSVx->Divide(histEtaFullSLJ->h_eta_denom);
    histEtaFullSLJ->h_eta_pTrig->Divide(histEtaFullSLJ->h_eta_denom);
    histEtaFullSLJ->h_pT_eta_pMSVx->Divide(histEtaFullSLJ->h_pT_eta_denom);
    histEtaFullSLJ->h_pT_eta_pTrig->Divide(histEtaFullSLJ->h_pT_eta_denom);

    if(doCRVariables){
    histEtaFull->h_CR_logR_BDT->Divide(histEtaFull->h_CR_logR);
    histEtaFull->h_CR_logR_nTrk->Divide(histEtaFull->h_CR_logR);
    histEtaFull->h_CR_logR_sumTrkpT->Divide(histEtaFull->h_CR_logR);
    histEtaFull->h_CR_logR_maxTrkpT->Divide(histEtaFull->h_CR_logR);
    histEtaFull->h_CR_logR_width->Divide(histEtaFull->h_CR_logR);
    histEtaFull->h_CR_logR_jetDRTo2GeVTrack->Divide(histEtaFull->h_CR_logR);
    histEtaFull->h_CR_logR_l1frac->Divide(histEtaFull->h_CR_logR);
    histEtaFull->h_CR_logR_c_firenden->Divide(histEtaFull->h_CR_logR);
    histEtaFull->h_CR_logR_c_lat->Divide(histEtaFull->h_CR_logR);
    histEtaFull->h_CR_logR_c_long->Divide(histEtaFull->h_CR_logR);
    histEtaFull->h_CR_logR_c_r->Divide(histEtaFull->h_CR_logR);
    histEtaFull->h_CR_logR_c_lambda->Divide(histEtaFull->h_CR_logR);
    //std::cout << "filled ms histograms 10" << std::endl;

    histEtaFullLJ->h_CR_logR_BDT->Divide(histEtaFullLJ->h_CR_logR);
    histEtaFullLJ->h_CR_logR_nTrk->Divide(histEtaFullLJ->h_CR_logR);
    histEtaFullLJ->h_CR_logR_sumTrkpT->Divide(histEtaFullLJ->h_CR_logR);
    histEtaFullLJ->h_CR_logR_maxTrkpT->Divide(histEtaFullLJ->h_CR_logR);
    histEtaFullLJ->h_CR_logR_width->Divide(histEtaFullLJ->h_CR_logR);
    histEtaFullLJ->h_CR_logR_jetDRTo2GeVTrack->Divide(histEtaFullLJ->h_CR_logR);
    histEtaFullLJ->h_CR_logR_l1frac->Divide(histEtaFullLJ->h_CR_logR);
    histEtaFullLJ->h_CR_logR_c_firenden->Divide(histEtaFullLJ->h_CR_logR);
    histEtaFullLJ->h_CR_logR_c_lat->Divide(histEtaFullLJ->h_CR_logR);
    histEtaFullLJ->h_CR_logR_c_long->Divide(histEtaFullLJ->h_CR_logR);
    histEtaFullLJ->h_CR_logR_c_r->Divide(histEtaFullLJ->h_CR_logR);
    histEtaFullLJ->h_CR_logR_c_lambda->Divide(histEtaFullLJ->h_CR_logR);
    //std::cout << "filled ms histograms 11" << std::endl;

    histEtaFullSLJ->h_CR_logR_BDT->Divide(histEtaFullSLJ->h_CR_logR);
    histEtaFullSLJ->h_CR_logR_nTrk->Divide(histEtaFullSLJ->h_CR_logR);
    histEtaFullSLJ->h_CR_logR_sumTrkpT->Divide(histEtaFullSLJ->h_CR_logR);
    histEtaFullSLJ->h_CR_logR_maxTrkpT->Divide(histEtaFullSLJ->h_CR_logR);
    histEtaFullSLJ->h_CR_logR_width->Divide(histEtaFullSLJ->h_CR_logR);
    histEtaFullSLJ->h_CR_logR_jetDRTo2GeVTrack->Divide(histEtaFullSLJ->h_CR_logR);
    histEtaFullSLJ->h_CR_logR_l1frac->Divide(histEtaFullSLJ->h_CR_logR);
    histEtaFullSLJ->h_CR_logR_c_firenden->Divide(histEtaFullSLJ->h_CR_logR);
    histEtaFullSLJ->h_CR_logR_c_lat->Divide(histEtaFullSLJ->h_CR_logR);
    histEtaFullSLJ->h_CR_logR_c_long->Divide(histEtaFullSLJ->h_CR_logR);
    histEtaFullSLJ->h_CR_logR_c_r->Divide(histEtaFullSLJ->h_CR_logR);
    histEtaFullSLJ->h_CR_logR_c_lambda->Divide(histEtaFullSLJ->h_CR_logR);
    }
    for(int i=0;i<5;i++){
        histEta[i]->h_nMSeg_nMSTracklets->Divide(histEta[i]->h_nMSeg_denom);
        histEta[i]->h_nMSeg_nMuonRoIs->Divide(histEta[i]->h_nMSeg_denom);
        histEta[i]->h_nMSeg_pTrig->Divide(histEta[i]->h_nMSeg_denom);
        histEta[i]->h_nMSeg_pMSVx->Divide(histEta[i]->h_nMSeg_denom);
        histEta[i]->h_pT_pMSVx->Divide(histEta[i]->h_pT_denom);
        histEta[i]->h_pT_pTrig->Divide(histEta[i]->h_pT_denom);
        histEta[i]->h_eta_pMSVx->Divide(histEta[i]->h_eta_denom);
        histEta[i]->h_eta_pTrig->Divide(histEta[i]->h_eta_denom);
        histEta[i]->h_pT_eta_pMSVx->Divide(histEta[i]->h_pT_eta_denom);
        histEta[i]->h_pT_eta_pTrig->Divide(histEta[i]->h_pT_eta_denom);

        histEtaLJ[i]->h_nMSeg_nMSTracklets->Divide(histEtaLJ[i]->h_nMSeg_denom);
        histEtaLJ[i]->h_nMSeg_nMuonRoIs->Divide(histEtaLJ[i]->h_nMSeg_denom);
        histEtaLJ[i]->h_nMSeg_pTrig->Divide(histEtaLJ[i]->h_nMSeg_denom);
        histEtaLJ[i]->h_nMSeg_pMSVx->Divide(histEtaLJ[i]->h_nMSeg_denom);

        histEtaLJ[i]->h_pT_pMSVx->Divide(histEtaLJ[i]->h_pT_denom);
        histEtaLJ[i]->h_pT_pTrig->Divide(histEtaLJ[i]->h_pT_denom);
        histEtaLJ[i]->h_eta_pMSVx->Divide(histEtaLJ[i]->h_eta_denom);
        histEtaLJ[i]->h_eta_pTrig->Divide(histEtaLJ[i]->h_eta_denom);
        histEtaLJ[i]->h_pT_eta_pMSVx->Divide(histEtaLJ[i]->h_pT_eta_denom);
        histEtaLJ[i]->h_pT_eta_pTrig->Divide(histEtaLJ[i]->h_pT_eta_denom);

        histEta[i]->h_eta_nAssocMSeg->Divide(histEta[i]->h_eta);
        histEta[i]->h_eta_nMSTracklets->Divide(histEta[i]->h_eta);
        histEta[i]->h_eta_nMuonRoIs->Divide(histEta[i]->h_eta);

        histEta[i]->h_pT_nAssocMSeg->Divide(histEta[i]->h_pT_denom);
        histEta[i]->h_pT_nMSTracklets->Divide(histEta[i]->h_pT_denom);
        histEta[i]->h_pT_nMuonRoIs->Divide(histEta[i]->h_pT_denom);

        histEta[i]->h_pT_pMSVx->Divide(histEta[i]->h_pT_denom);
        histEta[i]->h_pT_pTrig->Divide(histEta[i]->h_pT_denom);
        histEta[i]->h_eta_pMSVx->Divide(histEta[i]->h_eta_denom);
        histEta[i]->h_eta_pTrig->Divide(histEta[i]->h_eta_denom);
        histEta[i]->h_pT_eta_pMSVx->Divide(histEta[i]->h_pT_eta_denom);
        histEta[i]->h_pT_eta_pTrig->Divide(histEta[i]->h_pT_eta_denom);

        histEta[i]->h_cone_avgTracklets->Scale(1./histEta[i]->h_eta->Integral());

        histEtaLJ[i]->h_eta_nAssocMSeg->Divide(histEtaLJ[i]->h_eta);
        histEtaLJ[i]->h_eta_nMSTracklets->Divide(histEtaLJ[i]->h_eta);
        histEtaLJ[i]->h_eta_nMuonRoIs->Divide(histEtaLJ[i]->h_eta);

        histEtaLJ[i]->h_pT_nAssocMSeg->Divide(histEtaLJ[i]->h_pT_denom);
        histEtaLJ[i]->h_pT_nMSTracklets->Divide(histEtaLJ[i]->h_pT_denom);
        histEtaLJ[i]->h_pT_nMuonRoIs->Divide(histEtaLJ[i]->h_pT_denom);

        histEtaLJ[i]->h_pT_pMSVx->Divide(histEtaLJ[i]->h_pT_denom);
        histEtaLJ[i]->h_pT_pTrig->Divide(histEtaLJ[i]->h_pT_denom);
        histEtaLJ[i]->h_eta_pMSVx->Divide(histEtaLJ[i]->h_eta_denom);
        histEtaLJ[i]->h_eta_pTrig->Divide(histEtaLJ[i]->h_eta_denom);
        histEtaLJ[i]->h_pT_eta_pMSVx->Divide(histEtaLJ[i]->h_pT_eta_denom);
        histEtaLJ[i]->h_pT_eta_pTrig->Divide(histEtaLJ[i]->h_pT_eta_denom);

        histEtaLJ[i]->h_cone_avgTracklets->Scale(1./histEtaLJ[i]->h_eta->Integral());
    }


    // ** Make plot **
    //

    outputFile->Write();
}
