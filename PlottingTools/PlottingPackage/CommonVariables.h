//
//  CommonVariables.h
//  plotVertexParameters
//
//  Created by Heather Russell on 12/10/15.
//
//

#ifndef __plotVertexParameters__CommonVariables__
#define __plotVertexParameters__CommonVariables__

#include <vector>
#include "TChain.h"
#include "TMath.h"
struct CommonVariables {
    
public:
    bool    passMuvtx;
    bool    passCalRatio;
    bool    passMuvtx_noiso;
    double  eventWeight;
    double  pileupEventWeight;
    double prw_1up; double prw_1down;
    std::vector<double> *pdfWeights;
    double  eventHT;
    int     eventNJets;
    int     eventNJets50;
    double  eventHTMiss;
    double  eventMHT;
    double  eventMHT_phi;
    double  eventMET;
    double  eventMET_phi;
    double  eventMeff;
    
    ULong64_t eventNumber;
    uint32_t runNumber;
    uint32_t lumiBlock;
    
    int hasGoodPV;
    int isGoodLAr;
    int isGoodTile;
    int isGoodSCT;
    int isCompleteEvent;
    
    int isBadEventJetLow;
    int isBadEventJetHigh;
    
    double vertexJetPt;
    double vertexJetdR;
    double vertexTrackdR;
    double vertexJetdR_pT;
    double LJetPt;
    double SLJetPt;
    double SSLJetPt;
    int nJetsOutsideVx20i;
    int nJetsOutsideVx20;
    int nJetsOutsideVx50i;
    int nJetsOutsideVx50;
    int nJetsOutsideVx75i;
    int nJetsOutsideVx75;
    int nJetsOutsideVx100i;
    int nJetsOutsideVx100;
    bool passJ400;
    int CR_1j30;
    int CR_1j50;
    int CR_1j50_1j20;
    int CR_2j20;
    int CR_2j50;
    int CR_1j50_1j150;
    int CR_2j150;
    int CR_1j100_1j200;
    int CR_2j200;
    int CR_1j100_1j250;
    int CR_2j250;
    
    std::vector<int> *Jet_passJVT;
    std::vector<double> *Jet_width;
    std::vector<double> *Jet_jvt;
    std::vector<double> *Jet_EMF;
    std::vector<double> *Jet_pT;
    std::vector<double> *Jet_ET;
    std::vector<double> *Jet_E;
    std::vector<double> *Jet_t;
    std::vector<double> *Jet_eta;
    std::vector<double> *Jet_phi;
    std::vector<double> *Jet_logRatio;
    std::vector<int>    *Jet_indexLLP;
    std::vector<bool>    *Jet_isGoodLLP;
    std::vector<bool>    *Jet_isGoodStand;
    std::vector<int> *Jet_nAssocMSeg;
    std::vector<int> *Jet_nMSegCone;
    std::vector<int> *Jet_nMuonRoIs;
    std::vector<int> *Jet_nMSTracklets;
    
    std::vector<double> *Track_pT;
    std::vector<double> *Track_eta;
    std::vector<double> *Track_phi;

    double LogRClosestJet(double dR2Min);
    std::pair<double, double> ClosestJetVars();

    double wPhi(double phi);
    double DelR(double phi1, double phi2, double eta1, double eta2);
    double DelR2(double phi1, double phi2, double eta1, double eta2);
    void setZeroVectors();
    void setZeroJESVectors();   
    void setJESBranchAddresses(TChain *chain);
    void setBranchAdresses(TChain *chain,bool useJets, bool useTruthJets, bool isData, bool useTracks){
        setBranchAdresses(chain, useJets, useTruthJets, isData, useTracks, false);
    }
    void setBranchAdresses(TChain *chain,bool useJets, bool useTruthJets, bool isData, bool useTracks, bool doPDFSystematics);
    void clearAllVectors(bool useTracks, bool useJets){
        clearAllVectors(useTracks, useJets, false);
    }
    void fixJetET();
    void clearJESVectors();
    void clearAllVectors(bool useTracks, bool useJets, bool doPDFSystematics);
    void setGoodJetEventFlags();
    bool isGoodDijetEvent();
    bool isQualityEvent(bool isData);
    void getVertexTrackdR(double eta, double phi);
    bool eventIs2j150();
    bool eventIs2j150(double eta, double phi);
    bool eventIs2j250();
    bool eventIs2j250(double eta, double phi);

    void set2DPlotVars(double eta, double phi);
    void recalculateEvtProperties();
    void calcMHT();
    void recalculateEvtProperties(double eta, double phi);
};



#endif /* defined(__plotVertexParameters__CommonVariables__) */
