//
//  EventProperties.h
//  plotVertexParameters
//
//  Created by Heather Russell on 12/10/15.
//
//

#ifndef __plotVertexParameters__EventProperties__
#define __plotVertexParameters__EventProperties__

#include <vector>
#include "TChain.h"
#include "TMath.h"
struct EventProperties {
    
public:

    enum Cutflow { PassTrigger, HasGoodPV, HasVertex, TrigMatchesVx, PassVxHits, PassVxIso, HasVertex2, PassVx2Hits, PassVx2Iso, PassdR2Cut };
    
    double eventsPassCuts[10];
    
    double nTrigBarrel_1Trig;
    double nTrigEndcap_1Trig;

    double nTrig_BB;
    double nTrig_BE;
    double nTrig_EE;
    
    double n2TrigEvents;
    double n1TrigEvents;
    double nMoreThan2TrigEvents;
    
    double nVxMatchBarrel_BB;
    double nVxMatchBarrel_BE;
    double nVxMatchEndcap_BE;
    double nVxMatchEndcap_EE;
    
    double nVxMatchBarrel_1Trig;
    double nVxMatchEndcap_1Trig;
    
    double nVxMatch_1Trig;
    double n1VxMatch_2Trig;
    
    double nVxUnMatch_1Trig;
    double nVxUnMatch_2Trig;
    
    //all the possible 2 vertex events!
    double nVxMatchBarrel_1Trig_BVx;
    double nVxMatchEndcap_1Trig_BVx;
    double nVxMatchBarrel_1Trig_EVx;
    double nVxMatchEndcap_1Trig_EVx;
    
    double n2VxMatch_BB;
    double n2VxMatch_BE;
    double n2VxMatch_EE;
    
    double n2VxMatch_1Trig;
    double n2VxMatch_2Trig;
    
    //failed 2vx events b/c of matching.
    
    double n2VxUnMatch_1Trig;
    //at least one doesn't match
    double n2VxUnMatch_2Trig;
    
    
    void printResults(bool isBlind);
    void setZeroVectors();
    void add1TrigEvent(int trigRegion, int vx, int vxMatches, double weight); //barrel = 1, endcap = 2
    void add2Vertex1TrigEvent(int trigRegion, int vx1region, int vxMatches1, int vx2region, int vxMatches2, double weight);
    void add2TrigEvent(int trigRegion, int vxRegion, int vxMatches, double weight); //regions: bb be ee 1 2 3, vxRegion 0 = no vertex
    void add2Vertex2TrigEvent(int trigRegion, int verticesMatch, double weight);

    void addStrangeEvent(int nTrigs, double weight);
    void addEventToCutflow(enum EventProperties::Cutflow cf, double weight);
};



#endif /* defined(__plotVertexParameters__EventProperties__) */
