//
//  VertexHistograms.h
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#ifndef __plotVertexParameters__VertexHistograms__
#define __plotVertexParameters__VertexHistograms__

#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
#include "TChain.h"

struct VertexHistograms {
public:
    
    TString object;
    TString locn;

    TH1D *h_eta;
    TH1D *h_eta_ABS;
    TH1D *h_eta_2MATCH;
    TH1D *h_phi;
    TH1D *h_R;
    TH1D *h_z;
    
    TH1D *h_nTrks;
    TH1D *h_nMDT;
    TH1D *h_nRPC;
    TH1D *h_nTGC;
    
    //
    TH2D *h_phieta;

    TH2D *h_MDTvsTrig;
    
    TH2D *h_zR;

    TH2D *h_JetdR_vs_Et;
    TH2D *h_TrackdR_vs_Pt;
    
    
    void initializeHistograms(TString object, TString str);
    void fillFromTree(int isJZMC, TChain* &tree, const std::string &cuts);
};



#endif /* defined(__plotVertexParameters__VertexHistograms__) */
