
void asymmetry_from_systFiles(){
    //0.7 < |η| < 1.0 or 1.5 < |η| < 1.7

    TCanvas* c1 = new TCanvas("c1","c1",800,600);
    SetAtlasStyle();

    //c1->SetRightMargin(0.05);
    //c1->SetTopMargin(0.07);
    //c1->SetLeftMargin(0.15);
    //c1->SetBottomMargin(0.15);
    TPad *pad1 = new TPad("pad1","pad1",0,0.32,1,1);
    pad1->SetTopMargin(0.075);
    pad1->SetBottomMargin(0.04);
    pad1->SetLeftMargin(0.14);
    pad1->SetLogy();
    pad1->Draw();

    TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.32);
    pad2->SetTopMargin(0.05);
    pad2->SetBottomMargin(0.40);
    pad2->SetLeftMargin(0.14);
    pad2->Draw();



    TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/dijetData/outputMSSystematics_NEW.root");

    TFile *_file1 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/bkgdJZ/JZAll/outputMSSystematics_NEW.root");


    TString plotDir = "asymmetryPlots/";

    for (TObject* keyAsObj : *(_file1->GetListOfKeys())){
        auto key = dynamic_cast<TKey*>(keyAsObj);

        if((TString)key->GetClassName() == "TH2D") continue;
        pad1->cd(); 
        gStyle->SetPadTickX(2);
        gStyle->SetPadTickY(2);
        TH1D *h_MC = (TH1D*) key->ReadObj();
        TH1D *h_nMC = (TH1D*) h_MC->Clone();
        
        TString histName = TString(h_MC->GetName());
        //if(!histName.Contains("full")) continue;
        TH1D *h_Data = (TH1D*)_file0->Get(histName);
        TH1D *h_nData = (TH1D*) h_Data->Clone();
        //std::cout << "Key name: " << key->GetName() << " Type: " << key->GetClassName() << ", entries data/mc: " << h_Data->GetEntries()  <<"/" <<h_MC->GetEntries() <<  std::endl;
        //if(!histName.Contains("pMSVx") || !histName.Contains("pTrig")) continue;

        if(h_MC->GetEntries() == 0 || h_Data->GetEntries() == 0) continue;
        if(!histName.Contains("full_eta_n") ) continue; //this one is messed up right now
        std::cout << "hist: " << histName << std::endl;

        pad1->SetLogy(0); h_Data->SetMinimum(0);
        h_Data->SetLineColor(kBlack);h_Data->SetMarkerColor(kBlack);h_Data->SetMarkerSize(1.2);h_Data->SetLineWidth(2);h_Data->SetMarkerStyle(20);
        h_MC->SetLineColor(kBlue+2);h_MC->SetMarkerColor(kBlue+2);h_MC->SetMarkerSize(1.2);h_MC->SetLineWidth(2);h_MC->SetMarkerStyle(4);	
        
        if(histName.Contains("eta_nMSTracklets")){h_Data->GetXaxis()->SetTitle("jet #eta"); h_Data->GetYaxis()->SetTitle("<nTracklets in #DeltaR=0.4>");}
        else if(histName.Contains("eta_nAssoc")){h_Data->GetXaxis()->SetTitle("jet #eta"); h_Data->GetYaxis()->SetTitle("< nAssoc MSegs>");}
        else if(histName.Contains("eta_")) h_Data->GetXaxis()->SetTitle("jet #eta");
        
        double maxVal = h_MC->GetMaximum(); if(h_Data->GetMaximum() > maxVal) maxVal = h_Data->GetMaximum();
        h_Data->SetMaximum(1.3*maxVal);
        h_Data->SetTitle("");

        TString append = "";

        if(histName.Contains("SLJ")) append = ", sub-leading jets"; 
        else if(histName.Contains("LJ")) append = ", leading jets"; 
        else if(histName.Contains("AllJets")) append = ", leading and sub-leading jets"; 
        TString title = "";
        if(histName.Contains("full")) title = TString("|#eta| < 2.7") + append;
        
        h_Data->GetYaxis()->SetLabelSize(0.070);
        h_Data->GetYaxis()->SetTitleSize(0.065);
        h_Data->GetYaxis()->SetTitleOffset(0.18);
        h_Data->GetXaxis()->SetTitleOffset(2.7);
        h_Data->GetXaxis()->SetLabelOffset(0.5);
        h_Data->GetYaxis()->SetTitleOffset(0.95);

        int nBinsX = h_Data->GetNbinsX();
        for(unsigned int ibin = 1; ibin < nBinsX+1; ibin++){
            double binVal = h_Data->GetBinContent(ibin);
            double binErr = h_Data->GetBinError(ibin);
            h_nData->SetBinContent(nBinsX + 1 - ibin, binVal); h_nData->SetBinError(nBinsX + 1 - ibin, binErr);
            binVal = h_MC->GetBinContent(ibin);
            binErr = h_MC->GetBinError(ibin);
            h_nMC->SetBinContent(nBinsX + 1 - ibin, binVal); h_nMC->SetBinError(nBinsX + 1 - ibin, binErr);

        }
        
        h_Data->GetXaxis()->SetRangeUser(0,3);
        h_Data->Draw();
        h_MC->Draw("SAME");
        h_nData->SetLineColor(kRed+2);        h_nData->SetMarkerColor(kRed+2);h_nMC->SetMarkerStyle(20);
        h_nMC->SetLineColor(kGreen+2); h_nMC->SetMarkerColor(kGreen+2);h_nMC->SetMarkerStyle(4);
        h_nData->Draw("SAME"); h_nMC->Draw("SAME");
        
        TLegend *leg = new TLegend(0.25,0.75,0.55,0.9);
        leg->SetFillColor(0);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.05);
        leg->AddEntry(h_MC,"#eta^{+} Pythia JZXW MC","lp");
        leg->AddEntry(h_Data,"#eta^{+} data","lp");
        leg->AddEntry(h_nMC,"#eta^{-} Pythia JZXW MC","lp");
        leg->AddEntry(h_nData,"#eta^{-} data","lp");
        leg->Draw();

        TLatex latex2;
        latex2.SetNDC();
        latex2.SetTextColor(kBlack);
        latex2.SetTextSize(0.06);
        latex2.SetTextAlign(31);  //align at bottom
        latex2.DrawLatex(0.95,.94,title);

        //c1->Print(plotDir+histName+".pdf");
        TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
        l.SetNDC();
        l.SetTextFont(72);
        l.SetTextSize(0.06);
        l.SetTextColor(kBlack);
        l.DrawLatex(.72,.84,"ATLAS");
        TLatex p; 
        p.SetNDC();
        p.SetTextSize(0.06);
        p.SetTextFont(42);
        p.SetTextColor(kBlack);
        p.DrawLatex(0.72+0.105,0.84,"Internal");

        c1->cd();

        pad2->cd();
        gStyle->SetPadTickX(1);
        gStyle->SetPadTickY(1);

        //TH1D *p1_new = (TH1D*)h_Data->Clone("data_clone");
        //TH1D *p2_new = (TH1D*)h_MC->Clone("ndata_clone");

        TH1 *p1_new = h_Data->GetAsymmetry(h_nData);
        TH1 *p2_new = h_MC->GetAsymmetry(h_nMC);

        p2_new->SetMarkerStyle(4);
        p2_new->SetMarkerSize(0.8);
        p2_new->SetMarkerColor(kBlue+2);
        p2_new->SetLineColor(kBlue+2);
        p2_new->SetMinimum(0.0);
        p2_new->SetMaximum(2.0);
        
        p1_new->SetMarkerStyle(20);
        p1_new->SetMarkerSize(0.8);
        p1_new->SetMarkerColor(kBlack);
        p1_new->SetMinimum(-0.1);
        p1_new->SetMaximum(0.1);
        //p1_new->GetXaxis()->SetTitle("to do");
        p1_new->GetXaxis()->SetLabelFont(42);
        p1_new->GetXaxis()->SetLabelSize(0.16);
        p1_new->GetXaxis()->SetLabelOffset(0.05);
        p1_new->GetXaxis()->SetTitleFont(42);
        p1_new->GetXaxis()->SetTitleSize(0.14);
        p1_new->GetXaxis()->SetTitleOffset(1.3);
        p1_new->GetYaxis()->SetNdivisions(505);
        p1_new->GetYaxis()->SetTitle("#font[42]{Asymmetry}");
        p1_new->GetYaxis()->SetLabelFont(42);
        p1_new->GetYaxis()->SetLabelSize(0.15);
        p1_new->GetYaxis()->SetTitleFont(42);
        p1_new->GetYaxis()->SetTitleSize(0.13);
        p1_new->GetYaxis()->SetTitleOffset(0.44); 
        p1_new->DrawCopy("eP");
        p2_new->DrawCopy("eP SAME");

        TLine* line = new TLine();
        line->DrawLine(0,0,p1_new->GetXaxis()->GetXmax(),0);

        pad2->SetLogy(0);

        //latex2.DrawLatex(.15,.98,title + ", Data/MC");
        c1->Print(plotDir+"ratio_"+histName+".pdf");
        c1->Print(plotDir+"ratio_"+histName+".png");
    }


}
