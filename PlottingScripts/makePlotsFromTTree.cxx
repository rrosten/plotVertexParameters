//
//  makeCombinedPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include <utility>
#include "TFile.h"
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"

using namespace std;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;



int makePlotsFromTTree(){
    
	
gROOT->LoadMacro("AtlasUtils.C");
gROOT->LoadMacro("AtlasLabels.C");
gROOT->LoadMacro("AtlasStyle.C");

 TFile *_sig[4]; int nSIG=4;
 _sig[0] = new TFile("signalMC/mH125mS8lt5/output.root");
 _sig[1] = new TFile("signalMC/mH200mS25lt5/output.root");
 _sig[2] = new TFile("signalMC/mH600mS150lt9/output.root");
 _sig[3] = new TFile("signalMC/mg250/output.root");

    //labels for each sample, for the legend
 TString sigNames[4]; sigNames[0] = "m_{H},m_{S}=[125,8] GeV"; sigNames[1] = "m_{H},m_{S}=[200,25] GeV";sigNames[2] = "m_{H},m_{S}=[600,150] GeV";sigNames[3] = "m_{#tilde{g}}= 250 GeV";
   
 Int_t colorSig[4] = {kBlue-3, kAzure+5, kTeal-6,kTeal+2};
 Int_t markerSig[4] = {24,25,26,27};
  
 TString names[10]; //names of your histograms in the file you've loaded
 names[0] = "hist1";
 names[1] = "hist2";
 names[2] = "hist3";
 names[3] = "hist4";
 names[4] = "hist5";
 names[5] = "hist6";
 names[6] = "hist7";
 names[7] = "hist8";
 names[8] = "hist9";
 names[9] = "hist10";
    

    for(int i=0; i< 10; i++){
      hists[i] = new TH1D(names[i],names[i],100,0,1000);//this creates them all with the same limits, can do it by hand too.
        hists[i]->Sumw2(); //makes error bars
    }
 
    TTree *tree[4];
    for(int i=0; i<nSIG); i++){
        tree[i] = (TTree*)_sig[i]->Get("recoTree");
    }
    
   tree[i].Draw("TMath::Abs(MSVertex_eta) >>"+TString(hist1->GetName()),cuts,"");

    
    //x0-axis titles
 TString titles[10];
 titles[0] = "max jet E_{T}";
 titles[1] = "min jet #Delta R";
 titles[2] = "max track p_{T}";
 titles[3] = "min track #Delta R";
 titles[4] = "max #Sigma track p_{T} in #Delta R = 0.2";
 titles[5] = "max jet E_{T}";
 titles[6] = "min jet #Delta R";
 titles[7] = "max track p_{T}";
 titles[8] = "min track #Delta R";
 titles[9] = "max #Sigma track p_{T} in #Delta R = 0.2";

 SetAtlasStyle();
 gStyle->SetPalette(1);
 
 TCanvas* c = new TCanvas("c_1","c_1",800,600);
 
 //c->SetLogy();
    
 //loop through the ten histograms i have titles of above
 for(unsigned int i=0;i<10; i++){

     TH1D *h_sig[4];
	 for(int j=0; j<nSIG; j++){
		 h_sig[j] = (TH1D*) _sig[j]->Get(names[i]);

		 h_sig[j]->SetMarkerColor(colorSig[j]);
         h_sig[j]->SetLineColor(colorSig[j]);
         h_sig[j]->SetMarkerStyle(markerSig[j]);

         //set the x and y axis ranges depending on the histogram
         if(i==1 || i == 3 || i == 6 || i == 8) {
             h_sig[j]->GetXaxis()->SetRangeUser(0,1);
         } else if(i==0 || i == 5){
             h_sig[j]->GetXaxis()->SetRangeUser(20,200);
         } else if(i==2 || i == 7){
		 		 if(i==2)h_sig[j]->GetYaxis()->SetRangeUser(0,4000);
		 		 h_sig[j]->GetXaxis()->SetRangeUser(2,20);
         } else if(i==4 || i == 9) {
             h_sig[j]->GetXaxis()->SetRangeUser(2,30);
         }
        
          if(j==0) 	 h_sig[j]->Draw();
          else h_sig[j]->Draw("SAME");
     }
    
	 TLatex latex;
	 latex.SetNDC();
	 latex.SetTextColor(kBlack);
	 latex.SetTextSize(0.04);
	 latex.SetTextAlign(13);  //align at top
	 latex.DrawLatex(.18,.91,"#font[72]{ATLAS}  Internal");
	 //latex.DrawLatex(.43,.86,"#sqrt{s} = 13 TeV, 78 pb^{-1}"); //use if you have data
	 

	 TLegend *legS = new TLegend(0.38,0.8,0.566,0.935);
	 legS->SetFillColor(0);
	 legS->SetBorderSize(0);
	 legS->SetTextSize(0.03);
	 for(int j=0;j<nSIG;j++){
		     legS->AddEntry(h_sig[j],sigNames[j],"lp");
	 }
	 legS->Draw();

	 gPad->RedrawAxis();
   
     
	 c->Print("combinedPlots/"+names[i]+".pdf"); //change to a directory that exists

 }


 return 314;
 
}