void extractEvents_ctau(double ctau, double scaling, TString file){

	TFile *f = TFile::Open("../OutputPlots/signalMC/extrapolation/outputExtrapolation_"+file+".root");
	std::vector<TString> hists = {"Expected_BBMSVx","Expected_BEMSVx","Expected_EEMSVx"};
	TH1D *h[3];
	std::cout << "at ctau = " << ctau << ", estimates in full sample are: " << std::endl;
	for(int i=0;i<3;i++){
		h[i] = (TH1D*)f->Get(hists.at(i));
		h[i]->Scale(1./scaling);
		std::cout << hists.at(i) << ": ";
		std::cout << h[i]->GetBinContent(h[i]->FindBin(ctau)) << std::endl;
	}
}
