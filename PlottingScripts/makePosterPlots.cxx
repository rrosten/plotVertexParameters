void makePosterPlots(){
	SetAtlasStyle();
        TCanvas* c1 = new TCanvas("c_1","c_1",800,600);
	c1->SetLogy(1);
	c1->SetRightMargin(0.08);
  TGaxis::SetMaxDigits(2);  
	TFile* f[4];
	f[0] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/bkgdJZ/JZAll/outputDataSearch_noTrigIso_unscaled_smallestdR.root");
	f[1] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/signalMC/mg250/outputDataSearch_noTrigIso_unscaled_smallestdR.root");
	f[2] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/signalMC/mg800/outputDataSearch_noTrigIso_unscaled_smallestdR.root");
	f[3] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/signalMC/mg1500/outputDataSearch_noTrigIso_unscaled_smallestdR.root");

 	TH1D* h[2][4];
    
    Int_t colorSig[4] = {kGray, kBlue-3, kViolet-6, kTeal-6};
	TLegend *leg = new TLegend(0.6,0.73,.92,0.91);
	leg->SetFillStyle(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.03);
TString label[4] = {"","m_{#tilde{g}} = 250 GeV","m_{#tilde{g}} = 800 GeV","m_{#tilde{g}} = 1500 GeV"};

	for(int i=0;i<4;i++){
		h[0][i] = (TH1D*)f[i]->Get("e1_MSVxIso_2j150_b");
		h[1][i] = (TH1D*)f[i]->Get("e1_MSVxHits_2j150_b");
		h[1][i]->Rebin(4);
		h[0][i]->Scale(1./h[0][i]->Integral());
		h[1][i]->Scale(1./h[1][i]->Integral());
		h[0][i]->SetLineColor(colorSig[i]);
		h[0][i]->SetLineStyle(i+1);
		h[1][i]->SetLineColor(colorSig[i]);
		h[1][i]->SetLineStyle(i+1);
		if(i==0){ 
			h[0][i]->SetFillColor(kGray);
			h[0][i]->GetXaxis()->SetTitle("min[#DeltaR(jet,vx), #DeltaR(track, vx)]");
			h[0][i]->GetYaxis()->SetTitle("Fraction of vertices");
			h[0][i]->GetXaxis()->SetRangeUser(0,3);
			h[0][i]->GetYaxis()->SetRangeUser(0.0001,1);
			h[1][i]->SetFillColor(kGray);
			h[1][i]->GetYaxis()->SetTitle("Fraction of vertices");
        		leg->AddEntry(h[0][i],"QCD Multi-jets","f");
			h[0][i]->Draw("hist");
		}

		else{
		 h[0][i]->Draw("HIST SAME");
        	leg->AddEntry(h[0][i],label[i],"l");}
	}
	leg->Draw();
	gPad->RedrawAxis();
	ATLASLabel(0.19,0.88,"Work in progress", kBlack);
    
    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    //latex.SetTextSize(0.04);
    latex.SetTextAlign(13);  //align at top
    latex.DrawLatex(.19,.87,"Simulation");
    
	c1->Print("e1_MSVxIso_2j150_b.pdf");

	h[1][0]->GetXaxis()->SetTitle("Number of associated RPC + MDT hits");
	h[1][0]->GetXaxis()->SetRangeUser(0,10000);
	h[1][0]->GetYaxis()->SetRangeUser(0.0001,1);
	h[1][0]->Draw("hist");
	for(int i=1;i<4;i++) h[1][i]->Draw("hist same");
	leg->Draw();
	gPad->RedrawAxis();
	ATLASLabel(0.19,0.88,"Work in progress", kBlack);
    latex.DrawLatex(.19,.87,"Simulation");

	c1->Print("e1_MSVxHits_2j150_b.pdf");
}
