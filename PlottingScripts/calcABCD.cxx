void calcABCD(TString histName = "", bool blind = true){

 TH2D *h = (TH2D*)_file0->Get(histName);
 h->SetDirectory(0);
 TString reg = "_b"; TString exp = "e1";
 if(histName.Contains("_ec")) reg = "_ec";
 if(histName.Contains("e1t") ) exp = "e1t";
 
 TH2D *h2 = nullptr;
 if(histName.Contains("nTrks")) h2 = (TH2D*)_file0->Get(exp+"_ClosestdR_vs_nTrks_2j150"+reg);
 else h2 = (TH2D*)_file0->Get(exp+"_ClosestdR_vs_nHits_2j150"+reg);
 if(!blind) h->Add(h2,-1.);
 h2->SetDirectory(0);
 std::cout << "correlation: " << h->GetCorrelationFactor() << std::endl; 

 double errorA = 0;
 double errorB = 0;
 double errorC = 0;
 double errorD = 0;
 double errorBCD = 0;
 if(histName.Contains("nTrks")){

 double valA = h->IntegralAndError(13,401,7,51,errorA);
 double valB = h->IntegralAndError(13,401,5,6,errorB);
 double valC = h->IntegralAndError(1,12,7,51,errorC);
 double valD = h->IntegralAndError(1,12,5,6,errorD);

 double BCD = valB*valC/valD;
 errorBCD = sqrt(1/valB + 1/valC + 1/valD) * BCD;
 if(blind){
  std::cout << " blind & $" << BCD << " \\pm " << errorBCD << "$ & $";
 } else{
  std::cout << "$"<<valA << " \\pm " << errorA << "$ & $" << BCD << " \\pm " << errorBCD << "$ & $";
  }
  std::cout << valB << " \\pm " << errorB << "$ & $";
  std::cout << valC << " \\pm " << errorC << "$ & $";
  std::cout << valD << " \\pm " << errorD << "$ \\\\ " << std::endl;
 } else {

  double valA = h->IntegralAndError(9,201,21,101,errorA);
  double valB = h->IntegralAndError(1,8,21,101,errorB);
  double valC = h->IntegralAndError(9,201,1,20,errorC);
  double valD = h->IntegralAndError(1,8,1,20,errorD);

  double BCD = valB*valC/valD;
  errorBCD = sqrt(1/valB + 1/valC + 1/valD) * BCD;
  if(blind){
   std::cout << " blind & $" << BCD << " \\pm " << errorBCD << "$ & $";
  } else{
   std::cout << "$"<<valA << " \\pm " << errorA << "$ & $" << BCD << " \\pm " << errorBCD << "$ & $";
   }
   std::cout << valB << " \\pm " << errorB << "$ & $";
   std::cout << valC << " \\pm " << errorC << "$ & $";
   std::cout << valD << " \\pm " << errorD << "$ \\\\ " << std::endl;
 

}
 h = nullptr;
 h2 = nullptr;

}
