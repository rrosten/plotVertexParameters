//
//  TriggerVariables.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#include <iostream>
#include "PlottingPackage/TriggerVariables.h"
#include "TVector3.h"
#include "TMath.h"

double TriggerVariables::DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double TriggerVariables::DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double TriggerVariables::wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;
}

void TriggerVariables::setZeroVectors(){
    rnd.SetSeed(3.08234);//random but consistent number so the results are replicable.
    eta=0;
    phi=0;
    nRoI=0;
    nJet=0;
    nTrk=0;
    indexLLP=0;
    LLP_dR=0;
    jetdR = new std::vector<double>;
    trackdR = new std::vector<double>;
    l1Eta=0;
    l1Phi=0;
    cluEta = new std::vector<double>;
    cluPhi = new std::vector<double>;
    cluSyst = new std::vector<int>;
}
void TriggerVariables::setBranchAdresses(TChain *chain, bool includeRoIs){
    chain->SetBranchAddress("muRoIClus_Trig_eta"     ,&eta);
    chain->SetBranchAddress("muRoIClus_Trig_phi"     ,&phi);
    chain->SetBranchAddress("muRoIClus_Trig_nRoI"    ,&nRoI);
    chain->SetBranchAddress("muRoIClus_Trig_nJet"    ,&nJet);
    chain->SetBranchAddress("muRoIClus_Trig_nTrk"    ,&nTrk);
    chain->SetBranchAddress("muRoIClus_Trig_indexLLP",&indexLLP );
    chain->SetBranchAddress("muRoIClus_Trig_LLP_dR"  ,&LLP_dR);

    if(includeRoIs){
        chain->SetBranchAddress("L1Muon_eta", &l1Eta);
        chain->SetBranchAddress("L1Muon_phi", &l1Phi);
    }

}
void TriggerVariables::FindGoodTriggers(std::vector<double> &goodEta, std::vector<double> &goodPhi, std::vector<int> &goodRegion){

    for(size_t i = 0; i < eta->size(); i++){
        //if(TMath::Abs(nTrk->at(i)) || TMath::Abs(nJet->at(i))) continue; //isn't isolated trigger, ignore		

        if( TMath::Abs(eta->at(i)) > 1.0 && nRoI->at(i) < 4) continue;
        if( TMath::Abs(eta->at(i)) <= 1.0 && nRoI->at(i) < 3) continue;

        if( (TMath::Abs(eta->at(i)) > 0.8  && TMath::Abs(eta->at(i)) < 1.3) || TMath::Abs(eta->at(i)) > 2.5){ continue;}

        goodEta.push_back(eta->at(i));
        goodPhi.push_back(phi->at(i));

        if(TMath::Abs(eta->at(i)) < 1.0){
            goodRegion.push_back(1);
        }
        else{
            goodRegion.push_back(2);
        }
    }
}
void TriggerVariables::performJetIsolation(std::vector<double> *Jet_ET,std::vector<double> *Jet_eta,std::vector<double> *Jet_phi,std::vector<double> *Jet_logRatio)
{
    jetdR->assign(eta->size(), -99);
    for (unsigned int i_vx = 0; i_vx < eta->size(); i_vx++) {
        double mindR2 = 99; 
        for( unsigned int j_jet = 0; j_jet < Jet_ET->size(); j_jet++ ){

            if(Jet_ET->at(j_jet) < 30.) continue; //only consider jets above Et threshold

            if(Jet_logRatio->at(j_jet) >= 0.5) continue; //don't isolate from jets with low EMF/high logRatio

            double deltaR2 = DeltaR2(phi->at(i_vx),Jet_phi->at(j_jet),eta->at(i_vx),Jet_eta->at(j_jet));
            if(deltaR2 < mindR2) mindR2 = deltaR2;
        }

        jetdR->at(i_vx) = sqrt(mindR2); // set trigger variables to be written out
    }
}
void TriggerVariables::performTrackIsolation(std::vector<double> *Track_pT,std::vector<double> *Track_eta,std::vector<double> *Track_phi)
{
    trackdR->assign(eta->size(), -99);

    for (unsigned int i_vx = 0; i_vx < eta->size(); i_vx++) {

        TVector3 sumTrackPt; sumTrackPt.SetXYZ(0.,0.,0.);
        double mindR2 = 99; 
        for( unsigned int i_t=0; i_t < Track_eta->size(); i_t++ ){
            TVector3 tmpTrack; tmpTrack.SetPtEtaPhi(Track_pT->at(i_t),Track_eta->at(i_t),Track_phi->at(i_t));
            double deltaR2 = DeltaR2(phi->at(i_vx),Track_phi->at(i_t),eta->at(i_vx),Track_eta->at(i_t));

            if(deltaR2 < mindR2 &&  Track_pT->at(i_t) > 5. ) mindR2 = deltaR2;
        }
        trackdR->at(i_vx) = sqrt(mindR2);
    }
}
void TriggerVariables::reclusterRoIs(){
    std::vector<lvl1_muclu_roi> muRoIs;
    std::vector<lvl1_muclu_roi> muClu1;

    std::vector<lvl1_muclu_roi> muRoIs_1up;
    std::vector<lvl1_muclu_roi> muClu1_1up;

    std::vector<lvl1_muclu_roi> muRoIs_1down;
    std::vector<lvl1_muclu_roi> muClu1_1down;
    int n_RoI=0; int n_RoI_1up=0; int n_RoI_1down=0;
    //nominal SF
    for(size_t i_RoI=0; i_RoI < l1Eta->size(); i_RoI++){
        double prob = rnd.Rndm();
	bool isEndcap = (TMath::Abs(l1Eta->at(i_RoI)) < 1.0) ? false : true;

       // std::cout << prob << ", " << BarrelSF << ", " << EndcapSF << std::endl;
        if( (isEndcap && prob < EndcapSF) || (!isEndcap && prob < BarrelSF)){    
	//std::cout << "L1 Muon RoI eta, phi: " << muRoI->eta() << ", " << muRoI->phi() << std::endl;
           // std::cout << "passed nominal SF " << std::endl;
            lvl1_muclu_roi tmpRoI;
            tmpRoI.eta = l1Eta->at(i_RoI);
            tmpRoI.phi = l1Phi->at(i_RoI);
            muRoIs.push_back(tmpRoI);
            n_RoI++;
        }
        if((!isEndcap && prob < (BarrelSF+BarrelSyst) ) || (isEndcap && prob < ( EndcapSF+EndcapSyst)) ) { //MU10 or higher
            //std::cout << "L1 Muon RoI eta, phi: " << muRoI->eta() << ", " << muRoI->phi() << std::endl;
           //std::cout << "passed +1 sigma" <<std::endl;
	   lvl1_muclu_roi tmpRoI;
            tmpRoI.eta = l1Eta->at(i_RoI);
            tmpRoI.phi = l1Phi->at(i_RoI);
            muRoIs_1up.push_back(tmpRoI);
            n_RoI_1up++;
        }
        if((!isEndcap &&  prob < ( BarrelSF-BarrelSyst) ) || (isEndcap && prob < (EndcapSF-EndcapSyst) ) ){ //MU10 or higher
            //std::cout << "L1 Muon RoI eta, phi: " << muRoI->eta() << ", " << muRoI->phi() << std::endl;
            lvl1_muclu_roi tmpRoI;
            tmpRoI.eta = l1Eta->at(i_RoI);
            tmpRoI.phi = l1Phi->at(i_RoI);
            muRoIs_1down.push_back(tmpRoI);
            n_RoI_1down++;
        }
    }//end run through MuonRoIs
    for(int i=0; i<n_RoI; i++){
        muRoIs.at(i).inClu.assign(n_RoI,0);
    }
    for(int i=0; i<n_RoI_1up; i++){
        muRoIs_1up.at(i).inClu.assign(n_RoI_1up,0);
    }
    for(int i=0; i<n_RoI_1down; i++){
        muRoIs_1down.at(i).inClu.assign(n_RoI_1down,0);
    }
    //std::cout << "started with " << l1Eta->size() << " RoIs, after scale factors, have: " << n_RoI << ", syst up: " << n_RoI_1up << ", down: " << n_RoI_1down << std::endl;  
    findAllRoIClusters(n_RoI_1down,muRoIs_1down, muClu1_1down);
    findAllRoIClusters(n_RoI,muRoIs, muClu1);
    findAllRoIClusters(n_RoI_1up,muRoIs_1up, muClu1_1up);

    for(unsigned int i=0; i< muClu1_1down.size(); i++){
        if( (muClu1_1down.at(i).nRoI > 2 && fabs(muClu1_1down.at(i).eta) < 1.0)||(muClu1_1down.at(i).nRoI > 3 && fabs(muClu1_1down.at(i).eta) >= 1.0)){
        cluEta->push_back(muClu1_1down.at(i).eta);
        cluPhi->push_back(muClu1_1down.at(i).phi);
        cluSyst->push_back(-1);
        //std::cout << "syst down cluster" << std::endl;
	}
    }
    for(unsigned int i=0; i< muClu1.size(); i++){
        if( (muClu1.at(i).nRoI > 2 && fabs(muClu1.at(i).eta) < 1.0)||(muClu1.at(i).nRoI > 3 && fabs(muClu1.at(i).eta) >= 1.0)){
            cluEta->push_back(muClu1.at(i).eta);
            cluPhi->push_back(muClu1.at(i).phi);
            cluSyst->push_back(0);
        //std::cout << "nominal cluster" << std::endl;
        }
    }
    for(unsigned int i=0; i< muClu1_1up.size(); i++){
        if( (muClu1_1up.at(i).nRoI > 2 && fabs(muClu1_1up.at(i).eta) < 1.0)||(muClu1_1up.at(i).nRoI > 3 && fabs(muClu1_1up.at(i).eta) >= 1.0)){
            cluEta->push_back(muClu1_1up.at(i).eta);
            cluPhi->push_back(muClu1_1up.at(i).phi);
            cluSyst->push_back(1);
        //std::cout << "syst up cluster" << std::endl;
        }
    }
}
void TriggerVariables::findAllRoIClusters(int n_RoI,  const std::vector<lvl1_muclu_roi> &muRoIs, std::vector<lvl1_muclu_roi> &muClu1){
    std::vector<int> best_clu; best_clu.assign(3,-1);
    if(n_RoI>2){
        lvl1_muclu_roi emptyClu;
        muClu1.push_back(emptyClu);
        best_clu[0] = clusterMuonRoIs(muRoIs, muClu1[0], n_RoI);
	//std::cout << "clustered once, best_clu[0] = " << best_clu[0] << std::endl;
        if(best_clu[0] >= 0){
            int nUnusedRoIs = n_RoI - muClu1[0].nRoI;
            if(nUnusedRoIs > 2){

                std::vector<lvl1_muclu_roi> muRoIs_2 = findUnusedRoIs(muRoIs, muClu1[0]);
                muClu1.push_back(emptyClu);
                best_clu[1] = clusterMuonRoIs(muRoIs_2, muClu1[1], nUnusedRoIs);

                if(best_clu[1] >=0 ){
                    nUnusedRoIs = n_RoI - muClu1[0].nRoI - muClu1[1].nRoI;

                    if(nUnusedRoIs > 2){
                        std::vector<lvl1_muclu_roi> muRoIs_3 = findUnusedRoIs(muRoIs_2,muClu1[1]);
                        muClu1.push_back(emptyClu);
                        best_clu[2] = clusterMuonRoIs(muRoIs_3, muClu1[2], nUnusedRoIs);

                        if(best_clu[2] >= 0){
                            nUnusedRoIs = n_RoI - muClu1[0].nRoI - muClu1[1].nRoI - muClu1[2].nRoI;
                            //if(nUnusedRoIs > 2) std::cout << "Clustered three times, but still " << nUnusedRoIs << " remaining!" << std::endl;
                        }
                    }
                }
            }
        }
    }
}
int TriggerVariables::clusterMuonRoIs(const std::vector<lvl1_muclu_roi> &muonClu0, lvl1_muclu_roi &bestCluster, const int n_clu){

    //Now, cluster the L1MuonRoIs
    std::vector<lvl1_muclu_roi> muonClu = muonClu0;
    bool debug = false;
    if(debug) std::cout << "nRoIs: " << muonClu.size() << ", " << muonClu0.size() << std::endl;
    for(int i_cl=0; i_cl<n_clu; ++i_cl) { // loop on RoIs
        bool improvement = true;
        int n_itr = 0;
        while(improvement){
            ++n_itr;
	    if(debug) std::cout << "nitr: " << n_itr << std::endl;
            double eta_avg=0.0;
            double cosPhi_avg=0.0;
            double sinPhi_avg=0.0;
            int n_in_clu = 0;
            for (int j_cl=0; j_cl<n_clu; ++j_cl) { // loop on RoIs
                float deltaR = DeltaR(muonClu0[j_cl].phi,muonClu[i_cl].phi,muonClu0[j_cl].eta,muonClu[i_cl].eta);
		if(debug) std::cout << "delta R between RoIs: " << deltaR << "("<< i_cl << ", " << j_cl << ")" << std::endl; 
               if(deltaR<m_ClusterRadius){
                    ++n_in_clu;
                    if(n_itr==1){
                        muonClu[i_cl].eta = muonClu[i_cl].eta + (muonClu0[j_cl].eta-muonClu[i_cl].eta)/n_in_clu;
                        muonClu[i_cl].phi = wrapPhi(muonClu[i_cl].phi + wrapPhi(muonClu0[j_cl].phi-muonClu[i_cl].phi)/n_in_clu);
                    } else{
                        //to recalculate the average with all RoIs within a dR = 0.4 cone of the seed position
                        eta_avg += muonClu0[j_cl].eta;
                        cosPhi_avg += cos(muonClu0[j_cl].phi);
                        sinPhi_avg += sin(muonClu0[j_cl].phi);
                    }
                }
            }
            if(n_itr > 1){
                //set cluster position as average position of RoIs
                //This, coupled with the improvement=true/false below, makes an assumption that
                //improvement = false means same # RoIs in cluster, but never less (code had this before, too)
                muonClu[i_cl].eta = eta_avg/n_in_clu;
                muonClu[i_cl].phi = atan2(sinPhi_avg,cosPhi_avg);
            }
            //find the number of ROIs in the new cluster
            //if the number is the same as before,
            Int_t n_in_clu2=0;
            for (int j_cl=0; j_cl<n_clu; ++j_cl) { // loop on cluster
                float deltaR = DeltaR(muonClu0[j_cl].phi,muonClu[i_cl].phi,muonClu0[j_cl].eta,muonClu[i_cl].eta);
                if(deltaR<m_ClusterRadius){
                    ++n_in_clu2;            
                    muonClu[i_cl].inClu[j_cl] = 1;
                } else muonClu[i_cl].inClu[j_cl] = 0;
            }
            //      std::cout << "Finding the number of Muon RoIs in the new Cluster....   " << n_in_clu2 << std::endl;

            if(n_in_clu2>muonClu[i_cl].nRoI){
                muonClu[i_cl].nRoI=n_in_clu2;
                improvement = true;
            } else  improvement = false;
        }//end while
    } //end loop on RoIs

    int bestCluIndex = -1;
    int ncl_max = 0;
    for(int j_cl=0; j_cl<n_clu; ++j_cl) { // loop on RoIs
        if(muonClu[j_cl].nRoI > ncl_max){
            ncl_max = muonClu[j_cl].nRoI;
            if( (muonClu[j_cl].nRoI > 2 && fabs(muonClu[j_cl].eta)<1.0)
                    || (muonClu[j_cl].nRoI > 3 && fabs(muonClu[j_cl].eta)>1.0 && fabs(muonClu[j_cl].eta)) < 2.5 ){
                bestCluIndex = j_cl;
                bestCluster = muonClu[j_cl];
                if(debug) std::cout << "cluster with " << bestCluster.nRoI << " RoIs found" << std::endl;
            }
        }
    }
    return bestCluIndex;
}
std::vector<lvl1_muclu_roi> TriggerVariables::findUnusedRoIs(const std::vector<lvl1_muclu_roi> &muRoIs, const lvl1_muclu_roi &muClu){
    std::vector<lvl1_muclu_roi> muRoIs_2;
    for(unsigned int i_cl=0; i_cl < muRoIs.size(); i_cl++){
        if(!muClu.inClu[i_cl]){
            muRoIs_2.push_back(muRoIs[i_cl]);
        }
    }
    return muRoIs_2;
}
void TriggerVariables::clearAllVectors(bool includeRoIs){

    eta->clear();
    phi->clear();
    nRoI->clear();
    nJet->clear();
    nTrk->clear();
    indexLLP->clear();
    LLP_dR->clear();
    jetdR->clear();
    trackdR->clear();
    if(includeRoIs){
        l1Eta->clear();
        l1Phi->clear();
    }
    cluEta->clear();
    cluPhi->clear();
    cluSyst->clear();
    
}
