

#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/LLPVariables.h"
#include "PlottingPackage/CommonVariables.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/ExtrapolationUtils.h"

#include "TVector3.h"
//#include "PlottingPackage/PlottingUtils.h"
//#include "CommonUtils/MathUtils.h"
#include "TChain.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "TLorentzVector.h"
#include <TString.h>
#include <TRandom3.h>

using namespace std;
using namespace plotVertexParameters;
using namespace ExtrapolationUtils;

double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;

}

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

int isSignal; 
TStopwatch timer;
//
double nBB_Decays;
double nBE_Decays;
double nEE_Decays;
TRandom3 rnd;

int main(int argc, char **argv){

    std::cout << "running program!" << std::endl;

    TChain *chain = new TChain("recoTree");

    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;

    InputFiles::AddFilesToChain(TString(argv[1]), chain);

    //Directory the plots will go in


    CommonVariables *commonVar = new CommonVariables;
    LLPVariables *llpVar = new LLPVariables;


    isSignal = 1;
    chain->SetBranchStatus("*",0);
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain, false, false, false ,false, false);
    llpVar->setZeroVectors();
    llpVar->setBranchAddresses(chain);
    //

    timer.Start();

    double finalWeight = 0;
    bool debug = false;

    //double ctau = 0.96;
    
    for (int l=0;l<chain->GetEntries();l++)
    {

        if(l % 50000 == 0){
            timer.Stop();
            std::cout << "event " << l  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }
        chain->GetEntry(l);
        llpVar->SetAllVariables();
        if(l==0) rnd.SetSeed(llpVar->pT->at(0));
        
        finalWeight = commonVar->pileupEventWeight;

        std::vector<TVector3> llp_pos;
        for(unsigned int i=0; i<llpVar->eta->size(); ++i) {

            ////////////////////////////////
            //* Generating v-pion decays *//O
            TVector3 tmp_pos;
            double Lxyz;
            //use original
            Lxyz = sqrt(pow(llpVar->Lxy->at(i),2) + pow(llpVar->Lz->at(i),2));
            tmp_pos.SetMagThetaPhi(Lxyz,llpVar->theta->at(i),llpVar->phi->at(i));

            //regenerate
            //double bgct = llpVar->bg->at(i)*ctau*1000.0; //in mm
            //Lxyz = rnd.Exp(bgct);
            //tmp_pos.SetMagThetaPhi(Lxyz,llpVar->theta->at(i),llpVar->phi->at(i));
            
            llp_pos.push_back(tmp_pos);
        }
        if(isBB_Event(llp_pos)){nBB_Decays+= finalWeight;}
        else if(isBE_Event(llp_pos)){nBE_Decays+= finalWeight;}
        else if(isEE_Event(llp_pos)){nEE_Decays+= finalWeight;}

        commonVar->clearAllVectors(false, false, false);
        llpVar->clearAllVectors();
        if(debug) std::cout << "vectors are cleared" << std::endl;
    }


    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;

    cout << "" << endl;
    cout << " nEntries                 = " << chain->GetEntries() << endl;
    cout << "" << endl;
    cout << "" << endl;
    cout << " nBB, BE, EE = " << nBB_Decays << ", " << nBE_Decays << ", " << nEE_Decays << endl;
    cout << "" << endl;

}

