//
//  Inputiles.h
//  plotVertexParameters
//
//  Created by Heather Russell on 16/01/16.
//
//

#ifndef plotVertexParameters_InputFiles_h
#define plotVertexParameters_InputFiles_h

#include "TChain.h"
#include "TString.h"

namespace InputFiles{

void AddFilesToChain(TString files, TChain* chain){ 
    std::cout << "adding files " << files << " to tchain" << std::endl;
    if( files.Contains(".root") ){
        std::cout <<" adding the exact file you specified: " << files << std::endl;
        chain->Add(files);
        return;
    }
    else if (files.Contains("/")){
        std::cout << "adding files in the folder: " << files  << std::endl;
        chain->Add(files+"/*root*");
        return;
    }

    //this method needs a pdgid lookup table

    else if(files == "JZAll"){
        //JZXW slices
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361023.Pythia8EvtGen_jetjet_JZ3W.EXOT15_DVAna.r7725_dv16_hist/*.root*");
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361024.Pythia8EvtGen_jetjet_JZ4W.EXOT15_DVAna.r7725_dv16_hist/*.root*");
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361025.Pythia8EvtGen_jetjet_JZ5W.EXOT15_DVAna.r7725_dv16_hist/*.root*");
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361026.Pythia8EvtGen_jetjet_JZ6W.EXOT15_DVAna.r7725_dv16_hist/*.root*");
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361027.Pythia8EvtGen_jetjet_JZ7W.EXOT15_DVAna.r7725_dv16_hist/*.root*");
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361028.Pythia8EvtGen_jetjet_JZ8W.EXOT15_DVAna.r7772_dv16_hist/*.root*");
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361029.Pythia8EvtGen_jetjet_JZ9W.EXOT15_DVAna.r7772_dv16_hist/*.root*");
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361030.Pythia8EvtGen_jetjet_JZ10W.EXOT15_DVAna.r7772_dv16_hist/*.root*");
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361031.Pythia8EvtGen_jetjet_JZ11W.EXOT15_DVAna.r7772_dv16_hist/*.root*");
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361032.Pythia8EvtGen_jetjet_JZ12W.EXOT15_DVAna.r7772_dv16_hist/*.root*");
    } else if(files == "JZ2W"){
    } else if(files == "JZ3W"){
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361023.Pythia8EvtGen_jetjet_JZ3W.EXOT15_DVAna.r7725_dv16_hist/*.root*");
    } else if(files == "JZ4W"){
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361024.Pythia8EvtGen_jetjet_JZ4W.EXOT15_DVAna.r7725_dv16_hist/*.root*");
    } else if(files == "JZ5W"){
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361025.Pythia8EvtGen_jetjet_JZ5W.EXOT15_DVAna.r7725_dv16_hist/*.root*");
    } else if(files == "JZ6W"){
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361026.Pythia8EvtGen_jetjet_JZ6W.EXOT15_DVAna.r7725_dv16_hist/*.root*");
    } else if(files == "JZ7W"){
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361027.Pythia8EvtGen_jetjet_JZ7W.EXOT15_DVAna.r7725_dv16_hist/*.root*");
    } else if(files == "JZ8W"){
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361028.Pythia8EvtGen_jetjet_JZ8W.EXOT15_DVAna.r7772_dv16_hist/*.root*");
    } else if(files == "JZ9W"){
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361029.Pythia8EvtGen_jetjet_JZ9W.EXOT15_DVAna.r7772_dv16_hist/*.root*");
    } else if(files == "JZ10W"){
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361030.Pythia8EvtGen_jetjet_JZ10W.EXOT15_DVAna.r7772_dv16_hist/*.root*");
    } else if(files == "JZ11W"){
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361031.Pythia8EvtGen_jetjet_JZ11W.EXOT15_DVAna.r7772_dv16_hist/*.root*");
    } else if(files == "JZ12W"){
        chain->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v16/user.hrussell.mc15_13TeV.361032.Pythia8EvtGen_jetjet_JZ12W.EXOT15_DVAna.r7772_dv16_hist/*.root*");
    } else if(files == "JZ4WOverlayOld"){
        chain->Add("/LLPData/Outputs/JZXW_DVAna/OverlayMC/JZ4W/*.root*");
        //Signal
    } else if(files== "JZ6WOverlay") {
        chain->Add("/LLPData/Outputs/JZXW_DVAna/OverlayMC/AnalysisCode_v16/JZ6W/*root*");
    } else if(files== "JZ3WOverlay") {
        chain->Add("/LLPData/Outputs/JZXW_DVAna/OverlayMC/AnalysisCode_v16/JZ3W/*root*");
    } else if(files== "JZ4WOverlay") {
        chain->Add("/LLPData/Outputs/JZXW_DVAna/OverlayMC/AnalysisCode_v16/JZ4W/*root*");
    } else if(files == "mH100mS8lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304802.MadGraphPythia8EvtGen_HSS_LongLived_mH100_mS8_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH100mS25lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304803.MadGraphPythia8EvtGen_HSS_LongLived_mH100_mS25_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH125mS5lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.305588.MadGraphPythia8EvtGen_HSS_LongLived_mH125_mS5_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH125mS5lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.305589.MadGraphPythia8EvtGen_HSS_LongLived_mH125_mS5_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH125mS8lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304794.MadGraphPythia8EvtGen_HSS_LongLived_mH125_mS8_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH125mS8lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304797.MadGraphPythia8EvtGen_HSS_LongLived_mH125_mS8_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH125mS15lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.303680.MadGraphPythia8EvtGen_HSS_LongLived_mH125_mS15_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH125mS15lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304798.MadGraphPythia8EvtGen_HSS_LongLived_mH125_mS15_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH125mS40lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.303681.MadGraphPythia8EvtGen_HSS_LongLived_mH125_mS40_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
	 std::cout << "we added files for mH125mS40lt5, with " << chain->GetEntries() << " entries." << std::endl;
    } else if(files == "mH125mS40lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304800.MadGraphPythia8EvtGen_HSS_LongLived_mH125_mS40_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH125mS25lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304795.MadGraphPythia8EvtGen_HSS_LongLived_mH125_mS25_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH125mS25lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304799.MadGraphPythia8EvtGen_HSS_LongLived_mH125_mS25_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH125mS55lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304796.MadGraphPythia8EvtGen_HSS_LongLived_mH125_mS55_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH125mS55lt9"){
        std::cout << " adding: " << files << std::endl;
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304801.MadGraphPythia8EvtGen_HSS_LongLived_mH125_mS55_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH200mS8lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304804.MadGraphPythia8EvtGen_HSS_LongLived_mH200_mS8_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH200mS8lt9"){
        std::cout << "yep " << std::endl;
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304807.MadGraphPythia8EvtGen_HSS_LongLived_mH200_mS8_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH200mS25lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304805.MadGraphPythia8EvtGen_HSS_LongLived_mH200_mS25_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH200mS25lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304808.MadGraphPythia8EvtGen_HSS_LongLived_mH200_mS25_lt9m.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "mH200mS50lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304806.MadGraphPythia8EvtGen_HSS_LongLived_mH200_mS50_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH200mS50lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304809.MadGraphPythia8EvtGen_HSS_LongLived_mH200_mS50_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH400mS50lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304810.MadGraphPythia8EvtGen_HSS_LongLived_mH400_mS50_lt5m.AOD_DVAna.r7772_dv17_hist/*root*"); 
    } else if(files == "mH400mS100lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304811.MadGraphPythia8EvtGen_HSS_LongLived_mH400_mS100_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH400mS50lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304812.MadGraphPythia8EvtGen_HSS_LongLived_mH400_mS50_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH400mS100lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304813.MadGraphPythia8EvtGen_HSS_LongLived_mH400_mS100_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH600mS50lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304814.MadGraphPythia8EvtGen_HSS_LongLived_mH600_mS50_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH600mS150lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304815.MadGraphPythia8EvtGen_HSS_LongLived_mH600_mS150_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH600mS50lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304816.MadGraphPythia8EvtGen_HSS_LongLived_mH600_mS50_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH600mS150lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304817.MadGraphPythia8EvtGen_HSS_LongLived_mH600_mS150_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH1000mS50lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304818.MadGraphPythia8EvtGen_HSS_LongLived_mH1000_mS50_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH1000mS50lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304821.MadGraphPythia8EvtGen_HSS_LongLived_mH1000_mS50_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH1000mS150lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304819.MadGraphPythia8EvtGen_HSS_LongLived_mH1000_mS150_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH1000mS150lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304822.MadGraphPythia8EvtGen_HSS_LongLived_mH1000_mS150_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH1000mS400lt5"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304820.MadGraphPythia8EvtGen_HSS_LongLived_mH1000_mS400_lt5m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mH1000mS400lt9"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304823.MadGraphPythia8EvtGen_HSS_LongLived_mH1000_mS400_lt9m.AOD_DVAna.r7772_dv17_hist/*.root*");
    } 
    //stealth susy
    else if(files == "mg250"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304903.MadGraphPythia8EvtGen_StealthSUSY_mG250.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mg500"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304904.MadGraphPythia8EvtGen_StealthSUSY_mG500.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mg800"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304905.MadGraphPythia8EvtGen_StealthSUSY_mG800.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mg1200"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304906.MadGraphPythia8EvtGen_StealthSUSY_mG1200.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mg1500"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304907.MadGraphPythia8EvtGen_StealthSUSY_mG1500.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "mg2000"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v17/user.hrussell.mc15_13TeV.304908.MadGraphPythia8EvtGen_StealthSUSY_mG2000.AOD_DVAna.r7772_dv17_hist/*.root*");
    }
    //baryogenesis HChiChi
    else if(files == "HChiChi_tatanu_mH125mChi10"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304600.MadGraphPythia8EvtGen_A14N23LO_HChiChi_tatanu_mH125_mChi10.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_tatanu_mH125mChi30"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304601.MadGraphPythia8EvtGen_A14N23LO_HChiChi_tatanu_mH125_mChi30.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_tatanu_mH125mChi50"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304602.MadGraphPythia8EvtGen_A14N23LO_HChiChi_tatanu_mH125_mChi50.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_tatanu_mH125mChi100"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304603.MadGraphPythia8EvtGen_A14N23LO_HChiChi_tatanu_mH125_mChi100.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_cbs_mH125mChi10"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304604.MadGraphPythia8EvtGen_A14N23LO_HChiChi_cbs_mH125_mChi10.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_cbs_mH125mChi30"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304605.MadGraphPythia8EvtGen_A14N23LO_HChiChi_cbs_mH125_mChi30.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_cbs_mH125mChi50"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304606.MadGraphPythia8EvtGen_A14N23LO_HChiChi_cbs_mH125_mChi50.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_cbs_mH125mChi100"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304607.MadGraphPythia8EvtGen_A14N23LO_HChiChi_cbs_mH125_mChi100.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_lcb_mH125mChi10"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304608.MadGraphPythia8EvtGen_A14N23LO_HChiChi_lcb_mH125_mChi10.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_lcb_mH125mChi30"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304609.MadGraphPythia8EvtGen_A14N23LO_HChiChi_lcb_mH125_mChi30.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_lcb_mH125mChi50"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304610.MadGraphPythia8EvtGen_A14N23LO_HChiChi_lcb_mH125_mChi50.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_lcb_mH125mChi100"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304611.MadGraphPythia8EvtGen_A14N23LO_HChiChi_lcb_mH125_mChi100.AOD_DVAna.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_nubb_mH125mChi10"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304612.MadGraphPythia8EvtGen_A14N23LO_HChiChi_nubb_mH125_mChi10.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_nubb_mH125mChi30"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304613.MadGraphPythia8EvtGen_A14N23LO_HChiChi_nubb_mH125_mChi30.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_nubb_mH125mChi50"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304614.MadGraphPythia8EvtGen_A14N23LO_HChiChi_nubb_mH125_mChi50.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "HChiChi_nubb_mH125mChi100"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304615.MadGraphPythia8EvtGen_A14N23LO_HChiChi_nubb_mH125_mChi100.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "WChiChi_tatanu_mChi500"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304710.MadGraphPythia8EvtGen_A14N23LO_WChiChi_Wino_tatanu_mChi500.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "WChiChi_tatanu_mChi1000"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304711.MadGraphPythia8EvtGen_A14N23LO_WChiChi_Wino_tatanu_mChi1000.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "WChiChi_tatanu_mChi1500"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304712.MadGraphPythia8EvtGen_A14N23LO_WChiChi_Wino_tatanu_mChi1500.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "WChiChi_tbs_mChi500"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304713.MadGraphPythia8EvtGen_A14N23LO_WChiChi_Wino_tbs_mChi500.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "WChiChi_tbs_mChi1000"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304714.MadGraphPythia8EvtGen_A14N23LO_WChiChi_Wino_tbs_mChi1000.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "WChiChi_tbs_mChi1500"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304715.MadGraphPythia8EvtGen_A14N23LO_WChiChi_Wino_tbs_mChi1500.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "WChiChi_ltb_mChi500"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304716.MadGraphPythia8EvtGen_A14N23LO_WChiChi_Wino_ltb_mChi500.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "WChiChi_ltb_mChi1000"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304717.MadGraphPythia8EvtGen_A14N23LO_WChiChi_Wino_ltb_mChi1000.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "WChiChi_ltb_mChi1500"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304718.MadGraphPythia8EvtGen_A14N23LO_WChiChi_Wino_ltb_mChi1500.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "WChiChi_nubb_mChi500"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304719.MadGraphPythia8EvtGen_A14N23LO_WChiChi_Wino_nubb_mChi500.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "WChiChi_nubb_mChi1000"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304720.MadGraphPythia8EvtGen_A14N23LO_WChiChi_Wino_nubb_mChi1000.AOD_DVana.r7772_dv17_hist/*.root*");
    } else if(files == "WChiChi_nubb_mChi1500"){
        chain->Add("/LLPData/Outputs/OfficialMC_DVAna/AnalysisCode_v13/user.calpigia.mc15_13TeV.304721.MadGraphPythia8EvtGen_A14N23LO_WChiChi_Wino_nubb_mChi1500.AOD_DVana.r7772_dv17_hist/*.root*");
    }
    else if(files == "dataSmall"){
        //some 2016 Data

        //some 2015 data
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279932.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279984.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280231.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280273.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");

    } 
    else if(files == "data15"){
        //Data
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276262.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276329.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276336.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276416.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276511.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276689.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276778.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276790.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276952.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276954.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00278880.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00278912.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00278968.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279169.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279259.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279279.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279284.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279345.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279515.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279598.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279685.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279813.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279867.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279928.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279932.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279984.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280231.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280273.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280319.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280368.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280423.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280464.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280500.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280520.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280614.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280673.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280753.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280853.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280862.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280950.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280977.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00281070.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00281074.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00281075.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00281317.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00281385.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00281411.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00282625.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00282631.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00282712.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00282784.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00282992.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00283074.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00283155.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00283270.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00283429.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00283608.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00283780.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284006.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284154.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284213.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284285.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284420.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284427.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284484.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");


    }else if(files == "data16"){
        //2016 Data

        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00297730.physics_Main.merge.DAOD_EXOT15.DVAna.f694_m1583_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298595.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298609.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298633.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298687.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298690.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298771.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298773.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298862.physics_Main.merge.DAOD_EXOT15.DVAna.f696_m1588_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298967.physics_Main.merge.DAOD_EXOT15.DVAna.f696_m1588_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00299055.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00299144.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00299147.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00299184.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00299243.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00299584.physics_Main.merge.DAOD_EXOT15.DVAna.f703_m1600_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300279.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300345.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300415.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300418.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300487.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300540.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300571.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300600.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300655.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300687.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300784.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300800.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300863.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300908.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00301912.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1611_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00301918.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1611_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00301932.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1611_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00301973.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1611_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302053.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1611_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302137.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302265.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302269.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302300.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302347.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302380.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302391.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302393.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302737.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302831.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302872.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302919.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302925.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302956.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303007.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303079.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303201.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303208.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303264.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303266.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303291.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303304.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303338.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303421.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303499.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303560.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303638.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303832.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303846.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303892.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303943.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304006.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304008.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304128.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304178.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304198.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304211.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304243.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304308.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304337.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304409.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304431.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304494.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305380.physics_Main.merge.DAOD_EXOT15.DVAna.f727_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305543.physics_Main.merge.DAOD_EXOT15.DVAna.f729_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305571.physics_Main.merge.DAOD_EXOT15.DVAna.f729_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305618.physics_Main.merge.DAOD_EXOT15.DVAna.f729_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305671.physics_Main.merge.DAOD_EXOT15.DVAna.f729_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305674.physics_Main.merge.DAOD_EXOT15.DVAna.f729_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305723.physics_Main.merge.DAOD_EXOT15.DVAna.f729_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305727.physics_Main.merge.DAOD_EXOT15.DVAna.f730_m1653_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305735.physics_Main.merge.DAOD_EXOT15.DVAna.f730_m1653_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305777.physics_Main.merge.DAOD_EXOT15.DVAna.f730_m1653_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305811.physics_Main.merge.DAOD_EXOT15.DVAna.f730_m1653_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305920.physics_Main.merge.DAOD_EXOT15.DVAna.f730_m1653_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306269.physics_Main.merge.DAOD_EXOT15.DVAna.f731_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306278.physics_Main.merge.DAOD_EXOT15.DVAna.f732_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306310.physics_Main.merge.DAOD_EXOT15.DVAna.f731_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306384.physics_Main.merge.DAOD_EXOT15.DVAna.f733_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306419.physics_Main.merge.DAOD_EXOT15.DVAna.f731_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306442.physics_Main.merge.DAOD_EXOT15.DVAna.f731_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306448.physics_Main.merge.DAOD_EXOT15.DVAna.f731_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306451.physics_Main.merge.DAOD_EXOT15.DVAna.f731_m1659_p2769_dv17_hist/*root*");
//Runs after MD2
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311481.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311473.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311402.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311365.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311321.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311287.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311244.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311170.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311071.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310969.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310872.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310863.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310809.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310738.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310691.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310634.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310473.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310468.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310405.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310370.physics_Main.merge.DAOD_EXOT15.DVAna.f755_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310341.physics_Main.merge.DAOD_EXOT15.DVAna.f755_m1699_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310249.physics_Main.merge.DAOD_EXOT15.DVAna.f755_m1699_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310247.physics_Main.merge.DAOD_EXOT15.DVAna.f755_m1699_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310015.physics_Main.merge.DAOD_EXOT15.DVAna.f753_m1689_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309759.physics_Main.merge.DAOD_EXOT15.DVAna.f750_m1689_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309674.physics_Main.merge.DAOD_EXOT15.DVAna.f750_m1689_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309640.physics_Main.merge.DAOD_EXOT15.DVAna.f750_m1689_p2769_v01c_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309516.physics_Main.merge.DAOD_EXOT15.DVAna.f750_m1689_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309440.physics_Main.merge.DAOD_EXOT15.DVAna.f750_m1689_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309390.physics_Main.merge.DAOD_EXOT15.DVAna.f750_m1689_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309375.physics_Main.merge.DAOD_EXOT15.DVAna.f749_m1684_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00308084.physics_Main.merge.DAOD_EXOT15.DVAna.f741_m1673_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00308047.physics_Main.merge.DAOD_EXOT15.DVAna.f741_m1673_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00307935.physics_Main.merge.DAOD_EXOT15.DVAna.f741_m1673_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00307861.physics_Main.merge.DAOD_EXOT15.DVAna.f741_m1673_p2769_dv17_hist/*root*");
        
    }else if(files == "allData"){
        //Data
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276262.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276329.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276336.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276416.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276511.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276689.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276778.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276790.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276952.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00276954.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00278880.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00278912.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00278968.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279169.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279259.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279279.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279284.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279345.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279515.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279598.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279685.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279813.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279867.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279928.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279932.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00279984.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280231.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280273.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280319.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280368.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280423.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280464.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280500.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280520.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280614.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280673.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280753.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280853.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280862.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280950.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00280977.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00281070.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00281074.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00281075.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00281317.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00281385.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00281411.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00282625.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00282631.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00282712.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00282784.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00282992.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00283074.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00283155.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00283270.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00283429.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00283608.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00283780.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284006.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284154.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284213.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284285.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284420.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284427.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data15_13TeV.00284484.physics_Main.merge.DAOD_EXOT15.DVAna.r7562_p2521_p2667_dv17_hist/*root*");

        //2016
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00297730.physics_Main.merge.DAOD_EXOT15.DVAna.f694_m1583_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298595.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298609.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298633.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298687.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298690.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298771.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298773.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298862.physics_Main.merge.DAOD_EXOT15.DVAna.f696_m1588_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00298967.physics_Main.merge.DAOD_EXOT15.DVAna.f696_m1588_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00299055.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00299144.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00299147.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00299184.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00299243.physics_Main.merge.DAOD_EXOT15.DVAna.f698_m1594_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00299584.physics_Main.merge.DAOD_EXOT15.DVAna.f703_m1600_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300279.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300345.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300415.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300418.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300487.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300540.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300571.physics_Main.merge.DAOD_EXOT15.DVAna.f705_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300600.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300655.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300687.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300784.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300800.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300863.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00300908.physics_Main.merge.DAOD_EXOT15.DVAna.f708_m1606_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00301912.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1611_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00301918.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1611_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00301932.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1611_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00301973.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1611_p2667_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302053.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1611_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302137.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302265.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302269.physics_Main.merge.DAOD_EXOT15.DVAna.f709_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302300.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302347.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302380.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302391.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302393.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302737.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302831.physics_Main.merge.DAOD_EXOT15.DVAna.f711_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302872.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302919.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302925.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00302956.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303007.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303079.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303201.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303208.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303264.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303266.physics_Main.merge.DAOD_EXOT15.DVAna.f715_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303291.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303304.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303338.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303421.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303499.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303560.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303638.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303832.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303846.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303892.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00303943.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304006.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304008.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304128.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304178.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304198.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304211.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304243.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304308.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304337.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304409.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304431.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00304494.physics_Main.merge.DAOD_EXOT15.DVAna.f716_m1620_p2689_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305380.physics_Main.merge.DAOD_EXOT15.DVAna.f727_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305543.physics_Main.merge.DAOD_EXOT15.DVAna.f729_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305571.physics_Main.merge.DAOD_EXOT15.DVAna.f729_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305618.physics_Main.merge.DAOD_EXOT15.DVAna.f729_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305671.physics_Main.merge.DAOD_EXOT15.DVAna.f729_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305674.physics_Main.merge.DAOD_EXOT15.DVAna.f729_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305723.physics_Main.merge.DAOD_EXOT15.DVAna.f729_m1646_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305727.physics_Main.merge.DAOD_EXOT15.DVAna.f730_m1653_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305735.physics_Main.merge.DAOD_EXOT15.DVAna.f730_m1653_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305777.physics_Main.merge.DAOD_EXOT15.DVAna.f730_m1653_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305811.physics_Main.merge.DAOD_EXOT15.DVAna.f730_m1653_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00305920.physics_Main.merge.DAOD_EXOT15.DVAna.f730_m1653_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306269.physics_Main.merge.DAOD_EXOT15.DVAna.f731_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306278.physics_Main.merge.DAOD_EXOT15.DVAna.f732_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306310.physics_Main.merge.DAOD_EXOT15.DVAna.f731_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306384.physics_Main.merge.DAOD_EXOT15.DVAna.f733_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306419.physics_Main.merge.DAOD_EXOT15.DVAna.f731_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306442.physics_Main.merge.DAOD_EXOT15.DVAna.f731_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306448.physics_Main.merge.DAOD_EXOT15.DVAna.f731_m1659_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00306451.physics_Main.merge.DAOD_EXOT15.DVAna.f731_m1659_p2769_dv17_hist/*root*");

        //2016 post-MD2
        
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311481.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311473.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311402.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311365.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311321.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311287.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311244.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311170.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00311071.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310969.physics_Main.merge.DAOD_EXOT15.DVAna.f758_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310872.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310863.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310809.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1710_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310738.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310691.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310634.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310473.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310468.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310405.physics_Main.merge.DAOD_EXOT15.DVAna.f756_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310370.physics_Main.merge.DAOD_EXOT15.DVAna.f755_m1704_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310341.physics_Main.merge.DAOD_EXOT15.DVAna.f755_m1699_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310249.physics_Main.merge.DAOD_EXOT15.DVAna.f755_m1699_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310247.physics_Main.merge.DAOD_EXOT15.DVAna.f755_m1699_p2840_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00310015.physics_Main.merge.DAOD_EXOT15.DVAna.f753_m1689_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309759.physics_Main.merge.DAOD_EXOT15.DVAna.f750_m1689_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309674.physics_Main.merge.DAOD_EXOT15.DVAna.f750_m1689_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309640.physics_Main.merge.DAOD_EXOT15.DVAna.f750_m1689_p2769_v01c_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309516.physics_Main.merge.DAOD_EXOT15.DVAna.f750_m1689_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309440.physics_Main.merge.DAOD_EXOT15.DVAna.f750_m1689_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309390.physics_Main.merge.DAOD_EXOT15.DVAna.f750_m1689_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00309375.physics_Main.merge.DAOD_EXOT15.DVAna.f749_m1684_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00308084.physics_Main.merge.DAOD_EXOT15.DVAna.f741_m1673_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00308047.physics_Main.merge.DAOD_EXOT15.DVAna.f741_m1673_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00307935.physics_Main.merge.DAOD_EXOT15.DVAna.f741_m1673_p2769_dv17_hist/*root*");
        chain->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/AnalysisCode_v17/user.hrussell.data16_13TeV.00307861.physics_Main.merge.DAOD_EXOT15.DVAna.f741_m1673_p2769_dv17_hist/*root*");
        
    } else if(files == "LocalTest"){
        chain->Add("/afs/cern.ch/work/h/hrussell/WorkareaRun2/TEST/myoutput/*hist*.root*");
    }  else { std::cout << "WARNING: FILE " << files << " NOT FOUND. TChain will be empty" << std::endl; throw std::runtime_error("Exiting.");}

    //
    return;
}

void AddVertexRerunFiles(TString files, TChain* chain){
    if(files == "mg250"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304903.StealthSUSY_mG250.ESD_vertexSF_syst.e4811_s2698_r7772_v01_EXT0/mG250_vertex_nominalSF.root*");
    } else if(files == "mg500"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304904.StealthSUSY_mG500.ESD_vertexSF_syst.e4811_s2698_r7772_v01_EXT0/mG500_vertex_nominalSF.root*");
    } else if(files == "mg800"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304905.StealthSUSY_mG800.ESD_vertexSF_syst.e4811_s2698_r7772_v01d_EXT0/mG800_vertex_nominalSF.root*");
    } else if(files == "mg1200"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304906.StealthSUSY_mG1200.ESD_vertexSF_syst.e4811_s2698_r7772_v01_EXT0/mG1200_vertex_nominalSF.root*");
    } else if(files == "mg1500"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304907.StealthSUSY_mG1500.ESD_vertexSF_syst.e4811_s2698_r7772_v01_EXT0/mG1500_vertex_nominalSF.root*");
    } else if(files == "mg2000"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304908.StealthSUSY_mG2000.ESD_vertexSF_syst.e4811_s2698_r7772_v01c_EXT0/mG2000_vertex_nominalSF.root*");
    } else if(files == "mH125mS5lt5"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.305588.HSS_LLP_mH125_mS5_lt5m.ESD.e5082_s2698_r7772_v01_EXT0/mH125mS5lt5_vertex_nominalSF.root");
    } else if(files == "mH125mS8lt5"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304794.HSS_LLP_mH125_mS8_lt5m.ESD.e4754_s2698_r7772_v01_EXT0/mH125mS8lt5_vertex_nominalSF.root");
    } else if(files == "mH125mS15lt5"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.303680.HSS_LLP_mH125_mS15_lt5m.ESD.e4623_s2698_r7772_v01_EXT0/mH125mS15lt5_vertex_nominalSF.root");
    } else if(files == "mH125mS25lt5"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304795.HSS_LLP_mH125_mS25_lt5m.ESD.e4754_s2698_r7772_v01_EXT0/mH125mS25lt5_vertex_nominalSF.root");
    } else if(files == "mH125mS40lt5"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.303681.HSS_LLP_mH125_mS40_lt5m.ESD.e4623_s2698_r7772_v01_EXT0/mH125mS40lt5_vertex_nominalSF.root");
    } else if(files == "mH125mS55lt5"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304796.HSS_LLP_mH125_mS55_lt5m.ESD.e4754_s2698_r7772_v01_EXT0/mH125mS55lt5_vertex_nominalSF.root");
    } else if(files == "mH125mS5lt9"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.305589.HSS_LLP_mH125_mS5_lt9m.ESD.e5082_s2698_r7772_v01_EXT0/mH125mS5lt9_vertex_nominalSF.root");
    } else if(files == "mH125mS8lt9"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304797.HSS_LLP_mH125_mS8_lt9m.ESD.e4987_s2698_r7772_v01_EXT0/mH125mS8lt9_vertex_nominalSF.root");
    } else if(files == "mH125mS15lt9"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304798.HSS_LLP_mH125_mS15_lt9m.ESD.e4754_s2698_r7772_v01_EXT0/mH125mS15lt9_vertex_nominalSF.root");
    } else if(files == "mH125mS25lt9"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304799.HSS_LLP_mH125_mS25_lt9m.ESD.e4754_s2698_r7772_v01_EXT0/mH125mS25lt9_vertex_nominalSF.root");
    } else if(files == "mH125mS40lt9"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304800.HSS_LLP_mH125_mS40_lt9m.ESD.e4754_s2698_r7772_v01_EXT0/mH125mS40lt9_vertex_nominalSF.root");
    } else if(files == "mH125mS55lt9"){
        chain->Add("/LLPData/Outputs/MSVertex_Rerun_SF/user.hrussell.mc15_13TeV.304801.HSS_LLP_mH125_mS55_lt9m.ESD.e4804_s2726_r7772_v01_EXT0/mH125mS55lt9_vertex_nominalSF.root");
    } else { std::cout << "WARNING: FILE " << files << " NOT FOUND. TChain will be empty" << std::endl; throw std::runtime_error("Exiting.");}

    return;
}
}
#endif
