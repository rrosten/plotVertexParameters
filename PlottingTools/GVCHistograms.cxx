//
//  GVCHistograms.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>
#include "PlottingPackage/GVCHistograms.h"

void GVCHistograms::initializeHistograms(TString obj, TString str){
    
    std::cout << "initializing good vertex criteria histograms...  " << std::endl;
    
    object = obj;
    locn = str;

    //1D GVC Plots
    
    //these matter for proper error calculations - to get the event weights into the divide.
    h_denom_dR = new TH1D(obj+"_denom_dR_"+str,obj+"_denom_dR_"+str, 20,0,1);
    h_denom_dR->Sumw2();
    h_denom_Et = new TH1D(obj+"_denom_Et_"+str,obj+"_denom_Et_"+str, 40,0,200);
    h_denom_Et->Sumw2();
    h_denom_Pt = new TH1D(obj+"_denom_Pt_"+str,obj+"_denom_Pt_"+str, 40,0,20);
    h_denom_Pt->Sumw2();
    h_denom_sumPt = new TH1D(obj+"_denom_sumPt_"+str,obj+"_denom_sumPt_"+str, 30,0,30);
    h_denom_sumPt->Sumw2();

    h_denom_trks = new TH1D(obj+"_denom_trks_"+str,obj+"_denom_trks_"+str, 15,0,15);
    h_denom_trks->Sumw2();
    
    h_denom_hits = new TH1D(obj+"_denom_hits_"+str,obj+"_denom_hits_"+str, 100,0,5000);
    h_denom_hits->Sumw2();
    
    //1D GVC Plots
    h_nTrks_fPass = new TH1D(obj+"_nTrks_fPass_"+str,obj+"_nTrks_fPass_"+str ,15,0,15);    h_nTrks_fPass->Sumw2();
    h_nMDT_fPass  = new TH1D(obj+"_nMDT_fPass_"+str ,obj+"_nMDT_fPass_"+str  ,100,0,5000); h_nMDT_fPass->Sumw2();
    h_nMDT_up_fPass  = new TH1D(obj+"_nMDT_up_fPass_"+str ,obj+"_nMDT_up_fPass_"+str  ,100,0,5000); h_nMDT_up_fPass->Sumw2();
    h_nRPC_fPass  = new TH1D(obj+"_nRPC_fPass_"+str ,obj+"_nRPC_fPass_"+str  ,100,0,5000); h_nRPC_fPass->Sumw2();
    h_nTGC_fPass  = new TH1D(obj+"_nTGC_fPass_"+str ,obj+"_nTGC_fPass_"+str  ,100,0,5000); h_nTGC_fPass->Sumw2();
    
    h_nMDT_vs_nRPC_fPass = new TH2D(obj+"_nMDT_vs_nRPC_fPass_"+str,obj+"_nMDT_vs_nRPC_fPass_"+str,100,0,1000,100,0,1000);
    h_nMDT_vs_nRPC_fPass->Sumw2();
    h_nMDT_vs_nTGC_fPass = new TH2D(obj+"_nMDT_vs_nTGC_fPass_"+str,obj+"_nMDT_vs_nTGC_fPass_"+str,100,0,1000,100,0,1000);
    h_nMDT_vs_nTGC_fPass->Sumw2();
    
    h_jetIso_vsEt_fPass = new TH1D(obj+"_jetIso_vsEt_fPass_"+str,obj+"_jetIso_vsEt_fPass_"+str ,40,0,200);
    h_jetIso_vsEt_fPass->Sumw2();
    
    h_jetIso_vsdR_fPass = new TH1D(obj+"_jetIso_vsdR_fPass_"+str,obj+"_jetIso_vsdR_fPass_"+str ,20,0,1);
    h_jetIso_vsdR_fPass->Sumw2();
    
    h_trkIso_vsdR_fPass = new TH1D(obj+"_trkIso_vsdR_fPass_"+str,obj+"_trkIso_vsdR_fPass_"+str ,20,0,1);
    h_trkIso_vsdR_fPass->Sumw2();
    
    h_trkIso_vsPt_fPass = new TH1D(obj+"_trkIso_vsPt_fPass_"+str,obj+"_trkIso_vsPt_fPass_"+str ,40,0,20);
    h_trkIso_vsPt_fPass->Sumw2();
    
    h_trkSumIso_vsPt_fPass = new TH1D(obj+"_trkSumIso_vsPt_fPass_"+str,obj+"_trkSumIso_vsPt_fPass_"+str ,30,0,30);
    h_trkSumIso_vsPt_fPass->Sumw2();
    
    h_trkSumIso_vsdR_fPass = new TH1D(obj+"_trkSumIso_vsdR_fPass_"+str,obj+"_trkSumIso_vsdR_fPass_"+str ,20,0,1);
    h_trkSumIso_vsdR_fPass->Sumw2();
    
    //TGraphAsymmErrors
    g_jetIso_vsEt_fPass = new TGraphAsymmErrors();
    g_jetIso_vsdR_fPass = new TGraphAsymmErrors();
    g_trkIso_vsdR_fPass = new TGraphAsymmErrors();
    g_trkIso_vsPt_fPass = new TGraphAsymmErrors();
    g_trkSumIso_vsPt_fPass = new TGraphAsymmErrors();
    g_trkSumIso_vsdR_fPass = new TGraphAsymmErrors();

    g_nMDT_fPass = new TGraphAsymmErrors();
    g_nMDT_up_fPass = new TGraphAsymmErrors();
    g_nRPC_fPass = new TGraphAsymmErrors();
    g_nTGC_fPass = new TGraphAsymmErrors();
    
    //2D GVC Plots
    h_JetdR_vs_Et_highestE = new TH2D(obj+"_JetdR_vs_Et_highestE"+str,obj+"_JetdR_vs_Et_highestE"+str,14,0,0.7,40,0,200);
    h_JetdR_vs_Et_highestE->Sumw2();
    h_JetdR_vs_Et_fPass = new TH2D(obj+"_JetdR_vs_Et_fPass"+str,obj+"_JetdR_vs_Et_fPass"+str,20,0,1,20,0,100);
    h_JetdR_vs_Et_fPass->Sumw2();
    h_TrackdR_vs_Pt_highestPt = new TH2D(obj+"_TrackdR_vs_Pt_highestPt"+str,obj+"_TrackdR_vs_Pt_highestPt"+str,14,0,0.7,40,0,20);
    h_TrackdR_vs_Pt_highestPt->Sumw2();
    h_TrackdR_vs_Pt_fPass = new TH2D(obj+"_TrackdR_vs_Pt_fPass"+str,obj+"_TrackdR_vs_Pt_fPass"+str,20,0,1,40,0,20);
    h_TrackdR_vs_Pt_fPass->Sumw2();
    h_TrackdR_vs_sumPt_fPass = new TH2D(obj+"_TrackdR_vs_sumPt_fPass"+str,obj+"_TrackdR_vs_sumPt_fPass"+str,20,0,1,30,0,30);
    h_TrackdR_vs_sumPt_fPass->Sumw2();
    
    return;
    
}