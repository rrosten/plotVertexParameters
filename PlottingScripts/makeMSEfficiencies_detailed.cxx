//
//  makeCombinedPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "../AtlasLabels.h"
#include "../AtlasStyle.h"
#include "../AtlasUtils.h"

using namespace std;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
TStopwatch timer;

TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){
    fileName = (TString)fileName(0,fileName.Sizeof() - 18);
    TString title = "";

    if(fileName.Contains("HChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        if(channel == "nubb") channel = "#nu b#bar{b}";
        TString mchi = (TString)fileName(fileName.First("5")+5,fileName.Sizeof());
        title = (TString) "H #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    } else if(fileName.Contains("WChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        TString mchi = (TString)fileName(fileName.First("m")+4,fileName.Sizeof());
        title = (TString) "W/Z #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    }else if(fileName.Contains("mS")){
        TString lt = "";
        TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
        TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
        if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
        if(mH == "125") title = (TString) "m_{H},m_{s}=[125,"+mS+"] GeV" + lt;
        else title = (TString) "m_{#Phi},m_{s}=["+mH+","+mS+"] GeV" + lt;
        return title;
    } else if(fileName.Contains("mg")){
        TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
        title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
    }
    return title;
}
void makeCombinedPlot(TFile *_sig[6], int nSIG, TString type);
void makeCombinedPlot(TFile *_sig[6], int nSIG, TString type){
    bool add_ctau = false;    
    TString sigNames[10];
    for(int i=0; i<nSIG; i++){ sigNames[i] = convertFiletoLabel(_sig[i]->GetName(), add_ctau);}

    Int_t colorSig[16] = {kBlue-3, kAzure+5, kTeal-6,kTeal+2,kViolet-3, kRed+2, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2};
    Int_t markerSig[16] = {24,25,26,27,28,25,26,27,24,25,26,27,24,25,26,27};

    Int_t colors[20] = {1,2,3,4,6,1,2,3,4,6,1,2,3,4,6,1,2,3,4,6};



    SetAtlasStyle();
    gStyle->SetPalette(1);

    std::cout << "running on : " << _sig[0]->GetName() << std::endl;

    TCanvas* c = new TCanvas("c_1","c_1",800,600);
    //c->SetLogy();

    TString names[5];
    names[0] = "eta1_pT_nMSTrackletsMSDecays";
    names[1] = "eta2_pT_nMSTrackletsMSDecays";
    names[2] = "eta3_pT_nMSTrackletsMSDecays";
    names[3] = "eta4_pT_nMSTrackletsMSDecays";
    names[4] = "eta5_pT_nMSTrackletsMSDecays";

    //vertex reco
    for(unsigned int i=0;i<2; i++){

        std::cout << "plot: " << names[i] << std::endl;

        TH1D* h_sig[30];
        TH1D* h_denom[30];
        TLegend *legS = new TLegend(0.18,0.7,0.566,0.93);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.03);

        for(int j=0; j<nSIG; j++){

            if(i==0){
                h_sig[j] = (TH1D*) _sig[j]->Get(names[0]);
                h_denom[j] = (TH1D*) _sig[j]->Get("eta1_pT_denom_MSDecays");
                h_sig[j]->Add((TH1D*) _sig[j]->Get(names[1]));
                h_denom[j]->Add((TH1D*) _sig[j]->Get("eta2_pT_denom_MSDecays"));
		h_sig[j]->Divide(h_denom[j]);
            } else if(i==1){
                h_sig[j] = (TH1D*) _sig[j]->Get(names[2]);
                h_sig[j]->Add((TH1D*) _sig[j]->Get(names[3]));
                h_sig[j]->Add((TH1D*) _sig[j]->Get(names[4]));
		h_denom[j] = (TH1D*) _sig[j]->Get("eta3_pT_denom_MSDecays");
		h_denom[j]->Add((TH1D*) _sig[j]->Get("eta4_pT_denom_MSDecays"));
		h_denom[j]->Add((TH1D*) _sig[j]->Get("eta5_pT_denom_MSDecays"));
                h_sig[j]->Divide(h_denom[j]);
	    }

            h_sig[j] = (TH1D*) _sig[j]->Get(names[i]);
            if(!h_sig[j] ) continue;
            h_sig[j]->SetMarkerColor(colorSig[j]);
            h_sig[j]->SetLineColor(colorSig[j]);
            h_sig[j]->SetMarkerStyle(markerSig[j]);

            h_sig[j]->GetXaxis()->SetTitle("LLP p_{T} [GeV]");
    //        h_sig[j]->GetYaxis()->SetRangeUser(0,10);

            legS->SetX1(0.35);legS->SetX2(0.75);

            h_sig[j]->GetYaxis()->SetTitle("<MS Tracklets in #Delta R = 0.4>");

            legS->AddEntry(h_sig[j],sigNames[j],"lp");


            if(j == 0){ h_sig[j]->Draw("");}
            else h_sig[j]->Draw("SAME");

            if(j == nSIG-1){
                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.045);
                latex.SetTextAlign(13);  //align at top
                latex.DrawLatex(.68,.91,"#font[72]{ATLAS}  Internal");

                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.035);
                latex2.SetTextAlign(13);  //align at top
                if(i==0){latex2.DrawLatex(.68,.86,"#font[52]{Barrel decays}#font[42]{(|#eta| < 0.8)}");}
                else {latex2.DrawLatex(.68,.86,"#font[52]{Endcap decays }#font[42]{(1.3 < |#eta| < 2.5)}");}

                legS->Draw();
                gPad->RedrawAxis();

                c->Print("combinedPlotsNote/"+names[i]+std::to_string(j)+"_"+type+".pdf");
                legS->Clear();
            }

        }
    }
}


int makeMSEfficiencies_detailed(){


    gROOT->LoadMacro("AtlasUtils.C");
    gROOT->LoadMacro("AtlasLabels.C");
    gROOT->LoadMacro("AtlasStyle.C");

    bool add_ctau = false;

    //_bkg[5] = new TFile("bkgdJZ/JZ7W/outputGVC.root");
    //_bkg[6] = new TFile("bkgdJZ/JZ8W/outputGVC.root");
    TFile *_sig[6];

    _sig[0] = new TFile("mg250/outputMSEff.root");
    _sig[1] = new TFile("mg500/outputMSEff.root");
    _sig[2] = new TFile("mg800/outputMSEff.root");
    _sig[3] = new TFile("mg1200/outputMSEff.root");
    _sig[4] = new TFile("mg1500/outputMSEff.root");
    _sig[5] = new TFile("mg2000/outputMSEff.root");

    makeCombinedPlot(_sig, 6, "stealth");
    /*
    _sig[0] = new TFile("mH125mS8lt5/outputMSEff.root");
    _sig[1] = new TFile("mH125mS15lt5/outputMSEff.root");
    _sig[2] = new TFile("mH125mS25lt5/outputMSEff.root");
    _sig[3] = new TFile("mH125mS40lt5/outputMSEff.root");
    _sig[4] = new TFile("mH125mS55lt5/outputMSEff.root");

    makeCombinedPlot(_sig, 5, "higgs");

    _sig[0] = new TFile("mH100mS8lt5/outputMSEff.root");
    _sig[1] = new TFile("mH200mS25lt5/outputMSEff.root");
    _sig[2] = new TFile("mH400mS50lt5/outputMSEff.root");
    _sig[3] = new TFile("mH600mS50lt5/outputMSEff.root");
    _sig[4] = new TFile("mH600mS150lt5/outputMSEff.root");

    makeCombinedPlot(_sig, 5, "scalar");

    _sig[0] = new TFile("HChiChi_cbs_mH125mChi100/outputMSEff.root");
    _sig[1] = new TFile("HChiChi_lcb_mH125mChi30/outputMSEff.root");
    _sig[2] = new TFile("HChiChi_nubb_mH125mChi30/outputMSEff.root");
    _sig[3] = new TFile("WChiChi_ltb_mChi1000/outputMSEff.root");
    _sig[4] = new TFile("WChiChi_tbs_mChi1500/outputMSEff.root");

    makeCombinedPlot(_sig, 5, "bg");
     */
    return 314;

}
