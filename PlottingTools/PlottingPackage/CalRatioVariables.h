//
//  CalRatioVariables.h
//  plotVertexParameters
//
//  Created by Heather Russell on 12/10/15.
//
//

#ifndef __plotVertexParameters__CalRatioVariables__
#define __plotVertexParameters__CalRatioVariables__

#include <vector>
#include "TChain.h"

struct CalRatioVariables {
    
public:

    std::vector<double> *BDT;
    std::vector<unsigned int> *nTrk;
    std::vector<double> *sumTrkpT;
    std::vector<double> *maxTrkpT;
    std::vector<double> *width;
    std::vector<double> *minDRTrkpt2;
    std::vector<double> *l1frac;
    std::vector<double> *c_firenden;
    std::vector<double> *c_lat;
    std::vector<double> *c_long;
    std::vector<double> *c_r;
    std::vector<double> *c_lambda;
    std::vector<double> *logR;
    
    void setZeroVectors();
    void setBranchAdresses(TChain *chain);
    void clearAllVectors();
};


#endif /* defined(__plotVertexParameters__CalRatioVariables__) */
