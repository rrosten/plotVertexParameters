//
//  makeCombinedPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include "TMultiGraph.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "TLine.h"
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"

using namespace std;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
TStopwatch timer;

TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){
    int numberOfSlashes = 0; int tmpSlashNumber = 0;
    int indexOf2ndLastSlash = -1;
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            numberOfSlashes++;
        }
    }
    for( int charN=0; charN < fileName.Sizeof(); charN++){
        if(TString(fileName(charN,1)) == "/"){
            tmpSlashNumber++;
        }
        if(tmpSlashNumber == (numberOfSlashes - 1)){
            indexOf2ndLastSlash = charN;
            break;
        }
    }
	fileName = (TString)fileName(0+indexOf2ndLastSlash+1,fileName.Sizeof() - 20 - indexOf2ndLastSlash - 1);
    std::cout << "file name : " << fileName << std::endl;
	TString title = "";

	if(fileName.Contains("HChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        if(channel == "nubb") channel = "#nu b#bar{b}";
        TString mchi = (TString)fileName(fileName.First("5")+5,fileName.Sizeof());
        title = (TString) "H #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
		return title;
    } else if(fileName.Contains("WChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        TString mchi = (TString)fileName(fileName.First("m")+4,fileName.Sizeof());
        title = (TString) "W/Z #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    }else if(fileName.Contains("mS")){
		TString lt = "";
		TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
		TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
		if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
		if(mH == "125") title = (TString) "m_{H},m_{s}=[125,"+mS+"] GeV" + lt;
		else title = (TString) "m_{#Phi},m_{s}=["+mH+","+mS+"] GeV" + lt;
		return title;
	} else if(fileName.Contains("mg")){
		TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
		title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
	}
	return title;
}
void makePlotWithLines(TFile *_sig[6],  TString type, bool forThesis);
void makePlotWithLines(TFile *_sig[6],  TString type, bool forThesis = false){
bool add_ctau = false;    
    TString sigNames[3];
    int nSIG=3;
    std::cout << "file name: " << _sig[0]->GetName() << std::endl;
    for(int i=0; i<3; i++){ sigNames[i] = convertFiletoLabel(_sig[i]->GetName(), add_ctau);}

    Int_t markerSig[3] = {24,25,26};
    Int_t colorSig[3] = {kViolet -3, kTeal - 6, kBlue -3};
    if(nSIG < 4){
        colorSig[0] = kBlue - 3;
        colorSig[1] = kAzure + 5;
        colorSig[2] = kTeal - 6;
    }
        
    TString names[2];
    names[0] = "MSVx_Barrel_Lxy_eff";
    names[1] = "MSVx_Endcap_Lz_eff";
    
    TString trignames[2];
    trignames[0] = "MSTrig_1B_Lxy_eff";
    trignames[1] = "MSTrig_1E_Lz_eff";
    
    std::cout << "running on : " << _sig[0]->GetName() << std::endl;
    
    TCanvas* c = new TCanvas("c_1","c_1",800,600);
    //vertex reco
    for(unsigned int i=0;i<2; i++){
        
        std::cout << "plot: " << names[i] << std::endl;
        
        TH1D* h_sig[30];
        
        TLegend *legS = new TLegend(0.18,0.72,0.566,0.85);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.03);
        
        for(int j=0; j<nSIG; j++){
            h_sig[j] = (TH1D*) _sig[j]->Get(names[i]);
            if(!h_sig[j] ) continue;
            h_sig[j]->SetMarkerColor(colorSig[j]);
            h_sig[j]->SetLineColor(colorSig[j]);
            h_sig[j]->SetMarkerStyle(markerSig[j]);
            
            if(names[i].Contains("Lxy")){
                h_sig[j]->GetXaxis()->SetTitle("Lxy [m]");
                h_sig[j]->GetYaxis()->SetRangeUser(0,0.3);
                h_sig[j]->GetXaxis()->SetRangeUser(0,9);
            }
            else if(names[i].Contains("Lz")){
                h_sig[j]->GetXaxis()->SetTitle("|Lz| [m]");
                h_sig[j]->GetYaxis()->SetRangeUser(0,1);
                h_sig[j]->GetXaxis()->SetRangeUser(0,22);
            }
            h_sig[j]->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");
            
            legS->AddEntry(h_sig[j],sigNames[j],"lp");
            
            
            if(j == 0){ h_sig[j]->Draw("");}
            else h_sig[j]->Draw("SAME");
            
            if(j == nSIG-1){
                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.045);
                latex.SetTextAlign(13);  //align at top
                double y_val_label = 0.91;
                if(!forThesis){
                    y_val_label = 0.86;
                    latex.DrawLatex(.68,.91,"#font[72]{ATLAS}  Internal");
                }
                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.035);
                latex2.SetTextAlign(13);  //align at top
                if(names[i].Contains("Lxy")){
                    latex2.DrawLatex(.19,y_val_label,"#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}");
                }
                else if(names[i].Contains("Lz") ){
                    latex2.DrawLatex(.19,y_val_label,"#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}");
                }
                
                legS->Draw();
                TLatex detLabels;
                //detLabels.SetNDC();
                detLabels.SetTextColor(kBlack);
                detLabels.SetTextSize(0.03);
                detLabels.SetTextAlign(10);
                detLabels.SetTextAngle(90);
                
                if(i==0){
                    //barrel lines
                    TLine *hcalEnd = new TLine(3.865,0,3.865,0.3);
                    hcalEnd->SetLineColor(kGray);
                    hcalEnd->SetLineStyle(9);
                    hcalEnd->SetLineWidth(2);
                    hcalEnd->Draw();
                    TLine *mdts[6];
                    mdts[0]= new TLine(4.383,0,4.383,0.3);
                    mdts[1]= new TLine(4.718,0,4.718,0.3);
                    mdts[2]= new TLine(7.888,0,7.888,0.3);
                    mdts[3]= new TLine(6.861,0,6.861,0.3);
                    mdts[4]= new TLine(10.275,0,10.275,0.3);
                    mdts[5]= new TLine(9.222,0,9.222,0.3);
                    
                    detLabels.DrawLatex(3.85,0.24,"#font[42]{HCal end}");
                    detLabels.DrawLatex(4.37,0.24,"#font[42]{MDT1 S}");
                    detLabels.DrawLatex(4.7,0.24,"#font[42]{MDT1 L}");
                    detLabels.DrawLatex(7.87,0.24,"#font[42]{MDT2 S}");
                    detLabels.DrawLatex(6.85,0.24,"#font[42]{MDT2 L}");
                    //detLabels.DrawLatex(10.26,0.46,"#font[42]{MDT3 S}");
                    //detLabels.DrawLatex(9.21,0.46,"#font[42]{MDT3 L}");
                    
                    for(int line=0;line<4;line++){
                        mdts[line]->SetLineColor(kGray+(line+2)/2);
                        mdts[line]->SetLineStyle(2 + 5*(line%2));
                        mdts[line]->SetLineWidth(2);

                        mdts[line]->Draw();
                    }
                }else if(i==1){
                    //endcap lines
                    TLine *hcalEnd = new TLine(6.0,0,6.0,1);
                    hcalEnd->SetLineColor(kGray);
                    hcalEnd->SetLineStyle(9);
                    hcalEnd->SetLineWidth(2);
                    hcalEnd->Draw();
                    TLine *mdts[6];
                    mdts[0]= new TLine(7.023,0,7.023,1);
                    mdts[1]= new TLine(7.409,0,7.409,1);
                    mdts[2]= new TLine(13.265,0,13.265,1);
                    mdts[3]= new TLine(13.660,0,13.660,1);
                    mdts[4]= new TLine(21.260,0,21.260,1);
                    mdts[5]= new TLine(21.680,0,21.680,1);
                    detLabels.DrawLatex(5.98,0.75,"#font[42]{HCal end}");
                    detLabels.DrawLatex(7.01,0.75,"#font[42]{MDT1 S}");
                    detLabels.DrawLatex(7.39,0.75,"#font[42]{MDT1 L}");
                    detLabels.DrawLatex(13.25,0.75,"#font[42]{MDT2 S}");
                    detLabels.DrawLatex(13.65,0.75,"#font[42]{MDT2 L}");
                    //detLabels.DrawLatex(21.25,0.75,"#font[42]{MDT3 S}");
                    //detLabels.DrawLatex(21.67,0.75,"#font[42]{MDT3 L}");
                    
                    for(int line=0;line<4;line++){
                        mdts[line]->SetLineColor(kGray+(line+2)/2);
                        mdts[line]->SetLineStyle(2 + 5*(line%2));
                        mdts[line]->SetLineWidth(2);

                        mdts[line]->Draw();
                    }
                }
                
                gPad->RedrawAxis();
                
                c->Print("../OutputPlots/notePlots/"+names[i]+"_"+type+".pdf");
                legS->Clear();
            }
            
        }
    }
    //trigger reco
    for(unsigned int i=0;i<0; i++){
        TGraphAsymmErrors* h_sig[30];
        TMultiGraph *mg = new TMultiGraph();
        
        TLegend *legS = new TLegend(0.18,0.75,0.566,0.85);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.03);
        
        for(int j=0; j<nSIG; j++){
            h_sig[j] = new TGraphAsymmErrors();
            h_sig[j] = (TGraphAsymmErrors*) _sig[j]->Get(trignames[i]);
            
            if(!h_sig[j] ) continue;
            h_sig[j]->SetMarkerColor(colorSig[j]); h_sig[j]->SetLineColor(colorSig[j]); h_sig[j]->SetMarkerStyle(markerSig[j]);
            
            legS->AddEntry(h_sig[j],sigNames[j],"lp");
            mg->Add(h_sig[j]);
            
            if(j == nSIG-1){
                mg->Draw("AP");
                
                if(names[i].Contains("Lxy")){
                    mg->GetXaxis()->SetLimits(0,9);
                    mg->SetMaximum(1);
                    if(type == "bg" || type == "higgs"){mg->SetMaximum(0.6);}
                    mg->GetXaxis()->SetTitle("Lxy [m]");
                }
                else if(names[i].Contains("Lz")){
                    mg->GetXaxis()->SetLimits(0,15);
                    mg->SetMaximum(1);
                    if(type == "bg" || type == "higgs"){mg->SetMaximum(0.6);}
                    mg->GetXaxis()->SetTitle("|Lz| [m]");
                }
                mg->GetYaxis()->SetTitle("Muvtx trigger efficiency");
                
                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.045);
                latex.SetTextAlign(13);  //align at top
                double y_val_label = 0.91;
                if(!forThesis){
                    y_val_label = 0.86;
                    latex.DrawLatex(.68,.91,"#font[72]{ATLAS}  Internal");
                }
                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.035);
                latex2.SetTextAlign(13);  //align at top
                if(names[i].Contains("Lxy")){
                    latex2.DrawLatex(.19,y_val_label,"#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}");}
                else {latex2.DrawLatex(.19,y_val_label,"#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}");}
                
                legS->Draw();
                TLatex detLabels;
                //detLabels.SetNDC();
                detLabels.SetTextColor(kBlack);
                detLabels.SetTextSize(0.03);
                detLabels.SetTextAlign(10);
                detLabels.SetTextAngle(90);
                if(i==0){
                    TLine *rpcs[6];
                    rpcs[0]= new TLine(7.758,0,7.758,1);
                    rpcs[1]= new TLine(6.729,0,6.729,1);
                    rpcs[2]= new TLine(8.282,0,8.282,1);
                    rpcs[3]= new TLine(7.372,0,7.372,1);
                    rpcs[4]= new TLine(10.158,0,10.158,1);
                    rpcs[5]= new TLine(9.733,0,9.733,1);
                    TLine *hcalEnd = new TLine(3.865,0,3.865,1);
                    hcalEnd->SetLineColor(kGray);
                    hcalEnd->SetLineStyle(9);
                    hcalEnd->SetLineWidth(2);
                    hcalEnd->Draw();
                    detLabels.DrawLatex(3.85,0.75,"#font[42]{HCal end}");
                    detLabels.DrawLatex(7.75,0.75,"#font[42]{RPC1 S}");
                    detLabels.DrawLatex(6.72,0.75,"#font[42]{RPC1 L}");
                    detLabels.DrawLatex(8.27,0.75,"#font[42]{RPC2 S}");
                    detLabels.DrawLatex(7.36,0.75,"#font[42]{RPC2 L}");
                    //detLabels.DrawLatex(10.15,0.75,"#font[42]{RPC3 S}");
                    //detLabels.DrawLatex(9.72,0.75,"#font[42]{RPC3 L}");
                    
                    for(int line=0;line<4;line++){
                        rpcs[line]->SetLineColor(kGray+(line+2)/2);
                        rpcs[line]->SetLineStyle(2 + 5*(line%2));
                        rpcs[line]->SetLineWidth(2);

                        rpcs[line]->Draw();
                    }
                }else if(i==1){
                    TLine *rpcs[5];
                    rpcs[0]= new TLine(13.3,0,13.3,1);
                    rpcs[1]= new TLine(14.04,0,14.04,1);
                    rpcs[2]= new TLine(14.34,0,14.34,1);
                    rpcs[3]= new TLine(14.54,0,14.54,1);
                    rpcs[4]= new TLine(14.81,0,14.81,1);
                    TLine *hcalEnd = new TLine(6,0,6,1);
                    hcalEnd->SetLineColor(kGray);
                    hcalEnd->SetLineStyle(9);
                    hcalEnd->SetLineWidth(2);
                    hcalEnd->Draw();
                    detLabels.DrawLatex(5.98,0.75,"#font[42]{HCal end}");
                    detLabels.DrawLatex(13.28,0.75,"#font[42]{TGC1}");
                    /*detLabels.DrawLatex(14.03,0.75,"#font[42]{TGC1 L}");
                    detLabels.DrawLatex(14.33,0.75,"#font[42]{TGC2 S}");
                    detLabels.DrawLatex(14.54,0.75,"#font[42]{TGC2 L}");
                    detLabels.DrawLatex(14.81,0.75,"#font[42]{TGC3 S}");*/
                    
                    for(int line=0;line<1;line++){
                        rpcs[line]->SetLineColor(kGray+(line+2)/2);
                        rpcs[line]->SetLineStyle(2);
                        rpcs[line]->SetLineWidth(2);
                        if(line == 2 || line == 4) rpcs[line]->SetLineStyle(7);
                        rpcs[line]->Draw();
                    }
                }
                gPad->RedrawAxis();
                c->Print("../OutputPlots/notePlots/"+trignames[i]+"_"+type+".pdf");
                
                legS->Clear(); 
                mg->Clear();
            }
            
        }
    }
}
void makeCombinedPlot(TFile *_sig[6], int nSIG, TString type, bool forThesis);
void makeCombinedPlot(TFile *_sig[6], int nSIG, TString type, bool forThesis = false){
    bool add_ctau = false;
    TString sigNames[6];
    std::cout << "file name: " << _sig[0]->GetName() << std::endl;
    for(int i=0; i<nSIG; i++){ sigNames[i] = convertFiletoLabel(_sig[i]->GetName(), add_ctau);}
    
    Int_t colorSig[16] = { kPink-3,kViolet-3,kBlue-3, kAzure+5, kTeal-6,kTeal+2, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2};
    
    if(nSIG < 4){
        colorSig[0] = kBlue - 3;
        colorSig[1] = kAzure + 5;
        colorSig[2] = kTeal - 6;
    }
    Int_t markerSig[16] = {24,25,26,27,28,30,26,27,24,25,26,27,24,25,26,27};
    
    Int_t colors[20] = {1,2,3,4,6,1,2,3,4,6,1,2,3,4,6,1,2,3,4,6};
    
    TString names[12];
    names[0] = "MSVx_Barrel_Lxy_eff";
    names[1] = "MSVx_Endcap_Lz_eff";
    
    
    //names[2] = "MSTrig_1B_Lxy_eff";
    //names[3] = "MSTrig_1E_Lz_eff";
    
    TString trignames[4];
    trignames[0] = "MSTrig_1B_Lxy_eff";
    //trignames[1] = "MSTrig_BID_Lxy_eff";
    trignames[1] = "MSTrig_1E_Lz_eff";
    //trignames[3] = "MSTrig_EID_Lz_eff";
    
    names[2] = "MSVx_Barrel_pT_A_eff";
    names[3] = "MSVx_Barrel_pT_B_eff";
    names[4] = "MSVx_Endcap_pT_A_eff";
    names[5] = "MSVx_Endcap_pT_B_eff";
    
    
    //SetAtlasStyle();
    //gStyle->SetPalette(1);
    
    std::cout << "running on : " << _sig[0]->GetName() << std::endl;
    
    TCanvas* c = new TCanvas("c_1","c_1",800,600);
    //c->SetLogy();
    
    //vertex reco
    for(unsigned int i=0;i<6; i++){
        
        std::cout << "plot: " << names[i] << std::endl;
        
        TH1D* h_sig[30];
        
        TLegend *legS = new TLegend(0.18,0.7,0.566,0.93);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.03);
        
        for(int j=0; j<nSIG; j++){
            h_sig[j] = (TH1D*) _sig[j]->Get(names[i]);
            if(!h_sig[j] ) continue;
            h_sig[j]->SetMarkerColor(colorSig[j]);
            h_sig[j]->SetLineColor(colorSig[j]);
            h_sig[j]->SetMarkerStyle(markerSig[j]);
            
            if(names[i].Contains("Lxy")){
                h_sig[j]->GetXaxis()->SetTitle("Lxy [m]");
                h_sig[j]->GetYaxis()->SetRangeUser(0,0.3);
                h_sig[j]->GetXaxis()->SetRangeUser(0,8);
            }
            else if(names[i].Contains("Lz")){ h_sig[j]->GetXaxis()->SetTitle("|Lz| [m]");
                h_sig[j]->GetYaxis()->SetRangeUser(0,1);}
            else if(names[i].Contains("pT")){
                legS->SetX1(0.65); legS->SetX2(0.95);
                if(names[i].Contains("Barrel")) h_sig[j]->GetYaxis()->SetRangeUser(0,0.3);
                else h_sig[j]->GetYaxis()->SetRangeUser(0,1);
                h_sig[j]->GetXaxis()->SetRangeUser(0,1200);
                h_sig[j]->GetXaxis()->SetTitle("p_{T} [GeV]");
            }
            h_sig[j]->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");
            
            legS->AddEntry(h_sig[j],sigNames[j],"lp");
            
            
            if(j == 0){ h_sig[j]->Draw("");}
            else h_sig[j]->Draw("SAME");
            
            if(j == nSIG-1){
                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.045);
                latex.SetTextAlign(13);  //align at top
                double y_val_label = 0.91;
                if(!forThesis){
                    y_val_label = 0.86;
                    latex.DrawLatex(.68,.91,"#font[72]{ATLAS}  Internal");
                }
                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.035);
                latex2.SetTextAlign(13);  //align at top
                if(names[i].Contains("Lxy")){latex2.DrawLatex(.70,y_val_label,"#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}");}
                else if(names[i].Contains("Lz") ){latex2.DrawLatex(.65,y_val_label,"#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}");}
                else if(names[i].Contains("Barrel_pT_A")){latex2.DrawLatex(.2,y_val_label,"#splitline{#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}}{3 < L_{xy} < 3.8 m}");}
                else if(names[i].Contains("Barrel_pT_B")){latex2.DrawLatex(.2,y_val_label,"#splitline{#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}}{3.8 < L_{xy} < 8 m}");}
                else if(names[i].Contains("Endcap_pT_A") ){latex2.DrawLatex(.2,y_val_label,"#splitline{#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}}{5 < L_{z} < 6 m}");}
                else if(names[i].Contains("Endcap_pT_B") ){latex2.DrawLatex(.2,y_val_label,"#splitline{#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}}{6 < L_{z} < 15 m}");}
                
                legS->Draw();
                gPad->RedrawAxis();
                
                c->Print("../OutputPlots/notePlots/"+names[i]+"_"+type+".pdf");
                legS->Clear();
            }
            
        }
    }
    //trigger reco
    for(unsigned int i=0;i<0; i++){
        TGraphAsymmErrors* h_sig[30];
        TMultiGraph *mg = new TMultiGraph();
        
        TLegend *legS = new TLegend(0.18,0.7,0.566,0.93);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.03);
        
        for(int j=0; j<nSIG; j++){
            h_sig[j] = new TGraphAsymmErrors();
            h_sig[j] = (TGraphAsymmErrors*) _sig[j]->Get(trignames[i]);
            
            if(!h_sig[j] ) continue;
            h_sig[j]->SetMarkerColor(colorSig[j]); h_sig[j]->SetLineColor(colorSig[j]); h_sig[j]->SetMarkerStyle(markerSig[j]);
            
            legS->AddEntry(h_sig[j],sigNames[j],"lp");
            mg->Add(h_sig[j]);
            
            if(j == nSIG-1){
                mg->Draw("AP");
                
                if(names[i].Contains("Lxy")){
                    mg->GetXaxis()->SetLimits(0,8);
                    mg->SetMaximum(1);
                    if(type == "bg" || type == "higgs"){mg->SetMaximum(0.6);}
                    mg->GetXaxis()->SetTitle("Lxy [m]");
                }
                else if(names[i].Contains("Lz")){
                    mg->GetXaxis()->SetLimits(0,15);
                    mg->SetMaximum(1);
                    if(type == "bg" || type == "higgs"){mg->SetMaximum(0.6);}
                    mg->GetXaxis()->SetTitle("|Lz| [m]");
                }
                mg->GetYaxis()->SetTitle("Muvtx trigger efficiency");
                
                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.045);
                latex.SetTextAlign(13);  //align at top
                double y_val_label = 0.91;
                if(!forThesis){
                    y_val_label = 0.86;
                    latex.DrawLatex(.68,.91,"#font[72]{ATLAS}  Internal");
                }
                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.035);
                latex2.SetTextAlign(13);  //align at top
                if(names[i].Contains("Lxy")){latex2.DrawLatex(.70,y_val_label,"#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}");}
                else {latex2.DrawLatex(.65,y_val_label,"#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}");}
                
                legS->Draw();
                gPad->RedrawAxis();
                c->Print("../OutputPlots/notePlots/"+trignames[i]+"_"+type+".pdf");
                
                legS->Clear();
                mg->Clear();
            }
            
        }
    }
}


int makeMSEfficiencies_Note(bool forThesis = false){


	//gROOT->LoadMacro("AtlasUtils.C");
	//gROOT->LoadMacro("AtlasLabels.C");
	//gROOT->LoadMacro("AtlasStyle.C");

	bool add_ctau = false;
    
	//_bkg[5] = new TFile("bkgdJZ/JZ7W/outputGVC.root");
	//_bkg[6] = new TFile("bkgdJZ/JZ8W/outputGVC.root");
	TFile *_sig[6];
    //TString locn = "/Users/hrussell/Work/Run2Plots/signalMC/";
    TString locn = "../OutputPlots/signalMC/";

/*    _sig[0] = new TFile(locn+"mg250/outputMSVxEff.root");
    _sig[1] = new TFile(locn+"mg500/outputMSVxEff.root");
    _sig[2] = new TFile(locn+"mg800/outputMSVxEff.root");
    _sig[3] = new TFile(locn+"mg1200/outputMSVxEff.root");
    _sig[4] = new TFile(locn+"mg1500/outputMSVxEff.root");
    _sig[5] = new TFile(locn+"mg2000/outputMSVxEff.root");
    
    makeCombinedPlot(_sig, 6, "stealth", forThesis);
   makeCombinedPlot(_sig, 3, "stealthLow", forThesis);
    for(int i=0;i<6;i++){delete _sig[i];}
    
    _sig[0] = new TFile(locn+"mg1200/outputMSEff.root");
    _sig[1] = new TFile(locn+"mg1500/outputMSEff.root");
    _sig[2] = new TFile(locn+"mg2000/outputMSEff.root");
    
    makeCombinedPlot(_sig, 3, "stealthHigh", forThesis);
    for(int i=0;i<3;i++){delete _sig[i];}
    */
    _sig[0] = new TFile(locn+"mg250/outputMSVxEff.root");
    _sig[1] = new TFile(locn+"mg800/outputMSVxEff.root");
    _sig[2] = new TFile(locn+"mg2000/outputMSVxEff.root");

    makePlotWithLines(_sig, "detectorDetails",forThesis);

    for(int i=0;i<3;i++){delete _sig[i];}
    
    _sig[0] = new TFile(locn+"mg250/outputMSVxEff.root");
       _sig[1] = new TFile(locn+"mg500/outputMSVxEff.root");
       _sig[2] = new TFile(locn+"mg800/outputMSVxEff.root");
       _sig[3] = new TFile(locn+"mg1200/outputMSVxEff.root");
       _sig[4] = new TFile(locn+"mg1500/outputMSVxEff.root");
       _sig[5] = new TFile(locn+"mg2000/outputMSVxEff.root");
       makeCombinedPlot(_sig, 6, "stealthMixed", forThesis);

    if(!forThesis){
    _sig[0] = new TFile("mH125mS8lt5/outputMSEff.root");
    _sig[1] = new TFile("mH125mS15lt5/outputMSEff.root");
    _sig[2] = new TFile("mH125mS25lt5/outputMSEff.root");
    _sig[3] = new TFile("mH125mS40lt5/outputMSEff.root");
    _sig[4] = new TFile("mH125mS55lt5/outputMSEff.root");
    
    makeCombinedPlot(_sig, 5, "higgs");
    for(int i=0;i<5;i++){delete _sig[i];}
    
    _sig[0] = new TFile("mH100mS8lt5/outputMSEff.root");
    _sig[1] = new TFile("mH200mS25lt5/outputMSEff.root");
    _sig[2] = new TFile("mH400mS50lt5/outputMSEff.root");
    _sig[3] = new TFile("mH600mS50lt5/outputMSEff.root");
    _sig[4] = new TFile("mH600mS150lt5/outputMSEff.root");
    
    makeCombinedPlot(_sig, 5, "scalar");
    for(int i=0;i<6;i++){delete _sig[i];}

    _sig[0] = new TFile("HChiChi_cbs_mH125mChi100/outputMSEff.root");
    _sig[1] = new TFile("HChiChi_lcb_mH125mChi30/outputMSEff.root");
    _sig[2] = new TFile("HChiChi_nubb_mH125mChi30/outputMSEff.root");
    _sig[3] = new TFile("WChiChi_ltb_mChi1000/outputMSEff.root");
    _sig[4] = new TFile("WChiChi_tbs_mChi1500/outputMSEff.root");
    
    makeCombinedPlot(_sig, 5, "bg");
        for(int i=0;i<5;i++){delete _sig[i];}

    }
	return 314;

}
